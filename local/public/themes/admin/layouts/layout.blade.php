<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
  <base href="">
  <meta charset="utf-8" />
  <title>@get('title')</title>
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta name="BASE_URL" content="{{ url('/') }}" />

  <meta name="description" content="" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <link rel="canonical" href="" />
  <!--begin::Fonts-->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
  <!--end::Fonts-->
  <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
  <!--begin::Page Vendors Styles(used by this page)-->
  <link href="{{getBaseURL()}}/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
  <!--end::Page Vendors Styles-->
  <!--begin::Global Theme Styles(used by all pages)-->
  <link href="{{getBaseURL()}}/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
  <link href="{{getBaseURL()}}/assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
  <link href="{{getBaseURL()}}/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
  <!--end::Global Theme Styles-->
  <!--begin::Layout Themes(used by all pages)-->
  <link href="{{getBaseURL()}}/assets/css/themes/layout/header/base/light.css" rel="stylesheet" type="text/css" />
  <link href="{{getBaseURL()}}/assets/css/themes/layout/header/menu/light.css" rel="stylesheet" type="text/css" />
  <link href="{{getBaseURL()}}/assets/css/themes/layout/brand/dark.css" rel="stylesheet" type="text/css" />
  <link href="{{getBaseURL()}}/assets/css/themes/layout/aside/dark.css" rel="stylesheet" type="text/css" />
  <link href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" rel="stylesheet" type="text/css" />
  <!--end::Layout Themes-->
  <link rel="shortcut icon" href="{{applogo()}}" />

</head>
<!--end::Head-->
<!--begin::Body-->
<style>
  .header-fixed.subheader-fixed.subheader-enabled .wrapper {
    padding-top: 50px;
}
</style>

<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
  @partial('header')
  @partial('left_side')
  @partial('top_toolbar')
  @content()
  @partial('footer')
  <!--begin::Global Theme Bundle(used by all pages)-->
  <script src="{{getBaseURL()}}/assets/plugins/global/plugins.bundle.js"></script>

  <script src="{{getBaseURL()}}/assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
  <script src="{{getBaseURL()}}/assets/js/scripts.bundle.js"></script>
  <!--end::Global Theme Bundle-->
  <!--begin::Page Vendors(used by this page)-->
  <script src="{{getBaseURL()}}/assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
  <!--end::Page Vendors-->
  <!--begin::Page Scripts(used by this page)-->
  <script src="{{getBaseURL()}}/assets/js/pages/widgets.js"></script>
  <script src="{{getBaseURL()}}/assets/js/pages/crud/forms/widgets/select2.js?v=7.2.7"></script>


  <script src="{{getBaseURL()}}/assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.7"></script>
  
  <!--end::Page Vendors-->
  <!--begin::Page Scripts(used by this page)-->

  <!-- <script src="{{getBaseURL()}}/assets/js/pages/crud/datatables/data-sources/ajax-server-side.js?v=7.2.7"></script> -->




  <!-- custom  -->
  <script type="text/javascript">
    BASE_URL = $('meta[name="BASE_URL"]').attr('content');
    CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    UID_ID = '<?php echo Auth::user()->id ?>';
    //	location.reload(1);
  </script>
  <script src="{{getBaseURL()}}/assets/js/admin_datatable_ajax.js?v=7.2.7"></script>
  {{-- <script src="{{getBaseURL()}}/assets/plugins/custom/ckeditor/ckeditor-document.bundle.js?v=7.2.7"></script> --}}
  <script src="{{getBaseURL()}}/assets/js/pages/crud/forms/widgets/form-repeater.js?v=7.2.7"></script>
  <!-- <script src="{{getBaseURL()}}/assets/js/pages/crud/file-upload/dropzonejs.js?v=7.2.7"></script> -->
  <script src="{{getBaseURL()}}/assets/js/pages/features/miscellaneous/sweetalert2.js?v=7.2.7"></script>
  {{-- <script src="{{getBaseURL()}}/corex/js/custom_chat_school.js"></script> --}}
  <script src="{{getBaseURL()}}/corex/js/admin_datatable_ajax.js?v=7.2.7"></script>
  <!--end::Page Vendors-->
  <!--begin::Page Scripts(used by this page)-->
  <script src="{{ asset('local/public/Highcharts820/code/highcharts.js')}}"></script>
  
  <script src="{{ asset('local/public/themes/admin/high_chart.js')}}" type="text/javascript"></script>

  <!-- <script src="{{getBaseURL()}}/assets/js/superadmin.js"></script> -->
  <script src="{{getBaseURL()}}/corex/js/school_admin.js"></script>
  <script src="{{getBaseURL()}}/assets/js/pages/custom/chat/chat.js"></script>


  <script type="text/javascript">
    // Enable pusher logging - don't include this in production
    // Pusher.logToConsole = true;

    // Add API Key & cluster here to make the connection 
    // var pusher = new Pusher('4c1630c06a480f5f76de', {
    //   cluster: 'ap2',
    //   encrypted: true
    // });

    // // Enter a unique channel you wish your users to be subscribed in.
    // var channel = pusher.subscribe('test_channel');

    // var imEventID = 'zelosUser_' + UID_ID;

    // // bind the server event to get the response data and append it to the message div
    // channel.bind(imEventID,
    //   function(data) {

        
    //     //console.log(data);
    //     $('.messages_display').append(`<div class="d-flex mb-5 align-items-start">
    //       <div class="d-flex align-items-center">
    //         <div class="symbol symbol-circle symbol-40 mr-4">
    //           <img alt="Pic" src="assets/media/users/300_12.jpg" />
    //         </div>

    //       </div>
    //       <div class="msg_time_mn rounded ml-5 p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px position-relative">
    //         ${data}
    //         <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">2 Hours</span></div>
    //       </div>`);
    //     $('.input_send_holder').html('<input type = "submit" value = "Send" class = "btn btn-primary btn-block input_send" />');
    //     $(".messages_display").scrollTop($(".messages_display")[0].scrollHeight);
    //   });

    // // check if the user is subscribed to the above channel
    // channel.bind('pusher:subscription_succeeded', function(members) {
    //   console.log('successfully subscribed!');
    // });

    // Send AJAX request to the PHP file on server 
    function ajaxCall(ajax_url, ajax_data) {
      $.ajax({
        type: "GET",
        url: ajax_url,
        //dataType: "json",
        data: ajax_data,
        success: function(response) {
          console.log(response);
        },
        error: function(msg) {}
      });
    }

    // Trigger for the Enter key when clicked.
    $.fn.enterKey = function(fnc) {
      return this.each(function() {
        $(this).keypress(function(ev) {
          var keycode = (ev.keyCode ? ev.keyCode : ev.which);
          if (keycode == '13') {
            fnc.call(this, ev);
          }
        });
      });
    }


    

    // Send the Message enter by User
    $('body').on('click', '.btnSendchatMessage', function(e) {
      


      
      



      e.preventDefault();
      var oldscrollHeight = $("#chatbox").prop("scrollHeight");
      alert(oldscrollHeight);



      var message = $('.chat_boxFooter').val();
      var to_id = $('#to_id').val()


      // Validate Name field
      if (to_id === '') {
        bootbox.alert('<br /><p class = "bg-danger">Please enter a Name.</p>');
      } else if (message !== '') {
        // Define ajax data
        var formData = {
          '_token': $('meta[name="csrf-token"]').attr('content'),
          'to_id': to_id,
          'message': message

        };

        //console.log(chat_message);

        var url = BASE_URL + '/chat_sendMessageAdmin';

        // Send the message to the server passing File Url and chat person name & message
        ajaxCall(url, formData);

        // Clear the message input field
        $('.chat_box .input_message').val('');
        $('.messages_display').append(`55<div class="d-flex mb-5 align-items-start justify-content-start flex-row-reverse">
          <div class="d-flex align-items-center">

            <div class="symbol symbol-circle symbol-40 ml-4">
              <img alt="Pic" src="assets/media/users/300_21.jpg" />
            </div>
          </div>
          <div class="msg_time_mn my_msg_time_mn rounded mr-5 p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px position-relative">
            ${message} <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">just now</span></div>
          </div>
          `);
        $('.chat_boxFooter').val("");
        var newscrollHeight =$("#chatbox").prop("scrollHeight");
       
        if (newscrollHeight > oldscrollHeight) {
          $(document).scrollTop($(document).height());
        }
        // Show a loading image while sending
        // $('.input_send_holder').html('<input type = "submit" value = "Send" class = "btn btn-primary btn-block" disabled /> &nbsp;<img     src = "loading.gif" />');
      }
    });

    // Send the message when enter key is clicked
    $('.chat_boxFooter').enterKey(function(e) {
      e.preventDefault();
      $('.btnSendchatMessage').click();
    });



    $("#loginCredentialSend").change(function() {
      
      if ($(this).prop("checked") == true) {
        //run code
        statusAction = 1;
        Swal.fire({
          title: "Are you sure?",
          text: "You want to created or send credentials",
          icon: "warning",
          showCancelButton: true,
          confirmButtonText: "Yes, sent it!",
        }).then(function(result) {
          if (result.value) {
            // ajax ayra

            //ajax call
            var formData = {
              txtSID: $("input[name=txtSID]").val(),
              statusAction: statusAction,
              _token: $('meta[name="csrf-token"]').attr("content"),
            };

            $.ajax({
              url: BASE_URL + "/createOrSentSchoolAccount",
              type: "POST",
              data: formData,
              success: function(res) {},
            });

            //ajax call
            //ayra ajax
          }
        });
      } else {
        //run code
        statusAction = 2;
        Swal.fire({
          title: "Are you sure?",
          text: "You want to created or send credentials",
          icon: "warning",
          showCancelButton: true,
          confirmButtonText: "Yes, sent it!",
        }).then(function(result) {
          if (result.value) {
            // ajax ayra

            //ajax call
            var formData = {
              txtSID: $("input[name=txtSID]").val(),
              statusAction: 1,
              _token: $('meta[name="csrf-token"]').attr("content"),
            };

            $.ajax({
              url: BASE_URL + "/createOrSentSchoolAccount",
              type: "POST",
              data: formData,
              success: function(res) {
                if (res.status == 1) {
                  swal
                  .fire({
                    text: res.msg,
                    icon: "success",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                      confirmButton: "btn font-weight-bold btn-light-primary",
                    },
                  })
                  .then(function() {
                    KTUtil.scrollTop();
                  });
                } else {
                  swal
                  .fire({
                    text: res.msg,
                    icon: "success",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                      confirmButton: "btn font-weight-bold btn-light-primary",
                    },
                  })
                  .then(function() {
                    KTUtil.scrollTop();
                  });
                }
              },
            });

            //ajax call
            //ayra ajax
          }
        });
      }
    });
  </script>

  <script>
    var avatar3 = new KTImageInput('kt_image_3');
    
    $(document).ready(function() {
      $('.scroll').scrollTop( $(document).height()+25545454 );

    // $('.scroll').animate({
    //     scrollTop: $('.scroll').get(0).scrollHeight+25555
        
    // }, 2000);
});

    $(document).ready(function (e) {
 $("#formChatSuperAdmin").on('submit',(function(e) {
  e.preventDefault();
  $.ajax({
  url: "uploadChatFileAdmin",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
   beforeSend : function()
   {
    //$("#preview").fadeOut();
    $("#err").fadeOut();
   },
   success: function(data)
      {
    if(data.status==0)
    {
     // invalid file format.
     $("#err").html("Invalid File !").fadeIn();
    }
    else
    {
      var userid=$('#to_id').val();

      // showUserChatDetail(userid);

      $('#img_file_select').modal('toggle');
      //location.reload(1);
      showUserChatDetail(userid);
     // view uploaded file.
     $("#preview").html(data).fadeIn();
     $("#formChatSuperAdmin")[0].reset(); 
     
    }
      },
     error: function(e) 
      {
    $("#err").html(e).fadeIn();
      }          
    });
 }));
});




  </script>
  <!--end::Page Scripts-->

  

</body>
<!--end::Body-->

</html>