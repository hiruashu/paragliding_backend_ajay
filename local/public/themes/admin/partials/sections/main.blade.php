<!--begin::Container-->
<div class="container">
    <style>
        .form-controlA {
            display: block;
            width: 100%;
            height: calc(1.5em + 1.3rem + 2px);
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #3F4254;
            background-color: #ffffff;
            background-clip: padding-box;
            border: 1px solid #E4E6EF;
            border-radius: 0.42rem;
            -webkit-box-shadow: none;
            box-shadow: none;
            -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }

        .highcharts-credits {
            display: none;
        }
    </style>

    <?php
    $schoolCount = DB::table('school_course')
        ->where('is_deleted', 0)
        ->where('sid', Auth::user()->sid)

        ->count();

    $userCount = DB::table('users')
        ->where('is_deleted', 0)
        ->where('sid', Auth::user()->sid)
        ->where('user_type', 3)
        ->count();

    $schoolRCount = DB::table('schools')
        ->where('is_deleted', 0)
        ->where('added_from_status', 2)
        ->count();

        
        $current_weekErn =DB::table('course_payment')->where('sid',Auth::user()->id)->whereBetween('paid_on', [\Carbon\Carbon::now()->startOfWeek(), \Carbon\Carbon::now()->endOfWeek()])->sum('payment_amt');
        $current_weekMonth =DB::table('course_payment')->where('sid',Auth::user()->id)->whereMonth('paid_on', date('m'))->whereYear('paid_on', date('Y'))->sum('payment_amt');





    ?>

    <div class="row">
        <div class="col-md-4">
            <!--begin::Mixed Widget 1-->
            <div class="card card-custom bg-gray-100 card-stretch gutter-b">
                <!--begin::Header-->
             
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body p-0 position-relative overflow-hidden">
                    <!--begin::Chart-->
                    <img src="{{getBaseURL().'/dashline.png'}}">
                    <!--end::Chart-->
                    <!--begin::Stats-->
                    <div class="card-spacer mt-n25">
                        <!--begin::Row-->
                        <div class="row m-0">
                            <div class="col bg-light-warning px-6 py-8 rounded-xl mr-7 mb-7">
                                <span class="svg-icon svg-icon-3x svg-icon-warning d-block my-2">
                                    <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Media/Equalizer.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <rect fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16" rx="1.5" />
                                            <rect fill="#000000" x="8" y="9" width="3" height="11" rx="1.5" />
                                            <rect fill="#000000" x="18" y="11" width="3" height="9" rx="1.5" />
                                            <rect fill="#000000" x="3" y="13" width="3" height="7" rx="1.5" />
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                                <a href="{{route('userList')}}" class="text-warning font-weight-bold font-size-h6"> Student :{{$userCount}}</a>
                            </div>
                            <div class="col bg-light-primary px-6 py-8 rounded-xl mb-7">
                                <span class="svg-icon svg-icon-3x svg-icon-primary d-block my-2">
                                    <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Communication/Add-user.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24" />
                                            <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                            <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                                <a href="{{route('schoolList')}}" class="text-primary font-weight-bold font-size-h6 mt-2">Course:{{$schoolCount}}</a>
                            </div>
                        </div>
                        <div class="row m-0">
                            <div class="col bg-light-danger px-6 py-8 rounded-xl mr-7 mb-7">
                                <span class="svg-icon svg-icon-3x svg-icon-brand d-block my-2">
                                    <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Media/Equalizer.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <rect fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16" rx="1.5"  style="fill: #F64E60;"/>
                                            <rect fill="#000000" x="8" y="9" width="3" height="11" rx="1.5"  style="fill: #F64E60;"/>
                                            <rect fill="#000000" x="18" y="11" width="3" height="9" rx="1.5"  style="fill: #F64E60;"/>
                                            <rect fill="#000000" x="3" y="13" width="3" height="7" rx="1.5"  style="fill: #F64E60;"/>
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                                <a href="{{route('AdminUserList')}}" class="text-danger font-weight-bold font-size-h6 mt-2"> Weekly Earn :{{$current_weekErn}}</a>
                            </div>
                            <div class="col bg-light-success px-6 py-8 rounded-xl mb-7">
                                <span class="svg-icon svg-icon-3x svg-icon-success d-block my-2">
                                    <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Communication/Add-user.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24" />
                                            <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" style="fill: #1BC5BD;"/>
                                            <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"  style="fill: #1BC5BD;"/>
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                                <a href="{{route('AdminUserList')}}" class="text-success font-weight-bold font-size-h6 mt-2">Monthly Earn:{{$current_weekMonth}}</a>
                            </div>
                        </div>
                        <!--end::Row-->

                    </div>
                    <!--end::Stats-->


                </div>


                <!--end::Body-->




            </div>

            <!--end::Mixed Widget 1-->
        </div>
        <div class="col-md-4">
            <div class="card ">


                <figure class="highcharts-figure">

                    <div id="AdmincontainerPIE"></div>

                </figure>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card ">
                <select class="form-control txtSchoolPie_Course" id="txtSchoolPie_Course">

                    <option value="">-ALL-</option>
                    <?php

                    $schoolCount = DB::table('school_course')
                        ->where('is_deleted', 0)
                        ->where('sid', Auth::user()->sid)
                        ->get();
                    foreach ($schoolCount as $key => $rowData) {

                    ?>
                        <option value="{{$rowData->id}}">{{$rowData->certificate_title}}</option>
                    <?php
                    }
                    ?>

                </select>
                <br>


                <figure class="highcharts-figure">

                    <div id="AdmincontainerPIE_Course"></div>

                </figure>
            </div>
        </div>





    </div>
    <!--end::Row-->
    <div class="row">
        <div class="col-md-12">
            <!--begin::Stats Widget 11-->
            <div class="card ">
                <style>
                    
                    .nav.nav-pills .nav-item {
                        margin-right: -0.75rem;
                        margin: 10px;
                    }
                </style>
                <!--begin::Body-->
                <!--begin::Advance Table Widget 2-->
                <div class="card card-custom card-stretch gutter-b">
                    <!--begin::Header-->
                    <div class="card-header border-0 pt-5">


                        <div class="card-toolbar">
                            <ul class="nav nav-pills nav-pills-sm">
                                <li class="nav-item" style="margin-left:25px">
                                    <select name="" id="txtCouserID" class="form-controlA" style="max-width:105%;">
                                        <option value="">ALL COURSE</option>
                                        <?php
                                        $schoolCount = DB::table('school_course')
                                            ->where('is_deleted', 0)
                                            ->where('sid', Auth::user()->sid)
                                            ->get();
                                        foreach ($schoolCount as $key => $rowData) {

                                        ?>
                                            <option value="{{$rowData->id}}">{{$rowData->certificate_title}}</option>
                                        <?php
                                        }
                                        ?>
                                        ?>

                                    </select>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link py-2 px-4 active" onclick="btnShowChartNewUser(1,7)" data-toggle="tab" href="#kt_tab_pane_11_1_1">WEEK</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link py-2 px-4 " onclick="btnShowChartNewUser(2,365)" data-toggle="tab" href="#kt_tab_pane_11_3_3_D">MONTH</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link py-2 px-4 " onclick="btnShowChartNewUser(3,365)" data-toggle="tab" href="#kt_tab_pane_11_3_3">YEAR</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link py-2 px-4 " onclick="btnShowChartNewUser(4,365)" data-toggle="tab" href="#kt_tab_pane_11_3_4">ALL</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body pt-2 pb-0 mt-n3">
                        <div class="tab-content mt-5" id="myTabTables11">
                            <!--begin::Tap pane-->
                            <div class="tab-pane fade active show" id="kt_tab_pane_11_1_1" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                                <!--begin::Table-->

                                <figure class="highcharts-figure">
                                    <div id="containerUserDataView_1"></div>
                                    <p class="highcharts-description">
                                        <!-- This above chart show total Orders monthly -->
                                    </p>
                                </figure>

                                <!--end::Table-->
                            </div>

                            <div class="tab-pane fade active show" id="kt_tab_pane_11_3_3_D" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                                <!--begin::Table-->

                                <figure class="highcharts-figure">
                                    <div id="containerUserDataView_2"></div>
                                    <p class="highcharts-description">
                                        <!-- This above chart show total Orders monthly -->
                                    </p>
                                </figure>

                                <!--end::Table-->
                            </div>

                            <div class="tab-pane fade active show" id="kt_tab_pane_11_3_3" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                                <!--begin::Table-->

                                <figure class="highcharts-figure">
                                    <div id="containerUserDataView_3"></div>
                                    <p class="highcharts-description">
                                        <!-- This above chart show total Orders monthly -->
                                    </p>
                                </figure>

                                <!--end::Table-->
                            </div>
                            <div class="tab-pane fade active show" id="kt_tab_pane_11_3_4" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                                <!--begin::Table-->

                                <figure class="highcharts-figure">
                                    <div id="containerUserDataView_4"></div>
                                    <p class="highcharts-description">
                                        <!-- This above chart show total Orders monthly -->
                                    </p>
                                </figure>

                                <!--end::Table-->
                            </div>
                            <!--end::Tap pane-->

                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Advance Table Widget 2-->
                <!--end::Body-->
            </div>
            <!-- <div class="card ">
                <figure class="highcharts-figure">
                    <div id="AdmincontainerPIE_Payment"></div>
                    <p class="highcharts-description">
                        
                    </p>
                </figure>
            </div> -->
            <!--end::Stats Widget 11-->
            <!--begin::Stats Widget 12-->





            <!--end::Stats Widget 12-->
        </div>

    </div>


    <!-- payment graph  -->
    <div class="row">
        <div class="col-md-12">
            <!--begin::Stats Widget 11-->
            <div class="card ">
                <style>
                    .nav.nav-pills .nav-item {
                        margin-right: -0.75rem;
                    }
                </style>
                <!--begin::Body-->
                <!--begin::Advance Table Widget 2-->
                <div class="card card-custom card-stretch gutter-b">
                    <!--begin::Header-->
                    <div class="card-header border-0 pt-5">


                        <div class="card-toolbar">
                            <ul class="nav nav-pills nav-pills-sm">
                                <li class="nav-item" style="margin-left:25px">
                                    <select name="" id="txtCouserIDPay" class="form-controlA" style="max-width:105%;">
                                        <option value="">ALL COURSE</option>
                                        <?php
                                        $schoolCount = DB::table('school_course')
                                            ->where('is_deleted', 0)
                                            ->where('sid', Auth::user()->sid)
                                            ->get();
                                        foreach ($schoolCount as $key => $rowData) {

                                        ?>
                                            <option value="{{$rowData->id}}">{{$rowData->certificate_title}}</option>
                                        <?php
                                        }
                                        ?>
                                        ?>

                                    </select>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link py-2 px-4 active" onclick="btnShowChartNewUserPay(1,7)" data-toggle="tab" href="#kt_tab_pane_11_1_1_pay_1">WEEK</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link py-2 px-4 " onclick="btnShowChartNewUserPay(2,365)" data-toggle="tab" href="#kt_tab_pane_11_1_1_pay_2">MONTH</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link py-2 px-4 " onclick="btnShowChartNewUserPay(3,365)" data-toggle="tab" href="#kt_tab_pane_11_1_1_pay_3">YEAR</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link py-2 px-4 " onclick="btnShowChartNewUserPay(4,365)" data-toggle="tab" href="#kt_tab_pane_11_1_1_pay_4">ALL</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body pt-2 pb-0 mt-n3">
                        <div class="tab-content mt-5" id="myTabTables11">
                            <!--begin::Tap pane-->
                            <div class="tab-pane fade active show" id="kt_tab_pane_11_1_1_pay_1" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                                <!--begin::Table-->

                                <figure class="highcharts-figure">
                                    <div id="containerUserDataViewPay_1"></div>
                                    <p class="highcharts-description">
                                        <!-- This above chart show total Orders monthly -->
                                    </p>
                                </figure>

                                <!--end::Table-->
                            </div>

                            <div class="tab-pane fade  show" id="kt_tab_pane_11_1_1_pay_2" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                                <!--begin::Table-->

                                <figure class="highcharts-figure">
                                    <div id="containerUserDataViewPay_2"></div>
                                    <p class="highcharts-description">
                                        <!-- This above chart show total Orders monthly -->
                                    </p>
                                </figure>

                                <!--end::Table-->
                            </div>

                            <div class="tab-pane fade  show" id="kt_tab_pane_11_1_1_pay_3" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                                <!--begin::Table-->

                                <figure class="highcharts-figure">
                                    <div id="containerUserDataViewPay_3"></div>
                                    <p class="highcharts-description">
                                        <!-- This above chart show total Orders monthly -->
                                    </p>
                                </figure>

                                <!--end::Table-->
                            </div>
                            <div class="tab-pane fade  show" id="kt_tab_pane_11_1_1_pay_4" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                                <!--begin::Table-->

                                <figure class="highcharts-figure">
                                    <div id="containerUserDataViewPay_4"></div>
                                    <p class="highcharts-description">
                                        <!-- This above chart show total Orders monthly -->
                                    </p>
                                </figure>

                                <!--end::Table-->
                            </div>
                            <!--end::Tap pane-->

                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Advance Table Widget 2-->
                <!--end::Body-->
            </div>
            <!-- <div class="card ">
                <figure class="highcharts-figure">
                    <div id="AdmincontainerPIE_Payment"></div>
                    <p class="highcharts-description">
                        
                    </p>
                </figure>
            </div> -->
            <!--end::Stats Widget 11-->
            <!--begin::Stats Widget 12-->





            <!--end::Stats Widget 12-->
        </div>

    </div>
    <!-- payment graph  -->
    <!--end::Dashboard-->
</div>
<!--end::Container-->