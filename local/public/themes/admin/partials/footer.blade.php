</div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
    
 <!--begin::Footer-->
 
 <!--end::Footer-->
 </div>
 <!--end::Wrapper-->
 </div>
 <!--end::Page-->
 </div>
 <!--end::Main-->
 <!-- begin::User Panel-->
 <div id="kt_quick_user" class="offcanvas offcanvas-right p-10">
     <!--begin::Header-->
     <div class="offcanvas-header d-flex align-items-center justify-content-between pb-5">
         <h3 class="font-weight-bold m-0">User Profile
             <!-- <small class="text-muted font-size-sm ml-2">12 messages</small> -->
         </h3>
         <a href="#" class="btn btn-xs btn-icon btn-light btn-hover-primary" id="kt_quick_user_close">
             <i class="ki ki-close icon-xs text-muted"></i>
         </a>
     </div>
     <!--end::Header-->
     <!--begin::Content-->
     <div class="offcanvas-content pr-5 mr-n5">
        <?php
      $schoolArr = DB::table('users')->where('id', Auth::user()->id)->whereNotNull('avatar')->first();
      if ($schoolArr == null) {
          $schLogo = NoImage();
      } else {
          $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
      }
  
      ?>
         <!--begin::Header-->
         <div class="d-flex align-items-center mt-5">
             <div class="symbol symbol-100 mr-5">
                 <div class="symbol-label" style="background-image:url({{$schLogo}})"></div>
                 <i class="symbol-badge bg-success"></i>
             </div>
             <div class="d-flex flex-column">
                 <a href="#" class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary">{{Auth::user()->name}}</a>
                 <div class="text-muted mt-1"></div>Admininstrator
                 <div class="navi mt-2">
                     <a href="#" class="navi-item">
                         <span class="navi-link p-0 pb-2">
                             <span class="navi-icon mr-1">
                                 <span class="svg-icon svg-icon-lg svg-icon-primary">
                                     <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-notification.svg-->
                                     <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                         <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                             <rect x="0" y="0" width="24" height="24" />
                                             <path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000" />
                                             <circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5" />
                                         </g>
                                     </svg>
                                     <!--end::Svg Icon-->
                                 </span>
                             </span>
                             <span class="navi-text text-muted text-hover-primary">{{user_email()}}</span>
                         </span>
                     </a>
                     <a href="href=" {{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="btn btn-sm btn-light-primary font-weight-bolder py-2 px-5">Sign Out</a>
                     <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                         @csrf
                     </form>
                 </div>
             </div>
         </div>
         <!--end::Header-->
         <!--begin::Separator-->
         <div class="separator separator-dashed mt-8 mb-5"></div>
         <!--end::Separator-->
         <!--begin::Nav-->
         <div class="navi navi-spacer-x-0 p-0">
             <!--begin::Item-->
             <a href="{{route('admin_profileV1')}}" class="navi-item">
                 <div class="navi-link">
                     <div class="symbol symbol-40 bg-light mr-3">
                         <div class="symbol-label">
                             <span class="svg-icon svg-icon-md svg-icon-success">
                                 <!--begin::Svg Icon | path:assets/media/svg/icons/General/Notification2.svg-->
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                     <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                         <rect x="0" y="0" width="24" height="24" />
                                         <path d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z" fill="#000000" />
                                         <circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5" />
                                     </g>
                                 </svg>
                                 <!--end::Svg Icon-->
                             </span>
                         </div>
                     </div>
                     <div class="navi-text">
                         <div class="font-weight-bold">My Profile</div>
                         <div class="text-muted">Account settings and more
                             <!-- <span class="label label-light-danger label-inline font-weight-bold">update</span> -->
                         </div>
                     </div>
                 </div>
             </a>
             <!--end:Item-->
            
         </div>
         <!--end::Nav-->
         <!--begin::Separator-->
         <div class="separator separator-dashed my-7"></div>
         <!--end::Separator-->
        
     </div>
     <!--end::Content-->
 </div>
 <!-- end::User Panel-->
 
 
 <!--begin::Chat Panel-->
 <div class="modal modal-sticky modal-sticky-bottom-right" id="kt_chat_modal" role="dialog" data-backdrop="false">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <!--begin::Card-->
             <div class="card card-custom">
                 <!--begin::Header-->
                 <div class="card-header align-items-center px-4 py-3">
                     <div class="text-left flex-grow-1">
                         <!--begin::Dropdown Menu-->
                         <div class="dropdown dropdown-inline">
                             <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 <span class="svg-icon svg-icon-lg">
                                     <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->
                                     <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                         <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                             <polygon points="0 0 24 0 24 24 0 24" />
                                             <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                             <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                         </g>
                                     </svg>
                                     <!--end::Svg Icon-->
                                 </span>
                             </button>
                             <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-md">
                                 <!--begin::Navigation-->
                                 <ul class="navi navi-hover py-5">
                                     <li class="navi-item">
                                         <a href="#" class="navi-link">
                                             <span class="navi-icon">
                                                 <i class="flaticon2-drop"></i>
                                             </span>
                                             <span class="navi-text">New Group</span>
                                         </a>
                                     </li>
                                     <li class="navi-item">
                                         <a href="#" class="navi-link">
                                             <span class="navi-icon">
                                                 <i class="flaticon2-list-3"></i>
                                             </span>
                                             <span class="navi-text">Contacts</span>
                                         </a>
                                     </li>
                                     <li class="navi-item">
                                         <a href="#" class="navi-link">
                                             <span class="navi-icon">
                                                 <i class="flaticon2-rocket-1"></i>
                                             </span>
                                             <span class="navi-text">Groups</span>
                                             <span class="navi-link-badge">
                                                 <span class="label label-light-primary label-inline font-weight-bold">new</span>
                                             </span>
                                         </a>
                                     </li>
                                     <li class="navi-item">
                                         <a href="#" class="navi-link">
                                             <span class="navi-icon">
                                                 <i class="flaticon2-bell-2"></i>
                                             </span>
                                             <span class="navi-text">Calls</span>
                                         </a>
                                     </li>
                                     <li class="navi-item">
                                         <a href="#" class="navi-link">
                                             <span class="navi-icon">
                                                 <i class="flaticon2-gear"></i>
                                             </span>
                                             <span class="navi-text">Settings</span>
                                         </a>
                                     </li>
                                     <li class="navi-separator my-3"></li>
                                     <li class="navi-item">
                                         <a href="#" class="navi-link">
                                             <span class="navi-icon">
                                                 <i class="flaticon2-magnifier-tool"></i>
                                             </span>
                                             <span class="navi-text">Help</span>
                                         </a>
                                     </li>
                                     <li class="navi-item">
                                         <a href="#" class="navi-link">
                                             <span class="navi-icon">
                                                 <i class="flaticon2-bell-2"></i>
                                             </span>
                                             <span class="navi-text">Privacy</span>
                                             <span class="navi-link-badge">
                                                 <span class="label label-light-danger label-rounded font-weight-bold">5</span>
                                             </span>
                                         </a>
                                     </li>
                                 </ul>
                                 <!--end::Navigation-->
                             </div>
                         </div>
                         <!--end::Dropdown Menu-->
                     </div>
                     <div class="text-center flex-grow-1">
                         <div class="text-dark-75 font-weight-bold font-size-h5">Matt Pears</div>
                         <div>
                             <span class="label label-dot label-success"></span>
                             <span class="font-weight-bold text-muted font-size-sm">Active</span>
                         </div>
                     </div>
                     <div class="text-right flex-grow-1">
                         <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-dismiss="modal">
                             <i class="ki ki-close icon-1x"></i>
                         </button>
                     </div>
                 </div>
                 <!--end::Header-->
                 <!--begin::Body-->
                 <div class="card-body">
                     <!--begin::Scroll-->
                     <div class="scroll scroll-pull" data-height="375" data-mobile-height="300">
                         <!--begin::Messages-->
                         <div class="messages">
                             <!--begin::Message In-->
                             <div class="d-flex flex-column mb-5 align-items-start">
                                 <div class="d-flex align-items-center">
                                     <div class="symbol symbol-circle symbol-40 mr-3">
                                         <img alt="Pic" src="assets/media/users/300_12.jpg" />
                                     </div>
                                     <div>
                                         <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">Matt Pears</a>
                                         <span class="text-muted font-size-sm">2 Hours</span>
                                     </div>
                                 </div>
                                 <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">How likely are you to recommend our company to your friends and family?</div>
                             </div>
                             <!--end::Message In-->
                             <!--begin::Message Out-->
                             <div class="d-flex flex-column mb-5 align-items-end">
                                 <div class="d-flex align-items-center">
                                     <div>
                                         <span class="text-muted font-size-sm">3 minutes</span>
                                         <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
                                     </div>
                                     <div class="symbol symbol-circle symbol-40 ml-3">
                                         <img alt="Pic" src="assets/media/users/300_21.jpg" />
                                     </div>
                                 </div>
                                 <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px">Hey there, we’re just writing to let you know that you’ve been subscribed to a repository on GitHub.</div>
                             </div>
                             <!--end::Message Out-->
                             <!--begin::Message In-->
                             <div class="d-flex flex-column mb-5 align-items-start">
                                 <div class="d-flex align-items-center">
                                     <div class="symbol symbol-circle symbol-40 mr-3">
                                         <img alt="Pic" src="assets/media/users/300_21.jpg" />
                                     </div>
                                     <div>
                                         <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">Matt Pears</a>
                                         <span class="text-muted font-size-sm">40 seconds</span>
                                     </div>
                                 </div>
                                 <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">Ok, Understood!</div>
                             </div>
                             <!--end::Message In-->
                             <!--begin::Message Out-->
                             <div class="d-flex flex-column mb-5 align-items-end">
                                 <div class="d-flex align-items-center">
                                     <div>
                                         <span class="text-muted font-size-sm">Just now</span>
                                         <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
                                     </div>
                                     <div class="symbol symbol-circle symbol-40 ml-3">
                                         <img alt="Pic" src="assets/media/users/300_21.jpg" />
                                     </div>
                                 </div>
                                 <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px">You’ll receive notifications for all issues, pull requests!</div>
                             </div>
                             <!--end::Message Out-->
                             <!--begin::Message In-->
                             <div class="d-flex flex-column mb-5 align-items-start">
                                 <div class="d-flex align-items-center">
                                     <div class="symbol symbol-circle symbol-40 mr-3">
                                         <img alt="Pic" src="assets/media/users/300_12.jpg" />
                                     </div>
                                     <div>
                                         <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">Matt Pears</a>
                                         <span class="text-muted font-size-sm">40 seconds</span>
                                     </div>
                                 </div>
                                 <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">You can unwatch this repository immediately by clicking here:
                                     <a href="#">https://github.com</a>
                                 </div>
                             </div>
                             <!--end::Message In-->
                             <!--begin::Message Out-->
                             <div class="d-flex flex-column mb-5 align-items-end">
                                 <div class="d-flex align-items-center">
                                     <div>
                                         <span class="text-muted font-size-sm">Just now</span>
                                         <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
                                     </div>
                                     <div class="symbol symbol-circle symbol-40 ml-3">
                                         <img alt="Pic" src="assets/media/users/300_21.jpg" />
                                     </div>
                                 </div>
                                 <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px">Discover what students who viewed Learn Figma - UI/UX Design. Essential Training also viewed</div>
                             </div>
                             <!--end::Message Out-->
                             <!--begin::Message In-->
                             <div class="d-flex flex-column mb-5 align-items-start">
                                 <div class="d-flex align-items-center">
                                     <div class="symbol symbol-circle symbol-40 mr-3">
                                         <img alt="Pic" src="assets/media/users/300_12.jpg" />
                                     </div>
                                     <div>
                                         <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">Matt Pears</a>
                                         <span class="text-muted font-size-sm">40 seconds</span>
                                     </div>
                                 </div>
                                 <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">Most purchased Business courses during this sale!</div>
                             </div>
                             <!--end::Message In-->
                             <!--begin::Message Out-->
                             <div class="d-flex flex-column mb-5 align-items-end">
                                 <div class="d-flex align-items-center">
                                     <div>
                                         <span class="text-muted font-size-sm">Just now</span>
                                         <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
                                     </div>
                                     <div class="symbol symbol-circle symbol-40 ml-3">
                                         <img alt="Pic" src="assets/media/users/300_21.jpg" />
                                     </div>
                                 </div>
                                 <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px">Company BBQ to celebrate the last quater achievements and goals. Food and drinks provided</div>
                             </div>
                             <!--end::Message Out-->
                         </div>
                         <!--end::Messages-->
                     </div>
                     <!--end::Scroll-->
                 </div>
                 <!--end::Body-->
                 <!--begin::Footer-->
                 <div class="card-footer align-items-center">
                     <!--begin::Compose-->
                     <textarea class="form-control border-0 p-0" rows="2" placeholder="Type a message"></textarea>
                     <div class="d-flex align-items-center justify-content-between mt-5">
                         <div class="mr-3">
                             <a href="#" class="btn btn-clean btn-icon btn-md mr-1">
                                 <i class="flaticon2-photograph icon-lg"></i>
                             </a>
                             <a href="#" class="btn btn-clean btn-icon btn-md">
                                 <i class="flaticon2-photo-camera icon-lg"></i>
                             </a>
                         </div>
                         <div>
                             <button type="button" class="btn btn-primary btn-md text-uppercase font-weight-bold chat-send py-2 px-6">Send</button>
                         </div>
                     </div>
                     <!--begin::Compose-->
                 </div>
                 <!--end::Footer-->
             </div>
             <!--end::Card-->
         </div>
     </div>
 </div>
 <!--end::Chat Panel-->
 <!--begin::Scrolltop-->
 <div id="kt_scrolltop" class="scrolltop">
     <span class="svg-icon">
         <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Up-2.svg-->
         <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
             <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                 <polygon points="0 0 24 0 24 24 0 24" />
                 <rect fill="#000000" opacity="0.3" x="11" y="10" width="2" height="10" rx="1" />
                 <path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fill-rule="nonzero" />
             </g>
         </svg>
         <!--end::Svg Icon-->
     </span>
 </div>
 <!--end::Scrolltop-->
 <!--begin::Sticky Toolbar-->
 <!-- <ul class="sticky-toolbar nav flex-column pl-2 pr-2 pt-3 pb-3 mt-4"> -->
    
    <!--begin::Item-->
			<!-- <li class="nav-item" id="kt_sticky_toolbar_chat_toggler5" data-toggle="tooltip" title="" data-placement="left" data-original-title="Chat with Admin">
				<a class="btn btn-sm btn-icon btn-bg-light btn-icon-danger btn-hover-danger" href="{{route('zelosChatAdmin')}}" >
					<i class="flaticon2-chat-1"></i>
				</a>
			</li>
            <li class="nav-item" id="kt_sticky_toolbar_chat_toggler5" data-toggle="tooltip" title="" data-placement="left" data-original-title="Group Chat">
				<a class="btn btn-sm btn-icon btn-bg-light btn-icon-danger btn-hover-danger" href="{{route('zelosChatAdminGroupView')}}" >
					<i class="flaticon2-plus-1"></i>
				</a>
			</li> -->
			<!--end::Item-->
     <!--end::Item-->
 <!-- </ul> -->
 <!--end::Sticky Toolbar-->
 
 
 <!--begin::Global Config(global config for global JS scripts)-->
 <script>
     var KTAppSettings = {
         "breakpoints": {
             "sm": 576,
             "md": 768,
             "lg": 992,
             "xl": 1200,
             "xxl": 1400
         },
         "colors": {
             "theme": {
                 "base": {
                     "white": "#ffffff",
                     "primary": "#3699FF",
                     "secondary": "#E5EAEE",
                     "success": "#1BC5BD",
                     "info": "#8950FC",
                     "warning": "#FFA800",
                     "danger": "#F64E60",
                     "light": "#E4E6EF",
                     "dark": "#181C32"
                 },
                 "light": {
                     "white": "#ffffff",
                     "primary": "#E1F0FF",
                     "secondary": "#EBEDF3",
                     "success": "#C9F7F5",
                     "info": "#EEE5FF",
                     "warning": "#FFF4DE",
                     "danger": "#FFE2E5",
                     "light": "#F3F6F9",
                     "dark": "#D6D6E0"
                 },
                 "inverse": {
                     "white": "#ffffff",
                     "primary": "#ffffff",
                     "secondary": "#3F4254",
                     "success": "#ffffff",
                     "info": "#ffffff",
                     "warning": "#ffffff",
                     "danger": "#ffffff",
                     "light": "#464E5F",
                     "dark": "#ffffff"
                 }
             },
             "gray": {
                 "gray-100": "#F3F6F9",
                 "gray-200": "#EBEDF3",
                 "gray-300": "#E4E6EF",
                 "gray-400": "#D1D3E0",
                 "gray-500": "#B5B5C3",
                 "gray-600": "#7E8299",
                 "gray-700": "#5E6278",
                 "gray-800": "#3F4254",
                 "gray-900": "#181C32"
             }
         },
         "font-family": "Poppins"
     };
 </script>
 <!--end::Global Config-->