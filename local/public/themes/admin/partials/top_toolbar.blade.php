 <!--begin::Topbar-->
 <style type="text/css" media="screen">
.navi-item .navi-link , .footer-content button {
   transition: .5s;
}
.navi-item .navi-link:hover {
   background: #fff !important;
}
.footer-content button {
   border-radius: 0 0 0.42rem 0.42rem;
   font-size: 14px;
}
.footer-content button:hover {
   background: #3699FF;
   color: #fff;
   transition: .5s;
}
.scroll.ps > .ps__rail-y {
   background-color: #d8d8d8;
}
.scroll.ps > .ps__rail-y > .ps__thumb-y {
   background: #68b3ff;
   opacity: 1;
   right: 0;
}
.ps > .ps__rail-x, .ps > .ps__rail-y, .ps--focus > .ps__rail-x, .ps--focus > .ps__rail-y, .ps--scrolling-x > .ps__rail-x, .ps--scrolling-y > .ps__rail-y {
   opacity: 1 !important;
}
</style>
 <div class="topbar">

    <!--begin::Notifications-->
    <div class="dropdown">
        <!--begin::Toggle-->
        <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
            <div class="btn btn-icon btn-clean btn-dropdown btn-lg mr-1 pulse pulse-primary">
                <span class="svg-icon svg-icon-xl svg-icon-primary">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Code/Compiling.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24" />
                            <path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z" fill="#000000" opacity="0.3" />
                            <path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z" fill="#000000" />
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>
                <span class="pulse-ring"></span>
            </div>
        </div>
        <!--end::Toggle-->
        <!--begin::Dropdown-->
        <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg" style="border: 1px solid rgb(226 226 226); overflow: hidden;box-shadow: rgb(0 0 0 / 20%) 0px 3px 10px 0px">
            <form>
                <?php 
                 $notiArr = DB::table('tbl_notify')->orderBy('id', 'desc')->where('is_view', 0)->get();

                ?>
                <!--begin::Header-->
                <div class="d-flex flex-column p-4 bg-white rounded-top position-relative" style="box-shadow: 0px 5px 25px 0px rgb(0 0 0 / 10%); z-index: 1;">
                    <!--begin::Title-->
                    <h6 class="d-flex flex-center rounded-top m-0 text-uppercase">

                       
                        <span class="text-dark">Notifications</span>
                        <span class="label label-rounded label-primary ml-4">{{count($notiArr)}}</span>

                        <!-- <span class="btn btn-text btn-success btn-sm font-weight-bold btn-font-md ml-2">23 new</span> -->
                    </h6>
                    <!--end::Title-->
                    <!--begin::Tabs-->

                    <!--end::Tabs-->
                </div>
                <!--end::Header-->
                <!--begin::Content-->
                <div class="tab-content">
                    <!--begin::Tabpane-->
                    <div class="tab-pane active show" id="topbar_notifications_notifications" role="tabpanel">
                        <!--begin::Scroll-->
                        <div class="navi navi-hover scroll" data-scroll="true" data-height="400" data-mobile-height="200">
                            <!--begin::Item-->
                            <?php
                           
                            foreach ($notiArr as $key => $rowData) {

                                $dt =  \Carbon\Carbon::parse($rowData->created_at);
                                $da= $dt->diffForHumans();



                                ?>
                                <a href="#" class="navi-item">
                                    <div class="navi-link px-5 py-2 bg-light" style="border-bottom: 1px solid rgb(226 226 226);">
                                        <div class="navi-icon mr-2 symbol-list d-flex flex-wrap">
                                            <div class="symbol symbol-30 symbol-primary mr-3">
                                                <span class="symbol-label text-white"><i class="flaticon2-bell-5 icon-sm text-white"></i></span>
                                            </div>
                                        </div>
                                        <div class="navi-text">
                                            <div class=" text-primary" style="font-size: 12px;">{{$rowData->noti_message}}</div>
                                            <div class="text-muted" style="font-size: 11px;"> {{$da}}</div>
                                        </div>
                                    </div>
                                </a>
                                <a href="#" class="navi-item">
                                    <div class="navi-link px-5 py-2 bg-light align-items-start" style="border-bottom: 1px solid rgb(226 226 226);">
                                        <div class="navi-icon mr-2 symbol-list d-flex flex-wrap">
                                            <div class="symbol symbol-30 symbol-primary mr-3">
                                                <span class="symbol-label text-white"><i class="flaticon2-bell-5 icon-sm text-white"></i></span>
                                            </div>
                                        </div>
                                        <div class="navi-text">
                                            <div class=" text-primary" style="font-size: 12px;">{{$rowData->noti_message}}</div>
                                            <div class="text-muted" style="font-size: 11px;">{{$da}}</div>
                                        </div>
                                    </div>
                                </a>
                                <?php
                            }

                            ?>


                        </div>
                        <!--end::Action-->
                    </div>
                    <!--end::Tabpane-->
                    
                </div>

                <!--end::Content-->
                <!--end::Content-->                
               

            </form>
        </div>
        <!--end::Dropdown-->
    </div>
    <!--end::Notifications-->   




    <!--begin::User-->
    <div class="topbar-item">
        <div class="btn btn-icon btn-icon-mobile w-auto btn-clean d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
            <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Hi,</span>
            <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">{{Auth::user()->name}}</span>
            <span class="symbol symbol-lg-35 symbol-25 symbol-light-success">
                <span class="symbol-label font-size-h5 font-weight-bold">A</span>
            </span>
        </div>
    </div>
    <!--end::User-->
</div>
<!--end::Topbar-->

</div>
<!--end::Container-->
</div>
<!--end::Header-->

<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
