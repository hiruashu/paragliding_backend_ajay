
var windowLoadAjax = {

  initA: function ( vType, day, ActionType )
  {
    //G1.Admin Payment Recived  Graph : this show all payment Recived graph approved by admin
    var courseID=$("#txtCouserID option:selected").val();
    //ajax call
    var formData = {
      '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      'daysCount': 30,
      'days': day,
      'course_id':courseID,
      'ActionType': ActionType
    };

    if ( vType == 1 )
    {
      var URLX = BASE_URL + '/getHighcartUsersWeeklyAdmin';
      var wiseDara = "Weekly";

    }
    if ( vType == 2 )
    {
      var URLX = BASE_URL + '/getHighcartUsersMonthlyAdmin';
      var wiseDara = "Month";

    }
    if ( vType == 3 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearlyAdmin';
      var wiseDara = "Month wise";

    }
    if ( vType == 4 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearlyYearAdmin';
      var wiseDara = "Yearly";

    }

    var jsonDataOrder = $.ajax( {
      url: URLX,
      dataType: "json",
      type: "GET",
      data: formData,
      async: false
    } ).responseJSON;

    var colors = [];
    colors[ 0 ] = '#16426b';
    colors[ 1 ] = '#C70039';

    //ajax call
    //containerTotalOrderMonthly
    var viewX = "containerUserDataView_" + vType;

    Highcharts.chart( viewX, {
      chart: {
        type: 'line',
        height: 250,
      },
      title: {
        text: 'New user Line Graph'
      },
      xAxis: {
        categories: jsonDataOrder.MonthName,
      },
      yAxis: {
        title: {
          text: 'Students'
        },
        labels: {
          formatter: function ()
          {
            return this.value;
          }
        }
      },
      tooltip: {
        crosshairs: true,
        shared: true
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: [ {
        name: wiseDara,
        marker: {
          symbol: 'square'
        },
        data: jsonDataOrder.monthlyValue,


      } ]
    } );


    // Highcharts.chart( viewX, {
    //   chart: {
    //     height: 175,
    //     type: 'column',
    //     options3d: {
    //       enabled: false,
    //       alpha: 10,
    //       beta: 25,
    //       depth: 10
    //     }
    //   },
    //   title: {
    //     text: ''
    //   },
    //   colors: colors,
    //   subtitle: {
    //     text: ''
    //   },
    //   plotOptions: {
    //     column: {
    //       depth: 25
    //     },

    //   },
    //   xAxis: {
    //     categories: jsonDataOrder.MonthName,
    //     labels: {
    //       skew3d: true,
    //       style: {
    //         fontSize: '8x',
    //         fontFamily: 'serif'
    //       }
    //     }
    //   },
    //   yAxis: {
    //     title: {
    //       text: null,
    //       color: '#16426B'
    //     }
    //   },
    //   series: [ {
    //     name: wiseDara,
    //     data: jsonDataOrder.monthlyValue
    //   } ]
    // } );

    //containerTotalOrderMonthly



    //G1.=============================


  },
  initA_pay: function ( vType, day, ActionType )
  {
    //G1.Admin Payment Recived  Graph : this show all payment Recived graph approved by admin

    var courseID=$("#txtCouserIDPay option:selected").val();
  
    //ajax call
    var formData = {
      '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      'daysCount': 30,
      'days': day,
      'course_id':courseID,
      'ActionType': ActionType
    };

    if ( vType == 1 )
    {
      var URLX = BASE_URL + '/getHighcartUsersWeeklyAdminPay';
      var wiseDara = "Weekly";

    }
    if ( vType == 2 )
    {
      var URLX = BASE_URL + '/getHighcartUsersMonthlyAdminPay';
      var wiseDara = "Month";

    }
    if ( vType == 3 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearlyAdminPay';
      var wiseDara = "Month wise";

    }
    if ( vType == 4 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearlyYearAdminPay';
      var wiseDara = "Yearly";

    }


    var jsonDataOrder = $.ajax( {
      url: URLX,
      dataType: "json",
      type: "GET",
      data: formData,
      async: false
    } ).responseJSON;

    var colors = [];
    colors[ 0 ] = '#16426b';
    colors[ 1 ] = '#C70039';

    //ajax call
    //containerTotalOrderMonthly
    var viewX = "containerUserDataViewPay_" + vType;

    Highcharts.chart( viewX, {
      chart: {
        type: 'line',
        height: 250,
      },
      title: {
        text: 'Payment Recieved Line Graph'
      },
      xAxis: {
        categories: jsonDataOrder.MonthName,
      },
      yAxis: {
        title: {
          text: 'Payment Amount'
        },
        labels: {
          formatter: function ()
          {
            return this.value;
          }
        }
      },
      tooltip: {
        crosshairs: true,
        shared: true
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: [ {
        name: wiseDara,
        marker: {
          symbol: 'square'
        },
        data: jsonDataOrder.monthlyValue,


      } ]
    } );


    // Highcharts.chart( viewX, {
    //   chart: {
    //     height: 175,
    //     type: 'column',
    //     options3d: {
    //       enabled: false,
    //       alpha: 10,
    //       beta: 25,
    //       depth: 10
    //     }
    //   },
    //   title: {
    //     text: ''
    //   },
    //   colors: colors,
    //   subtitle: {
    //     text: ''
    //   },
    //   plotOptions: {
    //     column: {
    //       depth: 25
    //     },

    //   },
    //   xAxis: {
    //     categories: jsonDataOrder.MonthName,
    //     labels: {
    //       skew3d: true,
    //       style: {
    //         fontSize: '8x',
    //         fontFamily: 'serif'
    //       }
    //     }
    //   },
    //   yAxis: {
    //     title: {
    //       text: null,
    //       color: '#16426B'
    //     }
    //   },
    //   series: [ {
    //     name: wiseDara,
    //     data: jsonDataOrder.monthlyValue
    //   } ]
    // } );

    //containerTotalOrderMonthly



    //G1.=============================


  },
  
  initB: function ( vType, day, ActionType )
  {
    
    //G1.Admin Payment Recived  Graph : this show all payment Recived graph approved by admin

    var countryID=$("#txtCounrySchool option:selected").val();
    var sid=$("#myschool option:selected").val();
    var courseID=$("#myschoolCourse option:selected").val();



    //ajax call
    var formData = {
      '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      'daysCount': 30,
      'days': day,
      'country_id': countryID,
      'sid': sid,
      'courseID': courseID,
      'ActionType': ActionType
    };

    if ( vType == 1 )
    {
      var URLX = BASE_URL + '/getHighcartUsersWeeklyEnAdmin';
      var wiseDara = "Weekly";

    }
    if ( vType == 2 )
    {
      var URLX = BASE_URL + '/getHighcartUsersMonthlyEnAdmin';
      var wiseDara = "Month";

    }
    if ( vType == 3 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearlyEnAdmin';
      var wiseDara = "Month wise";

    }
    if ( vType == 4 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearlyYearEnAdmin';
      var wiseDara = "Year wise";

    }

    var jsonDataOrder = $.ajax( {
      url: URLX,
      dataType: "json",
      type: "GET",
      data: formData,
      async: false
    } ).responseJSON;

    var colors = [];
    colors[ 0 ] = '#16426b';
    colors[ 1 ] = '#C70039';

    //ajax call
    //containerTotalOrderMonthly
    var viewX = "containerSchoolDataView_" + vType;

    Highcharts.chart( viewX, {
      chart: {
        type: 'line',
        height: 175,
      },
      title: {
        text: 'School wise Enrolled Line Graph'
      },
      xAxis: {
        categories: jsonDataOrder.MonthName,
      },
      yAxis: {
        title: {
          text: 'Students'
        },
        labels: {
          formatter: function ()
          {
            return this.value;
          }
        }
      },
      tooltip: {
        crosshairs: true,
        shared: true
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: [ {
        name: wiseDara,
        marker: {
          symbol: 'square'
        },
        data: jsonDataOrder.monthlyValue,


      } ]
    } );

    // Highcharts.chart( viewX, {
    //   chart: {
    //     height: 175,
    //     type: 'column',
    //     options3d: {
    //       enabled: false,
    //       alpha: 10,
    //       beta: 25,
    //       depth: 10
    //     }
    //   },
    //   title: {
    //     text: ''
    //   },
    //   colors: colors,
    //   subtitle: {
    //     text: ''
    //   },
    //   plotOptions: {
    //     column: {
    //       depth: 25
    //     },

    //   },
    //   xAxis: {
    //     categories: jsonDataOrder.MonthName,
    //     labels: {
    //       skew3d: false,
    //       style: {
    //         fontSize: '8px',
    //         fontFamily: 'serif'
    //       }
    //     }
    //   },
    //   yAxis: {
    //     title: {
    //       text: null,
    //       color: '#16426B'
    //     }
    //   },
    //   series: [ {
    //     name: wiseDara,
    //     data: jsonDataOrder.monthlyValue
    //   } ]
    // } );

    //containerTotalOrderMonthly



    //G1.=============================


  },
  initC: function ()
  {
    Highcharts.chart( 'containerUserData', {
      chart: {
        type: 'line'
      },
      title: {
        text: 'Monthly User Line Grapgh'
      },
      subtitle: {
        text: 'ZELOS'
      },
      xAxis: {
        categories: [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ]
      },
      yAxis: {
        title: {
          text: 'NO'
        }
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: [ {
        name: 'Weekly',
        data: [ 7, 6, 9, 14, 18, 21, 25, 26, 23, 18, 13, 9 ]
      }, {
        name: 'Monthly',
        data: [ 3, 4, 5, 8, 11, 15, 17, 16, 14, 10, 6, 4 ]
      },
      {
        name: 'Yearly',
        data: [ 3, 4, 5, 8, 2, 14, 17, 16, 14, 10, 6, 19 ]
      }
      ]
    } );

  },
  initD: function ()
  {
   
    
      //ajax call
      var formData = {
        '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' ),     
       
        'country_id':$("#txtCountryPie option:selected").val(),
        'sid': $("#txtSchoolPie option:selected").val(),        
      };
      var URLX = BASE_URL + '/getPIEChartDataByCountrySchoolAdmin';
     

    var jsonDataOrder = $.ajax( {
      url: URLX,
      dataType: "json",
      type: "GET",
      data: formData,
      async: false
    } ).responseJSON;
    

    Highcharts.chart('AdmincontainerPIE', {
      chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie',
          height: 348,
      },
      title: {
          text: 'School Rating '
      },
      tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      accessibility: {
          point: {
              valueSuffix: '%'
          }
      },
     
      series: [{
          name: 'School Rating',
          colorByPoint: true,
          data: jsonDataOrder.data_value
      }]
  });

  },
  initDAA: function ()
  {
    
      //ajax call
      var formData = {
        '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' ),      
       
        'course_id':$("#txtSchoolPie_Course option:selected").val(),
        'sid': $("#txtSchoolPie option:selected").val(),        
      };
      var URLX = BASE_URL + '/getPIEChartDataByCountrySchoolAdminCourse';
     

    var jsonDataOrder = $.ajax( {
      url: URLX,
      dataType: "json",
      type: "GET",
      data: formData,
      async: false
    } ).responseJSON;
    

    Highcharts.chart('AdmincontainerPIE_Course', {
      chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie',
          height: 305,
      },
      title: {
          text: 'Course Rating '
      },
      tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      accessibility: {
          point: {
              valueSuffix: '%'
          }
      },
     
      series: [{
          name: 'School Rating',
          colorByPoint: true,
          data: jsonDataOrder.data_value
      }]
  });

  },
  initDAA_payment: function ()
  {
    
      //ajax call
      var formData = {
        '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' ),      
       
        'course_id':$("#txtSchoolPie_Course option:selected").val(),
        'sid': $("#txtSchoolPie option:selected").val(),        
      };
      var URLX = BASE_URL + '/getPIEChartDataByCountrySchoolAdminCourse';
     

    var jsonDataOrder = $.ajax( {
      url: URLX,
      dataType: "json",
      type: "GET",
      data: formData,
      async: false
    } ).responseJSON;
    
    

    Highcharts.chart('AdmincontainerPIE_Payment', {
      chart: {
        type: 'line'
    },
    title: {
        text: 'Weekly payment Recieved'
    },
    subtitle: {
        text: 'ZELOS'
    },
    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    },
    yAxis: {
        title: {
            text: 'Payment Recieved '
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: [{
        name: 'Payment',
        data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
    }
  ]

  });

  },
//
  initE: function ( vType, day, ActionType )
  {
    //G1.Admin Payment Recived  Graph : this show all payment Recived graph approved by admin
    var countryID=$("#txtCounrySchoolShow option:selected").val();

    //ajax call
    var formData = {
      '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      'daysCount': 30,
      'days': day,
      'country_id': countryID,
      'ActionType': ActionType
    };

    if ( vType == 1 )
    {
      var URLX = BASE_URL + '/getHighcartUsersWeekly';
      var wiseDara = "Weekly";

    }
    if ( vType == 2 )
    {
      var URLX = BASE_URL + '/getHighcartUsersMonthly';
      var wiseDara = "Month";

    }
    if ( vType == 3 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearly';
      var wiseDara = "Month wise";

    }
    if ( vType == 4 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearlyYear';
      var wiseDara = "Year wise";

    }

    var jsonDataOrder = $.ajax( {
      url: URLX,
      dataType: "json",
      type: "GET",
      data: formData,
      async: false
    } ).responseJSON;

    var colors = [];
    colors[ 0 ] = '#16426b';
    colors[ 1 ] = '#C70039';

    //ajax call
    //containerTotalOrderMonthly
    var viewX = "containerNewSchoolDataView_" + vType;

    Highcharts.chart( viewX, {
      chart: {
        type: 'line',
        height: 175,
      },
      title: {
        text: 'New School Join Line graph'
      },
      xAxis: {
        categories: jsonDataOrder.MonthName,
      },
      yAxis: {
        title: {
          text: 'School'
        },
        labels: {
          formatter: function ()
          {
            return this.value;
          }
        }
      },
      tooltip: {
        crosshairs: true,
        shared: true
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: [ {
        name: wiseDara,
        marker: {
          symbol: 'square'
        },
        data: jsonDataOrder.monthlyValue,


      } ]
    } );

    // Highcharts.chart( viewX, {
    //   chart: {
    //     height: 175,
    //     type: 'column',
    //     options3d: {
    //       enabled: false,
    //       alpha: 10,
    //       beta: 25,
    //       depth: 10
    //     }
    //   },
    //   title: {
    //     text: ''
    //   },
    //   colors: colors,
    //   subtitle: {
    //     text: ''
    //   },
    //   plotOptions: {
    //     column: {
    //       depth: 25
    //     },

    //   },
    //   xAxis: {
    //     categories: jsonDataOrder.MonthName,
    //     labels: {
    //       skew3d: false,
    //       style: {
    //         fontSize: '8px',
    //         fontFamily: 'serif'
    //       }
    //     }
    //   },
    //   yAxis: {
    //     title: {
    //       text: null,
    //       color: '#16426B'
    //     }
    //   },
    //   series: [ {
    //     name: wiseDara,
    //     data: jsonDataOrder.monthlyValue
    //   } ]
    // } );

    //containerTotalOrderMonthly



    //G1.=============================


  },

}




jQuery( document ).ready( function ()
{

  
  windowLoadAjax.initD();
  windowLoadAjax.initDAA();
  // windowLoadAjax.initDAA_payment();  
  windowLoadAjax.initA( 1, 7, 1 );
  windowLoadAjax.initA_pay( 1, 7, 1 );
  //windowLoadAjax.initB( 1, 7, 2 ); 
  //windowLoadAjax.initD_Admin();
  //windowLoadAjax.initE( 1, 7, 2 );


//txtCountryPie

//alert(454545);
//txtCountryPie
$( '.txtSchoolPie_Course' ).on( 'change', function ()
{
  //alert(454545);
  windowLoadAjax.initDAA();
  // ajax

} );

//txtCounrySchool




//hight 

//hight 

} );



function btnShowChartNewUserPay( vType, day )
{
 
  windowLoadAjax.initA_pay( vType, day, 1 );

}

function btnShowChartNewUser( vType, day )
{
  windowLoadAjax.initA( vType, day, 1 );

}



function btnShowChartNewSchool( vType, day )
{
  windowLoadAjax.initB( vType, day, 2 );

}
function btnShowChartNewSchool_X( vType, day )
{
  windowLoadAjax.initE( vType, day, 3 );

}























