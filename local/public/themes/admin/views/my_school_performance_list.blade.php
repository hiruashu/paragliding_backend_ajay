<!--begin::Container-->
<div class="container">
	<div class="row">
		<div class="col-lg-12">


			<!--begin::Card-->
			<div class="card card-custom">
				<div class="card-header">
					<div class="card-title">
						<span class="card-icon">
						<i class="icon-xl la la-school"></i>
						</span>
						<h3 class="card-label">My School Performance</h3> List
					</div>
					<div class="card-toolbar">

						<!--begin::Button-->
						<!-- <a href="{{route('addStaticContent')}}" class="btn btn-primary font-weight-bolder">
											<i class="la la-plus"></i>New Content</a> -->
						<!--end::Button-->
					</div>
				</div>
				<div class="card-body">

					<form class="mb-15">
						<div class="row mb-6">

							<div class="col-lg-4 mb-lg-0 mb-6" style="display: none;">
								<label>Country:</label>
								<select class="form-control datatable-input ajcountry" id="myselect" >
									<option value="">Select</option>
									<?php
									$schools = DB::table('countries')
										->orderBy('name')
										->get();
									foreach ($schools as $key => $rowData) {
									?>
										<option value="{{$rowData->id}}">{{$rowData->name}}</option>
									<?php
									}

									?>

								</select>
							</div>
							

							<div class="col-lg-4 mb-lg-0 mb-6" style="display: none;">
								<div class="radio-inline">
								<label class="radio" style="margin-top:25px">
										<input checked type="radio" value="3" name="starRadioTOP">
										<span></span>ALL </label>


									<label class="radio" style="margin-top:25px">
										<input type="radio" value="1" name="starRadioTOP">
										<span></span>Top School </label>
									<label class="radio" style="margin-top:25px">
										<input type="radio" value="2" name="starRadioTOP">
										<span></span>Worst School </label>

									
									
								</div>
							</div>
							

						</div>


					</form>
					<div class="radio-inline">
					<label class="radio">
							<input checked type="radio" value="6" name="starRadioMyS">
							<span></span>ALL <i class="icon-xl la la-star text-warning"></i></label>
							
						<label class="radio">
							<input type="radio" value="1" name="starRadioMyS">
							<span></span>1 <i class="icon-xl la la-star text-warning"></i></label>
						<label class="radio">
							<input type="radio" value="2" name="starRadioMyS">
							<span></span>2 <i class="icon-xl la la-star text-warning"></i></label>
						<label class="radio">
							<input type="radio" value="3" name="starRadioMyS">
							<span></span>3 <i class="icon-xl la la-star text-warning"></i></label>
						<label class="radio">
							<input type="radio" value="4" name="starRadioMyS">
							<span></span>4 <i class="icon-xl la la-star text-warning"></i></label>
						<label class="radio">
							<input type="radio" value="5" name="starRadioMyS">
							<span></span>5 <i class="icon-xl la la-star text-warning"></i></label>

							

					</div>

					<!--begin: Datatable-->
					<table class="table table-bordered table-hover table-checkable" id="kt_datatable_schoolPerformanceMy" style="margin-top: 13px !important">
						<thead>
							<tr>
								<th>Record ID</th>
								<th>S#</th>
								<th>School</th>
								<th>Total Rating</th>
								<th>Avg Rating</th>								
								<th>Actions</th>
							</tr>
						</thead>



					</table>
					<!--end: Datatable-->
				</div>
			</div>
			<!--end::Card-->

		</div>
	</div>
</div>
<!--end::Container-->