<div class="container">
@if (\Session::has('success'))
            <div class="alert alert-custom alert-notice alert-light-success fade show mb-1" role="alert">
                <div class="alert-icon">
                    <i class="flaticon-warning"></i>
                </div>
                <div class="alert-text">{!! \Session::get('success') !!}</div>
                <div class="alert-close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">
                            <i class="ki ki-close"></i>
                        </span>
                    </button>
                </div>
            </div>



            @endif
    <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Add Member in Chat Group
                    <!-- <span class="d-block text-muted pt-2 font-size-sm">Datatable initialized from HTML table</span> -->
                </h3>
            </div>

        </div>
        <div class="card-body">
            <form action="{{route('saveGroupcharMember')}}" method="post">
                @csrf
                <input type="hidden" name="txtGID" value="{{ Request::segment(2) }}">
            <table class="table table-bordered mb-6">
                <thead>
                    <tr>
                        <th scope="col">Check</th>
                        <th scope="col">Photo</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Join On</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

use Illuminate\Support\Facades\Auth;

$studentArr=getEnrolledStudentBySchoolID(Auth::user()->sid);

foreach ($studentArr as $key => $rowData) {
    $schoolArr = DB::table('users')->where('id', $rowData->id)->whereNotNull('avatar')->first();
    if ($schoolArr == null) {
        $schLogo = NoImage();
    } else {
        $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
    }
    $usersInGroup = DB::table('group_chat_users')
    ->where('user_id',$rowData->id)
    ->where('is_deleted',0)
    ->where('gid',Request::segment(2))
    ->first();

    
   ?>
    <tr>
                        <th scope="row">
                            <style>
                                
                            </style>
                            <label class="checkbox">
                                <?php 
                                if($usersInGroup==null){
                                    ?>
                                    <input type="checkbox" name="userArr[]" value="{{$rowData->id}}">
                                    <?php
                                }else{
                                    ?>
                                    <input disabled="disabled"  checked type="checkbox" name="userArr[]" value="{{$rowData->id}}">
                                    <?php
                                }

                                ?>
                                
                                <span></span></label>
                                {{$rowData->name}}
                        </th>
                        <td>
                        <img src="{{$schLogo}}" width="45px">
                        </td>
                        <td>
                            {{$rowData->gender ==1 ? "Male":"Female"}}
                        </td>
                        <td>
                          {{date('j F Y ',strtotime($rowData->created_at))}}
                        </td>
                    </tr>
   <?php
}


                    ?>
                   
                   
                   
                </tbody>
            </table>
        <input type="submit" value="Add Members" class="btn btn-primary">
            </form>
        </div>
    </div>
    <!--end::Card-->
</div>