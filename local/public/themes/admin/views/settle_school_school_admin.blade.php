<!--begin::Container-->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <?php
            // echo "<pre>";
            // echo $data->student_id;

            $schoolsUserArr = DB::table('users')
                ->where('id', $data->student_id)
                ->first();

            $schoolsUserCouseArr = DB::table('school_course')
                ->where('sid', Auth::user()->sid)
                ->where('id', $data->course_id)
                ->first();

            $payidAmtUser = DB::table('course_payment')
                ->where('sid', Auth::user()->sid)
                ->where('user_id', $data->student_id)
                ->where('is_deleted', 0)
                ->where('course_id', $data->course_id)
                ->sum('payment_amt');

            $dueAt = $schoolsUserCouseArr->course_amt - $payidAmtUser;


            // print_r($schoolsUserCouseArr->certificate_title);



            ?>

            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Selletment Due Amount of :{{$schoolsUserArr->name}} of course :{{$schoolsUserCouseArr->certificate_title}}</h3>

                </div>
                @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
                @endif
                <!--begin::Form-->
                <form data-redirect="school-list" class="form" method="post" action="{{route('saveSettleDuePaymetOfCouser')}}" id="ayra_kt_form_add_school">
                    <input type="hidden" name="txtUID" value="{{$data->student_id}}">
                    <input type="hidden" name="txtCouserID" value="{{$data->course_id}}">
                    <input type="hidden" name="txtSID" value="{{Auth::user()->sid}}">
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">

                            <div class="form-group row">

                                <div class="col-lg-3">
                                    <label> Course Amount</label>
                                    <div class="input-group">
                                        <input type="text" name="txtCourseAmt" readonly value="{{$schoolsUserCouseArr->course_amt}}" id="txtCourseAmt" class="form-control form-control-m form-control-solid" placeholder="Course Amount" />
                                    </div>

                                    <span class="form-text text-muted"></span>
                                </div>

                                <div class="col-lg-3">
                                    <label>Total Paid Amount</label>
                                    <div class="input-group">

                                        <input type="text" readonly name="txtTotalPaidAmt" value="{{$payidAmtUser}}" id="txtPhoneCode" class="form-control form-control-m form-control-solid" placeholder="Phone" />
                                    </div>

                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="col-lg-3">
                                    <label> Due Amount</label>
                                    <div class="input-group">

                                        <input type="text" name="txtdueAmt" value="{{$dueAt}}" id="txtPhoneCode" class="form-control form-control-m form-control" placeholder="Pleae Enter Amount" />
                                    </div>

                                    <span class="form-text text-muted"></span>
                                </div>

                                <div class="col-lg-3">
                                    <label> Remarks</label>
                                    <div class="input-group">

                                        <input size="30px" width="100px" name="txtRemarks" type="text" id="" class="form-control" placeholder="Remarks" />

                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>








                            </div>



                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-8">
                                    <button type="submit" id="btnSaveSchool" data-wizard-action="submit" class="btn btn-primary mr-2">Settle Now</button>
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Card-->

        </div>
    </div>
</div>
<!--end::Container-->