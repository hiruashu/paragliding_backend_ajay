<div class="container">
    <div class="row">
        <div class="col-lg-12">

            @if (\Session::has('success'))
            <div class="alert alert-custom alert-notice alert-light-success fade show mb-1" role="alert">
                <div class="alert-icon">
                    <i class="flaticon-warning"></i>
                </div>
                <div class="alert-text">{!! \Session::get('success') !!}</div>
                <div class="alert-close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">
                            <i class="ki ki-close"></i>
                        </span>
                    </button>
                </div>
            </div>



            @endif
            <?php 

 if ($data->photo == null) {
    $schLogo = NoImage();
} else {
    $schLogo = asset('/local/public/upload/') . "/" . $data->photo;
}

?>
            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Edit Group Chat</h3>
                    <div class="card-toolbar">

                    </div>
                </div>

                <!--begin::Form-->
                <form class="form" action="{{route('saveEdit_group_chat')}}" method="post" enctype="multipart/form-data">
                    @csrf
         
                    <input type="hidden" name="txtID" value="{{$data->id}}">
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Title:</label>
                                <input required type="text" class="form-control" value="{{$data->group_title}}" name="group_title" placeholder="">
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-6">
                                <label>Group Name:</label>
                                <input required type="text" class="form-control" value="{{$data->group_name}}" name="group_name" placeholder="">
                                <span class="form-text text-muted"></span>
                            </div>


                        </div>
                        <div class="form-group row">

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Photo</label>
                                    <div></div>
                                    <div class="custom-file">
                                        <input type="file" name="file" class="custom-file-input" id="customFile" />
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-50 symbol-light mr-4">
                                        <span class="symbol-label">
                                            <img src="{{$schLogo}}" class="h-75 align-self-end" alt="">
                                        </span>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>



                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-8">
                                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                <button type="reset" class="btn btn-secondary">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Card-->

        </div>
    </div>
</div>