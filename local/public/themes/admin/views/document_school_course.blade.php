<div class="container">
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Course Document</h3>
                    <div class="card-toolbar">

                    </div>
                </div>

                <!--begin::Form-->
                <form>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-lg-right">Documents Name:</label>
                                    <div class="col-lg-7">
                                        <input type="text" id="txtDocInfo" class="form-control" placeholder="About Documents" name="name" />
                                        <input type="text" id="txtSID" value="{{$data->sid}}" class="form-control" placeholder="About Documents" name="name" />
                                        <input type="text" id="txtID"  value="{{$data->id}}" class="form-control" placeholder="About Documents" name="name" />
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-lg-right">Upload Files:</label>
                                    <div class="col-lg-9">
                                        <div class="dropzone dropzone-multi" id="kt_dropzone_4">
                                            <div class="dropzone-panel mb-lg-0 mb-2">
                                                <a class="dropzone-select btn btn-primary font-weight-bold btn-sm">Attach files</a>
                                                <a class="dropzone-upload btn btn-warning font-weight-bold btn-sm">Upload All</a>
                                                <a class="dropzone-remove-all btn btn-danger font-weight-bold btn-sm">Remove All</a>
                                            </div>
                                            <div class="dropzone-items">
                                                <div class="dropzone-item" style="display:none">
                                                    <div class="dropzone-file">
                                                        <div class="dropzone-filename" title="some_image_file_name.jpg">
                                                            <span data-dz-name="">some_image_file_name.jpg</span>
                                                            <strong>(
                                                                <span data-dz-size="">340kb</span>)</strong>
                                                        </div>
                                                        <div class="dropzone-error" data-dz-errormessage=""></div>
                                                    </div>
                                                    <div class="dropzone-progress">
                                                        <div class="progress">
                                                            <div class="progress-bar bg-primary" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress=""></div>
                                                        </div>
                                                    </div>
                                                    <div class="dropzone-toolbar">
                                                        <span class="dropzone-start">
                                                            <i class="flaticon2-arrow"></i>
                                                        </span>
                                                        <span class="dropzone-cancel" data-dz-remove="" style="display: none;">
                                                            <i class="flaticon2-cross"></i>
                                                        </span>
                                                        <span class="dropzone-delete" data-dz-remove="">
                                                            <i class="flaticon2-cross"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="form-text text-muted">Maximum 5MB file size is supported.</span>
                                    </div>
                                </div>
                            </div>

                            <div class="card card-custom card-stretch gutter-b">
                                <!--begin::Header-->
                                <div class="card-header border-0">
                                    <h3 class="card-title font-weight-bolder text-dark">Uploaded Docuemnts</h3>

                                </div>
                                <!--end::Header-->
                                <!--begin::Body-->
                                <div class="card-body pt-2">
                                   <!--begin::Item-->
                                   <?php
                                   $docArr = DB::table('school_documents_course')
                                       ->where('sid', $data->sid)
                                       ->get();

                                   foreach ($docArr as $key => $rowData) {

                                       $link = asset('/local/public/upload/') . "/" . $rowData->doc_name;

                                   ?>
                                   <div class="d-flex align-items-center mb-10">
                                       <!--begin::Symbol-->
                                       <div class="symbol symbol-40 symbol-light-success mr-5">
                                           <span class="symbol-label">
                                               <img src="{{noImage()}}" class="h-75 align-self-end" alt="">
                                           </span>
                                       </div>
                                       <!--end::Symbol-->
                                       <!--begin::Text-->
                                      
                                           <div class="d-flex flex-column flex-grow-1 font-weight-bold">
                                               <a target="_blank" href="{{$link}}" class="text-dark text-hover-primary mb-1 font-size-lg">{{$rowData->doc_info}}</a>
                                               <span class="text-muted">{{date('j F Y',strtotime($rowData->created_at))}}</span>
                                           </div>

                                      


                                       <!--end::Text-->

                                   </div>
                                   <?php
                               }

                               ?>
                                   <!--end::Item-->

                                </div>
                                <!--end::Body-->
                            </div>

                        </form>
                <!--end::Form-->
            </div>
            <!--end::Card-->

        </div>
    </div>
</div>