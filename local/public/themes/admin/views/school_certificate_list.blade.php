<!--begin::Container-->
<div class="container">
    <div class="row">
        <div class="col-lg-12">


          <!--begin::Card-->
								<div class="card card-custom">
									<div class="card-header">
										<div class="card-title">
											<span class="card-icon">
												<i class="flaticon2-favourite text-primary"></i>
											</span>
											<h3 class="card-label">School Certificate</h3> List
										</div>
										<div class="card-toolbar">
										
											<!--begin::Button-->
											<!-- <a href="{{route('addStaticContent')}}" class="btn btn-primary font-weight-bolder">
											<i class="la la-plus"></i>New Content</a> -->
											<!--end::Button-->
										</div>
									</div>
									<div class="card-body">
										<!--begin: Datatable-->
										<table class="table table-bordered table-hover table-checkable" id="kt_datatable_school_certificate" style="margin-top: 13px !important">
											<thead>
												<tr>
													<th>Record ID</th>
													<th>S#</th>
													<th>School</th>
													<th>Certificate</th>
													<th>Total Enrolled Student</th>	
													<th>Course Date</th>													
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
                                            <?php
                                            $contentArr= DB::table('school_course')
                                            ->where('is_deleted',0)
                                            ->get();
                                            $i=0;
                                            foreach ($contentArr as $key => $rowData) {
                                                $i++;
												$schoolArr= DB::table('schools')
                                            ->where('id',$rowData->sid)
                                            ->first();
                                               ?>
                                               <tr>
													<td>{{$rowData->id}}</td>
													<td>{{$i}}</td>
													<td>{{$schoolArr->title}}</td>
													<td>{{$rowData->certificate_title}}</td>
													<td>5</td>
													<td>{{$rowData->course_date}}</td>													
													<td nowrap="nowrap"></td>
												</tr>
                                               <?php
                                            }

                                             ?>
												
												
											</tbody>
										</table>
										<!--end: Datatable-->
									</div>
								</div>
								<!--end::Card-->

        </div>
    </div>
</div>
<!--end::Container-->