

<div class="container">
	<!--begin::Chat-->
	<input type="hidden" id="txtUserSender" value="{{Auth::user()->name}}">
	<input type="hidden" id="txtUserReciever" value="Admin">
	<input type="hidden" id="from_id" value="{{Auth::user()->id}}">
	<input type="hidden" id="to_id" value="1">
	<input type="hidden" id="messageID">
	<?php 
	$user_id = 1;
    $records = \App\Models\User::where('id', $user_id)->first();

	if ($records->avatar == null) {
            $schLogo = NoImage();
        } else {
            $schLogo = asset('/local/public/upload/') . "/" . $records->avatar;
        }

		$recordsA = \App\Models\User::where('id', Auth::user()->id)->first();

		if ($recordsA->avatar == null) {
            $schLogoMe = NoImage();
        } else {
            $schLogoMe = asset('/local/public/upload/') . "/" . $recordsA->avatar;
        }
	?>
		<!--begin::Chat-->
		<div class="d-flex flex-row">
			<!--begin::Aside-->
			<div class="flex-row-auto offcanvas-mobile w-350px w-xl-400px" id="kt_chat_aside">
				<!--begin::Card-->
				<div class="card card-custom">
					<!--begin::Body-->
					<div class="card-body">
						<style>
							.d-flex{
								cursor: pointer;
							}
						</style>
						<!--begin:Users-->
						<div class="mt-7 scroll scroll-pull">
							<!--begin:User-->
							<div onclick="showSchoolChatUser(1)" class="d-flex align-items-center justify-content-between mb-5">
								<div class="d-flex align-items-center">
									<div class="symbol symbol symbol-50 mr-3">
										<img alt="Pic" src="{{$schLogo}}" />
									</div>
									<div class="d-flex flex-column">
										<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-lg">{{$records->name}}</a>
										<span class="text-muted font-weight-bold font-size-sm">ZELOS-ADMIN</span>
									</div>
								</div>
								<div class="d-flex flex-column align-items-end">
									{{-- <span class="text-muted font-weight-bold font-size-sm">35 mins</span> --}}
								</div>
							</div>
							<!--end:User-->
							<hr>
							<?php 
							 $users_arrArr = DB::table('chat_group')->where('is_deleted',0)->where('status', 1)->orderBy('id', 'DESC')->get();


						foreach ($users_arrArr as $key => $rowData) {
							# code...
							if ($rowData->photo == null) {
								$schLogo = NoImage();
							} else {
								$schLogo = asset('/local/public/upload/') . "/" . $rowData->photo;
							}

					
									?>
									<!--begin:User-->
							<div class="d-flex align-items-center justify-content-between mb-5">
								<div class="d-flex align-items-center">
									<div class="symbol symbol-circle symbol-50 mr-3">
										<img alt="Pic" src="{{$schLogo}}" />
									</div>
									<div class="d-flex flex-column">
										<a href="{{route('zelosChatAdminGroupChat',$rowData->id)}}" class="text-dark-75 text-hover-primary font-weight-bold font-size-lg">{{$rowData->group_name}}</a>
										<span class="text-muted font-weight-bold font-size-sm">GROUP</span>
									</div>
								</div>
								<!-- <div class="d-flex flex-column align-items-end">
									<span class="text-muted font-weight-bold font-size-sm">7 hrs</span>
									<span class="label label-sm label-success">4</span>
								</div> -->
							</div>
							<!--end:User-->
									<?php

								}

							?>
							
							
						</div>
						<!--end:Users-->
					</div>
					<!--end::Body-->
				</div>
				<!--end::Card-->
			</div>
			<!--end::Aside-->
			<!--begin::Content-->
			<div class="flex-row-fluid ml-lg-8" id="kt_chat_content">
				<!--begin::Card-->
				<div class="card card-custom">
					<input type="hidden" id="txtuserLinkTo" value="{{$schLogo}}">
					<input type="hidden" id="txtuserLinkMe" value="{{$schLogoMe}}">

					<!--begin::Header-->
					<div class="card-header align-items-lft px-4 py-3">
						
						<div class="text-left flex-grow-3">
							<div class="symbol symbol-circle symbol-40 mr-3">
								<img alt="Pic" src="{{$schLogo}}" />
								<div class="text-dark-75 font-weight-bold font-size-h5">{{$records->name}}</div>
							</div>
							
							<div>
								<span class="label label-sm label-dot label-success"></span>
								<span class="font-weight-bold text-muted font-size-sm">Active</span>
							</div>
						</div>
						
					</div>
					<!--end::Header-->
					<!--begin::Body-->
					<div class="card-body">
						<!--begin::Scroll-->
						<div class="scroll scroll-pull" data-mobile-height="350">
							<!--begin::Messages-->
							<div class="messages">
									
								
								<?php

										$to_id = 1;

										$messages = \App\Models\Message::where(function ($query) use ($to_id) {
											$query->where('from_id', Auth::user()->id)->where('to_id', $to_id);
										})->orWhere(function ($query) use ($to_id) {
											$query->where('from_id', $to_id)->where('to_id', Auth::user()->id);
										})->orderBy('created_at', 'ASC')->get();

										//count($messages);

										foreach ($messages as $key => $rowData) {
											     $curr_time=$rowData->created_at;
												$time_ago = strtotime($curr_time);
												$time_agoView=time_Ago($time_ago) . "\n";

											if ($rowData->from_id == Auth::user()->id) {
												if (empty($rowData->attachment)) { //text message
													?>
													<!--begin::Message Out-->
													<div class="d-flex flex-column mb-5 align-items-end">
														<div class="d-flex align-items-center">
															<div>
																<span class="text-muted font-size-sm">{{$time_agoView}}</span>
																<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
															</div>
															<div class="symbol symbol-circle symbol-40 ml-3">
																<img alt="Pic" src="{{$schLogoMe}}" />
															</div>
														</div>
														<div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px">
															{{$rowData->body}}
														</div>
													</div>
													<!--end::Message Out-->
													<?php
												}
												// if attchement type is image
												if ($rowData->attachment_type == 1) {
													$imgDoc = asset('/local/public/upload/') . "/" . $rowData->attachment;
													?>
													<div class="d-flex flex-column mb-5 align-items-end">
														<div class="d-flex align-items-center">
															<div>
																<span class="text-muted font-size-sm">{{$time_agoView}}</span>
																<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
															</div>
															<div class="symbol symbol-circle symbol-40 ml-3">
																<img alt="Pic" src="{{$schLogoMe}}" />
															</div>
														</div>
														<div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
															<a href="{{$imgDoc}}" target="_blank" title="" class=" text-dark-50 font-weight-bold font-size-lg">
																<img src="{{$imgDoc}}" class="w-100 rounded" alt=""> 
															</a>
															<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">{{$rowData->body}}</span>
														</div>
													</div>
													<?php
												} 
												//if attchement tyoe is image 
												if ($rowData->attachment_type == 2) {
													$imgDoc = asset('/local/public/upload/') . "/" . $rowData->attachment;
													?>
													<div class="d-flex flex-column mb-5 align-items-end">
														<div class="d-flex align-items-center">
															<div>
																<span class="text-muted font-size-sm">{{$time_agoView}}</span>
																<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
															</div>
															<div class="symbol symbol-circle symbol-40 ml-3">
																<img alt="Pic" src="{{$schLogoMe}}" />
															</div>
														</div>
														<div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
															<a href="{{$imgDoc}}" target="_blank" title="" class=" text-dark-50 font-weight-bold font-size-lg">
																<video width="250" height="150" src="{{$imgDoc}}" controls>
																	Sorry, your browser doesn't support HTML5 <code>video</code>
																  </video>
															</a>
															<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">{{$rowData->body}}</span>
														</div>
													</div>
													<?php
												} 
												if ($rowData->attachment_type == 3) {
													$imgDoc = asset('/local/public/upload/') . "/" . $rowData->attachment;
													?>
													<div class="d-flex flex-column mb-5 align-items-end">
														<div class="d-flex align-items-center">
															<div>
																<span class="text-muted font-size-sm">{{$time_agoView}}</span>
																<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
															</div>
															<div class="symbol symbol-circle symbol-40 ml-3">
																<img alt="Pic" src="{{$schLogoMe}}" />
															</div>
														</div>
														<div class="msg_time_mn rounded ml-5 p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px position-relative">
															<a href="{{$imgDoc}}" target="_blank" title="" style="width: 300px;" class="d-flex align-items-center justify-content-between bg-white p-4 rounded">
																<p class="mb-0">abcd.docx</p>
																<i class="icon-xl fas fa-arrow-alt-circle-down ml-5 text-primary"></i>
															</a>
															
															<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">{{$rowData->body}}</span>
														</div>
													</div>
													<?php
												}
												
                                            ?>
											
										
											<?php
											
											
											}else{ //outer users
												$recordsUSer = \App\Models\User::where('id', $rowData->from_id)->first();

												if ($recordsUSer->avatar == null) {
														$schLogoU = NoImage();
													} else {
														$schLogoU = asset('/local/public/upload/') . "/" . $recordsUSer->avatar;
													}

												?>
												<?php 
												if (empty($rowData->attachment)) { //text message
													?>
													<!--begin::Message In-->
													<div class="d-flex flex-column mb-5 align-items-start">
														<div class="d-flex align-items-center">
															<div class="symbol symbol-circle symbol-40 mr-3">
																<img alt="Pic" src="{{$schLogoU}}" />
															</div>
															<div>
																<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">{{@$recordsUSer->name}}</a>
																<span class="text-muted font-size-sm">{{$time_agoView}}</span>
															</div>
														</div>
														<div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">
															{{$rowData->body}}
														</div>
													</div>
											        <!--end::Message In-->
													<?php
												}
												if ($rowData->attachment_type == 1) {
													$imgDoc = asset('/local/public/upload/') . "/" . $rowData->attachment;
													?>
													<!--begin::Message In-->
													<div class="d-flex flex-column mb-5 align-items-start">
														<div class="d-flex align-items-center">
															<div class="symbol symbol-circle symbol-40 mr-3">
																<img alt="Pic" src="{{$schLogoU}}" />
															</div>
															<div>
																<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">{{@$recordsUSer->name}}</a>
																<span class="text-muted font-size-sm">{{$time_agoView}}</span>
															</div>
														</div>
														<div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
															<a href="{{$imgDoc}}" target="_blank" title="" class=" text-dark-50 font-weight-bold font-size-lg">
																<img src="{{$imgDoc}}" class="w-100 rounded" alt=""> 
															</a>
															<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">{{$rowData->body}}</span>
														</div>
													</div>
											        <!--end::Message In-->
													<?php
												}
												if ($rowData->attachment_type == 2) {
													$imgDoc = asset('/local/public/upload/') . "/" . $rowData->attachment;
													?>
													<!--begin::Message In-->
													<div class="d-flex flex-column mb-5 align-items-start">
														<div class="d-flex align-items-center">
															<div class="symbol symbol-circle symbol-40 mr-3">
																<img alt="Pic" src="{{$schLogoU}}" />
															</div>
															<div>
																<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">{{@$recordsUSer->name}}</a>
																<span class="text-muted font-size-sm">{{$time_agoView}}</span>
															</div>
														</div>
														<div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
															<a href="{{$imgDoc}}" target="_blank" title="" class=" text-dark-50 font-weight-bold font-size-lg">
																<video width="250" height="150" src="{{$imgDoc}}" controls>
																	Sorry, your browser doesn't support HTML5 <code>video</code>
																  </video>
															</a>
															<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">{{$rowData->body}}</span>
														</div>
													</div>
											        <!--end::Message In-->
													<?php
												}
												if ($rowData->attachment_type == 3) {
													$imgDoc = asset('/local/public/upload/') . "/" . $rowData->attachment;
													?>
													<!--begin::Message In-->
													<div class="d-flex flex-column mb-5 align-items-start">
														<div class="d-flex align-items-center">
															<div class="symbol symbol-circle symbol-40 mr-3">
																<img alt="Pic" src="{{$schLogoU}}" />
															</div>
															<div>
																<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">{{@$recordsUSer->name}}</a>
																<span class="text-muted font-size-sm">{{$time_agoView}}</span>
															</div>
														</div>
														<div class="msg_time_mn rounded ml-5 p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px position-relative">
															<a href="{{$imgDoc}}" target="_blank" title="" style="width: 300px;" class="d-flex align-items-center justify-content-between bg-white p-4 rounded">
																<p class="mb-0">abcd.docx</p>
																<i class="icon-xl fas fa-arrow-alt-circle-down ml-5 text-primary"></i>
															</a>
															
															<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">{{$rowData->body}}</span>
														</div>
													</div>
											        <!--end::Message In-->
													<?php
												}
												?>
													
												<?php
											}
										}
											?>


							</div>
							<!--end::Messages-->
						</div>
						<!--end::Scroll-->
					</div>
					<!--end::Body-->
					<!--begin::Footer-->
					<div class="card-footer align-items-center">
						<!--begin::Compose-->
						<textarea  id="txtMsgArea" class="form-control border-0 p-0" rows="2" placeholder="Type a message"></textarea>
						<div class="d-flex align-items-center justify-content-between mt-5">
							<div class="mr-3">
								<a href="#" class="btn btn-clean btn-icon btn-md mr-1" data-toggle="modal" data-target="#img_file_select">
									<i class="icon-xl fas fa-image"></i>
								</a>

								<a href="#" class="btn btn-clean btn-icon btn-md" data-toggle="modal" data-target="#img_file_select">
									<i class="icon-xl fas fa-video"></i>
								</a>
								<a href="#" class="btn btn-clean btn-icon btn-md" data-toggle="modal" data-target="#img_file_select">
									<i class="icon-xl fas fa-file-alt"></i>
								</a>
							</div>
							<div>
								<button type="button" class="btn btn-primary btn-md text-uppercase font-weight-bold chat-send py-2 px-6">Send</button>
							</div>
						</div>
						<!--begin::Compose-->
					</div>
					<!--end::Footer-->
				</div>

				<div class="modal fade" id="img_file_select" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" aria-hidden="true">
					<div class="modal-dialog modal-dialog modal-lg" role="document">
						<form id="formChatSuperAdmin" action="ajaxupload.php" method="post" enctype="multipart/form-data">

							<div class="modal-content bg-light">
								<div class="modal-header border-0 bg-primary">
									<h5 class="modal-title text-center w-100 text-white" id="exampleModalLabel">Preview</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<i aria-hidden="true" class="ki ki-close text-white"></i>
									</button>
								</div>
								<div class="modal-body text-center border-0">
									<div class="image-input image-input-outline" id="kt_image_3">
										<div class="image-input-wrapper" style="background-image: url({{noImage()}}); width: 200px; height: 200px;"></div>

										<label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
											<i class="fa fa-pen icon-sm text-muted"></i>
											<input type="file" name="photo" accept=".png, .jpg, .jpeg" />
											<input type="hidden" name="profile_avatar_remove" />
										</label>



										<input type="hidden" name="to_id_model" id="to_id_model" value="1">
										<input type="hidden" name="attachment_type" value="1">
										<input name="_token" type="hidden" value="{{csrf_token()}}">

										<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar" style="left: -10px; top: -10px;">
											<i class="ki ki-bold-close icon-xs text-muted"></i>
										</span>
									</div>
									<div class="text_send_btn d-flex align-items-center justify-content-between mt-5 pt-4 px-3">
										<input type="text" class="form-control" placeholder="Add a caption..." name="message">
										<button type="submit" class="btn btn-primary font-weight-bold ml-2">Send</button>

									</div>
								</div>

							</form>
						</div>
					</div>


					<!--end::Footer-->
				</div>
				
				<!--end::Card-->
			</div>
			<!--end::Content-->
		</div>
		<!--end::Chat-->
</div>
</div>