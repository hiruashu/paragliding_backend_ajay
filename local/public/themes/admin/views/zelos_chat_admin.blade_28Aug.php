<!--begin::Container-->
<style type="text/css" media="screen">
	.msg_time_mn:before {
		content: "";
		position: absolute;
		top: 0;
		left: 0;
		height: 40px;
		width: 40px;
		transform: translateX(-50%);
		border-top: 20px solid #c9f7f5;
		border-bottom: 20px solid #0000;
		border-left: 20px solid #0000;
		border-right: 20px solid #0000;
	}

	.msg_time_mn.my_msg_time_mn:before {
		left: inherit;
		right: 0;
		transform: translateX(50%);
	}

	.card_body_custom::-webkit-scrollbar {
		width: 5px;
	}
	.card_body_custom::-webkit-scrollbar-track {
		border-radius: 10px;
	}
	.card_body_custom::-webkit-scrollbar-thumb {
		background: #68B3FF; 
		border-radius: 10px;
	}
	.card_body_custom::-webkit-scrollbar-thumb:hover {
		background: #D8D8D8; 
	}

	@media (min-width: 992px){
		.header-fixed.subheader-fixed.subheader-enabled .wrapper {
			padding-top: 40px !important;
		}
	}
</style>

<div class="container">
	<!--begin::Chat-->
	<input type="hidden" id="txtUserSender" value="{{Auth::user()->name}}">
	<input type="hidden" id="txtUserReciever" value="Admin">
	<input type="hidden" id="from_id" value="{{Auth::user()->id}}">
	<input type="hidden" id="to_id" value="1">
	<input type="hidden" id="messageID">
	<!--begin::Content-->
	<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
		<!--begin::Entry-->
		<div class="d-flex flex-column-fluid">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Chat-->
				<div class="d-flex flex-row">
					<!--begin::Aside-->

					<!--end::Aside-->

					<!--begin::Content-->
					<div class="flex-row-fluid ml-lg-8" id="kt_chat_content">
						<!--begin::Card-->
						<div class="card card-custom">
							<!--begin::Header-->
							<div class="card-header align-items-center px-4 py-3">
								<div class="text-left flex-grow-1">
									<!--begin::Aside Mobile Toggle-->
									<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md d-lg-none" id="kt_app_chat_toggle">
										<span class="svg-icon svg-icon-lg">
											<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Adress-book2.svg-->
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24" />
													<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />
													<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />
												</g>
											</svg>
											<!--end::Svg Icon-->
										</span>
									</button>
									<!--end::Aside Mobile Toggle-->
									<!--begin::Dropdown Menu-->

									<!--end::Dropdown Menu-->
								</div>
								<div class="w-100 text-left flex-grow-1 d-flex align-items-center">
									<div class="symbol symbol-50 symbol-circle mr-4">
										<img alt="Pic" src="<?php echo NoImage(); ?>">
									</div>
									<div>
										<div class="text-dark-75 font-weight-bold font-size-h5 mainUserIMG">Admin</div>
										<div>
											<span class="label label-sm label-dot label-success"></span>
											<span class="font-weight-bold text-muted font-size-sm">Active</span>
										</div>
									</div>
								</div>
								<div class="text-right flex-grow-1">
									<!--begin::Dropdown Menu-->

									<!--end::Dropdown Menu-->
								</div>
							</div>
							<!--end::Header-->
							<!--begin::Body-->
							<div class="card-body card_body_custom px-4 py-2" style="height: 60vh; overflow: auto;">
								<!--begin::Scroll-->
								<div class="scroll scroll-pull chat_box" id="chatbox" data-mobile-height="350">
									<!--begin::Messages-->
									<div class="messages messages_display" style="min-height: 230px;">
										<?php

										$to_id = 1;

										$messages = \App\Models\Message::where(function ($query) use ($to_id) {
											$query->where('from_id', Auth::user()->id)->where('to_id', $to_id);
										})->orWhere(function ($query) use ($to_id) {
											$query->where('from_id', $to_id)->where('to_id', Auth::user()->id);
										})->orderBy('created_at', 'ASC')->limit(10000)->get();

										//count($messages);

										foreach ($messages as $key => $rowData) {
											if ($rowData->from_id == Auth::user()->id) {
												
												if (empty($rowData->attachment)) {
													?>
													<div class="d-flex mb-2 align-items-start">
														<div class="d-flex align-items-center">
															<div class="symbol symbol-circle symbol-40 mr-4">
																<img alt="Pic" src="assets/media/users/300_12.jpg" />
															</div>

														</div>
														<div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
															{{$rowData->body}}
															<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">{{$rowData->created_at}}</span>
														</div>
													</div>

													<?php
												}else{	
													$imgDoc = asset('/local/public/upload/') . "/" . $rowData->attachment;
													if ($rowData->attachment_type == 1) {
														?>
														<div class="d-flex mb-2 align-items-start">
															<div class="d-flex align-items-center">
																<div class="symbol symbol-circle symbol-40 mr-4">
																	<img alt="Pic" src="assets/media/users/300_12.jpg" />
																</div>

															</div>
															<div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
																<a href="{{$imgDoc}}" target="_blank" title="" class=" text-dark-50 font-weight-bold font-size-lg">
																	<img src="{{$imgDoc}}" class="w-100 rounded" alt=""> {{$rowData->body}}
																</a>
																<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">10 seconds</span>
															</div>
														</div>

														<?php
													}
													if ($rowData->attachment_type == 2) {
														?>
														<div class="d-flex mb-2 align-items-start">
															<div class="d-flex align-items-center">
																<div class="symbol symbol-circle symbol-40 mr-4">
																	<img alt="Pic" src="assets/media/users/300_12.jpg" />
																</div>

															</div>
															<div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
																<a href="{{$imgDoc}}" target="_blank" title="" class=" text-dark-50 font-weight-bold font-size-lg">
																	<img src="{{$imgDoc}}" class="w-100 rounded mb-1" style="max-height: 200px; object-fit: cover; object-position: center;" alt=""> {{$rowData->body}}
																</a>
																<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">10 seconds</span>
															</div>
														</div>

														<?php
													}
													if ($rowData->attachment_type == 3) {
														?>
														<div class="d-flex mb-2 align-items-start">
															<div class="d-flex align-items-center">
																<div class="symbol symbol-circle symbol-40 mr-4">
																	<img alt="Pic" src="assets/media/users/300_12.jpg" />
																</div>

															</div>
															<div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
																<a href="{{$imgDoc}}" target="_blank" title="">
																	<div src="{{$imgDoc}}" class="w-100 rounded" alt=""> {{$rowData->body}}
																	</a>
																	<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">10 seconds</span>
																</div>
															</div>

															<?php
														}

													}

												} else {
													if (empty($rowData->attachment)) {
														?>
														<div class="d-flex mb-2 align-items-start justify-content-start flex-row-reverse">
															<div class="d-flex align-items-center">

																<div class="symbol symbol-circle symbol-40 ml-4">
																	<img alt="Pic" src="assets/media/users/300_21.jpg" />
																</div>
															</div>
															<div class="msg_time_mn my_msg_time_mn rounded mr-5 px-4 py-2 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-300px position-relative">
																{{$rowData->body}}
																<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">{{$rowData->created_at}}</span>
															</div>
														</div>

														<?php
													} else {
														$imgDoc = asset('/local/public/upload/') . "/" . $rowData->attachment;

														if ($rowData->attachment_type == 1) {
															?>
															<div class="d-flex mb-2 align-items-start">
																<div class="d-flex align-items-center">
																	<div class="symbol symbol-circle symbol-40 mr-4">
																		<img alt="Pic" src="assets/media/users/300_12.jpg" />
																	</div>

																</div>
																<div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
																	<a href="{{$imgDoc}}" target="_blank" title="" class=" text-dark-50 font-weight-bold font-size-lg">
																		<img src="{{$imgDoc}}" class="w-100 rounded mb-1" style="max-height: 200px; object-fit: cover; object-position: center;" alt=""> {{$rowData->body}}
																	</a>
																	<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">10 seconds</span>
																</div>
															</div>

															<?php
														}
														if ($rowData->attachment_type == 2) {
															?>
															<div class="d-flex mb-2 align-items-start">
																<div class="d-flex align-items-center">
																	<div class="symbol symbol-circle symbol-40 mr-4">
																		<img alt="Pic" src="assets/media/users/300_12.jpg" />
																	</div>

																</div>
																<div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
																	<a href="{{$imgDoc}}" target="_blank" title="" class=" text-dark-50 font-weight-bold font-size-lg">
																		<img src="{{$imgDoc}}" class="w-100 rounded mb-1" style="max-height: 200px; object-fit: cover; object-position: center;" alt=""> {{$rowData->body}}
																	</a>
																	<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">10 seconds</span>
																</div>
															</div>

															<?php
														}
														if ($rowData->attachment_type == 3) {
															?>
															<div class="d-flex mb-2 align-items-start">
																<div class="d-flex align-items-center">
																	<div class="symbol symbol-circle symbol-40 mr-4">
																		<img alt="Pic" src="assets/media/users/300_12.jpg" />
																	</div>

																</div>
																<div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative img_text_mn">
																	<a href="{{$imgDoc}}" target="_blank" title="" class=" text-dark-50 font-weight-bold font-size-lg">
																		<img src="{{$imgDoc}}" class="w-100 rounded mb-1" style="max-height: 200px; object-fit: cover; object-position: center;" alt=""> {{$rowData->body}}
																	</a>
																	<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">10 seconds</span>
																</div>
															</div>


															<?php
														}
													}
												}
												?>





												<?php
											}

											?>


											
											<div class="d-flex mb-2 align-items-start new_doc">
												<div class="d-flex align-items-center">
													<div class="symbol symbol-circle symbol-40 mr-4">
														<img alt="Pic" src="assets/media/users/300_12.jpg" />
													</div>
												</div>
												<div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
													<a href="" target="_blank" title="" style="width: 300px;" class="d-flex align-items-center justify-content-between bg-white p-4 rounded">
														<p class="mb-0">abcd.docx</p>
														<i class="icon-xl fas fa-arrow-alt-circle-down ml-5 text-primary"></i>
													</a>
													<div class="d-flex align-items-center mt-1">
														<p class="mb-0 mr-2">.docx</p> - 
														<p class="mb-0 ml-2">124kb</p>
													</div>
													<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">10 seconds</span>
												</div>
											</div>
										</div>
										<!--end::Messages-->
									</div>
									<!--end::Scroll-->
								</div>
								<!--end::Body-->
								<!--begin::Footer-->
								<div class="card-footer align-items-center">
									<!--begin::Compose-->
									<textarea class="form-control border-0 p-0 chat_boxFooter" rows="1" placeholder="Type a message"></textarea>
									<div class="d-flex align-items-center justify-content-between mt-5">
										<div class="mr-3">
											<a href="#" class="btn btn-clean btn-icon btn-md mr-1" data-toggle="modal" data-target="#img_file_select">
												<i class="icon-xl fas fa-image"></i>
											</a>

											<a href="#" class="btn btn-clean btn-icon btn-md" data-toggle="modal" data-target="#img_file_select">
												<i class="icon-xl fas fa-video"></i>
											</a>
											<a href="#" class="btn btn-clean btn-icon btn-md" data-toggle="modal" data-target="#img_file_select">
												<i class="icon-xl fas fa-file-alt"></i>
											</a>
										</div>
										<div>
											<button type="button" class="btn btn-primary btn-md text-uppercase font-weight-bold chat-send py-2 px-6 btnSendchatMessage" id="btnSendchatMessage">Send</button>
										</div>
									</div>
									<!--begin::Compose-->
								</div>

								<div class="modal fade" id="img_file_select" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" aria-hidden="true">
									<div class="modal-dialog modal-dialog modal-lg" role="document">
										<form id="formChatSuperAdmin" action="ajaxupload.php" method="post" enctype="multipart/form-data">

											<div class="modal-content bg-light">
												<div class="modal-header border-0 bg-primary">
													<h5 class="modal-title text-center w-100 text-white" id="exampleModalLabel">Preview</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<i aria-hidden="true" class="ki ki-close text-white"></i>
													</button>
												</div>
												<div class="modal-body text-center border-0">
													<div class="image-input image-input-outline" id="kt_image_3">
														<div class="image-input-wrapper" style="background-image: url({{noImage()}}); width: 200px; height: 200px;"></div>

														<label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
															<i class="fa fa-pen icon-sm text-muted"></i>
															<input type="file" name="file" accept=".png, .jpg, .jpeg" />
															<input type="hidden" name="profile_avatar_remove" />
														</label>



														<input type="hidden" name="to_id_model" id="to_id_model" value="1">
														<input type="hidden" name="attachment_type" value="1">
														<input name="_token" type="hidden" value="{{csrf_token()}}">

														<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar" style="left: -10px; top: -10px;">
															<i class="ki ki-bold-close icon-xs text-muted"></i>
														</span>
													</div>
													<div class="text_send_btn d-flex align-items-center justify-content-between mt-5 pt-4 px-3">
														<input type="text" class="form-control" placeholder="Add a caption..." name="message">
														<button type="submit" class="btn btn-primary font-weight-bold ml-2">Send</button>

													</div>
												</div>

											</form>
										</div>
									</div>


									<!--end::Footer-->
								</div>
								<!--end::Card-->
							</div>
							<!--end::Content-->
						</div>
						<!--end::Chat-->
					</div>
					<!--end::Container-->
				</div>
				<!--end::Entry-->
			</div>
			<!--end::Content-->
			<!--end::Chat-->
		</div>
		</div>
<!--end::Container-->