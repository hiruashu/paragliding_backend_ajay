<!--begin::Container-->
<div class="container">
	<div class="row">
		<div class="col-lg-12">
		<?php
    $id = Request::segment(2);

    $schoolCourse = DB::table('school_course')
        ->where('sid', $id)
        ->first();
    $schoolArr = DB::table('schools')
        ->where('id', $schoolCourse->sid)
        ->first();


    ?>

			<!--begin::Card-->
			<div class="card card-custom">
				<div class="card-header">
					<div class="card-title">
						<span class="card-icon">
						<i class="icon-xl la la-school"></i>
						</span>
						<h3 class="card-label">Course Rating and Comment</h3> List
					</div>
					<div class="card-toolbar">

						<!--begin::Button-->
						<!-- <a href="{{route('addStaticContent')}}" class="btn btn-primary font-weight-bolder">
											<i class="la la-plus"></i>New Content</a> -->
						<!--end::Button-->
					</div>
				</div>
				<div class="card-body">
					<!--begin: Search Form-->
					<div class="radio-inline">
					<label class="radio">
							<input checked type="radio" value="6" name="starRadioCommentMyCourse">
							<span></span>ALL <i class="icon-xl la la-star text-warning"></i></label>
							

						<label class="radio">
							<input type="radio" value="1" name="starRadioCommentMyCourse">
							<span></span>1 <i class="icon-xl la la-star text-warning"></i></label>
						<label class="radio">
							<input type="radio" value="2" name="starRadioCommentMyCourse">
							<span></span>2 <i class="icon-xl la la-star text-warning"></i></label>
						<label class="radio">
							<input type="radio" value="3" name="starRadioCommentMyCourse">
							<span></span>3 <i class="icon-xl la la-star text-warning"></i></label>
						<label class="radio">
							<input type="radio" value="4" name="starRadioCommentMyCourse">
							<span></span>4 <i class="icon-xl la la-star text-warning"></i></label>
						<label class="radio">
							<input type="radio" value="5" name="starRadioCommentMyCourse">
							<span></span>5 <i class="icon-xl la la-star text-warning"></i></label>
					</div>

 					<input type="hidden" id="txtSID" value="{{$id}}">
					<!--begin: Datatable-->
					<table class="table table-bordered table-hover table-checkable" id="kt_datatable_schoolRatingCommentMyCourse" style="margin-top: 13px !important">
						<thead>
							<tr>
								<th>Record ID</th>
								<th>S#</th>								
								<th>User</th>
								<th>Rating</th>
								<th>Comments</th>
                                <th>Posted On</th>
								<th>Actions</th>
							</tr>
						</thead>
						


					</table>
					<!--end: Datatable-->
				</div>
			</div>
			<!--end::Card-->

		</div>
	</div>
</div>
<!--end::Container-->