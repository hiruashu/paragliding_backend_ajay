<!--begin::Container-->
<div class="container">
    <div class="row">
        <div class="col-lg-12">


            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Enrolled Student to Course</h3>

                </div>
                <!--begin::Form-->
                <form data-redirect="school-list" class="form" method="post" action="444" id="ayra_kt_form_add_school">
                    <input type="hidden" name="txtAction" value="_add">

                    <div class="card-body">
                        <div class="form-group row">
                        <div class="col-lg-3">
                                <label>Course:</label>
                                <select class="form-control form-control-m form-control-solid country" id="kt_select2_1" name="country">
                                    <?php
                                    $school_courseArr = DB::table('school_course')
                                        ->where('sid',Auth::user()->sid)
                                        ->get();
                                    foreach ($school_courseArr as $key => $rowData) {

                                    ?>
                                        <option value="{{$rowData->id}}">{{$rowData->certificate_title}}</option>
                                    <?php

                                    }

                                    ?>

                                </select>


                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label>Student:</label>
                                <select class="form-control form-control-m form-control-solid country" id="kt_select2_2_1" name="country">
                                    <?php
                                    $school_courseArr = DB::table('school_course')
                                        ->where('sid',Auth::user()->sid)
                                        ->get();
                                    foreach ($school_courseArr as $key => $rowData) {

                                    ?>
                                        <option value="{{$rowData->id}}">{{$rowData->certificate_title}}</option>
                                    <?php

                                    }

                                    ?>

                                </select>


                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label>Payment Type </label>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option value="1">Full</option>
                                        <option value="2">Partially</option>
                                    </select>
                                   
                                </div>

                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label> Couser Amount</label>
                                <div class="input-group">

                                    <input type="text" readonly id="txtPhoneCode" class="form-control form-control-m form-control-solid" placeholder="Phone" />
                                </div>

                                <span class="form-text text-muted"></span>
                            </div>

                         

                        </div>
                        <div class="form-group row">

                           <div class="col-lg-3">
                                <label> Payment Amount</label>
                                <div class="input-group">

                                    <input type="text"  id="txtPhoneCode" class="form-control form-control-m form-control-solid" placeholder="Phone" />
                                </div>

                                <span class="form-text text-muted"></span>
                            </div>
                            
                            <div class="col-lg-3">
                                <label> Remarks</label>
                                <div class="input-group">

                                    <input size="30px" width="100px" name="phone" type="text" id="" class="form-control" placeholder="Phone" />

                                </div>
                                <span class="form-text text-muted"></span>
                            </div>








                        </div>



                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-8">
                                <button type="submit" id="btnSaveSchool" data-wizard-action="submit" class="btn btn-primary mr-2">Save</button>
                                <button type="reset" class="btn btn-secondary">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Card-->

        </div>
    </div>
</div>
<!--end::Container-->