<div class="container">
    <div class="row">
        <div class="col-lg-12">

            @if (\Session::has('success'))
            <div class="alert alert-custom alert-notice alert-light-success fade show mb-1" role="alert">
                <div class="alert-icon">
                    <i class="flaticon-warning"></i>
                </div>
                <div class="alert-text">{!! \Session::get('success') !!}</div>
                <div class="alert-close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">
                            <i class="ki ki-close"></i>
                        </span>
                    </button>
                </div>
            </div>



            @endif
            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Edit Course</h3>
                    <div class="card-toolbar">

                    </div>
                </div>

                <!--begin::Form-->
                <form class="form" action="{{route('edit_school_course')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="txtSID" value="{{$data->sid}}">
                    <input type="hidden" name="txtID" value="{{$data->id}}">
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label>Title:</label>
                                <input required type="text" class="form-control" value="{{$data->certificate_title}}" name="certificate_title" placeholder="">
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-4">
                                <label>Registration No:</label>
                                <input required type="text" class="form-control" value="{{$data->regno}}" name="regno" placeholder="">
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-4">
                                <label>Description:</label>
                                <input required type="text" class="form-control" value="{{$data->course_info}}" name="course_info" placeholder="">
                                <span class="form-text text-muted"></span>
                            </div>

                        </div>
                        <div class="form-group row">

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>File Browser</label>
                                    <div></div>
                                    <div class="custom-file">
                                        <input type="file" name="file" class="custom-file-input" id="customFile" />
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label>Certified By</label>
                                    <div class="checkbox-inline">
                                        <label class="checkbox">
                                            <input type="checkbox" name="certified_by" />
                                            <span></span>Option 1</label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="certified_by" />
                                            <span></span>Option 2</label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="certified_by" />
                                            <span></span>Option 3</label>
                                            <label class="checkbox">
                                            <input type="checkbox" name="certified_by" />
                                            <span></span>Option 3</label>
                                            <label class="checkbox">
                                            <input type="checkbox" name="certified_by" />
                                            <span></span>Option 3</label>
                                            <label class="checkbox">
                                            <input type="checkbox" name="certified_by" />
                                            <span></span>Option 3</label>
                                            <label class="checkbox">
                                            <input type="checkbox" name="certified_by" />
                                            <span></span>Option 3</label>
                                            <label class="checkbox">
                                            <input type="checkbox" name="certified_by" />
                                            <span></span>Option 3</label>
                                            <label class="checkbox">
                                            <input type="checkbox" name="certified_by" />
                                            <span></span>Option 3</label>

                                            
                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label>Duration:</label>
                                <input required type="text" name="duration" value="{{$data->duration}}" class="form-control" placeholder="">
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-4">
                                <label>Course Price :</label>
                                <input required type="text" name="course_amt" value="{{$data->course_amt}}" class="form-control" placeholder="">
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-4">
                                <label>Down payment %:</label>
                                <input required type="text" name="down_payment_per" value="{{$data->down_payment_per}}" class="form-control" placeholder="">
                                <span class="form-text text-muted"></span>
                            </div>


                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label>Country:</label>
                                <select class="form-control form-control-m form-control-solid countrySchool" id="kt_select2_1" name="country">
                                    <?php
                                    $countriesArr = DB::table('countries')
                                        ->select('id', 'name')
                                        ->get();
                                    foreach ($countriesArr as $key => $rowData) {

                                        if ($data->country_id == $rowData->id) {
                                    ?>
                                            <option selected value="{{$rowData->id}}">{{$rowData->name}}</option>
                                        <?php
                                        } else {
                                        ?>
                                            <option value="{{$rowData->id}}">{{$rowData->name}}</option>
                                    <?php
                                        }
                                    }

                                    ?>

                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label>City :</label>
                                <select class="form-control form-control-m form-control-solid schoolCity" id="kt_select2_2" name="city">
                                    <?php
                                    $countriesArr = DB::table('cities')
                                        ->select('id', 'name')
                                        ->get();
                                    foreach ($countriesArr as $key => $rowData) {

                                        if ($data->city_id == $rowData->id) {
                                    ?>
                                            <option selected value="{{$rowData->id}}">{{$rowData->name}}</option>
                                    <?php
                                        }
                                    }

                                    ?>

                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-6">
                                <label>Status :</label>
                                <select class="form-control form-control-m form-control-solid" name="is_active">
                                    <option value="1">Active</option>
                                    <option value="1">Active</option>
                                </select>
                            </div>




                        </div>

                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-8">
                                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                <button type="reset" class="btn btn-secondary">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Card-->

        </div>
    </div>
</div>