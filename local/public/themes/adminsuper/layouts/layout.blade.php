<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
  <base href="">
  <meta charset="utf-8" />
  <title>@get('title')</title>
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta name="BASE_URL" content="{{ url('/') }}" />
  <meta name="description" content="" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <link rel="canonical" href="" />
  <!--begin::Fonts-->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
  <!--end::Fonts-->
  <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
  <!--begin::Page Vendors Styles(used by this page)-->
  <link href="{{getBaseURL()}}/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
  <!--end::Page Vendors Styles-->
  <!--begin::Global Theme Styles(used by all pages)-->
  <link href="{{getBaseURL()}}/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
  <link href="{{getBaseURL()}}/assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
  <link href="{{getBaseURL()}}/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
  <!--end::Global Theme Styles-->
  <!--begin::Layout Themes(used by all pages)-->
  <link href="{{getBaseURL()}}/assets/css/themes/layout/header/base/light.css" rel="stylesheet" type="text/css" />
  <link href="{{getBaseURL()}}/assets/css/themes/layout/header/menu/light.css" rel="stylesheet" type="text/css" />
  <link href="{{getBaseURL()}}/assets/css/themes/layout/brand/dark.css" rel="stylesheet" type="text/css" />
  <link href="{{getBaseURL()}}/assets/css/themes/layout/aside/dark.css" rel="stylesheet" type="text/css" />
  <link href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" rel="stylesheet" type="text/css" />
  <!--end::Layout Themes-->
  <link rel="shortcut icon" href="{{applogo()}}" />

</head>
<!--end::Head-->
<!--begin::Body-->
<style>
  .header-fixed.subheader-fixed.subheader-enabled .wrapper {
    padding-top: 50px;
}
</style>

<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
  @partial('header')
  @partial('left_side')
  @partial('top_toolbar')
  @content()
  @partial('footer')
  <div class="modal fade" id="exampleModalSizeSm" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">School Rejection</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i aria-hidden="true" class="ki ki-close"></i>
          </button>
        </div>
        <div class="modal-body">
          <input type="hidden" name="txtSID" id="txtSID">

          <div class="form-group mb-1">
            <label for="exampleTextarea">Rejection Notes
              <span class="text-danger">*</span></label>
            <textarea class="form-control" id="txtRejectionNote" rows="3"></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
          <button type="button" id="btnRejectionSchool" class="btn btn-primary font-weight-bold">Submit</button>
        </div>
      </div>
    </div>
  </div>
  <!--end::Modal-->

  <!--begin::Global Theme Bundle(used by all pages)-->
  <script src="{{getBaseURL()}}/assets/plugins/global/plugins.bundle.js"></script>
  <script src="{{getBaseURL()}}/assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
  <script src="{{getBaseURL()}}/assets/js/scripts.bundle.js"></script>
  <!--end::Global Theme Bundle-->
  <script src="{{getBaseURL()}}/assets/js/pages/widgets.js"></script>
  <script src="{{getBaseURL()}}/assets/js/pages/crud/forms/widgets/select2.js?v=7.2.7"></script>
  <script type="text/javascript">
    BASE_URL = $('meta[name="BASE_URL"]').attr('content');
    CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    UID_ID = '<?php echo Auth::user()->id ?>';
    //	location.reload(1);
  </script>
  <script src="{{getBaseURL()}}/assets/plugins/custom/ckeditor/ckeditor-document.bundle.js?v=7.2.7"></script>
  <script src="{{getBaseURL()}}/assets/js/pages/crud/forms/widgets/form-repeater.js?v=7.2.7"></script>
  <script src="{{getBaseURL()}}/assets/js/pages/features/miscellaneous/sweetalert2.js?v=7.2.7"></script>
  <script src="{{getBaseURL()}}/assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.7"></script>
  <script src="{{getBaseURL()}}/assets/js/pages/custom/chat/chat.js?v=7.2.8"></script>
  <script src="{{getBaseURL()}}/corex/js/custom_chat.js"></script>




  <script src="{{getBaseURL()}}/corex/js/admin_datatable_ajax.js?v=7.2.7"></script>

  <script src="{{getBaseURL()}}/corex/js/superadmin.js"></script>
  <script src="{{ asset('local/public/Highcharts820/code/highcharts.js')}}"></script>
  <script src="{{ asset('local/public/themes/adminsuper/assets/js/high_chart.js')}}" type="text/javascript"></script>
  <!-- <script src="https://js.pusher.com/7.0/pusher.min.js"></script> -->


  <!--end::Page Scripts-->

  <script type="text/javascript">
    // Enable pusher logging - don't include this in production
    // Pusher.logToConsole = true;

    // // Add API Key & cluster here to make the connection 
    // var pusher = new Pusher('4c1630c06a480f5f76de', {
    //   cluster: 'ap2',
    //   encrypted: true
    // });

    // // Enter a unique channel you wish your users to be subscribed in.
    // var channel = pusher.subscribe('test_channel');

    // var imEventID = 'zelosUser_' + UID_ID;

    // // bind the server event to get the response data and append it to the message div
    // channel.bind(imEventID,
    //   function(data) {
    //     //console.log(data);
       

    // // check if the user is subscribed to the above channel
    // channel.bind('pusher:subscription_succeeded', function(members) {
    //   console.log('successfully subscribed!');
    // });

    // Send AJAX request to the PHP file on server 
    function ajaxCall(ajax_url, ajax_data) {
      $.ajax({
        type: "GET",
        url: ajax_url,
        //dataType: "json",
        data: ajax_data,
        success: function(response) {
          console.log(response);
        },
        error: function(msg) {}
      });
    }

    // Trigger for the Enter key when clicked.
    $.fn.enterKey = function(fnc) {
      return this.each(function() {
        $(this).keypress(function(ev) {
          var keycode = (ev.keyCode ? ev.keyCode : ev.which);
          if (keycode == '13') {
            fnc.call(this, ev);
          }
        });
      });
    }



    // Send the Message enter by User
    $('body').on('click', '.btnSendchatMessage', function(e) {

      e.preventDefault();

      var message = $('.chat_boxFooter').val();
      //alert(message)
      var to_id = $('#to_id').val()


      // Validate Name field
      if (to_id === '') {
        bootbox.alert('<br /><p class = "bg-danger">Please enter a Name.</p>');
      } else if (message !== '') {
        // Define ajax data
        var formData = {
          '_token': $('meta[name="csrf-token"]').attr('content'),
          'to_id': to_id,
          'message': message

        };

        //console.log(chat_message);

        var url = BASE_URL + '/chat_sendMessage';

        // Send the message to the server passing File Url and chat person name & message
        ajaxCall(url, formData);

        // Clear the message input field
        $('.chat_box .input_message').val('');
        $('.messages_display').append(`<div class="d-flex mb-5 align-items-start justify-content-start flex-row-reverse">
    <div class="d-flex align-items-center">
       
        <div class="symbol symbol-circle symbol-40 ml-4">
            <img alt="Pic" src="assets/media/users/300_21.jpg" />
        </div>
    </div>
    <div class="msg_time_mn my_msg_time_mn rounded mr-5 p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px position-relative">
    ${message} <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">just now</span></div>
</div>
`);
$('.chat_boxFooter').val("");

        // Show a loading image while sending
        // $('.input_send_holder').html('<input type = "submit" value = "Send" class = "btn btn-primary btn-block" disabled /> &nbsp;<img     src = "loading.gif" />');
      }
    });

    // Send the message when enter key is clicked
    $('.chat_boxFooter').enterKey(function(e) {
      e.preventDefault();
      $('.btnSendchatMessage').click();
    });



    $("#loginCredentialSend").change(function() {
    
      if ($(this).prop("checked") == true) {
        //run code
        statusAction = 1;
        Swal.fire({
          title: "Are you sure?",
          text: "You want to created or send credentials",
          icon: "warning",
          showCancelButton: true,
          confirmButtonText: "Yes, sent it!",
        }).then(function(result) {
          if (result.value) {
            // ajax ayra

            //ajax call
            var formData = {
              txtSID: $("input[name=txtSID]").val(),
              statusAction: statusAction,
              _token: $('meta[name="csrf-token"]').attr("content"),
            };

            $.ajax({
              url: BASE_URL + "/createOrSentSchoolAccount",
              type: "POST",
              data: formData,
              success: function(res) {},
            });

            //ajax call
            //ayra ajax
          }
        });
      } else {
        //run code
        statusAction = 2;
        Swal.fire({
          title: "Are you sure?",
          text: "You want to created or send credentials",
          icon: "warning",
          showCancelButton: true,
          confirmButtonText: "Yes, sent it!",
        }).then(function(result) {
          if (result.value) {
            // ajax ayra

            //ajax call
            var formData = {
              txtSID: $("input[name=txtSID]").val(),
              statusAction: 1,
              _token: $('meta[name="csrf-token"]').attr("content"),
            };

            $.ajax({
              url: BASE_URL + "/createOrSentSchoolAccount",
              type: "POST",
              data: formData,
              success: function(res) {
                if (res.status == 1) {
                  swal
                    .fire({
                      text: res.msg,
                      icon: "success",
                      buttonsStyling: false,
                      confirmButtonText: "Ok, got it!",
                      customClass: {
                        confirmButton: "btn font-weight-bold btn-light-primary",
                      },
                    })
                    .then(function() {
                      KTUtil.scrollTop();
                    });
                } else {
                  swal
                    .fire({
                      text: res.msg,
                      icon: "success",
                      buttonsStyling: false,
                      confirmButtonText: "Ok, got it!",
                      customClass: {
                        confirmButton: "btn font-weight-bold btn-light-primary",
                      },
                    })
                    .then(function() {
                      KTUtil.scrollTop();
                    });
                }
              },
            });

            //ajax call
            //ayra ajax
          }
        });
      }
    });
  </script>

  <script>


//txtCounrySchool
    var avatar3 = new KTImageInput('kt_image_3');

  $(document).ready(function (e) {
 $("#formChatSuperAdmin").on('submit',(function(e) {
  e.preventDefault();
  $.ajax({
  url: "uploadChatFile",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
   beforeSend : function()
   {
    //$("#preview").fadeOut();
    $("#err").fadeOut();
   },
   success: function(data)
      {
    if(data.status==0)
    {
     // invalid file format.
     $("#err").html("Invalid File !").fadeIn();
    }
    else
    {
      var userid=$('#to_id').val();

      showUserChatDetail(userid);

      $('#img_file_select').modal('toggle');
     // view uploaded file.
     $("#preview").html(data).fadeIn();
     $("#formChatSuperAdmin")[0].reset(); 
     
    }
      },
     error: function(e) 
      {
    $("#err").html(e).fadeIn();
      }          
    });
 }));




 $("select.country_filter").change(function(){
  var selectedCountry = $(this).children("option:selected").val();

  // location
  $( "#kt_datatable_schoolList" ).dataTable().fnDestroy();
    // begin first table
    var schoolListFrom = $("#schoolListFrom").val();

    var table = $("#kt_datatable_schoolList").DataTable({
      responsive: true,
      // Pagination settings
      dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
      // read more: https://datatables.net/examples/basic_init/dom.html

      lengthMenu: [5, 10, 25, 50],

      pageLength: 10,

      language: {
        lengthMenu: "Display _MENU_",
      },

      searchDelay: 500,
      processing: true,
      serverSide: true,
      ajax: {
        url: BASE_URL + "/getDatatableSchoolListFilter",
        type: "GET",
        data: {
          _token: $('meta[name="csrf-token"]').attr("content"),
          schoolListFrom: schoolListFrom,
          selectedCountry:selectedCountry,

          // parameters for custom backend script demo
          columnsDef: [
            "RecordID",
            "school_title",
            "IndexID",
            "rating",
            "city",
            "email",
            "status",
            "profile_status",
            "Actions",
          ],
        },
      },
      columns: [
        { data: "RecordID" },
        { data: "IndexID" },
        { data: "school_title" },
        { data: "rating" },
        { data: "city" },
        { data: "email" },
        { data: "status" },
        { data: "profile_status" },
        { data: "Actions", responsivePriority: -1 },
      ],

      initComplete: function () {
        this.api()
          .columns()
          .every(function () {
            var column = this;

            switch (column.title()) {
              case "Country":
                column
                  .data()
                  .unique()
                  .sort()
                  .each(function (d, j) {
                    $('.datatable-input[data-col-index="2"]').append(
                      '<option value="' + d + '">' + d + "</option>"
                    );
                  });
                break;

              case "Status":
                var status = {
                  1: { title: "Pending", class: "label-light-primary" },
                  2: { title: "Delivered", class: " label-light-danger" },
                  3: { title: "Canceled", class: " label-light-primary" },
                  4: { title: "Success", class: " label-light-success" },
                  5: { title: "Info", class: " label-light-info" },
                  6: { title: "Danger", class: " label-light-danger" },
                  7: { title: "Warning", class: " label-light-warning" },
                };
                column
                  .data()
                  .unique()
                  .sort()
                  .each(function (d, j) {
                    $('.datatable-input[data-col-index="6"]').append(
                      '<option value="' +
                        d +
                        '">' +
                        status[d].title +
                        "</option>"
                    );
                  });
                break;

              case "Type":
                var status = {
                  1: { title: "Online", state: "danger" },
                  2: { title: "Retail", state: "primary" },
                  3: { title: "Direct", state: "success" },
                };
                column
                  .data()
                  .unique()
                  .sort()
                  .each(function (d, j) {
                    $('.datatable-input[data-col-index="7"]').append(
                      '<option value="' +
                        d +
                        '">' +
                        status[d].title +
                        "</option>"
                    );
                  });
                break;
            }
          });
      },

      columnDefs: [
        {
          targets: [0],
          visible: !1,
        },
        {
          targets: -1,

          title: "Actions",
          orderable: false,
          render: function (data, type, full, meta) {
            var EDIT_URL = BASE_URL + "/edit-school/" + full.RecordID;
            var VIEW_URL = BASE_URL + "/view-school/" + full.RecordID;

            return `<a href="${VIEW_URL}" class="btn btn-sm btn-clean btn-icon" title="View Details">\
        <i class="la la-eye"></i>
      </a>						  
    <a href="${EDIT_URL}" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
      <i class="la la-edit"></i>
    </a>
    <a href="javascript::void(0)" onclick="deleteSchool(${full.RecordID})"  class="btn btn-sm btn-clean btn-icon" title="Delete">\
      <i class="la la-trash"></i>
    </a>
    <a class="btn btn-sm btn-icon btn-bg-light btn-icon-warning btn-hover-warning" href="#" data-toggle="modal" data-target="#kt_chat_modal">
       <i class="flaticon2-chat-1"></i>
   </a>
      `;
          },
        },
        {
          targets: 3,
          width: 175,
          render: function (data, type, full, meta) {
            var strRating = "";
            switch (parseFloat(data)) {
              case 1:
                strRating = `
                                <i class="icon-xl la la-star text-warning" ></i>
                                <i class="icon-xl la la-star " ></i>                                
                                <i class="icon-xl la la-star " ></i>
                                <i class="icon-xl la la-star " ></i>
                                <i class="icon-xl la la-star" ></i>
                               
                               
                                `;
                break;
              case 1.5:
                strRating = `
                                    <i class="icon-xl la la-star text-warning" ></i>
                                    <i class="icon-xl la la-star-half-alt text-warning"></i>                             
                                    <i class="icon-xl la la-star " ></i>
                                    <i class="icon-xl la la-star " ></i>
                                    <i class="icon-xl la la-star" ></i>
                                   
                                   
                                    `;
                break;
              case 2:
                strRating = `
                                        <i class="icon-xl la la-star text-warning" ></i>
                                        <i class="icon-xl la la-star text-warning" ></i>                        
                                        <i class="icon-xl la la-star " ></i>
                                        <i class="icon-xl la la-star " ></i>
                                        <i class="icon-xl la la-star" ></i>
                                       
                                       
                                        `;
                break;
              case 2.5:
                strRating = `
                                            <i class="icon-xl la la-star text-warning" ></i>
                                            <i class="icon-xl la la-star text-warning" ></i>                        
                                            <i class="icon-xl la la-star-half-alt text-warning"></i>
                                            <i class="icon-xl la la-star " ></i>
                                            <i class="icon-xl la la-star" ></i>
                                           
                                           
                                            `;
                break;
              case 3:
                strRating = `
                                            <i class="icon-xl la la-star text-warning" ></i>
                                            <i class="icon-xl la la-star text-warning" ></i>                        
                                            <i class="icon-xl la la-star text-warning" ></i> 
                                            <i class="icon-xl la la-star " ></i>
                                            <i class="icon-xl la la-star" ></i>
                                           
                                           
                                            `;
                break;

              case 3.5:
                strRating = `
                                            <i class="icon-xl la la-star text-warning" ></i>
                                            <i class="icon-xl la la-star text-warning" ></i>                        
                                            <i class="icon-xl la la-star text-warning" ></i> 
                                            <i class="icon-xl la la-star-half-alt text-warning"></i>
                                            <i class="icon-xl la la-star" ></i>
                                           
                                           
                                            `;
                break;
              case 4:
                strRating = `
                                                <i class="icon-xl la la-star text-warning" ></i>
                                                <i class="icon-xl la la-star text-warning" ></i>                        
                                                <i class="icon-xl la la-star text-warning" ></i> 
                                                <i class="icon-xl la la-star text-warning" ></i> 
                                                <i class="icon-xl la la-star" ></i>
                                               
                                               
                                                `;
                break;
              case 4.5:
                strRating = `
                                    <i class="icon-xl la la-star text-warning" ></i>
                                    <i class="icon-xl la la-star text-warning" ></i>                                
                                    <i class="icon-xl la la-star text-warning" ></i>
                                    <i class="icon-xl la la-star text-warning" ></i>                                
                                    <i class="icon-xl la la-star-half-alt text-warning"></i>
                                   
                                    `;
                break;
              case 5:
                strRating = `
                                    <i class="icon-xl la la-star text-warning" ></i>
                                    <i class="icon-xl la la-star text-warning" ></i>                                
                                    <i class="icon-xl la la-star text-warning" ></i>
                                    <i class="icon-xl la la-star text-warning" ></i>                                
                                    <i class="icon-xl la la-star text-warning" ></i>        
                                   
                                    `;
                break;

              default:
                strRating = `NA`;
                break;
            }
            return "(" + data + "/5)<br>" + strRating;
          },
        },
        {
          targets: 6,
          width: 50,
          title: "Status",
          orderable: false,
          render: function (a, t, e, n) {
            var i = {
              1: {
                title: "Active",
                class: "primary",
              },
              2: {
                title: "Deactive",
                class: "danger",
              },
            };
            //return void 0 === i[a] ? a : '<span class="m-badge ' + i[a].class + ' m-badge--wide">' + i[a].title + "</span>"
            return (
              '<span class="font-weight-bold text-' +
              i[a].class +
              '">' +
              i[a].title +
              "</span>"
            );
          },
        },
        {
          targets: 7,
          width: 50,
          title: "Profile",
          orderable: false,
          render: function (a, t, e, n) {
            var i = {
              1: {
                title: "Completed",
                class: "primary",
              },
              2: {
                title: "Incomplete",
                class: "danger",
              },
            };
            //return void 0 === i[a] ? a : '<span class="m-badge ' + i[a].class + ' m-badge--wide">' + i[a].title + "</span>"
            return (
              '<span class="font-weight-bold text-' +
              i[a].class +
              '">' +
              i[a].title +
              "</span>"
            );
          },
        },
      ],
    });

    var filter = function () {
      var val = $.fn.dataTable.util.escapeRegex($(this).val());
      table
        .column($(this).data("col-index"))
        .search(val ? val : "", false, false)
        .draw();
    };

    var asdasd = function (value, index) {
      var val = $.fn.dataTable.util.escapeRegex(value);
      table.column(index).search(val ? val : "", false, true);
    };

    $("#kt_search").on("click", function (e) {
      e.preventDefault();
      var params = {};
      $(".datatable-input").each(function () {
        var i = $(this).data("col-index");
        if (params[i]) {
          params[i] += "|" + $(this).val();
        } else {
          params[i] = $(this).val();
        }
      });
      $.each(params, function (i, val) {
        // apply search params to datatable
        table.column(i).search(val ? val : "", false, false);
      });
      table.table().draw();
    });

    $("#kt_reset").on("click", function (e) {
      e.preventDefault();
      $(".datatable-input").each(function () {
        $(this).val("");
        table.column($(this).data("col-index")).search("", false, false);
      });
      table.table().draw();
    });

    $("#kt_datepicker").datepicker({
      todayHighlight: true,
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>',
      },
    });
  
  // location
  
 });
 
});


//javascrip 


$( '.txtCounrySchool' ).on( 'change', function ()
{
  var cid = $( this ).find( ":selected" ).val();
 
  // ajax
  var formData = {
    'country_id': cid,
    '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' )
  };
  $.ajax( {
    url: BASE_URL + '/getSchoolByCountryID',
    type: 'GET',
    data: formData,
    success: function ( res )
    {
      $('#myschool').html(res);      

    }
  } );
  // ajax

} );
// school select enroll
$("select.myschoolEn").change(function(){
  var selectedSchool = $(this).children("option:selected").val();
    //ajax
    $.ajax({
      url: BASE_URL + "/getSchoolCourseEn",
      type: "GET",
      data: {
        _token: $('meta[name="csrf-token"]').attr("content"),
        selectedSchool: selectedSchool,
        action: 1,
       

      },
      success: function (resp) {
      

          $("select.myschoolCourseEn")
    .empty()
    .append(resp);

         
      },
    });
    //ajax


});



$( '.txtCountryPie' ).on( 'change', function ()
{
  var cid = $( this ).find( ":selected" ).val();
  //$('#txtCountryPie').html('');
  // ajax
  var formData = {
    'country_id': cid,
    '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' )
  };
  $.ajax( {
    url: BASE_URL + '/getSchoolByCountryID',
    type: 'GET',
    data: formData,
    success: function ( res )
    {
      $('#txtSchoolPie').html(res);
      windowLoadAjax.initD();

    }
  } );
  // ajax

} );
//javascrip 

$("#userActiveAction").change(function () {
    if ($(this).prop("checked") == true) {
      //run code
      statusAction = 1;
      Swal.fire({
        title: "Are you sure?",
        text: "You want to active Account",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, active it!",
      }).then(function (result) {
        if (result.value) {
          // ajax ayra

          //ajax call
          var formData = {
            txtSID: $("input[name=txtSID]").val(),
            statusAction: statusAction,
            _token: $('meta[name="csrf-token"]').attr("content"),
          };

          $.ajax({
            url: BASE_URL + "/useractionSchoolAccount",
            type: "POST",
            data: formData,
            success: function (res) {
              console.log(res);
              if (parseInt(res.status) == 1) {
                swal
                  .fire({
                    text: res.msg,
                    icon: "success",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                      confirmButton: "btn font-weight-bold btn-light-primary",
                    },
                  })
                  .then(function () {
                    KTUtil.scrollTop();
                  });
              } 
              if (res.status == 2) {
                swal
                  .fire({
                    text: res.msg,
                    icon: "error",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                      confirmButton: "btn font-weight-bold btn-light-primary",
                    },
                  })
                  .then(function () {
                    location.reload(1);
                    KTUtil.scrollTop();
                  });
              } 
            },
            dataType: "json",
          });

          //ajax call
          //ayra ajax
        }
      });
    } else {
      //run code
      statusAction = 2;
      Swal.fire({
        title: "Are you sure?",
        text: "You want to De-Active Account",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, deactive it!",
      }).then(function (result) {
        if (result.value) {
          // ajax ayra

          //ajax call
          var formData = {
            txtSID: $("input[name=txtSID]").val(),
            statusAction: statusAction,
            _token: $('meta[name="csrf-token"]').attr("content"),
          };

          $.ajax({
            url: BASE_URL + "/useractionSchoolAccount",
            type: "POST",
            data: formData,
            success: function (res) {
              console.log(res);
              if (parseInt(res.status) == 1) {
                swal
                  .fire({
                    text: res.msg,
                    icon: "success",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                      confirmButton: "btn font-weight-bold btn-light-primary",
                    },
                  })
                  .then(function () {
                    KTUtil.scrollTop();
                  });
              } 
              if (res.status == 2) {
                swal
                  .fire({
                    text: res.msg,
                    icon: "error",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                      confirmButton: "btn font-weight-bold btn-light-primary",
                    },
                  })
                  .then(function () {
                    KTUtil.scrollTop();
                  });
              } 
            },
            dataType: "json",
          });

          //ajax call
          //ayra ajax
        }
      });
    }
  });

  $("#kt_inputmask_6").inputmask('decimal', {
            rightAlignNumerics: false
        }); 


         //userActiveActionUser
  $("#userActiveActionUser").change(function () {
    if ($(this).prop("checked") == true) {
      //run code
      statusAction = 1;
      Swal.fire({
        title: "Are you sure?",
        text: "You want to active Account",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, active it!",
      }).then(function (result) {
        if (result.value) {
          // ajax ayra

          //ajax call
          var formData = {
            txtSID: $("input[name=txtSID]").val(),
            statusAction: statusAction,
            _token: $('meta[name="csrf-token"]').attr("content"),
          };

          $.ajax({
            url: BASE_URL + "/useractionUserIsActive",
            type: "POST",
            data: formData,
            success: function (res) {
              if (res.status == 1) {
                swal
                  .fire({
                    text: res.msg,
                    icon: "success",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                      confirmButton: "btn font-weight-bold btn-light-primary",
                    },
                  })
                  .then(function () {
                    KTUtil.scrollTop();
                    location.reload(1);
                  });
              }
            },
          });

          //ajax call
          //ayra ajax
        }
      });
    } else {
      //run code
      statusAction = 2;
      Swal.fire({
        title: "Are you sure?",
        text: "You want to De-Active Account",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, deactive it!",
      }).then(function (result) {
        if (result.value) {
          // ajax ayra

          //ajax call
          var formData = {
            txtSID: $("input[name=txtSID]").val(),
            statusAction: statusAction,
            _token: $('meta[name="csrf-token"]').attr("content"),
          };

          $.ajax({
            url: BASE_URL + "/useractionUserIsActive",
            type: "POST",
            data: formData,
            success: function (res) {
              if (res.status == 1) {
                swal
                  .fire({
                    text: res.msg,
                    icon: "success",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                      confirmButton: "btn font-weight-bold btn-light-primary",
                    },
                  })
                  .then(function () {
                    KTUtil.scrollTop();
                    location.reload(1)
                  });
              } else {
                swal
                  .fire({
                    text: res.msg,
                    icon: "success",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                      confirmButton: "btn font-weight-bold btn-light-primary",
                    },
                  })
                  .then(function () {
                    KTUtil.scrollTop();
                    location.reload(1)
                  });
              }
            },
          });

          //ajax call
          //ayra ajax
        }
      });
    }
  });

  //userActiveActionUser
  
  </script>


</body>
<!--end::Body-->

</html>