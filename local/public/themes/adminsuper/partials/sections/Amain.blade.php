<div class="container">
    <div class="row">
        <?php
        $schoolCount = DB::table('schools')
            ->where('is_deleted', 0)
            ->where('added_from_status', 1)
            ->count();

        $userCount = DB::table('users')
            ->where('is_deleted', 0)
            ->where('user_type', 3)
            ->count();

        $schoolRCount = DB::table('schools')
            ->where('is_deleted', 0)
            ->where('added_from_status', 2)
            ->count()


        ?>
        <div class="col-xl-6">

            <div class="row">
                <div class="col-xl-6">
                    <!--begin::Tiles Widget 11-->
                    <div class="card card-custom bg-primary gutter-b" style="height: 150px">
                        <div class="card-body">
                            <span class="svg-icon svg-icon-3x svg-icon-white ml-n2">
                                <!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5" />
                                        <path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            <div class="text-inverse-primary font-weight-bolder font-size-h2 mt-3">{{$schoolCount}}</div>
                            <a href="{{route('schoolList')}}" class="text-inverse-primary font-weight-bold font-size-lg mt-1">Schools</a>
                        </div>
                    </div>
                    <!--end::Tiles Widget 11-->
                </div>
                <div class="col-xl-6">
                    <!--begin::Tiles Widget 12-->
                    <div class="card card-custom gutter-b" style="height: 150px">
                        <div class="card-body">
                            <span class="svg-icon svg-icon-3x svg-icon-success">
                                <!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Communication/Group.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24" />
                                        <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                        <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            <div class="text-dark font-weight-bolder font-size-h2 mt-3">{{$userCount}}</div>
                            <a href="{{route('userList')}}" class="text-muted text-hover-primary font-weight-bold font-size-lg mt-1">Students</a>
                        </div>
                    </div>
                    <!--end::Tiles Widget 12-->
                </div>
            </div>
        </div>
        <div class="col-xl-6">

            <div class="row">
                <div class="col-xl-6">
                    <!--begin::Tiles Widget 11-->
                    <div class="card card-custom bg-warning gutter-b" style="height: 150px">
                        <div class="card-body">
                            <span class="svg-icon svg-icon-3x svg-icon-white ml-n2">
                                <!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5" />
                                        <path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            <div class="text-inverse-primary font-weight-bolder font-size-h2 mt-3">XXX</div>
                            <a href="#" class="text-inverse-primary font-weight-bold font-size-lg mt-1">Payments</a>
                        </div>
                    </div>
                    <!--end::Tiles Widget 11-->
                </div>
                <div class="col-xl-6">
                    <!--begin::Tiles Widget 12-->
                    <div class="card card-custom gutter-b" style="height: 150px">
                        <div class="card-body">
                            <span class="svg-icon svg-icon-3x svg-icon-success">
                                <!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Communication/Group.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24" />
                                        <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                        <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            <div class="text-dark font-weight-bolder font-size-h2 mt-3">{{$schoolRCount}}</div>
                            <a href="{{route('SchoolRequestList')}}" class="text-muted text-hover-primary font-weight-bold font-size-lg mt-1">New Request</a>
                        </div>
                    </div>
                    <!--end::Tiles Widget 12-->
                </div>
            </div>
        </div>



    </div>
    <div class="row">

    <div class="col-md-12">
            <!--begin::Advance Table Widget 2-->
            <div class="card card-custom card-stretch gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">User's Graph</span>
                        <!-- <span class="text-muted mt-3 font-weight-bold font-size-sm">More than 400+ new members</span> -->
                    </h3>
                    <div class="card-toolbar">
                        <ul class="nav nav-pills nav-pills-sm nav-dark-75">
                            <li class="nav-item">
                                <a class="nav-link py-2 px-4 active" onclick="btnShowChartNewUser(1,7)" data-toggle="tab" href="#kt_tab_pane_11_1_1">Week</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link py-2 px-4" onclick="btnShowChartNewUser(2,30)" data-toggle="tab" href="#kt_tab_pane_11_2_2">Month</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link py-2 px-4 " onclick="btnShowChartNewUser(3,365)" data-toggle="tab" href="#kt_tab_pane_11_3_3">Year</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-2 pb-0 mt-n3">
                    <div class="tab-content mt-5" id="myTabTables11">
                        <!--begin::Tap pane-->
                        <div class="tab-pane fade active show" id="kt_tab_pane_11_1_1" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                            <!--begin::Table-->

                            <figure class="highcharts-figure">
                                <div id="containerUserDataView_1"></div>
                                <p class="highcharts-description">
                                    <!-- This above chart show total Orders monthly -->
                                </p>
                            </figure>

                            <!--end::Table-->
                        </div>
                        <div class="tab-pane fade active show" id="kt_tab_pane_11_2_2" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                            <!--begin::Table-->

                            <figure class="highcharts-figure">
                                <div id="containerUserDataView_2"></div>
                                <p class="highcharts-description">
                                    <!-- This above chart show total Orders monthly -->
                                </p>
                            </figure>

                            <!--end::Table-->
                        </div>
                        <div class="tab-pane fade active show" id="kt_tab_pane_11_3_3" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                            <!--begin::Table-->

                            <figure class="highcharts-figure">
                                <div id="containerUserDataView_3"></div>
                                <p class="highcharts-description">
                                    <!-- This above chart show total Orders monthly -->
                                </p>
                            </figure>

                            <!--end::Table-->
                        </div>
                        <!--end::Tap pane-->

                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Advance Table Widget 2-->
        </div>


        <div class="col-md-12">
            <!--begin::Advance Table Widget 2-->
            <div class="card card-custom card-stretch gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">School's Graph</span>
                        <!-- <span class="text-muted mt-3 font-weight-bold font-size-sm">More than 400+ new members</span> -->
                    </h3>
                    <div class="card-toolbar">
                        <ul class="nav nav-pills nav-pills-sm nav-dark-75">
                            <li class="nav-item">
                                <a class="nav-link py-2 px-4 active" onclick="btnShowChartNewSchool(1,7)" data-toggle="tab" href="#kt_tab_pane_11_1_1_A">Week</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link py-2 px-4" onclick="btnShowChartNewSchool(2,30)" data-toggle="tab" href="#kt_tab_pane_11_2_2_A">Month</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link py-2 px-4 " onclick="btnShowChartNewSchool(3,365)" data-toggle="tab" href="#kt_tab_pane_11_3_3_A">Year</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-2 pb-0 mt-n3">
                    <div class="tab-content mt-5" id="myTabTables11">
                        <!--begin::Tap pane-->
                        <div class="tab-pane fade active show" id="kt_tab_pane_11_1_1_A" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                            <!--begin::Table-->

                            <figure class="highcharts-figure">
                                <div id="containerSchoolDataView_1"></div>
                                <p class="highcharts-description">
                                    <!-- This above chart show total Orders monthly -->
                                </p>
                            </figure>

                            <!--end::Table-->
                        </div>
                        <div class="tab-pane fade active show" id="kt_tab_pane_11_2_2_A" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                            <!--begin::Table-->

                            <figure class="highcharts-figure">
                                <div id="containerSchoolDataView_2"></div>
                                <p class="highcharts-description">
                                    <!-- This above chart show total Orders monthly -->
                                </p>
                            </figure>

                            <!--end::Table-->
                        </div>
                        <div class="tab-pane fade active show" id="kt_tab_pane_11_3_3_A" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                            <!--begin::Table-->

                            <figure class="highcharts-figure">
                                <div id="containerSchoolDataView_3"></div>
                                <p class="highcharts-description">
                                    <!-- This above chart show total Orders monthly -->
                                </p>
                            </figure>

                            <!--end::Table-->
                        </div>
                        <!--end::Tap pane-->

                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Advance Table Widget 2-->
        </div>
        

        


    </div>
    <div class="row">


    <div class="col-md-12">
            <!--begin::Advance Table Widget 2-->
            <div class="card card-custom card-stretch gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">School Performance</span>
                        <!-- <span class="text-muted mt-3 font-weight-bold font-size-sm">More than 400+ new members</span> -->
                    </h3>
                    <div class="card-toolbar">
                        <ul class="nav nav-pills nav-pills-sm nav-dark-75">
                            <li class="nav-item">
                                <a class="nav-link py-2 px-4 active" data-toggle="tab" href="#kt_tab_pane_11_1">Week</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link py-2 px-4" data-toggle="tab" href="#kt_tab_pane_11_2">Month</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link py-2 px-4 " data-toggle="tab" href="#kt_tab_pane_11_3">Year</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-2 pb-0 mt-n3">
                    <div class="tab-content mt-5" id="myTabTables11">
                        <!--begin::Tap pane-->
                        <div class="tab-pane fade active show" id="kt_tab_pane_11_1" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                            <!--begin::Table-->

                            <figure class="highcharts-figure">
                <div id="containerUserA"></div>
                <p class="highcharts-description">
                    <!-- This above chart show total Orders monthly -->
                </p>
            </figure>

                            <!--end::Table-->
                        </div>
                        <!--end::Tap pane-->

                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Advance Table Widget 2-->
        </div>


    </div>

    



</div>

<!--end::Row-->