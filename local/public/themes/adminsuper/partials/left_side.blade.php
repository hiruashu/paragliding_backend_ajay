<style type="text/css" media="screen">
	.aside-menu .menu-nav > .menu-item:not(.menu-item-parent):not(.menu-item-open):not(.menu-item-here):not(.menu-item-active):hover > .menu-heading .menu-icon i, .aside-menu .menu-nav > .menu-item:not(.menu-item-parent):not(.menu-item-open):not(.menu-item-here):not(.menu-item-active):hover > .menu-link .menu-icon i {
    color: #3699FF !important;
}
</style>
<div class="d-flex flex-column flex-root">
	<!--begin::Page-->
	<div class="d-flex flex-row flex-column-fluid page">
		<!--begin::Aside-->
		<div class="aside aside-left aside-fixed d-flex flex-column flex-row-auto" id="kt_aside">
			<!--begin::Brand-->
			<div class="brand flex-column-auto" id="kt_brand">
				<!--begin::Logo-->
				<a href="{{getBaseURL()}}" class="brand-logo">
					<img alt="Logo" src="{{applogopng()}}" width="180px" />

				</a>
				<!--end::Logo-->
				<!--begin::Toggle-->
				<button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
					<span class="svg-icon svg-icon svg-icon-xl">
						<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-left.svg-->
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<polygon points="0 0 24 0 24 24 0 24" />
								<path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999)" />
								<path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999)" />
							</g>
						</svg>
						<!--end::Svg Icon-->
					</span>
				</button>
				<!--end::Toolbar-->
			</div>
			<!--end::Brand-->
			<?php

						$linkURL = Request::segment(1);
						if (!isset($linkURL)) {
							$linkActive = "active";
							$openMe1 = "";
							$openMe2 = "";
							$openMe3 = "";
							$openMe4 = "";
							$openMe5 = "";
							$openMe6 = "";
							$openMe7 = "";
							$openMe8 = "";
							$openMe9 = "";
							$openMe10 = "";
							
						} else {
							$linkActive = "";
							if ($linkURL == 'add-school' || $linkURL == 'school-list') {
								$openMe1 = "menu-item-open";
							} else {
								$openMe1 = "";
							}
							if ($linkURL == 'user-list') {
								$openMe2 = "menu-item-open";
							} else {
								$openMe2 = "";
							}
							if ($linkURL == 'school-payments') {
								$openMe3 = "menu-item-open";
							} else {
								$openMe3 = "";
							}

							if ($linkURL == 'add-static-content' || $linkURL == 'static-content-list') {
								$openMe4 = "menu-item-open";
							} else {
								$openMe4 = "";
							}

							if ($linkURL == 'certificate-list') {
								$openMe5 = "menu-item-open";
							} else {
								$openMe5 = "";
							}
							if ($linkURL == 'school-request-list') {
								$openMe6 = "menu-item-open";
							} else {
								$openMe6 = "";
							}
							if ($linkURL == 'basic-settings') {
								$openMe7 = "menu-item-open";
							} else {
								$openMe7 = "";
							}
							if ($linkURL == 'school-performance-list') {
								$openMe8 = "menu-item-open";
							} else {
								$openMe8 = "";
							}
							if ($linkURL == 'get-annoused-list') {
								$openMe9 = "menu-item-open";
							} else {
								$openMe9 = "";
							}
							if ($linkURL == 'zelosChat') {
								$openMe10 = "menu-item-open";
							} else {
								$openMe10 = "";
							}
							
						}


						?>

			<!--begin::Aside Menu-->
			<div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
				<!--begin::Menu Container-->
				<div id="kt_aside_menu" class="aside-menu my-4" data-menu-vertical="1" data-menu-scroll="1" data-menu-dropdown-timeout="500">
					<!--begin::Menu Nav-->
					<ul class="menu-nav">
						<li class="menu-item menu-item-{{$linkActive}}" aria-haspopup="true">
							<a href="{{getBaseURL()}}" class="menu-link">
								<span class="svg-icon menu-icon">
									<!--begin::Svg Icon | path:assets/media/svg/icons/Design/Layers.svg-->
									<i class="fas fa-tachometer-alt"></i>
									<!--end::Svg Icon-->
								</span>
								<span class="menu-text">Dashboard</span>
							</a>
						</li>
						
						<!-- ayra side end  -->
						<!-- <li class="menu-section">
									<h4 class="menu-text">Manage School</h4>
									<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
								</li> -->
						<li class="menu-item menu-item-submenu {{$openMe1}}" aria-haspopup="true" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon">
									<!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
									<i class="icon-md fas fa-school"></i>
									<!--end::Svg Icon-->
								</span>
								<span class="menu-text">School</span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text">School</span>
										</span>
									</li>
									<li class="menu-item" aria-haspopup="true">
										<a href="{{route('add_school')}}" class="menu-link">
											<i class="menu-bullet menu-bullet-line">
												<span></span>
											</i>
											<span class="menu-text">Add new School </span>

										</a>
									</li>

									<li class="menu-item" aria-haspopup="true">
										<a href="{{route('schoolList')}}" class="menu-link">
											<i class="menu-bullet menu-bullet-line">
												<span></span>
											</i>
											<span class="menu-text">List of School </span>
											<span class="menu-label">
												<span class="label label-primary label-inline">{{countSchool()}}</span>
											</span>
										</a>
									</li>

								</ul>
							</div>
						</li>
						<!-- ayra side end  -->



						<!-- ayra side end  -->
						<!-- <li class="menu-section">
									<h4 class="menu-text">User Management</h4>
									<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
								</li> -->
						<li class="menu-item menu-item-submenu {{$openMe2}}" aria-haspopup="true" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon">
									<!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
									<i class="icon-md fas fa-user"></i>
									<!--end::Svg Icon-->
								</span>
								<span class="menu-text">Users</span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text">Users</span>
										</span>
									</li>

									<li class="menu-item" aria-haspopup="true">
										<a href="{{route('userList')}}" class="menu-link">
											<i class="menu-bullet menu-bullet-line">
												<span></span>
											</i>
											<span class="menu-text">List of Users </span>
											<span class="menu-label">
												<!-- <span class="label label-primary label-inline">2</span> -->
											</span>
										</a>
									</li>

								</ul>
							</div>
						</li>
						<!-- ayra side end  -->

						<!-- ayra side end  -->
						<!-- <li class="menu-section">
									<h4 class="menu-text">Payment Management</h4>
									<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
								</li> -->
						<li class="menu-item menu-item-submenu {{$openMe3}}" aria-haspopup="true" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon">
									<!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
									<i class="icon-md fas fa-history"></i>
									<!--end::Svg Icon-->
								</span>
								<span class="menu-text">Payment History</span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text">Payment History</span>
										</span>
									</li>

									<li class="menu-item" aria-haspopup="true">
										<a href="{{route('paymentHistAdminList')}}" class="menu-link">
											<i class="menu-bullet menu-bullet-line">
												<span></span>
											</i>
											<span class="menu-text">List of Payment </span>
											<span class="menu-label">
												<!-- <span class="label label-primary label-inline">2</span> -->
											</span>
										</a>
									</li>

								</ul>
							</div>
						</li>
						<!-- ayra side end  -->

						<!-- ayra side end  -->
						<!-- <li class="menu-section">
									<h4 class="menu-text">Payment Management</h4>
									<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
								</li> -->
						<li class="menu-item menu-item-submenu {{$openMe4}}" aria-haspopup="true" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon">
									<!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
									<i class="fab fa-artstation"></i>
									<!--end::Svg Icon-->
								</span>
								<span class="menu-text">Static Content</span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text">Static Content</span>
										</span>
									</li>

									<li class="menu-item" aria-haspopup="true">
										<a href="{{route('addStaticContent')}}" class="menu-link">
											<i class="menu-bullet menu-bullet-line">
												<span></span>
											</i>
											<span class="menu-text">Add New </span>
											<span class="menu-label">
												<!-- <span class="label label-primary label-inline">2</span> -->
											</span>
										</a>
									</li>
									<li class="menu-item" aria-haspopup="true">
										<a href="{{route('staticContentList')}}" class="menu-link">
											<i class="menu-bullet menu-bullet-line">
												<span></span>
											</i>
											<span class="menu-text">Lists </span>
											<span class="menu-label">
												<!-- <span class="label label-primary label-inline">2</span> -->
											</span>
										</a>
									</li>

								</ul>
							</div>
						</li>
						<!-- ayra side end  -->


						<!-- ayra side end  -->
						<!-- <li class="menu-section">
									<h4 class="menu-text">Payment Management</h4>
									<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
								</li> -->
						<li class="menu-item menu-item-submenu {{$openMe5}}" aria-haspopup="true" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon">
									<!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
									<i class="icon-md fas fa-certificate"></i>
									<!--end::Svg Icon-->
								</span>
								<span class="menu-text">Enrolled Certificate</span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text">Enrolled Certificate</span>
										</span>
									</li>

									<li class="menu-item" aria-haspopup="true">
										<a href="{{route('schoolCertificateList')}}" class="menu-link">
											<i class="menu-bullet menu-bullet-line">
												<span></span>
											</i>
											<span class="menu-text">Lists of Enrolled Certificate </span>
											<span class="menu-label">
												<!-- <span class="label label-primary label-inline">2</span> -->
											</span>
										</a>
									</li>

								</ul>
							</div>
						</li>
						<!-- ayra side end  -->



						<!-- ayra side end  -->
						<!-- <li class="menu-section">
									<h4 class="menu-text">Payment Management</h4>
									<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
								</li> -->
						<li class="menu-item menu-item-submenu {{$openMe6}}" aria-haspopup="true" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon">
									<!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
									<i class="icon-md fas fa-school"></i>
									<!--end::Svg Icon-->
								</span>
								<span class="menu-text">New School Request</span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text">New School Request</span>
										</span>
									</li>

									<li class="menu-item" aria-haspopup="true">
										<a href="{{route('SchoolRequestList')}}" class="menu-link">
											<i class="menu-bullet menu-bullet-line">
												<span></span>
											</i>
											<span class="menu-text">List of New School Request </span>
											<span class="menu-label">
												<!-- <span class="label label-primary label-inline">2</span> -->
											</span>
										</a>
									</li>

								</ul>
							</div>
						</li>
						<!-- ayra side end  -->

						<!-- ayra side end  -->
						<!-- <li class="menu-section">
									<h4 class="menu-text">Payment Management</h4>
									<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
								</li> -->
						<li class="menu-item menu-item-submenu {{$openMe7}}" aria-haspopup="true" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon">
									<!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
									<i class="fas fa-cog"></i>
									<!--end::Svg Icon-->
								</span>
								<span class="menu-text">Settings</span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text">Settings</span>
										</span>
									</li>

									<li class="menu-item" aria-haspopup="true">
										<a href="{{route('basicSettings')}}" class="menu-link">
											<i class="menu-bullet menu-bullet-line">
												<span></span>
											</i>
											<span class="menu-text">Basic Settings</span>
											<span class="menu-label">
												<!-- <span class="label label-primary label-inline">2</span> -->
											</span>
										</a>
									</li>

								</ul>
							</div>
						</li>
						<!-- ayra side end  -->

						<!-- ayra side end  -->
						<!-- <li class="menu-section">
									<h4 class="menu-text">Payment Management</h4>
									<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
								</li> -->
						<li class="menu-item menu-item-submenu {{$openMe8}}" aria-haspopup="true" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon">
									<!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
									<i class="fas fa-graduation-cap"></i>
									<!--end::Svg Icon-->
								</span>
								<span class="menu-text">School Performance</span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text"> Performance List</span>
										</span>
									</li>

									<li class="menu-item" aria-haspopup="true">
										<a href="{{route('schoolPerformanceList')}}" class="menu-link">
											<i class="menu-bullet menu-bullet-line">
												<span></span>
											</i>
											<span class="menu-text">School Performance </span>
											<span class="menu-label">
												<!-- <span class="label label-primary label-inline">2</span> -->
											</span>
										</a>
									</li>


								</ul>
							</div>
						</li>
						<!-- ayra side end  -->


						<li class="menu-item menu-item-submenu {{$openMe9}}" aria-haspopup="true" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon">
									<!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
									<i class="fas fa-bullhorn"></i>
									<!--end::Svg Icon-->
								</span>
								<span class="menu-text">Announcements</span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text"> Announcements</span>
										</span>
									</li>

									<li class="menu-item" aria-haspopup="true">
										<a href="{{route('getAnnoucedList')}}" class="menu-link">
											<i class="menu-bullet menu-bullet-line">
												<span></span>
											</i>
											<span class="menu-text">Announcements List </span>
											<span class="menu-label">
												<!-- <span class="label label-primary label-inline">2</span> -->
											</span>
										</a>
									</li>


								</ul>
							</div>
						</li>

						<li class="menu-item menu-item-submenu {{$openMe10}}" aria-haspopup="true" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon">
									<!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
									<i class="fas fa-comments"></i>
									<!--end::Svg Icon-->
								</span>
								<span class="menu-text">Chat with School </span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text"> Chat with admin</span>
										</span>
									</li>

									<li class="menu-item" aria-haspopup="true">
										<a href="{{route('zelosChat')}}" class="menu-link">
											<i class="menu-bullet menu-bullet-line">
												<span></span>
											</i>
											<span class="menu-text">Chat  List </span>
											<span class="menu-label">
												<!-- <span class="label label-primary label-inline">2</span> -->
											</span>
										</a>
									</li>


								</ul>
							</div>
						</li>


					</ul>
					<!--end::Menu Nav-->
				</div>
				<!--end::Menu Container-->
			</div>
			<!--end::Aside Menu-->
		</div>
		<!--end::Aside-->


		<!-- ayra end  side  -->

		<!--begin::Wrapper-->
		<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
			<!--begin::Header-->
			<div id="kt_header" class="header header-fixed">
				<!--begin::Container-->
				<div class="container-fluid d-flex align-items-stretch justify-content-between">
					<!--begin::Header Menu Wrapper-->
					<div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
						<!--begin::Header Menu-->
						<div id="kt_header_menu" class="header-menu header-menu-mobile header-menu-layout-default">
							<!--begin::Header Nav-->

							<!--end::Header Nav-->
						</div>
						<!--end::Header Menu-->
					</div>
					<!--end::Header Menu Wrapper-->