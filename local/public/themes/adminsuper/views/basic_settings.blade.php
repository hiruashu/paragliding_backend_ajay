<!--begin::Container-->
<div class="container">
    <!--begin::Profile Overview-->
    <div class="d-flex flex-row">
        <!--begin::Aside-->
        <div class="flex-row-auto offcanvas-mobile w-300px w-xl-350px" id="kt_profile_aside">
            <!--begin::Profile Card-->
            <div class="card card-custom card-stretch">
                <!--begin::Body-->
                <div class="card-body pt-4">



                    <!--begin::Nav-->
                    <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                        <div class="navi-item mb-2">
                            <ul class="nav flex-column nav-pills">
                                <li class="nav-item mb-2">
                                    <a class="nav-link active" id="home-tab-5" data-toggle="tab" href="#kt_tab_pane_1">
                                        <span class="nav-icon">
                                            <i class="flaticon2-chat-1"></i>
                                        </span>
                                        <span class="nav-text">Sports</span>
                                    </a>
                                </li>
                                <li class="nav-item mb-2">
                                    <a class="nav-link" id="home-tab-5" data-toggle="tab" href="#kt_tab_pane_2">
                                        <span class="nav-icon">
                                            <i class="flaticon2-chat-1"></i>
                                        </span>
                                        <span class="nav-text">Intreset</span>
                                    </a>
                                </li>


                            </ul>

                        </div>

                    </div>
                    <!--end::Nav-->
                </div>
                <!--end::Body-->
            </div>
            <!--end::Profile Card-->
        </div>
        <!--end::Aside-->
        <!--begin::Content-->
        <div class="flex-row-fluid ml-lg-8">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Settings
                            <small></small></h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content mt-1" id="myTabContent">
                        <div class="tab-pane fade show active" id="kt_tab_pane_1" role="tabpanel"
                            aria-labelledby="kt_tab_pane_2">
                            {{-- Tab content 1 --}}
                            <!--begin::Form-->
                            <form class="form" action="{{route('saveSportIntrest')}}" method="POST"
                                data-redirect="static-content-list" id="kt_form_2_frmSport">
                                @csrf
                                <input type="hidden" value="1" name="txtAction">


                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label>* Sport:</label>
                                        <input type="text" required  name="txtSport" class="form-control"
                                            placeholder="Enter Sports" value="" />
                                    </div>
                                </div>



                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <button type="submit"
                                                class="btn btn-primary font-weight-bold mr-2">Submit</button>
                                            <button type="reset"
                                                class="btn btn-light-primary font-weight-bold">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->

                            <!--begin::Card-->
                            <div class="card card-custom">
                                <div class="card-header">
                                    <div class="card-title">
                                        <span class="card-icon">
                                            <i class="flaticon2-favourite text-primary"></i>
                                        </span>
                                        <h3 class="card-label">Sports List</h3>
                                    </div>

                                </div>
                                <div class="card-body">
                                    <!--begin: Datatable-->
                                    <table class="table table-bordered table-hover table-checkable" id="kt_datatable_sport1"
                                        style="margin-top: 13px !important">
                                        <thead>
                                            <tr>
                                                <th>Record ID</th>
                                                <th>Name</th>                                                
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            $sportsArr = DB::table('sports')->where('is_deleted',0)->get();
                                            $i=0;
                                            foreach ($sportsArr as $key => $rowData) {
                                                $i++;
                                               ?>
                                                <tr>
                                                    <td>{{$rowData->id}}</td>
                                                    <td>{{$rowData->name}}</td>                                                        
                                                    <td nowrap="nowrap"></td>
                                                </tr>

                                               <?php
                                            }

                                            ?>
                                           

                                        </tbody>
                                    </table>
                                    <!--end: Datatable-->
                                </div>
                            </div>
                            <!--end::Card-->




                        </div>
                        <div class="tab-pane fade" id="kt_tab_pane_2" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                            {{-- Tab content 2 --}}

                             <!--begin::Form-->
                             <form class="form" action="{{route('saveSportIntrest')}}" method="POST"
                             data-redirect="static-content-list" id="kt_form_2_frmSport_intrest">
                             @csrf
                             <input type="hidden" value="2" name="txtAction">


                             <div class="form-group row">
                                 <div class="col-lg-12">
                                     <label>* Sport:</label>
                                     <input type="text" required name="txtInterest" class="form-control"
                                         placeholder="Enter Sports" value="" />
                                 </div>
                             </div>



                             <div class="card-footer">
                                 <div class="row">
                                     <div class="col-lg-12">
                                         <button type="submit"
                                             class="btn btn-primary font-weight-bold mr-2">Submit</button>
                                         <button type="reset"
                                             class="btn btn-light-primary font-weight-bold">Cancel</button>
                                     </div>
                                 </div>
                             </div>
                         </form>
                         <!--end::Form-->

                          <!--begin::Card-->
                          <div class="card card-custom">
                            <div class="card-header">
                                <div class="card-title">
                                    <span class="card-icon">
                                        <i class="flaticon2-favourite text-primary"></i>
                                    </span>
                                    <h3 class="card-label">Intreset List</h3>
                                </div>

                            </div>
                            <div class="card-body">
                                <!--begin: Datatable-->
                                <table class="table table-bordered table-hover table-checkable" id="kt_datatable_interest"
                                    style="margin-top: 13px !important">
                                    <thead>
                                        <tr>
                                            <th>Record ID</th>
                                            <th>Name</th>                                                
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $sportsArr = DB::table('interest')->where('is_deleted',0)->get();
                                        $i=0;
                                        foreach ($sportsArr as $key => $rowData) {
                                            $i++;
                                           ?>
                                            <tr>
                                                <td>{{$rowData->id}}</td>
                                                <td>{{$rowData->name}}</td>                                                        
                                                <td nowrap="nowrap"></td>
                                            </tr>

                                           <?php
                                        }

                                        ?>
                                       

                                    </tbody>
                                </table>
                                <!--end: Datatable-->
                            </div>
                        </div>
                        <!--end::Card-->


                        </div>

                    </div>



                </div>

            </div>
            <!--end::Card-->


            <!--end::Example-->
            <!--begin::Example-->
            <!--begin::Card-->
        </div>
        <!--end::Content-->
    </div>
    <!--end::Profile Overview-->
</div>
<!--end::Container-->
