<table class="table table-bordered table-hover table-checkable" id="kt_datatable_school_certificate" style="margin-top: 13px !important">
						<thead>
							<tr>
								<th>Record ID</th>
								<th>S#</th>
								<th>School</th>
								<th>Certificate</th>
								<th>Total Enrolled Student</th>
								<th>Course Date</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$contentArr = DB::table('school_course')
								->where('is_deleted', 0)
								->orderBy('id', 'desc')
								->get();
							$i = 0;
							foreach ($contentArr as $key => $rowData) {
								$i++;
								$schoolArr = DB::table('schools')
									->where('id', $rowData->sid)
									->first();
								$URL = getBaseURL() . "/view-school-details/" . $rowData->sid
							?>
								<tr>
									<td>{{$rowData->id}}</td>
									<td>{{$i}}</td>
									<td> <a href="{{$URL}}">{{$schoolArr->title}}</a> </td>
									<td>{{$rowData->certificate_title}}</td>
									<td>5</td>
									<td>{{$rowData->course_date}}</td>
									<td nowrap="nowrap"></td>
								</tr>
							<?php
							}

							?>


						</tbody>
						<tfoot>
							<tr>
								<th>Record ID</th>
								<th>S#</th>
								<th>School</th>
								<th>Certificate</th>
								<th>Total Enrolled Student</th>
								<th>Course Date</th>
								<th>Actions</th>
							</tr>
						</tfoot>
					</table>