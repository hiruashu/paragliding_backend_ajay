<!--begin::Container-->
<div class="container">
    <!--begin::Card-->
    <div class="card card-custom gutter-b" style="display:none">
        <div class="card-body">
            <div class="d-flex">
                <!--begin::Pic-->
                <div class="flex-shrink-0 mr-7">
                    <div class="symbol symbol-50 symbol-lg-120">
                        <img alt="Pic" src="{{NoImage()}}">
                    </div>
                </div>
                <!--end::Pic-->
                <!--begin: Info-->
                <div class="flex-grow-1">
                    <!--begin::Title-->
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <!--begin::User-->
                        <div class="mr-3">
                            <div class="d-flex align-items-center mr-3">
                                <!--begin::Name-->
                                <a href="javascript:;" class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">{{$data->title}}</a>
                                <!--end::Name-->

                            </div>
                            <!--begin::Contacts-->
                            <div class="d-flex flex-wrap my-2">
                                <a href="#" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                    <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                        <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Communication/Mail-notification.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000"></path>
                                                <circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5"></circle>
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>{{@$data->email}}</a>
                                <a href="#" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                    <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                        <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/General/Lock.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <mask fill="white">
                                                    <use xlink:href="#path-1"></use>
                                                </mask>
                                                <g></g>
                                                <path d="M7,10 L7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 L17,10 L18,10 C19.1045695,10 20,10.8954305 20,12 L20,18 C20,19.1045695 19.1045695,20 18,20 L6,20 C4.8954305,20 4,19.1045695 4,18 L4,12 C4,10.8954305 4.8954305,10 6,10 L7,10 Z M12,5 C10.3431458,5 9,6.34314575 9,8 L9,10 L15,10 L15,8 C15,6.34314575 13.6568542,5 12,5 Z" fill="#000000"></path>
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>{{@$data->phone}}</a>
                                <a href="#" class="text-muted text-hover-primary font-weight-bold">
                                    <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                        <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Map/Marker2.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path d="M9.82829464,16.6565893 C7.02541569,15.7427556 5,13.1079084 5,10 C5,6.13400675 8.13400675,3 12,3 C15.8659932,3 19,6.13400675 19,10 C19,13.1079084 16.9745843,15.7427556 14.1717054,16.6565893 L12,21 L9.82829464,16.6565893 Z M12,12 C13.1045695,12 14,11.1045695 14,10 C14,8.8954305 13.1045695,8 12,8 C10.8954305,8 10,8.8954305 10,10 C10,11.1045695 10.8954305,12 12,12 Z" fill="#000000"></path>
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>{{@$data->city_name}}</a>
                            </div>
                            <!--end::Contacts-->
                        </div>
                        <!--begin::User-->

                    </div>

                    <!--end::Title-->
                    <!--begin::Content-->
                    <div class="d-flex align-items-center flex-wrap justify-content-between">
                        <!--begin::Description-->
                        <div class="flex-grow-1 font-weight-bold text-dark-50 py-2 py-lg-2 mr-5">

                            {!! @$data->about!!}
                        </div>
                        <!--end::Description-->

                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Info-->

            </div>

        </div>

    </div>
    <!--end::Card-->
    <!--begin::Row-->
    <div class="row">

        <div class="col-xl-12">
            <!--begin::Card-->
            <input type="hidden" name="txtSID" id="txtSID" value="{{ Request::segment(2) }}">
            <input type="hidden" name="txtAction" value="_edit">
            <div class="card card-custom gutter-b">
                <!--begin::Header-->
                <div class="card-header card-header-tabs-line">
                    <div class="card-toolbar">
                        <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-bold nav-tabs-line-3x" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#kt_apps_contacts_view_tab_1">
                                    <span class="nav-icon mr-2">
                                        <span class="svg-icon mr-3">
                                            <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/General/Notification2.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z" fill="#000000"></path>
                                                    <circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5"></circle>
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </span>
                                    <span class="nav-text">Basic</span>
                                </a>
                            </li>
                            <li class="nav-item mr-3">
                                <a class="nav-link" data-toggle="tab" href="#kt_apps_contacts_view_tab_2">
                                    <span class="nav-icon mr-2">
                                        <span class="svg-icon mr-3">
                                            <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Communication/Chat-check.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M4.875,20.75 C4.63541667,20.75 4.39583333,20.6541667 4.20416667,20.4625 L2.2875,18.5458333 C1.90416667,18.1625 1.90416667,17.5875 2.2875,17.2041667 C2.67083333,16.8208333 3.29375,16.8208333 3.62916667,17.2041667 L4.875,18.45 L8.0375,15.2875 C8.42083333,14.9041667 8.99583333,14.9041667 9.37916667,15.2875 C9.7625,15.6708333 9.7625,16.2458333 9.37916667,16.6291667 L5.54583333,20.4625 C5.35416667,20.6541667 5.11458333,20.75 4.875,20.75 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                    <path d="M2,11.8650466 L2,6 C2,4.34314575 3.34314575,3 5,3 L19,3 C20.6568542,3 22,4.34314575 22,6 L22,15 C22,15.0032706 21.9999948,15.0065399 21.9999843,15.009808 L22.0249378,15 L22.0249378,19.5857864 C22.0249378,20.1380712 21.5772226,20.5857864 21.0249378,20.5857864 C20.7597213,20.5857864 20.5053674,20.4804296 20.317831,20.2928932 L18.0249378,18 L12.9835977,18 C12.7263047,14.0909841 9.47412135,11 5.5,11 C4.23590829,11 3.04485894,11.3127315 2,11.8650466 Z M6,7 C5.44771525,7 5,7.44771525 5,8 C5,8.55228475 5.44771525,9 6,9 L15,9 C15.5522847,9 16,8.55228475 16,8 C16,7.44771525 15.5522847,7 15,7 L6,7 Z" fill="#000000"></path>
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </span>
                                    <span class="nav-text">History Data</span>
                                </a>
                            </li>
                            <li class="nav-item mr-3">
                                <a class="nav-link" data-toggle="tab" href="#kt_apps_contacts_view_tab_3">
                                    <span class="nav-icon mr-2">
                                        <span class="svg-icon mr-3">
                                            <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Devices/Display1.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M11,20 L11,17 C11,16.4477153 11.4477153,16 12,16 C12.5522847,16 13,16.4477153 13,17 L13,20 L15.5,20 C15.7761424,20 16,20.2238576 16,20.5 C16,20.7761424 15.7761424,21 15.5,21 L8.5,21 C8.22385763,21 8,20.7761424 8,20.5 C8,20.2238576 8.22385763,20 8.5,20 L11,20 Z" fill="#000000" opacity="0.3"></path>
                                                    <path d="M3,5 L21,5 C21.5522847,5 22,5.44771525 22,6 L22,16 C22,16.5522847 21.5522847,17 21,17 L3,17 C2.44771525,17 2,16.5522847 2,16 L2,6 C2,5.44771525 2.44771525,5 3,5 Z M4.5,8 C4.22385763,8 4,8.22385763 4,8.5 C4,8.77614237 4.22385763,9 4.5,9 L13.5,9 C13.7761424,9 14,8.77614237 14,8.5 C14,8.22385763 13.7761424,8 13.5,8 L4.5,8 Z M4.5,10 C4.22385763,10 4,10.2238576 4,10.5 C4,10.7761424 4.22385763,11 4.5,11 L7.5,11 C7.77614237,11 8,10.7761424 8,10.5 C8,10.2238576 7.77614237,10 7.5,10 L4.5,10 Z" fill="#000000"></path>
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </span>
                                    <span class="nav-text">Instructor</span>
                                </a>
                            </li>
                            <li class="nav-item mr-3">
                                <a class="nav-link" data-toggle="tab" href="#kt_apps_contacts_view_tab_5">
                                    <span class="nav-icon mr-2">
                                        <span class="svg-icon mr-3">
                                            <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Devices/Display1.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M11,20 L11,17 C11,16.4477153 11.4477153,16 12,16 C12.5522847,16 13,16.4477153 13,17 L13,20 L15.5,20 C15.7761424,20 16,20.2238576 16,20.5 C16,20.7761424 15.7761424,21 15.5,21 L8.5,21 C8.22385763,21 8,20.7761424 8,20.5 C8,20.2238576 8.22385763,20 8.5,20 L11,20 Z" fill="#000000" opacity="0.3"></path>
                                                    <path d="M3,5 L21,5 C21.5522847,5 22,5.44771525 22,6 L22,16 C22,16.5522847 21.5522847,17 21,17 L3,17 C2.44771525,17 2,16.5522847 2,16 L2,6 C2,5.44771525 2.44771525,5 3,5 Z M4.5,8 C4.22385763,8 4,8.22385763 4,8.5 C4,8.77614237 4.22385763,9 4.5,9 L13.5,9 C13.7761424,9 14,8.77614237 14,8.5 C14,8.22385763 13.7761424,8 13.5,8 L4.5,8 Z M4.5,10 C4.22385763,10 4,10.2238576 4,10.5 C4,10.7761424 4.22385763,11 4.5,11 L7.5,11 C7.77614237,11 8,10.7761424 8,10.5 C8,10.2238576 7.77614237,10 7.5,10 L4.5,10 Z" fill="#000000"></path>
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </span>
                                    <span class="nav-text">Documents</span>
                                </a>
                            </li>

                            <li class="nav-item mr-3" style="display: none;">
                                <a class="nav-link " data-toggle="tab" href="#kt_apps_contacts_view_tab_4">
                                    <span class="nav-icon mr-2">
                                        <span class="svg-icon mr-3">
                                            <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Home/Globe.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M13,18.9450712 L13,20 L14,20 C15.1045695,20 16,20.8954305 16,22 L8,22 C8,20.8954305 8.8954305,20 10,20 L11,20 L11,18.9448245 C9.02872877,18.7261967 7.20827378,17.866394 5.79372555,16.5182701 L4.73856106,17.6741866 C4.36621808,18.0820826 3.73370941,18.110904 3.32581341,17.7385611 C2.9179174,17.3662181 2.88909597,16.7337094 3.26143894,16.3258134 L5.04940685,14.367122 C5.46150313,13.9156769 6.17860937,13.9363085 6.56406875,14.4106998 C7.88623094,16.037907 9.86320756,17 12,17 C15.8659932,17 19,13.8659932 19,10 C19,7.73468744 17.9175842,5.65198725 16.1214335,4.34123851 C15.6753081,4.01567657 15.5775721,3.39010038 15.903134,2.94397499 C16.228696,2.49784959 16.8542722,2.4001136 17.3003976,2.72567554 C19.6071362,4.40902808 21,7.08906798 21,10 C21,14.6325537 17.4999505,18.4476269 13,18.9450712 Z" fill="#000000" fill-rule="nonzero"></path>
                                                    <circle fill="#000000" opacity="0.3" cx="12" cy="10" r="6"></circle>
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </span>
                                    <span class="nav-text">Settings</span>
                                </a>
                            </li>
                        </ul>

                        <div class="form-group row ml-23 mt-6 ">
                        <?php 

$schArrData = DB::table('schools')
->where('id',$data->id)
->first();
// print_r($schArrData);
$scoolArrData = DB::table('schools_slider_img')->where('sid', $data->id)->get();
$is_school_img=0;
if (count($scoolArrData) > 0) {
    $is_school_img=1;
}

                        ?>



                            <label class="form-label font-weight-bolder m-2">Status</label>
                            <span class="switch switch-outline switch-icon switch-success">
                                <label>
                                    <?php
                                    $aj=0;
                                    if ($data->status == 1) {
                                        if($data->school_logo){
                                            $aj++;
                                        }
                                        if($is_school_img==1){
                                            $aj++;
                                        }
                                        if($data->title){
                                            $aj++;
                                        }
                                        if($data->reg_no){
                                            $aj++;
                                        }
                                        if($data->country_id){
                                            $aj++;
                                        }
                                        if($data->state_id){
                                            $aj++;
                                        }
                                        if($data->city_id){
                                            $aj++;
                                        }
                                        if($data->phone){
                                            $aj++;
                                        }
                                        if($data->admin_comm){
                                            $aj++;
                                        }
                                        if($aj>=9){
                                            ?>
                                            <input type="checkbox" checked="checked" name="userActiveAction" id="userActiveAction" />
                                        <?php
                                        }else{
                                            $affected = DB::table('schools')
                                        ->where('id', $data->id)
                                        ->update(['status' => 2]);

                                            ?>
                                            <input type="checkbox"  name="userActiveAction" id="userActiveAction" />
                                        <?php
                                        }


                                   
                                    } else {
                                        $affected = DB::table('schools')
                                        ->where('id', $data->id)
                                        ->update(['status' => 2]);
                                        
                                    ?>
                                        <input type="checkbox" name="userActiveAction" id="userActiveAction" />
                                    <?php
                                    }
                                    ?>
                                    <span></span>
                                </label>
                            </span>



                        </div>
                        <?php
                        if ($data->added_from_status == 2) {
                        ?>
                            <!--begin::Dropdown-->

                            <div class="btn-group ml-6">
                                <button type="button" class="btn btn-primary font-weight-bold btn-sm px-3 font-size-base">School Approval</button>
                                <button type="button" class="btn btn-success font-weight-bold btn-sm px-3 font-size-base dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                <div class="dropdown-menu dropdown-menu-sm p-0 m-0 dropdown-menu-right">
                                    <ul class="navi py-5">
                                        <li class="navi-item">
                                            <a href="javascript:void(0)" onclick="schoolApprovalAction(1,{{$data->id}})" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="flaticon2-medical-records"></i>
                                                </span>

                                                <span class="navi-text">Accepted</span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="jjavascript:void(0)" onclick="schoolApprovalAction(2,{{$data->id}})" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="flaticon2-writing"></i>
                                                </span>
                                                <span class="navi-text">Rejected</span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <!--end::Dropdown-->
                        <?php
                        }
                        ?>

                        <div class="form-group row ml-23 mt-6 ">

                      
                        <div class="card-toolbar" style="margin-bottom: 1px;">
                                    <div class="dropdown dropdown-inline">
                                        <a href="#" class="btn btn-light-primary btn-sm font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">School Profile Status</a>
                                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                            <!--begin::Navigation-->
                                            <ul class="navi navi-hover">
                                                <li class="navi-header pb-1">
                                                    <span class="text-primary text-uppercase font-weight-bold font-size-sm">Need to complete:</span>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
                                                        <span class="navi-icon">
                                                        <i style="color:{{(!empty($data->school_logo)) ? 'green':''}}" class="icon-xl far fa-check-circle"></i>
                                                        </span>
                                                        
                                                        <span class="navi-text">Logo</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
                                                        <span class="navi-icon">
                                                        <i style="color:{{(!empty($is_school_img)) ? 'green':''}}" class="icon-xl far fa-check-circle"></i>
                                                        </span>
                                                        
                                                        <span class="navi-text">Gallery Image</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
                                                        <span class="navi-icon">
                                                        <i style="color:{{(!empty($data->title)) ? 'green':''}}" class="icon-xl far fa-check-circle"></i>
                                                        </span>
                                                        
                                                        <span class="navi-text">Title</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
                                                        <span class="navi-icon">
                                                        <i style="color:{{(!empty($data->reg_no)) ? 'green':''}}" class="icon-xl far fa-check-circle"></i>
                                                        </span>
                                                        <span class="navi-text">Registration No.</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
                                                        <span class="navi-icon">
                                                        <i style="color:{{(!empty($data->country_id)) ? 'green':''}}" class="icon-xl far fa-check-circle"></i>
                                                        </span>
                                                        <span class="navi-text">Country </span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
                                                        <span class="navi-icon">
                                                        <i style="color:{{(!empty($data->state_id)) ? 'green':''}}" class="icon-xl far fa-check-circle"></i>
                                                        </span>
                                                        <span class="navi-text">State</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
                                                        <span class="navi-icon">
                                                        <i style="color:{{(!empty($data->city_id)) ? 'green':''}}" class="icon-xl far fa-check-circle"></i>
                                                        </span>
                                                        <span class="navi-text">City</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
                                                        <span class="navi-icon">
                                                        <i style="color:{{(!empty($data->phone)) ? 'green':''}}" class="icon-xl far fa-check-circle"></i>
                                                        </span>
                                                        <span class="navi-text">Phone</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
                                                        <span class="navi-icon">
                                                        <i style="color:{{(!empty($data->admin_comm)) ? 'green':''}}" class="icon-xl far fa-check-circle"></i>
                                                        </span>
                                                        <span class="navi-text">Admin Commission</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <!--end::Navigation-->
                                        </div>
                                    </div>
                                </div>
                            <!--end::Header-->


                        </div>


                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body px-0">
                    <div class="tab-content pt-1">
                        <!--begin::Tab Content-->
                        <div class="tab-pane active" id="kt_apps_contacts_view_tab_1" role="tabpanel">
                            <form data-redirect="{{getBaseURL()}}/school-list" class="form" method="post" action="444" id="ayra_kt_form_add_school">
                                <div class="row">

                                    <?php
                                    $schoolArr = DB::table('schools')->where('id', $data->id)->whereNotNull('school_logo')->first();
                                    if ($schoolArr == null) {
                                        $schLogo = NoImage();
                                    } else {
                                        $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->school_logo;
                                    }

                                    ?>

                                    <div class="col-lg-3">
                                        <div class="form-group row">
                                            <label class="col-xl-1 col-lg-1 text-right col-form-label"></label>
                                            <div class="col-lg-9 col-xl-9">
                                                <div class="image-input image-input-outline image-input" id="kt_user_avatar" style="background-image: url({{$schLogo}})">
                                                    <div class="image-input-wrapper" style="background-image: url({{$schLogo}})"></div>
                                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" onclick="removeImage(1,{{$data->id}})" data-action="remove" data-toggle="tooltip" title="Remove ">
                                                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                    </span>

                                                </div>
                                                <div class="dropzone dropzone-multi" id="kt_dropzone_4_1">
                                                    <div class="dropzone-panel mb-lg-0 mb-2">
                                                        <a class="dropzone-select btn btn-primary font-weight-bold btn-sm">Logo</a>
                                                        <a class="dropzone-upload btn btn-warning font-weight-bold btn-sm">Upload </a>
                                                        <a class="dropzone-remove-all btn btn-danger font-weight-bold btn-sm"> <i class="ki ki-bold-close icon-xs text-muted"></i> </a>
                                                    </div>
                                                    <div class="dropzone-items">
                                                        <div class="dropzone-item" style="display:none">
                                                            <div class="dropzone-file">
                                                                <div class="dropzone-filename" title="some_image_file_name.jpg">
                                                                    <span data-dz-name="">some_image_file_name.jpg</span>
                                                                    <strong>(
                                                                        <span data-dz-size="">340kb</span>)</strong>
                                                                </div>
                                                                <div class="dropzone-error" data-dz-errormessage=""></div>
                                                            </div>
                                                            <div class="dropzone-progress">
                                                                <div class="progress">
                                                                    <div class="progress-bar bg-primary" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress=""></div>
                                                                </div>
                                                            </div>
                                                            <div class="dropzone-toolbar">
                                                                <span class="dropzone-start">
                                                                    <i class="flaticon2-arrow"></i>
                                                                </span>
                                                                <span class="dropzone-cancel" data-dz-remove="" style="display: none;">
                                                                    <i class="flaticon2-cross"></i>
                                                                </span>
                                                                <span class="dropzone-delete" data-dz-remove="">
                                                                    <i class="flaticon2-cross"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="form-text text-muted">Maximum 5MB file size is supported.</span>



                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-lg-9">
                                        <?php

                                        $scoolArrData = DB::table('schools_slider_img')->where('sid', $data->id)->limit(5)->orderBy('id', 'desc')->get();
                                        if (count($scoolArrData) > 0) {
                                            foreach ($scoolArrData as $key => $rows) {


                                                $schSlider = asset('/local/public/upload/') . "/" . $rows->slider_img;


                                        ?>
                                                <div class="image-input image-input-outline image-input" style="margin:5px" id="kt_user_avatar" style="background-image: url({{$schSlider}})">
                                                    <div class="image-input-wrapper" style="background-image: url({{$schSlider}})"></div>
                                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" onclick="removeImage(2,{{$rows->id}})" data-action="remove" data-toggle="tooltip" title="Remove ">
                                                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                    </span>

                                                </div>

                                            <?php
                                            }
                                        } else {

                                            $schSlider = NoImage();

                                            ?>
                                            <div class="image-input image-input-outline image-input" style="margin:5px" id="kt_user_avatar" style="background-image: url({{$schSlider}})">
                                                <div class="image-input-wrapper" style="background-image: url({{$schSlider}})"></div>


                                            </div>
                                        <?php
                                        }


                                        ?>




                                        <div class="dropzone dropzone-multi" id="kt_dropzone_4_3">
                                            <div class="dropzone-panel mb-lg-0 mb-2">
                                                <a class="dropzone-select btn btn-primary font-weight-bold btn-sm">School Images</a>
                                                <a class="dropzone-upload btn btn-warning font-weight-bold btn-sm">Upload </a>
                                                <a class="dropzone-remove-all btn btn-danger font-weight-bold btn-sm">Remove </a>
                                            </div>
                                            <div class="dropzone-items">
                                                <div class="dropzone-item" style="display:none">
                                                    <div class="dropzone-file">
                                                        <div class="dropzone-filename" title="some_image_file_name.jpg">
                                                            <span data-dz-name="">some_image_file_name.jpg</span>
                                                            <strong>(
                                                                <span data-dz-size="">340kb</span>)</strong>
                                                        </div>
                                                        <div class="dropzone-error" data-dz-errormessage=""></div>
                                                    </div>
                                                    <div class="dropzone-progress">
                                                        <div class="progress">
                                                            <div class="progress-bar bg-primary" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress=""></div>
                                                        </div>
                                                    </div>
                                                    <div class="dropzone-toolbar">
                                                        <span class="dropzone-start">
                                                            <i class="flaticon2-arrow"></i>
                                                        </span>
                                                        <span class="dropzone-cancel" data-dz-remove="" style="display: none;">
                                                            <i class="flaticon2-cross"></i>
                                                        </span>
                                                        <span class="dropzone-delete" data-dz-remove="">
                                                            <i class="flaticon2-cross"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="form-text text-muted">Maximum 5MB file size is supported.</span>


                                    </div>


                                    <div class="col-lg-12">




                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Title</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control form-control-m form-control-solid" type="text" name="title" value="{{optional($data)->title}}" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Registration No.</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control form-control-m form-control-solid" name="regno" type="text" value="{{@$data->reg_no}}" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Country</label>
                                            <div class="col-lg-9 col-xl-6">

                                                <select class="form-control form-control-m form-control-solid country" id="kt_select2_1" name="country">
                                                    <?php
                                                    $countriesArr = DB::table('countries')
                                                        ->select('id', 'name')
                                                        ->get();
                                                    foreach ($countriesArr as $key => $rowData) {
                                                        if ($rowData->id == $data->country_id) {
                                                    ?>
                                                            <option selected value="{{$rowData->id}}">{{$rowData->name}}</option>
                                                        <?php
                                                        } else {
                                                        ?>
                                                            <option value="{{$rowData->id}}">{{$rowData->name}}</option>
                                                    <?php
                                                        }
                                                    }

                                                    ?>

                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 text-right col-form-label">State</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <select class="form-control form-control-m form-control-solid stateID" id="kt_select2_2" name="state">
                                                    <?php
                                                    if ($data->state_id != "") {
                                                        $getStateArr = getState($data->state_id);

                                                    ?>
                                                        <option selected value="{{$data->state_id}}">{{$getStateArr->name}}</option>
                                                    <?php

                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">

                                            <label class="col-xl-3 col-lg-3 text-right col-form-label">City</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <select class="form-control form-control-m form-control-solid " id="kt_select2_3" name="city">
                                                    <?php
                                                    if ($data->city_id != "") {
                                                        $getStateArr = getCityData($data->city_id);

                                                    ?>
                                                        <option selected value="{{$data->city_id}}">{{$getStateArr->name}}</option>
                                                    <?php

                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 text-right col-form-label ">Phone</label>

                                            <div class="col-lg-3 col-xl-2 ">
                                                <div class="input-group">

                                                    <input type="text" value="{{$data->phone_code}}" readonly id="txtPhoneCode" class="form-control form-control-m form-control-solid" placeholder="Phone" />
                                                </div>

                                            </div>
                                            <div class="col-lg-6 col-xl-3">
                                                <div class="input-group">

                                                    <input size="30px" value="{{$data->phone}}" width="100px" name="phone" type="text" id="kt_inputmask_6" class="form-control" placeholder="Phone" />

                                                </div>

                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Discriptions.</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div id="kt-ckeditor-1-toolbar"></div>
                                                <div id="kt-ckeditor-1">
                                                    {!!$data->about!!}
                                                </div>

                                            </div>
                                        </div>



                                        <!--begin::Heading-->

                                        <!--end::Heading-->

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Email Address</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-group input-group-m input-group-solid">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="la la-at"></i>
                                                        </span>
                                                    </div>
                                                    <input type="email" value="{{$data->email}}" name="email" class="form-control form-control-m form-control-solid" value="{{$data->email}}" placeholder="Email" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Password</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-group input-group-m input-group-solid">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="la la-key"></i>
                                                        </span>
                                                    </div>
                                                    <input type="password" class="form-control form-control-m form-control-solid" value="{{$data->email}}" placeholder="Email" />
                                                </div>Login Credentials Send
                                                <span class="switch">
                                                    <label>
                                                        <input type="checkbox" name="loginCredentialSend" id="loginCredentialSend" />

                                                        <span>Active</span>
                                                    </label>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Website</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-group input-group-m input-group-solid">
                                                    <input type="text" value="{{$data->website}}" name="website" id="website" class="form-control form-control-lg form-control-solid" placeholder="Username" value="loop" />

                                                </div>
                                            </div>
                                        </div>

                                        <!--begin::Heading-->

                                        <!--end::Heading-->
                                        <style>
                                            .checkbox-inline .checkbox {
    margin-right: 1rem;
    margin-bottom: 0.35rem;
    margin-top: -11px;
}
                                        </style>

                                        <div class="form-group row" >
                                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Registration Body</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="form-group">
                                                    <label></label>
                                                    <div class="checkbox-inline">
                                                        <label class="checkbox">
                                                            <input type="checkbox" value="1" {{($data->reg_body_1 == 1)? "checked":""}} name="reg_body_1" />
                                                            <span></span>Option 1</label>
                                                        <label class="checkbox">
                                                            <input type="checkbox" value="2" {{($data->reg_body_2 == 2)? "checked":""}} name="reg_body_2" />
                                                            <span></span>Option 2</label>
                                                        <label class="checkbox">
                                                            <input type="checkbox" value="3" {{($data->reg_body_3 == 3)? "checked":""}} name="reg_body_3" />
                                                            <span></span>Option 3</label>
                                                    </div>
                                                    <span class="form-text text-muted"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Facebook</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-group input-group-m input-group-solid">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="socicon-facebook"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" value="{{$data->facebook}}" name="facebook" class="form-control form-control-m form-control-solid" placeholder="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Twitter</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-group input-group-m input-group-solid">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="socicon-twitter"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" value="{{$data->twitter}}" name="twitter" class="form-control form-control-m form-control-solid" placeholder="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Linkedin</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-group input-group-m input-group-solid">
                                                    <div class="input-group-prepend ">
                                                        <span class="input-group-text ">
                                                            <i class="socicon-linkedin"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" value="{{$data->linkedin}}" name="linkedin" class="form-control form-control-m form-control-solid" placeholder="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Commission%</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-group input-group-m input-group-solid">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="la la-at"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" value="{{$data->admin_comm}}" name="admin_comm" class="form-control form-control-m form-control-solid" placeholder="" />
                                                </div>
                                            </div>
                                        </div>



                                        <div class="card-footer">
                                            <button type="submit" id="btnSaveSchool" data-wizard-action="submit" class="btn btn-primary mr-2">Save</button>
                                            <button type="reset" class="btn btn-secondary">Cancel</button>
                                        </div>
                                    </div>


                                </div>
                            </form>


                        </div>
                        <!--end::Tab Content-->
                        <!--begin::Tab Content-->
                        <div class="tab-pane" id="kt_apps_contacts_view_tab_2" role="tabpanel">
                            <!-- reapeter  -->
                            <form action="{{route('updateSchoolHistory')}}" id="frmSchoollHistroy" method="post">
                                @csrf
                                <input type="hidden" name="txtSIDValue" value="{{ Request::segment(2) }}">
                                <div id="kt_repeater_1">
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-right">School Histroy:</label>
                                        <div data-repeater-list="schoolHistory" class="col-lg-10">
                                            <?php
                                            $schoolHistoryArr = DB::table('school_history')
                                                ->where('sid', $data->id)
                                                ->get();
                                            if (count($schoolHistoryArr) > 0) {
                                                foreach ($schoolHistoryArr as $key => $rowData) {
                                            ?>
                                                    <div data-repeater-item="" class="form-group row align-items-center">
                                                        <div class="col-md-3">
                                                            <label>Year:</label>
                                                            <select required class="form-control form-control-sm" name="txtSchoolYear" id="exampleSelects">
                                                                <?php
                                                                $currently_selected = $rowData->school_year;
                                                                // Year to start available options at
                                                                $earliest_year = $currently_selected - 20;
                                                                // Set your latest year you want in the range, in this case we use PHP to just set it to the current year.
                                                                $latest_year = date('Y') - 1;


                                                                // Loops over each int[year] from current year, back to the $earliest_year [1950]
                                                                foreach (range($latest_year, $earliest_year) as $i) {

                                                                    // Prints the option with the next year in range.
                                                                    echo  '<option value="' . $i . '"' . ($i === $currently_selected ? ' selected="selected"' : '') . '>' . $i . '</option>';
                                                                }


                                                                ?>
                                                            </select>
                                                            <div class="d-md-none mb-2"></div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label>Students:</label>
                                                            <input required type="text" value="{{$rowData->school_students}}" name="txtSchoolStudent" class="form-control" placeholder="" />
                                                            <div class="d-md-none mb-2"></div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>Note:</label>
                                                            <input required type="text" value="{{$rowData->school_notes}}" name="txtSchoolNotes" class="form-control" placeholder="" />
                                                            <div class="d-md-none mb-2"></div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <a href="javascript:;" data-repeater-delete="" class="btn btn-sm font-weight-bolder btn-light-danger">
                                                                <i class="la la-trash-o"></i>Delete</a>
                                                        </div>
                                                    </div>
                                                <?php
                                                }
                                            } else {
                                                ?>
                                                <div data-repeater-item="" class="form-group row align-items-center">
                                                    <div class="col-md-3">
                                                        <label>Year:</label>
                                                        <select required class="form-control form-control-sm" name="txtSchoolYear" id="exampleSelects">
                                                            <?php
                                                            $currently_selected = date('Y') - 1;
                                                            // Year to start available options at
                                                            $earliest_year = $currently_selected - 20;
                                                            // Set your latest year you want in the range, in this case we use PHP to just set it to the current year.
                                                            $latest_year = date('Y') - 1;


                                                            // Loops over each int[year] from current year, back to the $earliest_year [1950]
                                                            foreach (range($latest_year, $earliest_year) as $i) {

                                                                // Prints the option with the next year in range.
                                                                echo  '<option value="' . $i . '"' . ($i === $currently_selected ? ' selected="selected"' : '') . '>' . $i . '</option>';
                                                            }


                                                            ?>
                                                        </select>
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Students:</label>
                                                        <input required type="text" name="txtSchoolStudent" class="form-control" placeholder="" />
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>Note:</label>
                                                        <input required type="text" name="txtSchoolNotes" class="form-control" placeholder="" />
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <a href="javascript:;" data-repeater-delete="" class="btn btn-sm font-weight-bolder btn-light-danger">
                                                            <i class="la la-trash-o"></i>Delete</a>
                                                    </div>
                                                </div>
                                            <?php
                                            }

                                            ?>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-right"></label>
                                        <div class="col-lg-4">
                                            <a href="javascript:;" data-repeater-create="" class="btn btn-sm font-weight-bolder btn-light-primary">
                                                <i class="la la-plus"></i>Add</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>

                            </form>

                            <!-- reapter  -->
                        </div>
                        <!--end::Tab Content-->
                        <!--begin::Tab Content-->
                        <div class="tab-pane" id="kt_apps_contacts_view_tab_3" role="tabpanel">
                            <form action="{{route('updateSchoolInstructor')}}" id="frmSchoolInstructor" method="post">
                                @csrf
                                <input type="hidden" name="txtSValue" value="{{ Request::segment(2) }}">

                                <!-- reapeter  -->
                                <div id="kt_repeater_2">
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-right">School Instructor:</label>
                                        <div data-repeater-list="instructor" class="col-lg-10">
                                            <?php
                                            $schoolInstructArr = DB::table('school_instructor')
                                                ->where('sid', $data->id)
                                                ->get();
                                            if (count($schoolInstructArr) > 0) {
                                                foreach ($schoolInstructArr as $key => $rowDataS) {

                                            ?>
                                                    <div data-repeater-item="" class="form-group row align-items-center">

                                                        <div class="col-md-4">
                                                            <label>Instructor Name:</label>
                                                            <input required type="text" value="{{$rowDataS->name}}" name="txtInstName" class="form-control" placeholder="" />
                                                            <div class="d-md-none mb-2"></div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Profile URL:</label>
                                                            <input required type="url" value="{{$rowDataS->profile_url}}" name="txtInstProfileURL" class="form-control" placeholder="" />
                                                            <div class="d-md-none mb-2"></div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <a href="javascript:;" data-repeater-delete="" class="btn btn-sm font-weight-bolder btn-light-danger">
                                                                <i class="la la-trash-o"></i>Delete</a>
                                                        </div>
                                                    </div>

                                                <?php
                                                }
                                            } else {
                                                ?>
                                                <div data-repeater-item="" class="form-group row align-items-center">

                                                    <div class="col-md-4">
                                                        <label>Instructor Name:</label>
                                                        <input required type="text" name="txtInstName" class="form-control" placeholder="" />
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Profile URL:</label>
                                                        <input required type="url" name="txtInstProfileURL" class="form-control" placeholder="" />
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <a href="javascript:;" data-repeater-delete="" class="btn btn-sm font-weight-bolder btn-light-danger">
                                                            <i class="la la-trash-o"></i>Delete</a>
                                                    </div>
                                                </div>
                                            <?php
                                            }
                                            ?>


                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-right"></label>
                                        <div class="col-lg-4">
                                            <a href="javascript:;" data-repeater-create="" class="btn btn-sm font-weight-bolder btn-light-primary">
                                                <i class="la la-plus"></i>Add</a>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                        <button type="reset" class="btn btn-secondary">Cancel</button>
                                    </div>

                            </form>
                        </div>

                        <!-- reapter  -->
                    </div>
                    <!--end::Tab Content-->
                    <!--begin::Tab Content-->
                    <div class="tab-pane " id="kt_apps_contacts_view_tab_4" role="tabpanel">
                        <form class="form">
                            <!--begin::Heading-->
                            <div class="row">
                                <div class="col-lg-9 col-xl-6 offset-xl-3">
                                    <h3 class="font-size-h6 mb-5">Setup Email Notification:</h3>
                                </div>
                            </div>
                            <!--end::Heading-->
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Email Notification</label>
                                <div class="col-lg-9 col-xl-6">
                                    <span class="switch">
                                        <label>
                                            <input type="checkbox" checked="checked">
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Send Copy To Personal Email</label>
                                <div class="col-lg-9 col-xl-6">
                                    <span class="switch">
                                        <label>
                                            <input type="checkbox" name="email_notification_2">
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            <div class="separator separator-dashed my-10"></div>
                            <!--begin::Heading-->
                            <div class="row">
                                <div class="col-lg-9 col-xl-6 offset-xl-3">
                                    <h3 class="font-size-h6 mb-5">Activity Related Emails:</h3>
                                </div>
                            </div>
                            <!--end::Heading-->
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label text-right">When To Email</label>
                                <div class="col-lg-9 col-xl-6">
                                    <div class="checkbox-list">
                                        <label class="checkbox">
                                            <input type="checkbox">
                                            <span></span>You have new notifications.</label>
                                        <label class="checkbox">
                                            <input type="checkbox">
                                            <span></span>You're sent a direct message</label>
                                        <label class="checkbox">
                                            <input type="checkbox" checked="checked">
                                            <span></span>Someone adds you as a connection</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label class="col-xl-3 col-lg-3 col-form-label text-right">When To Escalate Emails</label>
                                <div class="col-lg-9 col-xl-6">
                                    <div class="checkbox-list">
                                        <label class="checkbox">
                                            <input type="checkbox">
                                            <span></span>Upon new order.</label>
                                        <label class="checkbox">
                                            <input type="checkbox">
                                            <span></span>New membership approval</label>
                                        <label class="checkbox">
                                            <input type="checkbox" checked="checked">
                                            <span></span>Member registration</label>
                                    </div>
                                </div>
                            </div>
                            <div class="separator separator-dashed my-10"></div>
                            <!--begin::Heading-->
                            <div class="row">
                                <div class="col-lg-9 col-xl-6 offset-xl-3">
                                    <h3 class="font-size-h6 mb-5">Updates From Keenthemes:</h3>
                                </div>
                            </div>
                            <!--end::Heading-->
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Email You With</label>
                                <div class="col-lg-9 col-xl-6">
                                    <div class="checkbox-list">
                                        <label class="checkbox">
                                            <input type="checkbox">
                                            <span></span>News about Metronic product and feature updates</label>
                                        <label class="checkbox">
                                            <input type="checkbox">
                                            <span></span>Tips on getting more out of Keen</label>
                                        <label class="checkbox">
                                            <input type="checkbox" checked="checked">
                                            <span></span>Things you missed since you last logged into Keen</label>
                                        <label class="checkbox">
                                            <input type="checkbox" checked="checked">
                                            <span></span>News about Metronic on partner products and other services</label>
                                        <label class="checkbox">
                                            <input type="checkbox" checked="checked">
                                            <span></span>Tips on Metronic business products</label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--end::Tab Content-->

                    <!--begin::Tab Content-->
                    <div class="tab-pane " id="kt_apps_contacts_view_tab_5" role="tabpanel">
                        <!-- documents -->

                        <!--begin::Form-->
                        <form>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-lg-right">Documents Name:</label>
                                    <div class="col-lg-7">
                                        <input type="text" id="txtDocInfo" class="form-control" placeholder="About Documents" name="name" />
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-lg-right">Upload Files:</label>
                                    <div class="col-lg-9">
                                        <div class="dropzone dropzone-multi" id="kt_dropzone_4">
                                            <div class="dropzone-panel mb-lg-0 mb-2">
                                                <a class="dropzone-select btn btn-primary font-weight-bold btn-sm">Attach files</a>
                                                <a class="dropzone-upload btn btn-warning font-weight-bold btn-sm">Upload All</a>
                                                <a class="dropzone-remove-all btn btn-danger font-weight-bold btn-sm">Remove All</a>
                                            </div>
                                            <div class="dropzone-items">
                                                <div class="dropzone-item" style="display:none">
                                                    <div class="dropzone-file">
                                                        <div class="dropzone-filename" title="some_image_file_name.jpg">
                                                            <span data-dz-name="">some_image_file_name.jpg</span>
                                                            <strong>(
                                                                <span data-dz-size="">340kb</span>)</strong>
                                                        </div>
                                                        <div class="dropzone-error" data-dz-errormessage=""></div>
                                                    </div>
                                                    <div class="dropzone-progress">
                                                        <div class="progress">
                                                            <div class="progress-bar bg-primary" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress=""></div>
                                                        </div>
                                                    </div>
                                                    <div class="dropzone-toolbar">
                                                        <span class="dropzone-start">
                                                            <i class="flaticon2-arrow"></i>
                                                        </span>
                                                        <span class="dropzone-cancel" data-dz-remove="" style="display: none;">
                                                            <i class="flaticon2-cross"></i>
                                                        </span>
                                                        <span class="dropzone-delete" data-dz-remove="">
                                                            <i class="flaticon2-cross"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="form-text text-muted">Maximum 5MB file size is supported.</span>
                                    </div>
                                </div>
                            </div>

                            <div class="card card-custom card-stretch gutter-b">
                                <!--begin::Header-->
                                <div class="card-header border-0">
                                    <h3 class="card-title font-weight-bolder text-dark">Uploaded Docuemnts</h3>

                                </div>
                                <!--end::Header-->
                                <!--begin::Body-->
                                <div class="card-body pt-2">
                                    <!--begin::Item-->
                                    <?php
                                    $docArr = DB::table('school_documents')
                                        ->where('sid', $data->id)
                                        ->get();

                                    foreach ($docArr as $key => $rowData) {

                                        $link = asset('/local/public/upload/') . "/" . $rowData->doc_name;

                                    ?>
                                        <div class="d-flex align-items-center mb-10">
                                            <!--begin::Symbol-->
                                            <div class="symbol symbol-40 symbol-light-success mr-5">
                                                <span class="symbol-label">
                                                    <img src="{{noImage()}}" class="h-75 align-self-end" alt="">
                                                </span>
                                            </div>
                                            <!--end::Symbol-->
                                            <!--begin::Text-->

                                            <div class="d-flex flex-column flex-grow-1 font-weight-bold">
                                                <a target="_blank" href="{{$link}}" class="text-dark text-hover-primary mb-1 font-size-lg">{{$rowData->doc_info}}</a>
                                                <span class="text-muted">{{date('j F Y',strtotime($rowData->created_at))}}</span>
                                            </div>




                                            <!--end::Text-->

                                        </div>
                                    <?php
                                    }

                                    ?>
                                    <!--end::Item-->

                                </div>
                                <!--end::Body-->
                            </div>

                        </form>


                        <!--end::Form-->

                        <!-- documents -->
                    </div>
                    <!--end::Tab Content-->




                </div>
            </div>
            <!--end::Body-->

        </div>
        <!--end::Card-->
    </div>
</div>
<!--end::Row-->
</div>
<!--end::Container-->