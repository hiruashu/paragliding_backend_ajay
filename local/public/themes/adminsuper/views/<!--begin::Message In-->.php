
<!--begin::Message In-->
<div class="d-flex mb-5 align-items-start">
    <div class="d-flex align-items-center">
        <div class="symbol symbol-circle symbol-40 mr-4">
            <img alt="Pic" src="assets/media/users/300_12.jpg" />
        </div>
        <!-- <div>
																	<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">Matt Pears</a>
																	<span class="text-muted font-size-sm">2 Hours</span>
																</div> -->
    </div>
    <div class="msg_time_mn rounded ml-5 p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px position-relative">How likely are you to recommend our company to your friends and family? <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">2 Hours</span></div>
</div>
<!--end::Message In-->
<!--begin::Message Out-->
<div class="d-flex mb-5 align-items-start justify-content-start flex-row-reverse">
    <div class="d-flex align-items-center">
       
        <div class="symbol symbol-circle symbol-40 ml-4">
            <img alt="Pic" src="assets/media/users/300_21.jpg" />
        </div>
    </div>
    <div class="msg_time_mn my_msg_time_mn rounded mr-5 p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px position-relative">Hey there, we’re just writing to let you know that you’ve been subscribed to a repository on GitHub. <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">30 minutes</span></div>
</div>
<!--end::Message Out-->
<!--begin::Message In-->
<div class="d-flex mb-5 align-items-start justify-content-start flex-row-reverse">
    <div class="d-flex align-items-center">
        <div class="symbol symbol-circle symbol-40 ml-4">
            <img alt="Pic" src="assets/media/users/300_21.jpg" />
        </div>
        <!-- <div>
																	<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">Matt Pears</a>
																	<span class="text-muted font-size-sm">40 seconds</span>
																</div> -->
    </div>
    <div class="msg_time_mn my_msg_time_mn rounded mr-5 p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-left max-w-400px position-relative">Ok, Understood! <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">28 minutes</span></div>
</div>
<!--end::Message In-->
<!--begin::Message Out-->
<div class="d-flex mb-5 align-items-start justify-content-start flex-row-reverse">
    <div class="d-flex align-items-center">
        <!-- <div>
																	<span class="text-muted font-size-sm">Just now</span>
																	<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
																</div> -->
        <div class="symbol symbol-circle symbol-40 ml-4">
            <img alt="Pic" src="assets/media/users/300_21.jpg" />
        </div>
    </div>
    <div class="msg_time_mn my_msg_time_mn rounded mr-5 p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px position-relative">You’ll receive notifications for all issues, pull requests! <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">25 minutes</span></div>
</div>
<!--end::Message Out-->
<!--begin::Message In-->
<div class="d-flex mb-5 align-items-start">
    <div class="d-flex align-items-center">
        <div class="symbol symbol-circle symbol-40 mr-4">
            <img alt="Pic" src="assets/media/users/300_12.jpg" />
        </div>
        <!-- <div>
																	<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">Matt Pears</a>
																	<span class="text-muted font-size-sm">40 seconds</span>
																</div> -->
    </div>
    <div class="msg_time_mn rounded ml-5 p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px position-relative">You can unwatch this repository immediately by clicking here:
        <a href="#">https://github.com</a> <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">20 minutes</span>
    </div>
</div>
<!--end::Message In-->
<!--begin::Message Out-->
<div class="d-flex mb-5 align-items-start justify-content-start flex-row-reverse">
    <div class="d-flex align-items-center">
        <!-- <div>
																	<span class="text-muted font-size-sm">Just now</span>
																	<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
																</div> -->
        <div class="symbol symbol-circle symbol-40 ml-4">
            <img alt="Pic" src="assets/media/users/300_21.jpg" />
        </div>
    </div>
    <div class="msg_time_mn my_msg_time_mn rounded mr-5 p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px position-relative">Discover what students who viewed Learn Figma - UI/UX Design. Essential Training also viewed <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">15 minutes</span></div>
</div>
<!--end::Message Out-->
<!--begin::Message In-->
<div class="d-flex mb-5 align-items-start">
    <div class="d-flex align-items-center">
        <div class="symbol symbol-circle symbol-40 mr-4">
            <img alt="Pic" src="assets/media/users/300_12.jpg" />
        </div>
        <!-- <div>
																	<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">Matt Pears</a>
																	<span class="text-muted font-size-sm">40 seconds</span>
																</div> -->
    </div>
    <div class="msg_time_mn rounded ml-5 p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px position-relative">Most purchased Business courses during this sale! <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">40 seconds</span></div>
</div>
<!--end::Message In-->
<!--begin::Message Out-->
<div class="d-flex flex-column mb-5 align-items-start justify-content-start flex-row-reverse">
    <div class="d-flex align-items-center">
        <!-- <div>
																	<span class="text-muted font-size-sm">Just now</span>
																	<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
																</div> -->
        <div class="symbol symbol-circle symbol-40 ml-4">
            <img alt="Pic" src="assets/media/users/300_21.jpg" />
        </div>
    </div>
    <div class="msg_time_mn my_msg_time_mn rounded mr-5 p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px position-relative">Company BBQ to celebrate the last quater achievements and goals. Food and drinks provided <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">10 seconds</span></div>
</div>
<!--end::Message Out-->
<!--begin::Message In-->
<div class="d-flex mb-5 align-items-start">
    <div class="d-flex align-items-center">
        <div class="symbol symbol-circle symbol-40 mr-4">
            <img alt="Pic" src="assets/media/users/300_12.jpg" />
        </div>
        <!-- <div>
																	<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">Matt Pears</a>
																	<span class="text-muted font-size-sm">40 seconds</span>
																</div> -->
    </div>
    <div class="msg_time_mn rounded ml-5 p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px position-relative"><a href="assets/media/stock-600x400/img-68.jpg" target="_blank" title=""><img src="assets/media/stock-600x400/img-68.jpg" class="w-100 rounded" alt=""></a><span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">10 seconds</span></div>
</div>
<!--end::Message In-->
<!--begin::Message Out-->
<div class="d-flex flex-column mb-5 align-items-start justify-content-start flex-row-reverse">
    <div class="d-flex align-items-center">
        <!-- <div>
																	<span class="text-muted font-size-sm">Just now</span>
																	<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
																</div> -->
        <div class="symbol symbol-circle symbol-40 ml-4">
            <img alt="Pic" src="assets/media/users/300_21.jpg" />
        </div>
    </div>
    <div class="msg_time_mn my_msg_time_mn rounded mr-5 p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px position-relative"><a href="assets/media/stock-600x400/img-71.jpg" target="_blank" title=""><img src="assets/media/stock-600x400/img-71.jpg" class="w-50 rounded px-1" alt=""></a><a href="assets/media/stock-600x400/img-72.jpg" target="_blank" title=""><img src="assets/media/stock-600x400/img-72.jpg" class="w-50 rounded px-1" alt=""></a><span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">Just now</span></div>
</div>
<!--end::Message Out-->