<!--begin::Container-->
<style type="text/css" media="screen">
	.msg_time_mn:before {
		content: "";
		position: absolute;
		top: 0;
		left: 0;
		height: 40px;
		width: 40px;
		transform: translateX(-50%);
		border-top: 20px solid #c9f7f5;
		border-bottom: 20px solid #0000;
		border-left: 20px solid #0000;
		border-right: 20px solid #0000;
	}

	.msg_time_mn.my_msg_time_mn:before {
		left: inherit;
		right: 0;
		transform: translateX(50%);
	}

	.chat_profile_mn:hover {
		background: #F3F6F9;
	}
</style>

<div class="container">
	<!--begin::Chat-->
	<input type="hidden" id="txtUserSender" value="{{Auth::user()->name}}">
	<input type="hidden" id="txtUserReciever">
	<input type="hidden" id="from_id" value="{{Auth::user()->id}}">
	<input type="hidden" id="to_id">
	<input type="hidden" id="messageID">
	
	<!--begin::Content-->
	<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
		<!--begin::Entry-->
		<div class="d-flex flex-column-fluid">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Chat-->
				<?php 
				$recordsA = App\Models\User::where('id', Auth::user()->id)->first();

if ($recordsA->avatar == null) {
		$schLogoMe = NoImage();
	} else {
		$schLogoMe = asset('/local/public/upload/') . "/" . $recordsA->avatar;
}

				?>
				<div class="d-flex flex-row">
					<!--begin::Aside-->
					<div class="flex-row-auto offcanvas-mobile w-350px w-xl-400px" id="kt_chat_aside">
						<!--begin::Card-->
						<div class="card card-custom">
							<!--begin::Body-->
							<div class="card-body">
								<!--begin:Search-->
								<div class="input-group input-group-solid">
									<div class="input-group-prepend">
										<span class="input-group-text">
											<span class="svg-icon svg-icon-lg">
												<!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg-->
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24" />
														<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
														<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
													</g>
												</svg>
												<!--end::Svg Icon-->
											</span>
										</span>
									</div>
									<input type="text" class="form-control py-4 h-auto" placeholder="Search" />
								</div>
								<!--end:Search-->
								<!--begin:Users-->
								<div class="mt-7 scroll scroll-pull">
									<!--begin:User-->
									<?php
									$schLogo = NoImage();
									
									$records = App\Models\User::where('user_type', 2)->get();

									if(count($records)>0){
										foreach ($records as $record) {

											if ($record->avatar == null) {
												$schLogo = NoImage();
											} else {
												$schLogo = asset('/local/public/upload/') . "/" . $record->avatar;
											}
	
	
											?>
											
											
	
											<div onclick="showUserChatDetail({{$record->id}})" class="d-flex align-items-center justify-content-between chat_profile_mn border-light p-4 border-right-0 border-left-0 border-top-0" style="border: 1px solid; cursor: pointer;">
												<div class="d-flex align-items-center">
													<div class="symbol symbol-40 symbol-circle mr-4">
														<img alt="Pic" src="{{$schLogo}}">
													</div>
													<div class="d-flex flex-column">
														<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-lg">{{$record->name}}</a>
														<span class="text-muted font-weight-bold font-size-sm"></span>
													</div>
												</div>
												<div class="d-flex flex-column align-items-end">
													<span class="text-muted font-weight-bold font-size-sm">35 mins</span>
												</div>
											</div>
	
	
	
											<?php
										}
									}else{
										?>
										<h1>No School Added</h1>
										<?php

									}


									

									?>
								</div>
								<!--end:Users-->
							</div>
							<!--end::Body-->
						</div>
						<!--end::Card-->
					</div>
					<!--end::Aside-->

					<!--begin::Content-->
					<div class="flex-row-fluid ml-lg-8" id="kt_chat_content">
						<!--begin::Card-->
						
						<div class="card card-custom">
							<input type="hidden" id="txtuserLinkTo" value="{{$schLogo}}">
							<input type="hidden" id="txtuserLinkMe" value="{{$schLogoMe}}">
							<div id="ajHeader">
									<center style="margin-top:25px"> 
									<img width="200px" alt="Zelos School" src="{{getBaseURL().'/svg_logo/logo.png'}}">
								    </center>
							</div>

							<!--begin::Header-->
						
							<!--end::Header-->
							<!--begin::Body-->
							<div class="card-body bg-white">
								<!--begin::Scroll-->
								<div class="scroll scroll-pull chat_box" id="chatbox" data-mobile-height="350">
									<!--begin::Messages-->
									<div class="messages messages_display">
										<center>
										<h3>You Conversation with school appear here</h3>
										</center>
									</div>
									<!--end::Messages-->
								</div>
								<!--end::Scroll-->
							</div>
							<!--end::Body-->
							<!--begin::Footer-->
							<div class="card-footer align-items-center  bg-light" style="visibility:hidden">
								<!--begin::Compose-->
								<textarea id="txtMsgArea" class="form-control border-0 p-0 chat_boxFooter bg-light" rows="1" placeholder="Type a message"></textarea>
								<div class="d-flex align-items-center justify-content-between mt-5">
									<div class="mr-3">
										<a href="#" class="btn btn-clean btn-icon btn-md mr-1" data-toggle="modal" data-target="#img_file_select">
											<i class="icon-xl fas fa-image"></i>
										</a>

										<a href="#" class="btn btn-clean btn-icon btn-md" data-toggle="modal" data-target="#img_file_select">
											<i class="icon-xl fas fa-video"></i>
										</a>
										<a href="#" class="btn btn-clean btn-icon btn-md" data-toggle="modal" data-target="#img_file_select">
											<i class="icon-xl fas fa-file-alt"></i>
										</a>
									</div>
									<div>
										<button type="button" class="btn btn-primary btn-md text-uppercase font-weight-bold chat-send py-2 px-6 btnSendchatMessage" id="btnSendchatMessage">Send</button>
									</div>
								</div>
								<!--begin::Compose-->
							</div>

							<div class="modal fade" id="img_file_select" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" aria-hidden="true">
								<div class="modal-dialog modal-dialog modal-lg" role="document">
									<form id="formChatSuperAdmin" action="ajaxupload.php" method="post" enctype="multipart/form-data">

										<div class="modal-content bg-light">
											<div class="modal-header border-0 bg-primary">
												<h5 class="modal-title text-center w-100 text-white" id="exampleModalLabel">Preview</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<i aria-hidden="true" class="ki ki-close text-white"></i>
												</button>
											</div>
											<div class="modal-body text-center border-0">
												<div class="image-input image-input-outline" id="kt_image_3">
													<div class="image-input-wrapper" style="background-image: url({{noImage()}}); width: 200px; height: 200px;"></div>

													<label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
														<i class="fa fa-pen icon-sm text-muted"></i>
														<input type="file" name="file" accept=".png, .jpg, .jpeg" />
														<input type="hidden" name="profile_avatar_remove" />
													</label>


													
													<input type="hidden" name="to_id_model" id="to_id_model">
													<input type="hidden" name="attachment_type" value="1">
													<input name="_token" type="hidden" value="{{csrf_token()}}">

													<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar" style="left: -10px; top: -10px;">
														<i class="ki ki-bold-close icon-xs text-muted"></i>
													</span>
												</div>
												<div class="text_send_btn d-flex align-items-center justify-content-between mt-5 pt-4 px-3">
													<input type="text" class="form-control" placeholder="Add a caption..." name="message">
													<button type="submit" class="btn btn-primary font-weight-bold ml-2">Send <i class="icon-xl la la-send"></i></button>

												</div>
											</div>

										</form>
									</div>
								</div>

							</div>

							<!--end::Footer-->
						</div>
						<!--end::Card-->
					</div>
					<!--end::Content-->
				</div>
				<!--end::Chat-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Entry-->
	</div>
	<!--end::Content-->
	<!--end::Chat-->
</div>
<!--end::Container