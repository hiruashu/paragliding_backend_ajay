<!--begin::Container-->
<div class="container">
    <div class="row">
        <div class="col-lg-12">


            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Add New</h3>

                </div>
                <!--begin::Form-->
                <form data-redirect="school-list" class="form" method="post" action="444" id="ayra_kt_form_add_school">
                    <input type="hidden" name="txtAction" value="_add">

                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label>Title:</label>
                                <input name="title" id="title" type="text" class="form-control" placeholder="Enter Title" />
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label> Registration No.:</label>
                                <input type="text" id="reg_no" name="regno" class="form-control" placeholder="Enter Registration No." />
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label> Email:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fa fa-envelope-open-text text-primary mr-5"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="email" id="email" class="form-control" placeholder="Enter Email" />
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label>Country:</label>
                                <select class="form-control form-control-m form-control-solid country" id="kt_select2_1" name="country">
                                    <?php
                                    $countriesArr = DB::table('countries')
                                        ->select('id', 'name')
                                        ->get();
                                    foreach ($countriesArr as $key => $rowData) {

                                    ?>
                                        <option value="{{$rowData->id}}">{{$rowData->name}}</option>
                                    <?php

                                    }

                                    ?>

                                </select>


                                <span class="form-text text-muted"></span>
                            </div>

                        </div>
                        <div class="form-group row">

                            <div class="col-lg-3">
                                <label> State</label>
                                <select class="form-control form-control-m form-control-solid stateID" id="kt_select2_2" name="state">
                                <option value="">Select</option>

                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label> City</label>
                                <select class="form-control form-control-m form-control-solid " id="kt_select2_3" name="city">

                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label> Phone Code</label>
                                <div class="input-group">

                                    <input type="text" readonly id="txtPhoneCode" class="form-control form-control-m form-control-solid" placeholder="Phone" />
                                </div>

                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label> Phone</label>
                                <div class="input-group">

                                    <input size="30px" width="100px" name="phone" type="text" id="kt_inputmask_6" class="form-control" placeholder="Phone" />

                                </div>
                                <span class="form-text text-muted"></span>
                            </div>








                        </div>



                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-8">
                                <button type="submit" id="btnSaveSchool" data-wizard-action="submit" class="btn btn-primary mr-2">Save</button>
                                <button type="reset" class="btn btn-secondary">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Card-->

        </div>
    </div>
</div>
<!--end::Container-->