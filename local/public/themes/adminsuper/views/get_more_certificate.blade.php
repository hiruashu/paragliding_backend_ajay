<!--begin::Container-->
<div class="container">

	<!--begin::Card-->
	<div class="card card-custom">
		<div class="card-header">
			<div class="card-title">
				<span class="card-icon">
					<i class="flaticon2-supermarket text-primary"></i>
				</span>
				<h3 class="card-label">Enrolled Student by Course</h3>
			</div>
		
		</div>
		<div class="card-body">
			<table class="table table-bordered mb-6">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Course</th>
                        <th scope="col">No. of Student</th>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
				   $schoolCourse = DB::table('school_course')
				   ->where('is_deleted', 0)
				   ->where('is_active', 1)
				   ->get();
				   $i=0;
				   foreach ($schoolCourse as $key => $rowData) {
					   $cid=$rowData->id;
					   $stuCount = DB::table('school_course_student')
						->where('course_id',$cid)
						->count();

					   $i++;
					   ?>
					   <th scope="row">{{$i}}</th>
                           <td>{{$rowData->certificate_title}}</td>
                           <td>{{$stuCount}}</td>                        
                       </tr>
					   <?php
				   }

				   ?>

                    
                </tbody>
            </table>
			
		</div>
	</div>
	<!--end::Card-->
</div>
<!--end::Container-->