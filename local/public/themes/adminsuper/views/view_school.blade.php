<!--begin::Container-->
<div class="container">

    
    <?php
    $schoolArr = DB::table('schools')->where('id', $data->id)->whereNotNull('school_logo')->first();
    if ($schoolArr == null) {
        $schLogo = NoImage();
    } else {
        $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->school_logo;
    }

    ?>
    
    
    <!--begin::Card-->
    <div class="card card-custom gutter-b" style="display:block">
        <div class="card-body">
            <div class="d-flex">
                <!--begin::Pic-->
                <div class="flex-shrink-0 mr-7">
                    <div class="symbol symbol-50 symbol-lg-120">
                        <img alt="Pic" src="{{$schLogo}}">
                    </div>
                </div>
                <!--end::Pic-->
                <!--begin: Info-->
                <div class="flex-grow-1">
                    <!--begin::Title-->
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <!--begin::User-->
                        <div class="mr-3">
                            <div class="d-flex align-items-center mr-3">
                                <!--begin::Name-->
                                <a href="javascript:;" class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">{{$data->title}}</a>
                                
                               
                                <?php 
                               // print_r($data);
                                if($data->is_approved==1){
                                    ?>
                                    <i class="flaticon2-correct text-primary icon-md ml-2"></i>
                                    <?php
                                }
                                if($data->status==1){
                                    ?>
                                     
                                    <a href="javascript:;" class="font-weight-bolder font-size-h5 text-success-75 text-hover-success">Active</a>
                                    <?php
                                }else{
                                    ?>
                                     <i class="flaticon2-correct text-danger icon-md ml-2"></i>
                                    <a  href="javascript:;" class="font-weight-bolder font-size-h5 text-danger-75 text-hover-danger">Deactive</a>
                                    <?php
                                }
                                ?>
                                
                                <!--end::Name-->

                            </div>
                            <!--begin::Contacts-->
                            <div class="d-flex flex-wrap my-2">
                                <a href="#" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                    <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                        <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Communication/Mail-notification.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000"></path>
                                                <circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5"></circle>
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>{{@$data->email}}</a>
                                <a href="#" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                    <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                        <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/General/Lock.svg-->
                                        <i class="icon-1x text-dark-50 flaticon2-phone"></i>
                                        <!--end::Svg Icon-->
                                    </span>{{@$data->phone}}</a>
                                <a href="#" class="text-muted text-hover-primary font-weight-bold">
                                    <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                        <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Map/Marker2.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path d="M9.82829464,16.6565893 C7.02541569,15.7427556 5,13.1079084 5,10 C5,6.13400675 8.13400675,3 12,3 C15.8659932,3 19,6.13400675 19,10 C19,13.1079084 16.9745843,15.7427556 14.1717054,16.6565893 L12,21 L9.82829464,16.6565893 Z M12,12 C13.1045695,12 14,11.1045695 14,10 C14,8.8954305 13.1045695,8 12,8 C10.8954305,8 10,8.8954305 10,10 C10,11.1045695 10.8954305,12 12,12 Z" fill="#000000"></path>
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>{{@getCityData($data->city_id)->name}}</a>
                            </div>
                            
                            <!--end::Contacts-->
                        </div>
                        
                        <!--begin::User-->

                    </div>
                    
                    <!--end::Title-->
                    <!--begin::Content-->
                    <div class="d-flex align-items-center flex-wrap justify-content-between">
                        <!--begin::Description-->
                        <div class="flex-grow-1 font-weight-bold text-dark-50 py-2 py-lg-2 mr-5">

                            {!! @$data->about!!}
                        </div>
                        <!--end::Description-->
                        

                    </div>
                    <!--end::Content-->
                    
                </div>
                <!--end::Info-->
            </div>
        </div>
        <div style="margin-left:25px">
        <?php

$scoolArrData = DB::table('schools_slider_img')->where('sid', $data->id)->limit(5)->orderBy('id', 'desc')->get();
if (count($scoolArrData) > 0) {
    foreach ($scoolArrData as $key => $rows) {


        $schSlider = asset('/local/public/upload/') . "/" . $rows->slider_img;


?>
        <div class="image-input image-input-outline image-input" style="margin:5px" id="kt_user_avatar" style="background-image: url({{$schSlider}})">
            <div class="image-input-wrapper" style="background-image: url({{$schSlider}})"></div>
            <!-- <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" onclick="removeImage(2,{{$rows->id}})" data-action="remove" data-toggle="tooltip" title="Remove ">
                <i class="ki ki-bold-close icon-xs text-muted"></i>
            </span> -->

        </div>

<?php
    }
}


?>
        </div>
    </div>
    <!--end::Card-->
    <!--begin::Row-->
    <div class="row">
        <div class="col-xl-6">

            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header h-auto py-4">
                    <div class="card-title">
                        <h3 class="card-label">Basic Details
                            <span class="d-block text-muted pt-2 font-size-sm"></span>
                        </h3>
                    </div>

                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-4">
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Reg No.:</label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{$data->reg_no}}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Country:</label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{@getCountry($data->country_id)->name}}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">State:</label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{@getState($data->state_id)->name}}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">City:</label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{@getCityData($data->city_id)->name}}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Phone:</label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{$data->phone_code}} {{$data->phone}}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Website:</label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                <a href="{{$data->website}}">{{$data->website}}</a>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Registration Body:</label>
                        <div class="col-8">
                            <ul>
                            <?php 
                            if($data->reg_body_1 == 1){
                                ?>
                                <li>Option 1</li>
                                
                                <?php
                            }
                            if($data->reg_body_2 == 2){
                                ?>
                                <li>Option 2</li>
                                
                                <?php
                            }
                            if($data->reg_body_3 == 3){
                                ?>
                                <li>Option 3</li>
                                
                                <?php
                            }

                            ?>
                            </ul>
                           
                            
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label"><i class="icon-xl fab fa-facebook"></i></label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                <a href="{{$data->facebook}}">{{$data->facebook}}</a>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label"><i class="icon-xl fab fa-twitter"></i></label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                <a href="{{$data->twitter}}">{{$data->twitter}}</a>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label"><i class="icon-xl fab fa-linkedin"></i></label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                <a href="{{$data->linkedin}}">{{$data->linkedin}}</a>
                            </span>
                        </div>
                    </div>

                    <?php 
                    if(!empty($data->admin_comm)){
                        ?>
                        <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Commission:</label>
                        <div class="col-8">
                            <span class="form-control-plaintext">
                                <span class="label label-inline label-primary label-bold">{{$data->admin_comm}}%</span></span>
                        </div>
                    </div>
                        <?php

                    }
                    ?>
                </div>
                <!--end::Body-->

            </div>
            <!--end::Card-->
        </div>
        <div class="col-xl-6">
            <!--begin::Card-->

            <!--begin::Card-->
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">School History</h3>
                    </div>
                </div>
                <div class="card-body">
                    <!--begin::Example-->
                    <div class="example mb-10">

                        <div class="example-preview">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">S#</th>
                                        <th scope="col">Year</th>
                                        <th scope="col">Students</th>
                                        <th scope="col">Notes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $schoolHistryArr = DB::table('school_history')->where('sid',$data->id)
                                        ->get();
                                    $i = 0;
                                    if(count($schoolHistryArr)>0){

                                   
                                    foreach ($schoolHistryArr as $key => $rowData) {
                                        $i++;
                                    ?>
                                        <tr>
                                            <th scope="row">{{$i}}</th>
                                            <td>{{$rowData->school_year}}</td>
                                            <td>{{$rowData->school_students}}</td>

                                            <td>{{$rowData->school_notes}}</td>

                                        </tr>
                                    <?php
                                    }
                                }else {
                                    ?>
                                    
                                    <tr>
                                    <td>No data avaible</td>
                                    </tr>
                                    <?php
                                }

                                    ?>


                                </tbody>
                            </table>
                        </div>



                        <!--end::Example-->
                    </div>
                </div>
            </div>
            <!--end::Card-->

            <!--begin::Card-->
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">School Instructor</h3>
                    </div>
                </div>
                <div class="card-body">
                    <!--begin::Example-->
                    <div class="example mb-10">

                        <div class="example-preview">
                            <table class="table">
                                <thead>
                                <tr>
                                        <th scope="col">S#</th>
                                        <th scope="col">name</th>
                                        <th scope="col">Profile URL:</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $schoolHistryArr = DB::table('school_instructor')->where('sid',$data->id)
                                        ->get();
                                    $i = 0;
                                    if(count($schoolHistryArr)>0){
                                        foreach ($schoolHistryArr as $key => $rowData) {
                                        $i++;
                                    ?>
                                        <tr>
                                            <th scope="row">{{$i}}</th>
                                            <td>{{$rowData->name}}</td>
                                            <td><a target="_blank" href="{{$rowData->profile_url}}">{{$rowData->profile_url}}</a> </td>
                                            

                                        </tr>
                                    <?php
                                    }
                                    }else{
                                        ?>
                                        
                                        <tr>
                                        <td>No data avaible</td>
                                        </tr>
                                        <?php
                                    }

                                    ?>
                                </tbody>
                            </table>
                        </div>



                        <!--end::Example-->
                    </div>
                </div>
            </div>
            <!--end::Card-->

            <!--begin::Card-->
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">School Documents</h3>
                    </div>
                </div>
                <div class="card-body">
                    <!--begin::Example-->
                    <div class="example mb-10">

                        <div class="example-preview">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">S#</th>
                                        <th scope="col">Documents Name</th>
                                        <th scope="col">Document:</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $schoolHistryArr = DB::table('school_documents')->where('sid',$data->id)
                                        ->get();
                                    $i = 0;
                                    if(count($schoolHistryArr)>0){
                                        foreach ($schoolHistryArr as $key => $rowData) {
                                        $i++;
                                        $link = asset('/local/public/upload/') . "/" . $rowData->doc_name;
                                    ?>
                                        <tr>
                                            <th scope="row">{{$i}}</th>
                                            <td>{{$rowData->doc_info}}</td>
                                            <td>
                                            <a target="_blank" href="{{$link}}"><i class="icon-2x text-dark-50 flaticon-file-1"></i></a>
                                            
                                            </td>
                                            

                                        </tr>
                                    <?php
                                    }
                                    }else{
                                        ?>
                                        
                                        <tr>
                                        <td>No data avaible</td>
                                        </tr>
                                        <?php
                                    }
                                    

                                    ?>
                                </tbody>
                            </table>
                        </div>



                        <!--end::Example-->
                    </div>
                </div>
            </div>
            <!--end::Card-->

        </div>
    </div>
    <!--end::Row-->
</div>
<!--end::Container-->