var windowLoadAjax = {

  initA: function ( vType, day, ActionType )
  {
    //G1.Admin Payment Recived  Graph : this show all payment Recived graph approved by admin

    //ajax call
    var formData = {
      '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      'daysCount': 30,
      'days': day,
      'country_id': $( '#txtCountryUser' ).val(),
      'ActionType': ActionType
    };

    if ( vType == 1 )
    {
      var URLX = BASE_URL + '/getHighcartUsersWeekly';
      var wiseDara = "Weekly";

    }
    if ( vType == 2 )
    {
      var URLX = BASE_URL + '/getHighcartUsersMonthly';
      var wiseDara = "Month";

    }
    if ( vType == 3 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearly';
      var wiseDara = "Month wise";

    }
    if ( vType == 4 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearlyYear';
      var wiseDara = "Yearly";

    }

    var jsonDataOrder = $.ajax( {
      url: URLX,
      dataType: "json",
      type: "GET",
      data: formData,
      async: false
    } ).responseJSON;

    var colors = [];
    colors[ 0 ] = '#16426b';
    colors[ 1 ] = '#C70039';

    //ajax call
    //containerTotalOrderMonthly
    var viewX = "containerUserDataView_" + vType;

    Highcharts.chart( viewX, {
      chart: {
        type: 'line',
        height: 175,
      },
      title: {
        text: 'New user Line Graph'
      },
      xAxis: {
        categories: jsonDataOrder.MonthName,
      },
      yAxis: {
        title: {
          text: 'Students'
        },
        labels: {
          formatter: function ()
          {
            return this.value;
          }
        }
      },
      tooltip: {
        crosshairs: true,
        shared: true
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: [ {
        name: wiseDara,
        marker: {
          symbol: 'square'
        },
        data: jsonDataOrder.monthlyValue,


      } ]
    } );


    // Highcharts.chart( viewX, {
    //   chart: {
    //     height: 175,
    //     type: 'column',
    //     options3d: {
    //       enabled: false,
    //       alpha: 10,
    //       beta: 25,
    //       depth: 10
    //     }
    //   },
    //   title: {
    //     text: ''
    //   },
    //   colors: colors,
    //   subtitle: {
    //     text: ''
    //   },
    //   plotOptions: {
    //     column: {
    //       depth: 25
    //     },

    //   },
    //   xAxis: {
    //     categories: jsonDataOrder.MonthName,
    //     labels: {
    //       skew3d: true,
    //       style: {
    //         fontSize: '8x',
    //         fontFamily: 'serif'
    //       }
    //     }
    //   },
    //   yAxis: {
    //     title: {
    //       text: null,
    //       color: '#16426B'
    //     }
    //   },
    //   series: [ {
    //     name: wiseDara,
    //     data: jsonDataOrder.monthlyValue
    //   } ]
    // } );

    //containerTotalOrderMonthly



    //G1.=============================


  },
  initB: function ( vType, day, ActionType )
  {
    //G1.Admin Payment Recived  Graph : this show all payment Recived graph approved by admin

    var countryID=$("#txtCounrySchool option:selected").val();
    var sid=$("#myschool option:selected").val();
    var courseID=$("#myschoolCourse option:selected").val();



    //ajax call
    var formData = {
      '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      'daysCount': 30,
      'days': day,
      'country_id': countryID,
      'sid': sid,
      'courseID': courseID,
      'ActionType': ActionType
    };

    if ( vType == 1 )
    {
      var URLX = BASE_URL + '/getHighcartUsersWeeklyEn';
      var wiseDara = "Weekly";

    }
    if ( vType == 2 )
    {
      var URLX = BASE_URL + '/getHighcartUsersMonthlyEn';
      var wiseDara = "Month";

    }
    if ( vType == 3 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearlyEn';
      var wiseDara = "Month wise";

    }
    if ( vType == 4 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearlyYearEn';
      var wiseDara = "Year wise";

    }

    var jsonDataOrder = $.ajax( {
      url: URLX,
      dataType: "json",
      type: "GET",
      data: formData,
      async: false
    } ).responseJSON;

    var colors = [];
    colors[ 0 ] = '#16426b';
    colors[ 1 ] = '#C70039';

    //ajax call
    //containerTotalOrderMonthly
    var viewX = "containerSchoolDataView_" + vType;

    Highcharts.chart( viewX, {
      chart: {
        type: 'line',
        height: 175,
      },
      title: {
        text: 'School wise Enrolled Line Graph'
      },
      xAxis: {
        categories: jsonDataOrder.MonthName,
      },
      yAxis: {
        title: {
          text: 'Students'
        },
        labels: {
          formatter: function ()
          {
            return this.value;
          }
        }
      },
      tooltip: {
        crosshairs: true,
        shared: true
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: [ {
        name: wiseDara,
        marker: {
          symbol: 'square'
        },
        data: jsonDataOrder.monthlyValue,


      } ]
    } );

    // Highcharts.chart( viewX, {
    //   chart: {
    //     height: 175,
    //     type: 'column',
    //     options3d: {
    //       enabled: false,
    //       alpha: 10,
    //       beta: 25,
    //       depth: 10
    //     }
    //   },
    //   title: {
    //     text: ''
    //   },
    //   colors: colors,
    //   subtitle: {
    //     text: ''
    //   },
    //   plotOptions: {
    //     column: {
    //       depth: 25
    //     },

    //   },
    //   xAxis: {
    //     categories: jsonDataOrder.MonthName,
    //     labels: {
    //       skew3d: false,
    //       style: {
    //         fontSize: '8px',
    //         fontFamily: 'serif'
    //       }
    //     }
    //   },
    //   yAxis: {
    //     title: {
    //       text: null,
    //       color: '#16426B'
    //     }
    //   },
    //   series: [ {
    //     name: wiseDara,
    //     data: jsonDataOrder.monthlyValue
    //   } ]
    // } );

    //containerTotalOrderMonthly



    //G1.=============================


  },
  initC: function ()
  {
    Highcharts.chart( 'containerUserData', {
      chart: {
        type: 'line'
      },
      title: {
        text: 'Monthly User Line Grapgh'
      },
      subtitle: {
        text: 'ZELOS'
      },
      xAxis: {
        categories: [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ]
      },
      yAxis: {
        title: {
          text: 'NO'
        }
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: [ {
        name: 'Weekly',
        data: [ 7, 6, 9, 14, 18, 21, 25, 26, 23, 18, 13, 9 ]
      }, {
        name: 'Monthly',
        data: [ 3, 4, 5, 8, 11, 15, 17, 16, 14, 10, 6, 4 ]
      },
      {
        name: 'Yearly',
        data: [ 3, 4, 5, 8, 2, 14, 17, 16, 14, 10, 6, 19 ]
      }
      ]
    } );

  },
  initD: function ()
  {
    
      //ajax call
      var formData = {
        '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' ),      
       
        'country_id':$("#txtCountryPie option:selected").val(),
        'sid': $("#txtSchoolPie option:selected").val(),        
      };
      var URLX = BASE_URL + '/getPIEChartDataByCountrySchool';
     

    var jsonDataOrder = $.ajax( {
      url: URLX,
      dataType: "json",
      type: "GET",
      data: formData,
      async: false
    } ).responseJSON;
    

    Highcharts.chart('containerPIE', {
      chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie',
          height: 315,
      },
      title: {
          text: 'School Rating '
      },
      tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      accessibility: {
          point: {
              valueSuffix: '%'
          }
      },
     
      series: [{
          name: 'School Rating',
          colorByPoint: true,
          data: jsonDataOrder.data_value
      }]
  });

  },
  initD_Admin: function ()
  {
    
      //ajax call
      var formData = {
        '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' ),      
       
        'country_id':$("#txtCountryPie option:selected").val(),
        'sid': $("#txtSchoolPie option:selected").val(),        
      };
      var URLX = BASE_URL + '/getPIEChartDataByCountrySchool';
     

    var jsonDataOrder = $.ajax( {
      url: URLX,
      dataType: "json",
      type: "GET",
      data: formData,
      async: false
    } ).responseJSON;
    

    Highcharts.chart('AdmincontainerPIE', {
      chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie',
          height: 315,
      },
      title: {
          text: 'School Rating '
      },
      tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      accessibility: {
          point: {
              valueSuffix: '%'
          }
      },
     
      series: [{
          name: 'School Rating',
          colorByPoint: true,
          data: jsonDataOrder.data_value
      }]
  });

  },
  initE: function ( vType, day, ActionType )
  {
    //G1.Admin Payment Recived  Graph : this show all payment Recived graph approved by admin
    var countryID=$("#txtCounrySchoolShow option:selected").val();

    //ajax call
    var formData = {
      '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      'daysCount': 30,
      'days': day,
      'country_id': countryID,
      'ActionType': ActionType
    };

    if ( vType == 1 )
    {
      var URLX = BASE_URL + '/getHighcartUsersWeekly';
      var wiseDara = "Weekly";

    }
    if ( vType == 2 )
    {
      var URLX = BASE_URL + '/getHighcartUsersMonthly';
      var wiseDara = "Month";

    }
    if ( vType == 3 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearly';
      var wiseDara = "Month wise";

    }
    if ( vType == 4 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearlyYear';
      var wiseDara = "Year wise";

    }

    var jsonDataOrder = $.ajax( {
      url: URLX,
      dataType: "json",
      type: "GET",
      data: formData,
      async: false
    } ).responseJSON;

    var colors = [];
    colors[ 0 ] = '#16426b';
    colors[ 1 ] = '#C70039';

    //ajax call
    //containerTotalOrderMonthly
    var viewX = "containerNewSchoolDataView_" + vType;

    Highcharts.chart( viewX, {
      chart: {
        type: 'line',
        height: 175,
      },
      title: {
        text: 'New School Join Line graph'
      },
      xAxis: {
        categories: jsonDataOrder.MonthName,
      },
      yAxis: {
        title: {
          text: 'School'
        },
        labels: {
          formatter: function ()
          {
            return this.value;
          }
        }
      },
      tooltip: {
        crosshairs: true,
        shared: true
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: [ {
        name: wiseDara,
        marker: {
          symbol: 'square'
        },
        data: jsonDataOrder.monthlyValue,


      } ]
    } );

    // Highcharts.chart( viewX, {
    //   chart: {
    //     height: 175,
    //     type: 'column',
    //     options3d: {
    //       enabled: false,
    //       alpha: 10,
    //       beta: 25,
    //       depth: 10
    //     }
    //   },
    //   title: {
    //     text: ''
    //   },
    //   colors: colors,
    //   subtitle: {
    //     text: ''
    //   },
    //   plotOptions: {
    //     column: {
    //       depth: 25
    //     },

    //   },
    //   xAxis: {
    //     categories: jsonDataOrder.MonthName,
    //     labels: {
    //       skew3d: false,
    //       style: {
    //         fontSize: '8px',
    //         fontFamily: 'serif'
    //       }
    //     }
    //   },
    //   yAxis: {
    //     title: {
    //       text: null,
    //       color: '#16426B'
    //     }
    //   },
    //   series: [ {
    //     name: wiseDara,
    //     data: jsonDataOrder.monthlyValue
    //   } ]
    // } );

    //containerTotalOrderMonthly



    //G1.=============================


  },
  initF: function ( vType, day, ActionType )
  {
    //G1.Admin Payment Recived  Graph : this show all payment Recived graph approved by admin
    
    var sid=$("#txtSchoolID option:selected").val();
    var courseID=$("#txtCouserIDPay option:selected").val();
    



    //ajax call
    var formData = {
      '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      'daysCount': 30,
      'days': day,
      'courseID': courseID,
      'sid': sid,
      'ActionType': ActionType
    };

    if ( vType == 1 )
    {
      var URLX = BASE_URL + '/getHighcartUsersWeeklyAdminPay_super';
      var wiseDara = "Weekly";

    }
    if ( vType == 2 )
    {
      var URLX = BASE_URL + '/getHighcartUsersMonthlyAdminPay_super';
      var wiseDara = "Month";

    }
    if ( vType == 3 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearlyAdminPay_super';
      var wiseDara = "Month wise";

    }
    if ( vType == 4 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearlyYearAdminPay_super';
      var wiseDara = "Yearly";

    }

    var jsonDataOrder = $.ajax( {
      url: URLX,
      dataType: "json",
      type: "GET",
      data: formData,
      async: false
    } ).responseJSON;

    var colors = [];
    colors[ 0 ] = '#16426b';
    colors[ 1 ] = '#C70039';

    //ajax call
    //containerTotalOrderMonthly
    var viewX = "containerSupay_" + vType;

    Highcharts.chart( viewX, {
      chart: {
        type: 'line',
        height: 175,
      },
      title: {
        text: 'Payment Graph'
      },
      xAxis: {
        categories: jsonDataOrder.MonthName,
      },
      yAxis: {
        title: {
          text: 'School'
        },
        labels: {
          formatter: function ()
          {
            return this.value;
          }
        }
      },
      tooltip: {
        crosshairs: true,
        shared: true
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: [ {
        name: wiseDara,
        marker: {
          symbol: 'square'
        },
        data: jsonDataOrder.monthlyValue,


      } ]
    } );



  },

}




jQuery( document ).ready( function ()
{
  windowLoadAjax.initA( 1, 7, 1 );
  windowLoadAjax.initB( 1, 7, 2 );
  windowLoadAjax.initE( 1, 7, 2 );
  windowLoadAjax.initF( 1, 7, 2 );
  // windowLoadAjax.initC();
  windowLoadAjax.initD();
  windowLoadAjax.initD_Admin();
  


//txtCountryPie



//txtCountryPie
$( '.txtSchoolPie' ).on( 'change', function ()
{
  windowLoadAjax.initD();
  // ajax

} );

//txtCounrySchool
$( '.txtCounrySchool' ).on( 'change', function ()
{
  var cid = $( this ).find( ":selected" ).val();
  
  // ajax
  var formData = {
    'country_id': cid,
    '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' )
  };
  $.ajax( {
    url: BASE_URL + '/getSchoolByCountryID',
    type: 'GET',
    data: formData,
    success: function ( res )
    {
      $('#myschool').html(res);      

    }
  } );
  // ajax

} );

//txtCounrySchool

// school select enroll
$("select.myschoolEn").change(function(){
  var selectedSchool = $(this).children("option:selected").val();
  
  


    //ajax
    $.ajax({
      url: BASE_URL + "/getSchoolCourseEn",
      type: "GET",
      data: {
        _token: $('meta[name="csrf-token"]').attr("content"),
        selectedSchool: selectedSchool,
        action: 1,
       

      },
      success: function (resp) {
      

          $("select.myschoolCourseEn")
    .empty()
    .append(resp);

         
      },
    });
    //ajax


});



} );



function btnShowChartNewUser( vType, day )
{
  windowLoadAjax.initA( vType, day, 1 );

}


function btnShowChartNewSchool( vType, day )
{
  windowLoadAjax.initB( vType, day, 2 );

}
function btnShowChartNewSchool_X( vType, day )
{
  windowLoadAjax.initE( vType, day, 3 );

}
function btnShowChartNewSchool_SPay( vType, day )
{
  windowLoadAjax.initF( vType, day, 3 );

}






















