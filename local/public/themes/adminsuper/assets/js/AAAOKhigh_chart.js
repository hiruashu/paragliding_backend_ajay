var windowLoadAjax = {

  initA: function ( vType, day,ActionType )
  {
    //G1.Admin Payment Recived  Graph : this show all payment Recived graph approved by admin

    //ajax call
    var formData = {
      '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      'daysCount': 30,
      'days': day,
      'country_id': $('#txtUserYear').val(),
      'ActionType':ActionType
    };

    if ( vType == 1 )
    {
      var URLX = BASE_URL + '/getHighcartUsersWeekly';
      var wiseDara = "Weekly";

    }
    if ( vType == 2 )
    {
      var URLX = BASE_URL + '/getHighcartUsersMonthly';
      var wiseDara = "Month";

    }
    if ( vType == 3 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearly';
      var wiseDara = "Month wise";

    }
    if ( vType == 4 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearlyYear';
      var wiseDara = "Yearly";

    }

    var jsonDataOrder = $.ajax( {
      url: URLX,
      dataType: "json",
      type: "GET",
      data: formData,
      async: false
    } ).responseJSON;

    var colors = [];
    colors[ 0 ] = '#16426b';
    colors[ 1 ] = '#C70039';

    //ajax call
    //containerTotalOrderMonthly
    var viewX = "containerUserDataView_" + vType;
    Highcharts.chart( viewX, {
      chart: {
        height: 175,
        type: 'column',
        options3d: {
          enabled: false,
          alpha: 10,
          beta: 25,
          depth: 10
        }
      },
      title: {
        text: ''
      },
      colors: colors,
      subtitle: {
        text: ''
      },
      plotOptions: {
        column: {
          depth: 25
        },

      },
      xAxis: {
        categories: jsonDataOrder.MonthName,
        labels: {
          skew3d: true,
          style: {
            fontSize: '8x',
            fontFamily: 'serif'
          }
        }
      },
      yAxis: {
        title: {
          text: null,
          color: '#16426B'
        }
      },
      series: [ {
        name: wiseDara,
        data: jsonDataOrder.monthlyValue
      } ]
    } );

    //containerTotalOrderMonthly



    //G1.=============================


  },
  initB: function ( vType, day,ActionType )
  {
    //G1.Admin Payment Recived  Graph : this show all payment Recived graph approved by admin

    //ajax call
    var formData = {
      '_token': $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      'daysCount': 30,
      'days': day,
      'country_id': $('#txtUserYear').val(),
      'ActionType':ActionType
    };

    if ( vType == 1 )
    {
      var URLX = BASE_URL + '/getHighcartUsersWeekly';
      var wiseDara = "Weekly";

    }
    if ( vType == 2 )
    {
      var URLX = BASE_URL + '/getHighcartUsersMonthly';
      var wiseDara = "Month";

    }
    if ( vType == 3 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearly';
      var wiseDara = "Month wise";

    }
    if ( vType == 4 )
    {
      var URLX = BASE_URL + '/getHighcartUsersYearlyYear';
      var wiseDara = "Year wise";

    }

    var jsonDataOrder = $.ajax( {
      url: URLX,
      dataType: "json",
      type: "GET",
      data: formData,
      async: false
    } ).responseJSON;

    var colors = [];
    colors[ 0 ] = '#16426b';
    colors[ 1 ] = '#C70039';

    //ajax call
    //containerTotalOrderMonthly
    var viewX = "containerSchoolDataView_" + vType;
    Highcharts.chart( viewX, {
      chart: {
        height: 175,
        type: 'column',
        options3d: {
          enabled: false,
          alpha: 10,
          beta: 25,
          depth: 10
        }
      },
      title: {
        text: ''
      },
      colors: colors,
      subtitle: {
        text: ''
      },
      plotOptions: {
        column: {
          depth: 25
        },

      },
      xAxis: {
        categories: jsonDataOrder.MonthName,
        labels: {
          skew3d: false,
          style: {
            fontSize: '8px',
            fontFamily: 'serif'
          }
        }
      },
      yAxis: {
        title: {
          text: null,
          color: '#16426B'
        }
      },
      series: [ {
        name: wiseDara,
        data: jsonDataOrder.monthlyValue
      } ]
    } );

    //containerTotalOrderMonthly



    //G1.=============================


  },
  initC: function ()
  {
    Highcharts.chart( 'containerUserData', {
      chart: {
        type: 'line'
      },
      title: {
        text: 'Monthly User Line Grapgh'
      },
      subtitle: {
        text: 'ZELOS'
      },
      xAxis: {
        categories: [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ]
      },
      yAxis: {
        title: {
          text: 'NO'
        }
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: [ {
        name: 'Weekly',
        data: [ 7, 6, 9, 14, 18, 21, 25, 26, 23, 18, 13, 9 ]
      }, {
        name: 'Monthly',
        data: [ 3, 4, 5, 8, 11, 15, 17, 16, 14, 10, 6, 4 ]
      },
      {
        name: 'Yearly',
        data: [ 3, 4, 5, 8, 2, 14, 17, 16, 14, 10, 6, 19 ]
      }
      ]
    } );

  },

}




jQuery( document ).ready( function ()
{
  windowLoadAjax.initA( 1, 7,1 );
  windowLoadAjax.initB( 1, 7,2 );
  windowLoadAjax.initC();






} );



function btnShowChartNewUser( vType, day )
{
  windowLoadAjax.initA( vType, day,1 );

}

function btnShowChartNewSchool( vType, day )
{
  windowLoadAjax.initB( vType, day,2 );

}















