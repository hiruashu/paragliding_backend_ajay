<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//Auth::routes();
Auth::routes([
  'register' => true, // Registration Routes...
  'reset' => false, // Password Reset Routes...
  'verify' => false, // Email Verification Routes...
  'login' => false, // Email Verification Routes...
]);
//Forget password

//get state  getStateByCountryID
Route::get('/getStateByCountryID', [App\Http\Controllers\HomeController::class, 'getStateByCountryID'])->name('getStateByCountryID');
Route::get('/getCityByStateID', [App\Http\Controllers\HomeController::class, 'getCityByStateID'])->name('getCityByStateID');



Route::get('/forget-password', [App\Http\Controllers\ForgotPasswordController::class, 'getEmail'])->name('getEmail');
Route::post('/postEmail', [App\Http\Controllers\ForgotPasswordController::class, 'postEmail'])->name('postEmail');



Route::get('/reset-password/{token}', [App\Http\Controllers\ForgotPasswordController::class, 'getPassword'])->name('getPassword');
Route::post('/updatePassword', [App\Http\Controllers\ForgotPasswordController::class, 'updatePassword'])->name('updatePassword');

//custom Login 

Route::post('/customLogin', [App\Http\Controllers\Auth\LoginController::class, 'customLogin'])->name('customLogin');
Route::get('/chat_sendMessageAdmin', [App\Http\Controllers\SuperAdminController::class, 'chat_sendMessageAdmin'])->name('chat_sendMessageAdmin');

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// Route::get('/roles', [App\Http\Controllers\PermissionController::class, 'Permission'])->name('Permission');
Route::get('/getSchoolCertificatesByWeek', [App\Http\Controllers\SuperAdminController::class, 'getSchoolCertificatesByWeek'])->name('getSchoolCertificatesByWeek');
Route::group(['middleware' => 'role:superadmin'], function () {

  //  Route::get('/admin', function() { 
  //     return 'Welcome Admin';

  //  });
  Route::get('/add-school', [App\Http\Controllers\SuperAdminController::class, 'add_school'])->name('add_school');
  Route::get('/edit-school/{id}', [App\Http\Controllers\SuperAdminController::class, 'edit_school'])->name('edit_school');
  Route::get('/view-school/{id}', [App\Http\Controllers\SuperAdminController::class, 'view_school'])->name('view_school');
  Route::get('/view-school-requested/{id}', [App\Http\Controllers\SuperAdminController::class, 'view_school_requested'])->name('view_school_requested');




  Route::get('/edit-user/{id}', [App\Http\Controllers\SuperAdminController::class, 'edit_user'])->name('edit_user');
  Route::get('/view-user/{id}', [App\Http\Controllers\SuperAdminController::class, 'view_user'])->name('view_user');


  Route::post('/saveSchool', [App\Http\Controllers\SuperAdminController::class, 'saveSchool'])->name('saveSchool');
  Route::get('/school-list', [App\Http\Controllers\SuperAdminController::class, 'schoolList'])->name('schoolList');
  Route::get('/school-payments', [App\Http\Controllers\SuperAdminController::class, 'paymentHistAdminList'])->name('paymentHistAdminList');
  
  

  Route::get('/school-request-list', [App\Http\Controllers\SuperAdminController::class, 'SchoolRequestList'])->name('SchoolRequestList');



  Route::post('/uploadSchoolDoc', [App\Http\Controllers\SuperAdminController::class, 'uploadSchoolDoc'])->name('uploadSchoolDoc');
  Route::post('/uploadSchoolLogo', [App\Http\Controllers\SuperAdminController::class, 'uploadSchoolLogo'])->name('uploadSchoolLogo');
  Route::post('/uploadSchoolSlider', [App\Http\Controllers\SuperAdminController::class, 'uploadSchoolSlider'])->name('uploadSchoolSlider');
  Route::post('/uploadUserPhoto', [App\Http\Controllers\SuperAdminController::class, 'uploadUserPhoto'])->name('uploadUserPhoto');
  Route::post('/uploadChatFile', [App\Http\Controllers\SuperAdminController::class, 'uploadChatFile'])->name('uploadChatFile');
  


  Route::post('/updateSchoolHistory', [App\Http\Controllers\SuperAdminController::class, 'updateSchoolHistory'])->name('updateSchoolHistory');
  Route::post('/updateUserInterest', [App\Http\Controllers\SuperAdminController::class, 'updateUserInterest'])->name('updateUserInterest');
  Route::post('/updateUserSport', [App\Http\Controllers\SuperAdminController::class, 'updateUserSport'])->name('updateUserSport');



  Route::post('/updateSchoolInstructor', [App\Http\Controllers\SuperAdminController::class, 'updateSchoolInstructor'])->name('updateSchoolInstructor');

  Route::get('/user-list', [App\Http\Controllers\SuperAdminController::class, 'userList'])->name('userList');

  Route::get('/admin-profile', [App\Http\Controllers\SuperAdminController::class, 'admin_profile'])->name('admin_profile');
  Route::get('/add-static-content', [App\Http\Controllers\SuperAdminController::class, 'addStaticContent'])->name('addStaticContent');
  Route::get('/add-notify', [App\Http\Controllers\SuperAdminController::class, 'addNotify'])->name('addNotify');



  Route::post('/saveStaticContent', [App\Http\Controllers\SuperAdminController::class, 'saveStaticContent'])->name('saveStaticContent');
  Route::post('/saveAdminNotify', [App\Http\Controllers\SuperAdminController::class, 'saveAdminNotify'])->name('saveAdminNotify');



  Route::get('/static-content-list', [App\Http\Controllers\SuperAdminController::class, 'staticContentList'])->name('staticContentList');
  Route::get('/edit-static-content/{id}', [App\Http\Controllers\SuperAdminController::class, 'editStaticContent'])->name('editStaticContent');
  Route::get('/view-school-details/{id}', [App\Http\Controllers\SuperAdminController::class, 'viewSchoolDetails'])->name('viewSchoolDetails');
  Route::get('/getSchoolCourse', [App\Http\Controllers\SuperAdminController::class, 'getSchoolCourse'])->name('getSchoolCourse');
  Route::get('/getSchoolCourseEn', [App\Http\Controllers\SuperAdminController::class, 'getSchoolCourseEn'])->name('getSchoolCourseEn');
  Route::get('/zelosChat', [App\Http\Controllers\SuperAdminController::class, 'zelosChat'])->name('zelosChat');
  Route::get('/chat_search', [App\Http\Controllers\SuperAdminController::class, 'chat_search'])->name('chat_search');
  Route::get('/getUserChatDetails', [App\Http\Controllers\SuperAdminController::class, 'getUserChatDetails'])->name('getUserChatDetails');
  Route::get('/chat_sendMessage', [App\Http\Controllers\SuperAdminController::class, 'chat_sendMessage'])->name('chat_sendMessage');
  Route::get('/getMessageByMID', [App\Http\Controllers\SuperAdminController::class, 'getMessageByMID'])->name('getMessageByMID');





  Route::get('/getDatatableAdminUserEnrolledCouseListSuper', [App\Http\Controllers\SuperAdminController::class, 'getDatatableAdminUserEnrolledCouseListSuper'])->name('getDatatableAdminUserEnrolledCouseListSuper');
  Route::get('/getDatatableAdminUserEnrolledCouseListFilterSuper', [App\Http\Controllers\SuperAdminController::class, 'getDatatableAdminUserEnrolledCouseListFilterSuper'])->name('getDatatableAdminUserEnrolledCouseListFilterSuper');






  Route::get('/getMoreCertificate', [App\Http\Controllers\SuperAdminController::class, 'getMoreCertificate'])->name('getMoreCertificate');
  Route::get('/getEnrollSchool/{id}', [App\Http\Controllers\SuperAdminController::class, 'getEnrollSchool'])->name('getEnrollSchool');










  Route::get('/certificate-list', [App\Http\Controllers\SuperAdminController::class, 'schoolCertificateList'])->name('schoolCertificateList');
  Route::get('/school-performance-list', [App\Http\Controllers\SuperAdminController::class, 'schoolPerformanceList'])->name('schoolPerformanceList');

  Route::post('/createOrSentSchoolAccount', [App\Http\Controllers\SuperAdminController::class, 'createOrSentSchoolAccount'])->name('createOrSentSchoolAccount');
  Route::post('/useractionSchoolAccount', [App\Http\Controllers\SuperAdminController::class, 'useractionSchoolAccount'])->name('useractionSchoolAccount');
  Route::post('/useractionUserIsActive', [App\Http\Controllers\SuperAdminController::class, 'useractionUserIsActive'])->name('useractionUserIsActive');

  Route::post('/saveUserEdit', [App\Http\Controllers\SuperAdminController::class, 'saveUserEdit'])->name('saveUserEdit');
  Route::post('/saveAdminProfile', [App\Http\Controllers\SuperAdminController::class, 'saveAdminProfile'])->name('saveAdminProfile');












  Route::post('/deleteSchool', [App\Http\Controllers\SuperAdminController::class, 'deleteSchool'])->name('deleteSchool');
  Route::post('/deleteNoify', [App\Http\Controllers\SuperAdminController::class, 'deleteNoify'])->name('deleteNoify');
  Route::post('/sendNoify', [App\Http\Controllers\SuperAdminController::class, 'sendNoify'])->name('sendNoify');






  Route::post('/deleteUser', [App\Http\Controllers\SuperAdminController::class, 'deleteUser'])->name('deleteUser');
  Route::post('/UserResetPassword', [App\Http\Controllers\SuperAdminController::class, 'UserResetPassword'])->name('UserResetPassword');





  Route::post('/deleteSportInterst', [App\Http\Controllers\SuperAdminController::class, 'deleteSportInterst'])->name('deleteSportInterst');
  Route::post('/deletebyAction', [App\Http\Controllers\SuperAdminController::class, 'deletebyAction'])->name('deletebyAction');
  Route::post('/schoolAcceptedRejectAction', [App\Http\Controllers\SuperAdminController::class, 'schoolAcceptedRejectAction'])->name('schoolAcceptedRejectAction');








  Route::post('/deletImage', [App\Http\Controllers\SuperAdminController::class, 'deletImage'])->name('deletImage');
  Route::post('/saveSportIntrest', [App\Http\Controllers\SuperAdminController::class, 'saveSportIntrest'])->name('saveSportIntrest');






  //  datatable

  Route::get('/getDatatableSchoolList', [App\Http\Controllers\SuperAdminController::class, 'getDatatableSchoolList'])->name('getDatatableSchoolList');
  Route::get('/getDatatableSchoolListFilter', [App\Http\Controllers\SuperAdminController::class, 'getDatatableSchoolListFilter'])->name('getDatatableSchoolListFilter');

  

  Route::get('/getDatatableSchoolPaymentList', [App\Http\Controllers\SuperAdminController::class, 'getDatatableSchoolPaymentList'])->name('getDatatableSchoolPaymentList');
  Route::get('/view-school-payment-details/{id}', [App\Http\Controllers\SuperAdminController::class, 'viewSchoolPaymentDetails'])->name('viewSchoolPaymentDetails');
  

  


  

  Route::get('/getDatatableSchoolListData', [App\Http\Controllers\SuperAdminController::class, 'getDatatableSchoolListData'])->name('getDatatableSchoolListData');
  Route::get('/getSchoolCertificates', [App\Http\Controllers\SuperAdminController::class, 'getSchoolCertificates'])->name('getSchoolCertificates');



  Route::get('/getSchoolEntrollPayment', [App\Http\Controllers\SuperAdminController::class, 'getSchoolEntrollPayment'])->name('getSchoolEntrollPayment');
  Route::get('/getSchoolEntrollPaymentFilter', [App\Http\Controllers\SuperAdminController::class, 'getSchoolEntrollPaymentFilter'])->name('getSchoolEntrollPaymentFilter');






  Route::get('/getSchoolCertificatesBYFilter', [App\Http\Controllers\SuperAdminController::class, 'getSchoolCertificatesBYFilter'])->name('getSchoolCertificatesBYFilter');




  Route::get('/getCousePaymentByFilter', [App\Http\Controllers\SuperAdminController::class, 'getCousePaymentByFilter'])->name('getCousePaymentByFilter');
  Route::get('/getCousePaymentByFilterThisWeek', [App\Http\Controllers\SuperAdminController::class, 'getCousePaymentByFilterThisWeek'])->name('getCousePaymentByFilterThisWeek');









  Route::get('/getSchoolPerformance', [App\Http\Controllers\SuperAdminController::class, 'getSchoolPerformance'])->name('getSchoolPerformance');

  Route::get('/getSchoolPerformanceFilter', [App\Http\Controllers\SuperAdminController::class, 'getSchoolPerformanceFilter'])->name('getSchoolPerformanceFilter');
  Route::get('/getSchoolPerformanceFilterCountry', [App\Http\Controllers\SuperAdminController::class, 'getSchoolPerformanceFilterCountry'])->name('getSchoolPerformanceFilterCountry');




  Route::get('/getSchoolPerformanceFilterTop', [App\Http\Controllers\SuperAdminController::class, 'getSchoolPerformanceFilterTop'])->name('getSchoolPerformanceFilterTop');






  Route::get('/getSchoolRatingComments', [App\Http\Controllers\SuperAdminController::class, 'getSchoolRatingComments'])->name('getSchoolRatingComments');
  Route::get('/getSchoolRatingCommentsBySchool', [App\Http\Controllers\SuperAdminController::class, 'getSchoolRatingCommentsBySchool'])->name('getSchoolRatingCommentsBySchool');

  Route::get('/getSchoolByCountryID', [App\Http\Controllers\SuperAdminController::class, 'getSchoolByCountryID'])->name('getSchoolByCountryID');








  Route::get('/view-school-ratings/{id}', [App\Http\Controllers\SuperAdminController::class, 'getSchoolCommentRating'])->name('getSchoolCommentRating');











  Route::get('/getDatatableUserList', [App\Http\Controllers\SuperAdminController::class, 'getDatatableUserList'])->name('getDatatableUserList');

  Route::get('/basic-settings', [App\Http\Controllers\SuperAdminController::class, 'basicSettings'])->name('basicSettings');
  Route::get('/getHighcartUsersAddedMonthly', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersAddedMonthly'])->name('getHighcartUsersAddedMonthly');
  Route::get('/getHighcartUsersWeekly', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersWeekly'])->name('getHighcartUsersWeekly');
  Route::get('/getHighcartUsersMonthly', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersMonthly'])->name('getHighcartUsersMonthly');
  Route::get('/getHighcartUsersYearly', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersYearly'])->name('getHighcartUsersYearly');
  Route::get('/getHighcartUsersYearlyYear', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersYearlyYear'])->name('getHighcartUsersYearlyYear');
  Route::get('/getPIEChartDataByCountrySchool', [App\Http\Controllers\SuperAdminController::class, 'getPIEChartDataByCountrySchool'])->name('getPIEChartDataByCountrySchool');


  Route::get('/getHighcartUsersWeeklyEn', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersWeeklyEn'])->name('getHighcartUsersWeeklyEn');
  Route::get('/getHighcartUsersMonthlyEn', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersMonthlyEn'])->name('getHighcartUsersMonthlyEn');
  Route::get('/getHighcartUsersYearlyEn', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersYearlyEn'])->name('getHighcartUsersYearlyEn');
  Route::get('/getHighcartUsersYearlyYearEn', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersYearlyYearEn'])->name('getHighcartUsersYearlyYearEn');



  Route::get('/getHighcartUsersWeeklyAdminPay_super', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersWeeklyAdminPay_super'])->name('getHighcartUsersWeeklyAdminPay_super');
  Route::get('/getHighcartUsersMonthlyAdminPay_super', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersMonthlyAdminPay_super'])->name('getHighcartUsersMonthlyAdminPay_super');
  Route::get('/getHighcartUsersYearlyAdminPay_super', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersYearlyAdminPay_super'])->name('getHighcartUsersYearlyAdminPay_super');
  Route::get('/getHighcartUsersYearlyYearAdminPay_super', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersYearlyYearAdminPay_super'])->name('getHighcartUsersYearlyYearAdminPay_super');
  





  Route::get('/get-annoused-list', [App\Http\Controllers\SuperAdminController::class, 'getAnnoucedList'])->name('getAnnoucedList');
  Route::get('/getDatatableNotifyList', [App\Http\Controllers\SuperAdminController::class, 'getDatatableNotifyList'])->name('getDatatableNotifyList');

  Route::get('/getPaymentHistoryAdmin', [App\Http\Controllers\SuperAdminController::class, 'getPaymentHistoryAdmin'])->name('getPaymentHistoryAdmin');
  Route::get('/getCertifyByIDAdmin', [App\Http\Controllers\SuperAdminController::class, 'getCertifyByIDAdmin'])->name('getCertifyByIDAdmin');

  


  // Route::get('/load-latest-messages', 'MessagesController@getLoadLatestMessages');

  //Route::post('/send', 'MessagesController@postSendMessage');

  //Route::get('/fetch-old-messages', 'MessagesController@getOldMessages');






});




Route::group(['middleware' => 'role:admin'], function () {

  Route::get('/zelosChatAdmin', [App\Http\Controllers\AdminController::class, 'zelosChat'])->name('zelosChatAdmin');
  Route::get('/view-group-chat/{id}', [App\Http\Controllers\AdminController::class, 'zelosChatAdminGroupChat'])->name('zelosChatAdminGroupChat');
  Route::get('/edit-group-chat/{id}', [App\Http\Controllers\AdminController::class, 'zelosChatAdminGroupChatEDIT'])->name('zelosChatAdminGroupChatEDIT');
  Route::post('/saveEdit_group_chat', [App\Http\Controllers\SuperAdminController::class, 'saveEdit_group_chat'])->name('saveEdit_group_chat');

  


  

  Route::get('/zelosChatAdminGroupView', [App\Http\Controllers\AdminController::class, 'zelosChatAdminGroupView'])->name('zelosChatAdminGroupView');



  Route::get('/getUserChatDetailsSchool', [App\Http\Controllers\SuperAdminController::class, 'getUserChatDetailsSchool'])->name('getUserChatDetailsSchool');
  

  Route::get('/get-course-list', [App\Http\Controllers\SuperAdminController::class, 'schoolCourseList'])->name('schoolCourseList');
  Route::get('/getDatatableSchoolCouserList', [App\Http\Controllers\SuperAdminController::class, 'getDatatableSchoolCouserList'])->name('getDatatableSchoolCouserList');
  Route::post('/deleteSchoolCouse', [App\Http\Controllers\SuperAdminController::class, 'deleteSchoolCouse'])->name('deleteSchoolCouse');

  Route::get('/add-new-course', [App\Http\Controllers\SuperAdminController::class, 'AddSchoolCourse'])->name('AddSchoolCourse');

  Route::get('/edit-course/{user_id}/{record_id}', [App\Http\Controllers\SuperAdminController::class, 'EditSchoolCourse'])->name('EditSchoolCourse');
  Route::get('/view-course/{user_id}/{record_id}', [App\Http\Controllers\SuperAdminController::class, 'ViewSchoolCourse'])->name('ViewSchoolCourse');
  Route::get('/document/{user_id}/{record_id}', [App\Http\Controllers\SuperAdminController::class, 'documentSchoolCourse'])->name('documentSchoolCourse');


  Route::post('edit_school_course', [App\Http\Controllers\SuperAdminController::class, 'edit_school_course'])->name('edit_school_course');
  Route::post('save_school_course', [App\Http\Controllers\SuperAdminController::class, 'save_school_course'])->name('save_school_course');

  Route::get('/admin-user-payment-list', [App\Http\Controllers\SuperAdminController::class, 'AdminUserList'])->name('AdminUserList');

  Route::get('/getDatatableAdminUserList', [App\Http\Controllers\SuperAdminController::class, 'getDatatableAdminUserList'])->name('getDatatableAdminUserList');
  Route::get('/getDatatableAdminUserEnrolledCouseList', [App\Http\Controllers\SuperAdminController::class, 'getDatatableAdminUserEnrolledCouseList'])->name('getDatatableAdminUserEnrolledCouseList');
  Route::get('/getDatatableAdminUserEnrolledCouseListFilter', [App\Http\Controllers\SuperAdminController::class, 'getDatatableAdminUserEnrolledCouseListFilter'])->name('getDatatableAdminUserEnrolledCouseListFilter');
  Route::get('/getDatatableAdminGroupChatList', [App\Http\Controllers\SuperAdminController::class, 'getDatatableAdminGroupChatList'])->name('getDatatableAdminGroupChatList');






  Route::get('/admin-view-user/{id}', [App\Http\Controllers\SuperAdminController::class, 'Adminview_user'])->name('Adminview_user');
  Route::get('/add-course-to-user/{id}', [App\Http\Controllers\SuperAdminController::class, 'AddCouserToUserAdminview'])->name('AddCouserToUserAdminview');
  Route::get('/settle-due-amount/{id}', [App\Http\Controllers\SuperAdminController::class, 'settleDueAmtSchoolAdmin'])->name('settleDueAmtSchoolAdmin');
  Route::post('/saveSettleDuePaymetOfCouser', [App\Http\Controllers\SuperAdminController::class, 'saveSettleDuePaymetOfCouser'])->name('saveSettleDuePaymetOfCouser');
  Route::get('/getPaymentHistory', [App\Http\Controllers\SuperAdminController::class, 'getPaymentHistory'])->name('getPaymentHistory');
  Route::get('/getEnrolledFilter', [App\Http\Controllers\SuperAdminController::class, 'getEnrolledFilter'])->name('getEnrolledFilter');


  Route::post('/saveSchoolCourse', [App\Http\Controllers\SuperAdminController::class, 'saveSchoolCourse'])->name('saveSchoolCourse');


  Route::get('/getcityBycountry', [App\Http\Controllers\SuperAdminController::class, 'getcityBycountry'])->name('getcityBycountry');
  Route::get('/add-in-group-chat/{id}', [App\Http\Controllers\SuperAdminController::class, 'addGroupChat'])->name('addGroupChat');
  Route::post('/saveGroupcharMember', [App\Http\Controllers\SuperAdminController::class, 'saveGroupcharMember'])->name('saveGroupcharMember');

  Route::post('/uploadChatFileAdmin', [App\Http\Controllers\SuperAdminController::class, 'uploadChatFileAdmin'])->name('uploadChatFileAdmin');
  
  Route::get('/my-performance-list', [App\Http\Controllers\SuperAdminController::class, 'MyschoolPerformanceList'])->name('MyschoolPerformanceList');
  Route::get('/my-course-performance-list', [App\Http\Controllers\SuperAdminController::class, 'MyschoolCoursePerformanceList'])->name('MyschoolCoursePerformanceList');

Route::get('/getSchoolPerformanceMy', [App\Http\Controllers\SuperAdminController::class, 'getSchoolPerformanceMy'])->name('getSchoolPerformanceMy');
Route::get('/getSchoolPerformanceFilterMy', [App\Http\Controllers\SuperAdminController::class, 'getSchoolPerformanceFilterMy'])->name('getSchoolPerformanceFilterMy');


Route::get('/view-my-school-ratings/{id}', [App\Http\Controllers\SuperAdminController::class, 'getSchoolCommentRatingMy'])->name('getSchoolCommentRatingMy');
Route::get('/view-my-course-ratings/{id}', [App\Http\Controllers\SuperAdminController::class, 'getSchoolCourseCommentRatingMy'])->name('getSchoolCourseCommentRatingMy');

Route::get('/getSchoolRatingCommentsMy', [App\Http\Controllers\SuperAdminController::class, 'getSchoolRatingCommentsMy'])->name('getSchoolRatingCommentsMy');
Route::get('/getSchoolRatingCommentsMyCourse', [App\Http\Controllers\SuperAdminController::class, 'getSchoolRatingCommentsMyCourse'])->name('getSchoolRatingCommentsMyCourse');
Route::get('/getSchoolGroupchatUsers', [App\Http\Controllers\SuperAdminController::class, 'getSchoolGroupchatUsers'])->name('getSchoolGroupchatUsers');
Route::post('/removeGroupchatUser', [App\Http\Controllers\SuperAdminController::class, 'removeGroupchatUser'])->name('removeGroupchatUser');
Route::post('/deleteGroupChat', [App\Http\Controllers\SuperAdminController::class, 'deleteGroupChat'])->name('deleteGroupChat');







Route::get('/getSchoolRatingCommentsBySchoolMy', [App\Http\Controllers\SuperAdminController::class, 'getSchoolRatingCommentsBySchoolMy'])->name('getSchoolRatingCommentsBySchoolMy');
Route::get('/getSchoolRatingCommentsBySchoolMyCourse', [App\Http\Controllers\SuperAdminController::class, 'getSchoolRatingCommentsBySchoolMyCourse'])->name('getSchoolRatingCommentsBySchoolMyCourse');

Route::get('/getPIEChartDataByCountrySchoolAdmin', [App\Http\Controllers\SuperAdminController::class, 'getPIEChartDataByCountrySchoolAdmin'])->name('getPIEChartDataByCountrySchoolAdmin');
Route::get('/getPIEChartDataByCountrySchoolAdminCourse', [App\Http\Controllers\SuperAdminController::class, 'getPIEChartDataByCountrySchoolAdminCourse'])->name('getPIEChartDataByCountrySchoolAdminCourse');



  Route::get('/getHighcartUsersWeeklyAdmin', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersWeeklyAdmin'])->name('getHighcartUsersWeeklyAdmin');
  Route::get('/getHighcartUsersMonthlyAdmin', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersMonthlyAdmin'])->name('getHighcartUsersMonthlyAdmin');
  Route::get('/getHighcartUsersYearlyAdmin', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersYearlyAdmin'])->name('getHighcartUsersYearlyAdmin');
  Route::get('/getHighcartUsersYearlyYearAdmin', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersYearlyYearAdmin'])->name('getHighcartUsersYearlyYearAdmin');
  

  Route::get('/getHighcartUsersWeeklyAdminPay', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersWeeklyAdminPay'])->name('getHighcartUsersWeeklyAdminPay');
  Route::get('/getHighcartUsersMonthlyAdminPay', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersMonthlyAdminPay'])->name('getHighcartUsersMonthlyAdminPay');
  Route::get('/getHighcartUsersYearlyAdminPay', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersYearlyAdminPay'])->name('getHighcartUsersYearlyAdminPay');
  Route::get('/getHighcartUsersYearlyYearAdminPay', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersYearlyYearAdminPay'])->name('getHighcartUsersYearlyYearAdminPay');
  


  Route::get('/getHighcartUsersWeeklyEnAdmin', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersWeeklyEnAdmin'])->name('getHighcartUsersWeeklyEnAdmin');
  Route::get('/getHighcartUsersMonthlyEnAdmin', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersMonthlyEnAdmin'])->name('getHighcartUsersMonthlyEnAdmin');
  Route::get('/getHighcartUsersYearlyEnAdmin', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersYearlyEnAdmin'])->name('getHighcartUsersYearlyEnAdmin');
  Route::get('/getHighcartUsersYearlyYearEnAdmin', [App\Http\Controllers\SuperAdminController::class, 'getHighcartUsersYearlyYearEnAdmin'])->name('getHighcartUsersYearlyYearEnAdmin');

  Route::get('/profile', [App\Http\Controllers\SuperAdminController::class, 'admin_profileV1'])->name('admin_profileV1');
  Route::post('/UserResetPasswordV2', [App\Http\Controllers\SuperAdminController::class, 'UserResetPasswordV2'])->name('UserResetPasswordV2');

  Route::get('/view-user-details/{id}', [App\Http\Controllers\SuperAdminController::class, 'view_userDetails'])->name('view_userDetails');

  Route::post('/uploadSchoolCourseDoc', [App\Http\Controllers\SuperAdminController::class, 'uploadSchoolCourseDoc'])->name('uploadSchoolCourseDoc');
  Route::post('/createGroupChat', [App\Http\Controllers\SuperAdminController::class, 'createGroupChat'])->name('createGroupChat');
});













/*
Super Admin : 


Dashboard and side menu
Done Need to improve this in both panels https://prnt.sc/1s1yeee (THIS IS NOT DONE YET) we need it like only few dates visible rather then all dates.. https://prnt.sc/1s5g31l like this 

Try but nor removed need to disable by css  or need to any images on that 
Need to remove this tooltip https://prnt.sc/1s1yh4h (NEED TO PUT IMAGE)

DONE Make all 4 boxes value dynamic and proper https://prnt.sc/1s1zuij alread done (Still there are missing values and in payment if its zero then it should come 0 but it shows nothing…. Also every TITLE : VALUE (after title and before value space will come)
Now check done but need data to show 




UI WORK Need to increase width in such a way so it can be read https://prnt.sc/1s2005s




List of Schools / Edit School 

We need to show somewhere like how much details still need to fill to make school profile complete so user can understand why its still incomplete and how he can complete .. we need to decide number of things which we need to make compulsory to make profile complete …. 

In any school when we view it then 1 thing is missing which we add in edit time but in view its not visible Registration body https://prnt.sc/1s5wnqn

Also we need to make sure that school will be activated only after the commission percentage is added. And based on that it will be calculated with all payment things. 
When i active profile through that switch then also it doesnt become active because of missing details may be,  yes i will show icon  that will show what add and what missing 








































*/