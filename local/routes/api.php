<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\CEOController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::get('getSports', [AuthController::class, 'getSports']);
Route::get('getIntrest', [AuthController::class, 'getIntrest']);
Route::post('updateProfile', [AuthController::class, 'updateProfile']);
Route::post('addTrip', [AuthController::class, 'addTrip']);
Route::post('editTrip', [AuthController::class, 'editTrip']);
Route::post('getTripByUserID', [AuthController::class, 'getTripByUserID']);
Route::post('getTripDelete', [AuthController::class, 'getTripDelete']);
Route::get('getProfile', [AuthController::class, 'getProfile']);
Route::post('setUserOTP', [AuthController::class, 'setUserOTP']); // send otp on email  user id
Route::post('setUserOTPResend', [AuthController::class, 'setUserOTPResend']); // send otp on email  user id
Route::post('getUserOTPVerify', [AuthController::class, 'getUserOTPVerify']);

Route::post('getSchoolList', [AuthController::class, 'getSchoolList']);
Route::post('setFlyUser', [AuthController::class, 'setFlyUser']);
Route::post('getFlyUserFeedback', [AuthController::class, 'getFlyUserFeedback']);
Route::post('setUserPost', [AuthController::class, 'setUserPost']);
Route::post('setUserPostComment', [AuthController::class, 'setUserPostComment']);
Route::post('setUserPostLike', [AuthController::class, 'setUserPostLike']);
Route::post('getUserPostDetail', [AuthController::class, 'getUserPostDetail']);
Route::post('getUserPostDetailV1', [AuthController::class, 'getUserPostDetailV1']);

Route::post('setFriendRequest', [AuthController::class, 'setFriendRequest']);
Route::post('setFriendRequestAcceptDecline', [AuthController::class, 'setFriendRequestAcceptDecline']);
Route::post('getGroupchatBySID', [AuthController::class, 'getGroupchatBySID']); //sid user id
Route::post('getGroupchatByUserID', [AuthController::class, 'getGroupchatByUserID']); //sid user id
Route::post('getGroupchatByGroupID', [AuthController::class, 'getGroupchatByGroupID']); //sid user id
Route::post('saveMessageToGroup', [AuthController::class, 'saveMessageToGroup']); //sid user id
Route::post('getBestMatchUsers', [AuthController::class, 'getBestMatchUsers']); //sid user id
Route::get('getCountryList', [AuthController::class, 'getCountryList']); //sid user id










// New Fly API
// Param : user_id, altitude, start_lat,start_long, end_lat,end_long, name, max_altitude, elevation, wind, temperature, current_timpstamp
// Response :
// Fly_id and whole data of flying


// XCTrack Feedback API
//Param : user_id, fly_id,q1_id, q1_ans, q2_id, q2_ans, q3_id, q3_ans, q4_id, q4_ans, q5_id, q5_ans
//Whole Fly Data




// Get Post List API
// Add Post API
//user_img_url(user’s photo of each post), name, timestamp(time of when user post), description, post_img_url, list of like user for particular post (array), count of share post, list of comment user for particular user (array)


// Add Comment on each post API
// Post Like/Unlike API
// Add Friend Request API

//Route::post('login', [AuthController::class, 'login']);


Route::fallback(function () {
    return response()->json([
        'message' => 'Page Not Found. IF ... error persists, contact codexage@gmail.com'
    ], 404);
});
