<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Role;

if (!function_exists('convertLocalToUTC')) {
    function convertLocalToUTC($time)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $time, 'Asia/Kolkata')->setTimezone('UTC');
    }
}

if (!function_exists('convertUTCToLocal')) {
    function convertUTCToLocal($time)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $time, 'UTC')->setTimezone('Asia/Kolkata');
    }
}
if (!function_exists('getBaseURL')) {
    function getBaseURL()
    {
        return URL::to('/');
    }
}
if (!function_exists('getCurrentURL')) {
    function getCurrentURL()
    {
        return URL::current();
    }
}
if (!function_exists('getProjectTitle')) {
    function getProjectTitle()
    {
        return 'ZELOS';
    }
}
if (!function_exists('Ayra')) {
    function Ayra()
    {
        echo  '<pre>';
    }
}

if (!function_exists('getEnrolledStudentBySchoolID')) {
    function getEnrolledStudentBySchoolID($sid)
    {
        $usersData = DB::table('users')
            ->where('sid', $sid)
            ->where('user_type', 3)
            ->get();
       return $usersData;
    }
}


if (!function_exists('getSchoolCode')) {
    function getSchoolCode()
    {
        $basic_settings = DB::table('basic_settings')
            ->where('option_name', 'school_prefix')
            ->first();
        $max_id = DB::table('schools')->max('id') + 1;


        $num = $max_id;
        $str_length = 4;
        $prifix = $basic_settings->option_value;
        $sid_code = $prifix . substr("0000{$num}", -$str_length);
        // $sid_code = $prifix . substr("0000{$num}", -$str_length);
        return $sid_code;
    }
}

if (!function_exists('applogo')) {
    function applogo()
    {
        return URL::to('/') . "/" . "svg_logo/logo vector file1.svg";
    }
}
if (!function_exists('is_school_completed')) {
    function is_school_completed($sid)
    {
        $schoolArr = DB::table('schools')->where('id', $sid)->get();
        $i = 0;
        foreach ($schoolArr as $key => $row) {

            if ($row->title != NULL) {
                $i++;
            }
            if ($row->reg_no != NULL) {
                $i++;
            }
            if ($row->country_id != NULL) {
                $i++;
            }
            if ($row->state_id != NULL) {
                $i++;
            }
            if ($row->city_id != NULL) {
                $i++;
            }
            if ($row->phone != NULL) {
                $i++;
            }
            if ($row->admin_comm != NULL) {
                $i++;
            }
            return $i;
        }
    }
}
if (!function_exists('applogopng')) {
    function applogopng()
    {
        return URL::to('/') . "/" . "svg_logo/logo.png";
    }
}
if (!function_exists('NoImage')) {
    function NoImage()
    {
        return URL::to('/') . "/" . "NoIcon.webp";
    }
}
if (!function_exists('time_Ago')) {
    function time_Ago($time)
    {
        // Calculate difference between current
        // time and given timestamp in seconds
        $diff     = time() - $time;
          
        // Time difference in seconds
        $sec     = $diff;
          
        // Convert time difference in minutes
        $min     = round($diff / 60 );
          
        // Convert time difference in hours
        $hrs     = round($diff / 3600);
          
        // Convert time difference in days
        $days     = round($diff / 86400 );
          
        // Convert time difference in weeks
        $weeks     = round($diff / 604800);
          
        // Convert time difference in months
        $mnths     = round($diff / 2600640 );
          
        // Convert time difference in years
        $yrs     = round($diff / 31207680 );
          
        // Check for seconds
        if($sec <= 60) {
            return "$sec seconds ago";
        }
          
        // Check for minutes
        else if($min <= 60) {
            if($min==1) {
                return "one minute ago";
            }
            else {
                return "$min minutes ago";
            }
        }
          
        // Check for hours
        else if($hrs <= 24) {
            if($hrs == 1) { 
                return "an hour ago";
            }
            else {
                return "$hrs hours ago";
            }
        }
          
        // Check for days
        else if($days <= 7) {
            if($days == 1) {
                return "Yesterday";
            }
            else {
                return "$days days ago";
            }
        }
          
        // Check for weeks
        else if($weeks <= 4.3) {
            if($weeks == 1) {
                return "a week ago";
            }
            else {
                return "$weeks weeks ago";
            }
        }
          
        // Check for months
        else if($mnths <= 12) {
            if($mnths == 1) {
                return "a month ago";
            }
            else {
                return "$mnths months ago";
            }
        }
          
        // Check for years
        else {
            if($yrs == 1) {
                return "one year ago";
            }
            else {
                return "$yrs years ago";
            }
        }

    }
}

if (!function_exists('countSchool')) {
    function countSchool()
    {
        return  DB::table('schools')->where('is_deleted', 0)->count();
    }
}

if (!function_exists('user_email')) {
    function user_email()
    {
        return Auth::user()->email;
    }
}
if (!function_exists('changeDateFormate')) {
    function changeDateFormate($date, $date_format)
    {

        return \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format($date_format);
    }
}
if (!function_exists('productImagePath')) {
    function productImagePath($image_name)
    {
        return public_path('images/products/' . $image_name);
    }
}
//getUserCountByDateEn
if (!function_exists('getUserCountByDateEn')) {
    function getUserCountByDateEn($byDate, $ActionType, $country_id,$sid,$courseID)
    {

        
            $users = DB::table('schools')
            ->join('school_course', 'schools.id', '=', 'school_course.sid')
            ->join('school_course_student', 'school_course.id', '=', 'school_course_student.course_id')
            ->select('schools.*')
            ->whereDate('school_course_student.created_at', $byDate)            
            
        ->Where(function($query) use ($country_id){
            
            if(empty($country_id)){

            }else{
                $query->Where('schools.country_id', $country_id);
            }
            
        })
        ->Where(function($query) use ($sid){
            
            if(isset($sid)){
                $query->Where('schools.id', $sid);
            }else{
               
            }
            
        })
        ->Where(function($query) use ($courseID){
            
            if(isset($courseID)){
                $query->Where('school_course_student.course_id', $courseID);
            }else{
               
            }
            
        })

        ->count();
        return $users;
    
       
       
    }
}
//getUserCountByDateEn

//getUserCountByDateAdminPay
if (!function_exists('getUserCountByDateAdminPay')) {
    function getUserCountByDateAdminPay($byDate, $ActionType, $course_id)
    {
       
        $users=array();

        if ($ActionType == 1) {
            if ($course_id == null || empty($course_id)) {
                $users =  DB::table('course_payment')
                    ->whereDate('paid_on', $byDate)                   
                    ->where('sid', Auth::user()->sid)
                    //  ->where('course_id',$course_id)
                    ->sum('payment_amt');
            } else {
                $users =  DB::table('course_payment')
                    ->whereDate('paid_on', $byDate)                   
                    ->where('sid', Auth::user()->sid)
                     ->where('course_id',$course_id)
                     ->sum('payment_amt');
            }

            return $users;
        }
        
    }
}
//getUserCountByDateAdminPay

//getUserCountByDateAdmin
if (!function_exists('getUserCountByDateAdmin')) {
    function getUserCountByDateAdmin($byDate, $ActionType, $course_id)
    {
        $users=array();

        if ($ActionType == 1) {
            if ($course_id == null || empty($course_id)) {
                $users =  DB::table('school_course_student')
                    ->whereDate('created_at', $byDate)                   
                    ->where('sid', Auth::user()->sid)
                    //  ->where('course_id',$course_id)
                    ->count();
            } else {
                $users =  DB::table('school_course_student')
                    ->whereDate('created_at', $byDate)                   
                    ->where('sid', Auth::user()->sid)
                     ->where('course_id',$course_id)
                    ->count();
            }

            return $users;
        }
        
    }
}
//getUserCountByDateAdmin


if (!function_exists('getUserCountByDate')) {
    function getUserCountByDate($byDate, $ActionType, $country_id)
    {

        if ($ActionType == 1) {
            if ($country_id == null || empty($country_id)) {
                $users =  DB::table('users')
                    ->whereDate('created_at', $byDate)
                    ->where('user_type', 3)
                    // ->where('country_id',$country_id)
                    ->count();
            } else {
                $users =  DB::table('users')
                    ->whereDate('created_at', $byDate)
                    ->where('user_type', 3)
                    ->where('country_id', $country_id)
                    ->count();
            }

            return $users;
        }
        if ($ActionType == 2) {
            if ($country_id == null || empty($country_id)) {
                $users =  DB::table('schools')
                    ->whereDate('created_at', $byDate)
                    // ->where('user_type',3)
                    // ->where('country_id',$country_id)
                    ->count();
                return $users;
            } else {
                $users =  DB::table('schools')
                    ->whereDate('created_at', $byDate)
                    // ->where('user_type',3)
                    ->where('country_id', $country_id)
                    ->count();
                return $users;
            }
        }
    }
}
//getUserCountByMonthYearEn
if (!function_exists('getUserCountByMonthYearEn')) {
    function getUserCountByMonthYearEn($year, $ActionType, $country_id,$sid,$courseID)
    {
        $users = DB::table('schools')
        ->join('school_course', 'schools.id', '=', 'school_course.sid')
        ->join('school_course_student', 'school_course.id', '=', 'school_course_student.course_id')
        ->select('schools.*')
        ->whereYear('school_course_student.created_at', $year)            
        
        ->Where(function($query) use ($country_id){
            
            if(empty($country_id)){

            }else{
                $query->Where('schools.country_id', $country_id);
            }
            
        })
        ->Where(function($query) use ($sid){
            
            if(isset($sid)){
                $query->Where('schools.id', $sid);
            }else{
               
            }
            
        })
        ->Where(function($query) use ($courseID){
            
            if(isset($courseID)){
                $query->Where('school_course_student.course_id', $courseID);
            }else{
               
            }
            
        })

        ->count();
        return $users;
        
    }
}

//getUserCountByMonthYearEn

//getUserCountByMonthYearAdminPay
if (!function_exists('getUserCountByMonthYearAdminPay')) {
    function getUserCountByMonthYearAdminPay($year, $ActionType, $course_id)
    {
       
       
        if ($ActionType == 1) {
            if ($course_id == null || empty($course_id)) {
                $users =  DB::table('course_payment')
                    ->whereYear('paid_on', $year)                   
                    ->where('sid', Auth::user()->sid)
                    //  ->where('course_id',$course_id)
                    ->sum('payment_amt');
                    return $users;
            } else {
                $users =  DB::table('course_payment')
                    ->whereYear('paid_on', $year)                   
                    ->where('sid', Auth::user()->sid)
                     ->where('course_id',$course_id)
                     ->sum('payment_amt');
                     return $users;
            }

          
        }

       

    }
}
//getUserCountByMonthYearAdminPay


//getUserCountByMonthYear
//getUserCountByMonthYearAdmin
if (!function_exists('getUserCountByMonthYearAdmin')) {
    function getUserCountByMonthYearAdmin($year, $ActionType, $country_id)
    {
        if ($ActionType == 1) {
            if ($country_id == null || empty($country_id)) {
                $users =  DB::table('users')
                ->whereYear('created_at', $year)
                ->where('user_type', 3)
                ->where('sid', Auth::user()->sid)
                // ->where('country_id', $country_id)
                ->count();
            return $users;

            }else{
                $users =  DB::table('users')
                ->whereYear('created_at', $year)
                ->where('user_type', 3)
                ->where('country_id', $country_id)
                ->where('sid', Auth::user()->sid)
                ->count();
            return $users;
            }
           
        }
        if ($ActionType == 2) {
            if ($country_id == null || empty($country_id)) {
                $users =  DB::table('schools')
                ->whereYear('created_at', $year)
                ->where('sid', Auth::user()->sid)
                // ->where('user_type',3)
                // ->where('country_id', $country_id)
                ->count();
                 return $users;
            }else{
                $users =  DB::table('schools')
                ->whereYear('created_at', $year)
                ->where('sid', Auth::user()->sid)
                // ->where('user_type',3)
                ->where('country_id', $country_id)
                ->count();
                 return $users;
            }
           
        }
    }
}


//getUserCountByMonthYearAdmin

if (!function_exists('getUserCountByMonthYear')) {
    function getUserCountByMonthYear($year, $ActionType, $country_id)
    {
        if ($ActionType == 1) {
            if ($country_id == null || empty($country_id)) {
                $users =  DB::table('users')
                ->whereYear('created_at', $year)
                ->where('user_type', 3)
                // ->where('country_id', $country_id)
                ->count();
            return $users;

            }else{
                $users =  DB::table('users')
                ->whereYear('created_at', $year)
                ->where('user_type', 3)
                ->where('country_id', $country_id)
                ->count();
            return $users;
            }
           
        }
        if ($ActionType == 2) {
            if ($country_id == null || empty($country_id)) {
                $users =  DB::table('schools')
                ->whereYear('created_at', $year)
                // ->where('user_type',3)
                // ->where('country_id', $country_id)
                ->count();
                 return $users;
            }else{
                $users =  DB::table('schools')
                ->whereYear('created_at', $year)
                // ->where('user_type',3)
                ->where('country_id', $country_id)
                ->count();
                 return $users;
            }
           
        }
    }
}

//getUserCountByMonthYear
//getUserCountByMonthEn
if (!function_exists('getUserCountByMonthEn')) {
    function getUserCountByMonthEn($month, $ActionType, $country_id,$sid,$courseID)
    {
       
        $users = DB::table('schools')
        ->join('school_course', 'schools.id', '=', 'school_course.sid')
        ->join('school_course_student', 'school_course.id', '=', 'school_course_student.course_id')
        ->select('schools.*')
        ->whereMonth('school_course_student.created_at', $month)            
      

        ->Where(function($query) use ($country_id){
            
            if(empty($country_id)){

            }else{
                $query->Where('schools.country_id', $country_id);
            }
            
        })
        ->Where(function($query) use ($sid){
            
            if(isset($sid)){
                $query->Where('schools.id', $sid);
            }else{
               
            }
            
        })
        ->Where(function($query) use ($courseID){
            
            if(isset($courseID)){
                $query->Where('school_course_student.course_id', $courseID);
            }else{
               
            }
            
        })

        ->count();
        return $users;


       
        
       
       
    }
}

//getUserCountByMonthEn
// getUserCountByMonthAdminPay
if (!function_exists('getUserCountByMonthAdminPay')) {
    function getUserCountByMonthAdminPay($month, $ActionType, $course_id)
    {
      

        
       
        if ($ActionType == 1) {
            if ($course_id == null || empty($course_id)) {
                $users =  DB::table('course_payment')
                    ->whereMonth('paid_on', $month)                   
                    ->where('sid', Auth::user()->sid)
                    //  ->where('course_id',$course_id)
                    ->sum('payment_amt');
                    return $users;
            } else {
                $users =  DB::table('course_payment')
                    ->whereMonth('paid_on', $month)                   
                    ->where('sid', Auth::user()->sid)
                     ->where('course_id',$course_id)
                     ->sum('payment_amt');
                     return $users;
            }

          
        }
       
        
    }
}
// getUserCountByMonthAdminPay

//getUserCountByMonthAdmin
if (!function_exists('getUserCountByMonthAdmin')) {
    function getUserCountByMonthAdmin($month, $ActionType, $country_id)
    {
       
        if ($ActionType == 1) {
            if ($country_id == null || empty($country_id)) {
                $users =  DB::table('users')
                    ->whereMonth('created_at', $month)
                    ->where('user_type', 3)
                    ->where('sid', Auth::user()->id)
                    // ->where('country_id',$country_id)
                    ->count();
                return $users;
            } else {
                $users =  DB::table('users')
                    ->whereMonth('created_at', $month)
                    ->where('user_type', 3)
                    ->where('sid', Auth::user()->id)
                    ->where('country_id', $country_id)
                    ->count();
                return $users;
            }
        }
        if ($ActionType == 2) {
            if ($country_id == null || empty($country_id)) {
                $users =  DB::table('schools')
                    ->whereMonth('created_at', $month)
                    ->where('sid', Auth::user()->id)
                    // ->where('user_type',3)
                    // ->where('country_id', $country_id)
                    ->count();
                return $users;
            } else {
                $users =  DB::table('schools')
                    ->whereMonth('created_at', $month)
                    // ->where('user_type',3)
                    ->where('sid', Auth::user()->id)
                    ->where('country_id', $country_id)
                    ->count();
                return $users;
            }
        }
        if ($ActionType == 3) { //
            if ($country_id == null || empty($country_id)) {
                $users =  DB::table('schools')
                    ->whereMonth('created_at', $month)
                    // ->where('user_type',3)
                    ->where('sid', Auth::user()->id)
                    // ->where('country_id', $country_id)
                    ->count();
                return $users;
            } else {
                $users =  DB::table('schools')
                    ->whereMonth('created_at', $month)
                    // ->where('user_type',3)
                    ->where('sid', Auth::user()->id)
                    ->where('country_id', $country_id)
                    ->count();
                return $users;
            }
        }
    }
}

//getUserCountByMonthAdmin

if (!function_exists('getUserCountByMonth')) {
    function getUserCountByMonth($month, $ActionType, $country_id)
    {
       
        if ($ActionType == 1) {
            if ($country_id == null || empty($country_id)) {
                $users =  DB::table('users')
                    ->whereMonth('created_at', $month)
                    ->where('user_type', 3)
                    // ->where('country_id',$country_id)
                    ->count();
                return $users;
            } else {
                $users =  DB::table('users')
                    ->whereMonth('created_at', $month)
                    ->where('user_type', 3)
                    ->where('country_id', $country_id)
                    ->count();
                return $users;
            }
        }
        if ($ActionType == 2) {
            if ($country_id == null || empty($country_id)) {
                $users =  DB::table('schools')
                    ->whereMonth('created_at', $month)
                    // ->where('user_type',3)
                    // ->where('country_id', $country_id)
                    ->count();
                return $users;
            } else {
                $users =  DB::table('schools')
                    ->whereMonth('created_at', $month)
                    // ->where('user_type',3)
                    ->where('country_id', $country_id)
                    ->count();
                return $users;
            }
        }
        if ($ActionType == 3) { //
            if ($country_id == null || empty($country_id)) {
                $users =  DB::table('schools')
                    ->whereMonth('created_at', $month)
                    // ->where('user_type',3)
                    // ->where('country_id', $country_id)
                    ->count();
                return $users;
            } else {
                $users =  DB::table('schools')
                    ->whereMonth('created_at', $month)
                    // ->where('user_type',3)
                    ->where('country_id', $country_id)
                    ->count();
                return $users;
            }
        }
    }
}

if (!function_exists('getUserRole')) {
    function getUserRole()
    {
        $user = auth()->user();
        // echo $user->id;
        $roles = DB::table('users_roles')
            ->where('user_id', $user->id)
            ->first();
        $role_id = $roles->role_id;

        $roleArr = Role::find($role_id)->slug;

        return $roleArr;
    }
}
if (!function_exists('getCountry')) {
    function getCountry($country_id)
    {
        return $basic_settings = DB::table('countries')
            ->where('id', $country_id)
            ->first();
    }
}
if (!function_exists('AllCountry')) {
    function AllCountry()
    {
        return $basic_settings = DB::table('countries')

            ->get();
    }
}

if (!function_exists('getState')) {
    function getState($country_id)
    {
        return $basic_settings = DB::table('states')
            ->where('id', $country_id)
            ->first();
    }
}
if (!function_exists('getCityData')) {
    function getCityData($city_id)
    {


        return $basic_settings = DB::table('cities')

            ->where('id', $city_id)
            ->first();
    }
}



//composer dump-autoload
