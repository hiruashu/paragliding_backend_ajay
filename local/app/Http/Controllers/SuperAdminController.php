<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Theme;
use DB;
use Auth;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Mail;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Pusher;

class SuperAdminController extends Controller
{




    //saveEdit_group_chat
    public function saveEdit_group_chat(Request $request)
    {
        // print_r($request->all());
        $id = $request->txtID;
        $affected = DB::table('chat_group')
            ->where('id', $id,)
            ->update([
                'group_name' => $request->group_name,
                'group_title' => $request->group_title
            ]);


        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = $id . "_group_img_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
            // save to local/public/uploads/photo/ as the new $filename
            //var/www/larachat/local/public/storage/users-avatar
            $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);




            $affected = DB::table('chat_group')
                ->where('id', $id,)
                ->update(['photo' => $filename]);
        }
        return redirect()->route('zelosChatAdminGroupView')->withSuccess(['Member Added Successfully!']);
    }
    //saveEdit_group_chat


    //saveGroupcharMember
    public function saveGroupcharMember(Request $request)
    {

        if (isset($request->userArr) > 0) {
            foreach ($request->userArr as $key => $rowID) {

                DB::table('group_chat_users')->insert([
                    'sid' => Auth::user()->sid,
                    'gid' => $request->txtGID,
                    'created_at' => date('Y-m-d H:i:s'),
                    'user_id' => $rowID,
                    'created_by' => Auth::user()->sid,

                ]);
            }
        }
        //DB::table('group_chat_users')->where('gid', '=',  $request->txtGID)->delete();


        return redirect()->route('zelosChatAdminGroupView')->withSuccess(['Member Added Successfully!']);

        //return redirect()->back()->with('success', 'Member Added Successfully');
    }
    //saveGroupcharMember

    //addGroupChat
    public function addGroupChat($id)
    {
        $usersData = DB::table('chat_group')->where('id', $id)->first();




        $theme = Theme::uses('admin')->layout('layout');

        $data = ["data" => $usersData];
        return $theme->scope('add_member_chat_group', $data)->render();
    }

    //addGroupChat

    //createGroupChat
    public function createGroupChat(Request $request)
    {
        $schoolArr = DB::table('chat_group')
            ->where('sid', Auth::user()->sid)
            ->where('group_name', $request->txtGroupName)->first();
        if ($schoolArr == null) {

            DB::table('chat_group')->insert([
                'sid' => Auth::user()->sid,
                'created_by' => Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'group_name' => $request->txtGroupName,
                'group_title' => $request->txtGroupTitle,
                'status' => 1,
            ]);
            $res_arr = array(
                'status' => 1,
                'msg' => 'Created successfully.',
            );
        } else {

            DB::table('chat_group')->insert([
                'sid' => Auth::user()->sid,
                'created_by' => Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'group_name' => $request->txtGroupName,
                'group_title' => $request->txtGroupTitle,
                'status' => 1,
            ]);
            $res_arr = array(
                'status' => 2,
                'msg' => 'Already group exits',
            );
        }

        return response()->json($res_arr);
    }
    //createGroupChat
    //getcityBycountry
    public function getcityBycountry(Request $request)
    {
        $country_id = $request->country_id;
        $cityArr = DB::table('cities')
            ->where('country_id', $country_id)->get();
        $HTML = "";
        foreach ($cityArr as $key => $rowData) {
            $HTML .= '<option value="' . $rowData->id . '">' . $rowData->name . '</option>';
        }
        echo $HTML;
    }
    //getcityBycountry

    //save_school_course
    public function save_school_course(Request $request)
    {
        $record_id = Auth::user()->sid;
        $schoolArr = DB::table('schools')
            ->where('id', $record_id)->first();


        DB::table('school_course')->insert([
            'certificate_title' => $request->certificate_title,
            'is_active' => $request->is_active,
            'sid' => $record_id,
            'school_name' => $schoolArr->title,
            'course_date' => date('Y-m-d H:i:s'),
            'regno' => $request->regno,
            'course_info' => $request->course_info,
            'duration' => $request->duration,
            'down_payment_per' => $request->down_payment_per,
            'course_amt' => $request->course_amt,
            'country_id' => $request->country,
            'city_id' => $request->city,
            'certified_by' => json_encode($request->certified_by),

        ]);
        $lid = DB::getPdo()->lastInsertId();

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = $record_id . "_user_course_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
            // save to local/public/uploads/photo/ as the new $filename
            //var/www/larachat/local/public/storage/users-avatar
            $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);



            $affected = DB::table('school_course')
                ->where('id', $lid,)
                ->update(['image' => $filename]);
        }


        return redirect()->back()->with('success', 'Course Added Successfully');
    }

    //save_school_course


    //edit_school_course
    public function edit_school_course(Request $request)
    {
        $record_id = $request->txtID;

        $affected = DB::table('school_course')
            ->where('id', $record_id)
            ->update([
                'certificate_title' => $request->certificate_title,
                'is_active' => $request->is_active,
                'regno' => $request->regno,
                'course_info' => $request->course_info,
                'duration' => $request->duration,
                'down_payment_per' => $request->down_payment_per,
                'course_amt' => $request->course_amt,
                'country_id' => $request->country,
                'city_id' => $request->city,

            ]);


        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = $record_id . "_user_course_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
            // save to local/public/uploads/photo/ as the new $filename
            //var/www/larachat/local/public/storage/users-avatar

            $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);


            $affected = DB::table('school_course')
                ->where('id', $record_id,)
                ->update(['image' => $filename]);
        }

        return redirect()->back()->with('success', 'Course Updated Successfully');
    }
    //edit_school_course

    //documentSchoolCourse
    public function documentSchoolCourse($user_id, $record_id)
    {
        $usersData = DB::table('users')->where('id', $user_id)->first();

        $usersDa = DB::table('school_course')->where('sid', $usersData->sid)->where('id', $record_id)->first();
        $theme = Theme::uses('admin')->layout('layout');
        $data = ["data" => $usersDa];
        return $theme->scope('document_school_course', $data)->render();
    }

    //documentSchoolCourse

    //ViewSchoolCourse
    public function ViewSchoolCourse($user_id, $record_id)
    {
        $usersData = DB::table('users')->where('id', $user_id)->first();

        $usersDa = DB::table('school_course')->where('sid', $usersData->sid)->where('id', $record_id)->first();


        $theme = Theme::uses('admin')->layout('layout');

        $data = ["data" => $usersDa];
        return $theme->scope('view_school_course', $data)->render();
    }

    //ViewSchoolCourse

    //EditSchoolCourse
    public function EditSchoolCourse($user_id, $record_id)
    {
        $usersData = DB::table('users')->where('id', $user_id)->first();

        $usersDa = DB::table('school_course')->where('sid', $usersData->sid)->where('id', $record_id)->first();


        $theme = Theme::uses('admin')->layout('layout');

        $data = ["data" => $usersDa];
        return $theme->scope('edit_school_course', $data)->render();
    }

    //EditSchoolCourse
    ////pusher 
    /*
    $options = array(
      'cluster' => 'ap2',
      'useTLS' => true
    );
    $pusher = new Pusher\Pusher(
      '9dfaf98953e291c9be80',
      '79bcb9731c4b2951e422',
      '1057116',
      $options
    );

    $eventID = 'AJ_ID' . $users->created_by;

    $data['message'] = 'Invoice is upload of client:' . $name . "-" . $compName . "<br>Account Message : " . ucwords($request->notes);

    $pusher->trigger('BO_CHANNEL', $eventID, $data);
*/
    //puhser 


    //getMessageByMID
    public function getMessageByMID(Request $request)
    {
        $mid = $request->mid;
        $messageArr = DB::table('messages')
            ->where('id', $mid)
            ->first();
        return response()->json([
            'messageData' => $messageArr,
            'addData' => 'html',
        ], 200);
    }
    //getMessageByMID
    public function chat_sendMessageAdmin(Request $request)
    {
        $options = array(

            'cluster' => 'ap2',

            'encrypted' => true

        );

        $pusher = new Pusher\Pusher(

            '4c1630c06a480f5f76de',

            '6c1b90583017db3293f9',

            '1239507',

            $options

        );
        $to_id = $request->to_id;
        $group_id = $request->to_id;
        $group_chat = $request->group_chat;
        if ($group_chat == 1) {
            $group_id = $request->to_id;
        } else {
            $group_id = null;
        }

        $users = DB::table('users')
            ->where('id', Auth::user()->id)
            ->first();
        if ($users->avatar == null) {
            $schLogo = NoImage();
        } else {
            $schLogo = asset('/local/public/upload/') . "/" . $users->avatar;
        }
        $message = $request->message;
        $name = $users->name;




        $chat_data = array(
            'user_id' => $users->id,
            'name' => $users->name,
            'body' => $message,
            'attachment' => null,
            'seen' => 0,
            'created_at' => date('Y-m-d H:i:s'),
            'doc_type' => $request->doc_type,
            'message_id' => '',
            'group_id' => $to_id,
            "g_id" => $to_id,
            "to_id" => $to_id,
            'msg' => $message,
            'photo' => $schLogo,
            'doc_type' => 55,

        );
        $zelosEvent = 'zelosUser_' . $to_id;
        if ($group_chat == 1) {
            if ($pusher->trigger('test_channel_group', $zelosEvent, $chat_data)) {

                // echo 'success';
                // print_r($request->all());
                $messageID = Auth::user()->id . mt_rand(9, 999999999) ;

                DB::table('messages')->insert([
                    'message_id' => $messageID,
                    'type' => 'user',
                    'from_id' => Auth::user()->id,
                    'to_id' => $request->to_id,
                    'body' => $request->message,
                    'group_id' => $group_id,
                    'created_at' => date('Y-m-d H:i:s'),

                ]);


                return response()->json([
                    'messageID' => $messageID,
                    'addData' => 'html',
                    'mob_message' => $message
                ], 200);
            } else {

                echo 'error';
            }
        } else {
            if ($pusher->trigger('test_channel', $zelosEvent, $chat_data)) {

                // echo 'success';
                // print_r($request->all());
                $messageID = Auth::user()->id . mt_rand(9, 999999999) ;

                DB::table('messages')->insert([
                    'message_id' => $messageID,
                    'type' => 'user',
                    'from_id' => Auth::user()->id,
                    'to_id' => $request->to_id,
                    'body' => $request->message,
                    'group_id' => $group_id,
                    'created_at' => date('Y-m-d H:i:s'),

                ]);
                DB::table('tbl_notify')->insert([
                    'noti_type' => 2,
                    'noti_name' => Auth::user()->name,
                    'noti_message' => $request->message,
                    'created_by' => Auth::user()->id


                ]);

                return response()->json([
                    'messageID' => $messageID,
                    'addData' => 'html',
                    'mob_message' => $message
                ], 200);
            } else {

                echo 'error';
            }
        }
    }
    //chat_sendMessageAdmin
    public function chat_sendMessageAdmin_1(Request $request)
    {


        $options = array(

            'cluster' => 'ap2',

            'encrypted' => true

        );

        $pusher = new Pusher\Pusher(

            '4c1630c06a480f5f76de',

            '6c1b90583017db3293f9',

            '1239507',

            $options

        );
        $to_id = $request->to_id;
        $group_id = $to_id;

        $users = DB::table('users')
            ->where('id', Auth::user()->id)
            ->first();
        if ($users->avatar == null) {
            $schLogo = NoImage();
        } else {
            $schLogo = asset('/local/public/upload/') . "/" . $users->avatar;
        }



        $message = $request->message;

        $HTML = '<div class="d-flex flex-column mb-5 align-items-start">
        <div class="d-flex align-items-center">
            <div class="symbol symbol-circle symbol-40 mr-3">
                <img alt="Pic" src="' . $schLogo . '" />
            </div>
            <div>
                <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">' . $users->name . '</a>
                <span class="text-muted font-size-sm">Just Now</span>
            </div>
        </div>
        <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">
        ' . $message . '
        </div>
    </div>';







        $chat_data = array(
            'user_id' => $users->id,
            'name' => $users->name,
            'body' => $message,
            'attachment' => null,
            'seen' => 0,
            'created_at' => date('Y-m-d H:i:s'),
            'doc_type' => $request->doc_type,
            'message_id' => '',
            'group_id' => $group_id,
            "g_id" => $group_id,
            'msg' => $message,
            'photo' => $schLogo,



        );






        $zelosEvent = 'zelosUser_' . $to_id;
        if ($pusher->trigger('test_channel', $zelosEvent, $HTML)) {

            // echo 'success';
            // print_r($request->all());
            $messageID = Auth::user()->id . mt_rand(9, 999999999) ;

            DB::table('messages')->insert([
                'message_id' => $messageID,
                'type' => 'user',
                'from_id' => Auth::user()->id,
                'to_id' => $request->to_id,
                'body' => $request->message,
                'group_id' => $group_id,
                'created_at' => date('Y-m-d H:i:s'),

            ]);
            return response()->json([
                'messageID' => $messageID,
                'addData' => 'html',
                'mob_message' => $message
            ], 200);
        } else {

            echo 'error';
        }
    }
    //chat_sendMessageAdmin

    //chat_sendMessage
    public function chat_sendMessage(Request $request)
    {


        $options = array(

            'cluster' => 'ap2',

            'encrypted' => true

        );

        $pusher = new Pusher\Pusher(

            '4c1630c06a480f5f76de',

            '6c1b90583017db3293f9',

            '1239507',

            $options

        );
        $to_id = $request->to_id;
        $message = $request->message;
        $zelosEvent = 'zelosUser_' . $to_id;
        if ($pusher->trigger('test_channel8', $zelosEvent, 'message99')) {

            //echo 'success';
            $messageID = Auth::user()->id . mt_rand(9, 999999999) ;

            DB::table('messages')->insert([
                'message_id' => $messageID,
                'type' => 'user',
                'from_id' => Auth::user()->id,
                'to_id' => $request->to_id,
                'body' => $request->message,
                'created_at' => date('Y-m-d H:i:s'),

            ]);
            return response()->json([
                'messageID' => $messageID,
                'addData' => 'html',
            ], 200);
        } else {

            echo 'error';
        }



        // // print_r($request->all());
        // $messageID = Auth::user()->id.mt_rand(9, 999999999) ;

        // DB::table('messages')->insert([
        //     'id' => $messageID,
        //     'type' => 'user',
        //     'from_id' => $request->from_id,
        //     'to_id' => $request->to_id,
        //     'body' => $request->bodyMessage,
        //     'body' => $request->bodyMessage,
        // ]);
        // return response()->json([
        //     'messageID' =>$messageID,
        //     'addData' => 'html',
        // ],200);

    }
    //chat_sendMessage
    //getUserChatDetailsSchool
    public function getUserChatDetailsSchool(Request $request)
    {
        $user_id = $request->user_id;
        $records = User::where('id', $user_id)->first();
        if ($records->avatar == null) {
            $schLogo = NoImage();
        } else {
            $schLogo = asset('/local/public/upload/') . "/" . $records->avatar;
        }

        $userData = array(
            'name' => $records->name,
            'user_id' => $records->id,
            'photo' => $schLogo,
        );
        return response()->json([
            'user_data' => $userData,
            'addData' => 'html',
        ], 200);
    }

    //getUserChatDetailsSchool


    //getUserChatDetails
    public function getUserChatDetails(Request $request)
    {
        $user_id = $request->user_id;
        $records = User::where('id', $user_id)->first();
        if ($records->avatar == null) {
            $schLogo = NoImage();
        } else {
            $schLogo = asset('/local/public/upload/') . "/" . $records->avatar;
        }

        $userData = array(
            'name' => $records->name,
            'user_id' => $records->id,
            'photo' => $schLogo,
        );

        $to_id = $user_id;

        $messages = \App\Models\Message::where(function ($query) use ($to_id) {
            $query->where('from_id', Auth::user()->id)->where('to_id', $to_id);
        })->orWhere(function ($query) use ($to_id) {
            $query->where('from_id', $to_id)->where('to_id', Auth::user()->id);
        })->orderBy('created_at', 'ASC')->get();

        $HTML = '';
        foreach ($messages as $key => $rowData) {
            $curr_time = $rowData->created_at;
            $time_ago = strtotime($curr_time);
            $time_agoView = time_Ago($time_ago) . "\n";

            if ($rowData->from_id != Auth::user()->id) {
                //user  
                $recordsUSer = \App\Models\User::where('id', $rowData->from_id)->first();

                if ($recordsUSer->avatar == null) {
                    $schLogoU = NoImage();
                } else {
                    $schLogoU = asset('/local/public/upload/') . "/" . $recordsUSer->avatar;
                }

?>
												<?php
                                                if (empty($rowData->attachment)) { //text message


                                                    $HTML .= '<div class="d-flex flex-column mb-5 align-items-start">
														<div class="d-flex align-items-center">
															<div class="symbol symbol-circle symbol-40 mr-3">
																<img alt="Pic" src="' . $schLogoU . '" />
															</div>
															<div>
																<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">' . $recordsUSer->name . '</a>
																<span class="text-muted font-size-sm">' . $time_agoView . '</span>
															</div>
														</div>
														<div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">
															' . $rowData->body . '
														</div>
													</div>';
                                                }
                                                if ($rowData->attachment_type == 1) {
                                                    $imgDoc = asset('/local/public/upload/') . "/" . $rowData->attachment;


                                                    $HTML .= '<div class="d-flex flex-column mb-5 align-items-start">
                                                    <div class="d-flex align-items-center">
                                                    <div class="symbol symbol-circle symbol-40 mr-3">
                                                        <img alt="Pic" src="' . $schLogoU . '" />
                                                    </div>
                                                    <div>
                                                        <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">' . $recordsUSer->name . '</a>
                                                        <span class="text-muted font-size-sm">' . $time_agoView . '</span>
                                                    </div>
                                                </div>
														<div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
															<a href="' . $imgDoc . '" target="_blank" title="" class=" text-dark-50 font-weight-bold font-size-lg">
																<img src="' . $imgDoc . '" class="w-100 rounded" alt=""> 
															</a>
															<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">' . $rowData->body . '</span>
														</div>
													</div>';
                                                }
                                                if ($rowData->attachment_type == 2) {
                                                    $imgDoc = asset('/local/public/upload/') . "/" . $rowData->attachment;

                                                    $HTML .= '<div class="d-flex flex-column mb-5 align-items-start">
                                                    <div class="d-flex align-items-center">
                                                    <div class="symbol symbol-circle symbol-40 mr-3">
                                                        <img alt="Pic" src="' . $schLogoU . '" />
                                                    </div>
                                                    <div>
                                                        <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">' . $recordsUSer->name . '</a>
                                                        <span class="text-muted font-size-sm">' . $time_agoView . '</span>
                                                    </div>
                                                </div>
														<div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
															<a href="' . $imgDoc . '" target="_blank" title="" class=" text-dark-50 font-weight-bold font-size-lg">
																<video width="250" height="150" src="' . $imgDoc . '" controls>
																	Sorry, your browser does nt support HTML5 <code>video</code>
																  </video>
															</a>
															<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">' . $rowData->body . '</span>
														</div>
													</div>';
                                                }
                                                if ($rowData->attachment_type == 3) {
                                                    $imgDoc = asset('/local/public/upload/') . "/" . $rowData->attachment;

                                                    $HTML .= '<div class="d-flex flex-column mb-5 align-items-start">
                                                    <div class="d-flex align-items-center">
                                                    <div class="symbol symbol-circle symbol-40 mr-3">
                                                        <img alt="Pic" src="' . $schLogoU . '" />
                                                    </div>
                                                    <div>
                                                        <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">' . $recordsUSer->name . '</a>
                                                        <span class="text-muted font-size-sm">' . $time_agoView . '</span>
                                                    </div>
                                                </div>
														<div class="msg_time_mn rounded ml-5 p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px position-relative">
															<a href="' . $imgDoc . '" target="_blank" title="" style="width: 300px;" class="d-flex align-items-center justify-content-between bg-white p-4 rounded">
																<p class="mb-0">abcd.docx</p>
																<i class="icon-xl fas fa-arrow-alt-circle-down ml-5 text-primary"></i>
															</a>
															
															<span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">' . $rowData->body . '</span>
														</div>
													</div>';
                                                }
                                            } else {
                                                //me
                                                $recordsMe = User::where('id', Auth::user()->id)->first();
                                                if ($recordsMe->avatar == null) {
                                                    $schLogoMe = NoImage();
                                                } else {
                                                    $schLogoMe = asset('/local/public/upload/') . "/" . $recordsMe->avatar;
                                                }

                                                if (empty($rowData->attachment)) { //text message


                                                    $HTML .= '<div class="d-flex flex-column mb-5 align-items-end">
                        <div class="d-flex align-items-center">
                            <div>
                                <span class="text-muted font-size-sm">' . $time_agoView . '</span>
                                <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
                            </div>
                            <div class="symbol symbol-circle symbol-40 ml-3">
                                <img alt="Pic" src="' . $schLogoMe . '" />
                            </div>
                        </div>
                        <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px">
                            ' . $rowData->body . '
                        </div>
                    </div>';
                                                }
                                                // if attchement type is image
                                                if ($rowData->attachment_type == 1) {
                                                    $imgDoc = asset('/local/public/upload/') . "/" . $rowData->attachment;

                                                    $HTML .= '<div class="d-flex flex-column mb-5 align-items-end">
                    <div class="d-flex align-items-center">
                    <div>
                        <span class="text-muted font-size-sm">' . $time_agoView . '</span>
                        <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
                    </div>
                    <div class="symbol symbol-circle symbol-40 ml-3">
                        <img alt="Pic" src="' . $schLogoMe . '" />
                    </div>
                     </div>
                        <div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-200px position-relative">
                            <a href="' . $imgDoc . '" target="_blank" title="" class=" text-dark-50 font-weight-bold font-size-lg">
                                <img src="' . $imgDoc . '" class="w-100 rounded" alt=""> 
                            </a>
                            <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">' . $rowData->body . '</span>
                        </div>
                    </div>';
                                                }
                                                //if attchement tyoe is image 
                                                if ($rowData->attachment_type == 2) {
                                                    $imgDoc = asset('/local/public/upload/') . "/" . $rowData->attachment;

                                                    $HTML .= '<div class="d-flex flex-column mb-5 align-items-end">
                    <div class="d-flex align-items-center">
                    <div>
                        <span class="text-muted font-size-sm">' . $time_agoView . '</span>
                        <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
                    </div>
                    <div class="symbol symbol-circle symbol-40 ml-3">
                        <img alt="Pic" src="' . $schLogoMe . '" />
                    </div>
                </div>
                        <div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
                            <a href="' . $imgDoc . '" target="_blank" title="" class=" text-dark-50 font-weight-bold font-size-lg">
                                <video width="250" height="150" src="' . $imgDoc . '" controls>
                                    Sorry, your browser does not support HTML5 <code>video</code>
                                  </video>
                            </a>
                            <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">' . $rowData->body . '</span>
                        </div>
                    </div>';
                                                }
                                                if ($rowData->attachment_type == 3) {
                                                    $imgDoc = asset('/local/public/upload/') . "/" . $rowData->attachment;

                                                    $HTML .= '<div class="d-flex flex-column mb-5 align-items-end">
                    <div class="d-flex align-items-center">
                    <div>
                        <span class="text-muted font-size-sm">' . $time_agoView . '</span>
                        <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
                    </div>
                    <div class="symbol symbol-circle symbol-40 ml-3">
                        <img alt="Pic" src="' . $schLogoMe . '" />
                    </div>
                </div>
                        <div class="msg_time_mn rounded ml-5 p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px position-relative">
                            <a href="' . $imgDoc . '" target="_blank" title="" style="width: 300px;" class="d-flex align-items-center justify-content-between bg-white p-4 rounded">
                                <p class="mb-0">document</p>
                                <i class="icon-xl fas fa-arrow-alt-circle-down ml-5 text-primary"></i>
                            </a>
                            
                            <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">' . $rowData->body . '</span>
                        </div>
                    </div>';
                                                }
                                            }
                                        }



                                        return response()->json([
                                            'user_data' => $userData,
                                            'htmlChat' => $HTML,
                                            'addData' => 'html',
                                        ], 200);
                                    }
                                    //getUserChatDetails


                                    //chat_search
                                    public function chat_search(Request $request)
                                    {
                                        $getRecords = null;
                                        $input = trim(filter_var($request['input'], FILTER_SANITIZE_STRING));
                                        $records = User::where('name', 'LIKE', "%{$input}%");
                                        foreach ($records->get() as $record) {
                                            if ($record->avatar == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $record->avatar;
                                            }

                                            $getRecords .= '<a href="javascript:void(0)" onclick="showUserChatDetail(' . $record->id . ')" ><div class="d-flex align-items-center justify-content-between mb-5">
        <div class="d-flex align-items-center">
            <div class="symbol symbol-circle symbol-50 mr-3">
                <img alt="" src="' . $schLogo . '" />
            </div>
            <div class="d-flex flex-column">
                <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-lg">' . $record->name . '</a>
                <span class="text-muted font-weight-bold font-size-sm"></span>
            </div>
        </div>
        <div class="d-flex flex-column align-items-end">
            <span class="text-muted font-weight-bold font-size-sm">35 mins</span>
        </div>
    </div></a>';
                                        }
                                        // send the response
                                        return response()->json([
                                            'records' => $records->count() > 0 ? $getRecords : '<p class="message-hint"><span>Nothing to show.</span></p>',
                                            'addData' => 'html',
                                        ], 200);
                                    }
                                    //chat_search

                                    //zelosChat
                                    public function zelosChat(Request $request)
                                    {
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('zelos_chat_admin', $data)->render();
                                    }
                                    //zelosChat

                                    //getSchoolByCountryID
                                    public function getSchoolByCountryID(Request $request)
                                    {
                                        $country_id = $request->country_id;


                                        $school_arr = DB::table('schools')->where('status', 1)->where('is_deleted', 0)->where('country_id', $country_id)->orderBy('id', 'DESC')->get();



                                        $HTML = '<option value="">-SELECT-</option>';

                                        foreach ($school_arr as $key => $row) {

                                            $HTML .= '<option value="' . $row->id . '">' . $row->title . '</option>';
                                        }

                                        echo $HTML;
                                    }
                                    //getSchoolByCountryID

                                    //getDatatableNotifyList
                                    //getDatatableNotifyList

                                    //getAnnoucedList
                                    //getAnnoucedList
                                    public function getAnnoucedList()
                                    {
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('get_annused_list', $data)->render();
                                    }
                                    //getPIEChartDataByCountrySchoolAdminCourse
                                    public function getPIEChartDataByCountrySchoolAdminCourse(Request $request)
                                    {
                                        $monthlyVal = array();
                                        //course_id

                                        $rating_1 = DB::table('school_user_rating_course')
                                            ->where('rating_val', 1)
                                            ->where('sid', Auth::user()->sid)
                                            ->where('course_id', $request->course_id)
                                            ->count();
                                        $rating_2 = DB::table('school_user_rating_course')
                                            ->where('rating_val', 2)
                                            ->where('sid', Auth::user()->sid)
                                            ->where('course_id', $request->course_id)
                                            ->count();
                                        $rating_3 = DB::table('school_user_rating_course')
                                            ->where('rating_val', 3)
                                            ->where('sid', Auth::user()->sid)
                                            ->where('course_id', $request->course_id)
                                            ->count();
                                        $rating_4 = DB::table('school_user_rating_course')
                                            ->where('rating_val', 4)
                                            ->where('sid', Auth::user()->sid)
                                            ->where('course_id', $request->course_id)
                                            ->count();
                                        $rating_5 = DB::table('school_user_rating_course')
                                            ->where('rating_val', 5)
                                            ->where('sid', Auth::user()->sid)
                                            ->where('course_id', $request->course_id)
                                            ->count();




                                        $ratinData[] = array(
                                            'y' => $rating_1,
                                            'name' => '1 Star'
                                        );
                                        $ratinData[] = array(
                                            'y' => $rating_2,
                                            'name' => '2 Star'
                                        );
                                        $ratinData[] = array(
                                            'y' => $rating_3,
                                            'name' => '3 Star'
                                        );
                                        $ratinData[] = array(
                                            'y' => $rating_4,
                                            'name' => '4 Star'
                                        );
                                        $ratinData[] = array(
                                            'y' => $rating_5,
                                            'name' => '5 Star'
                                        );
                                        $monthlyVal = $ratinData;

                                        $resp = array(
                                            'data_value' => $monthlyVal

                                        );
                                        return response()->json($resp);
                                    }

                                    //getPIEChartDataByCountrySchoolAdminCourse

                                    //getPIEChartDataByCountrySchoolAdmin
                                    public function getPIEChartDataByCountrySchoolAdmin(Request $request)
                                    {
                                        $monthlyVal = array();

                                        $rating_1 = DB::table('school_user_rating')
                                            ->where('rating_val', 1)
                                            ->where('sid', Auth::user()->sid)
                                            ->count();
                                        $rating_2 = DB::table('school_user_rating')
                                            ->where('rating_val', 2)
                                            ->where('sid', Auth::user()->sid)
                                            ->count();
                                        $rating_3 = DB::table('school_user_rating')
                                            ->where('rating_val', 3)
                                            ->where('sid', Auth::user()->sid)
                                            ->count();
                                        $rating_4 = DB::table('school_user_rating')
                                            ->where('rating_val', 4)
                                            ->where('sid', Auth::user()->sid)
                                            ->count();
                                        $rating_5 = DB::table('school_user_rating')
                                            ->where('rating_val', 5)
                                            ->where('sid', Auth::user()->sid)
                                            ->count();




                                        $ratinData[] = array(
                                            'y' => $rating_1,
                                            'name' => '1 Star'
                                        );
                                        $ratinData[] = array(
                                            'y' => $rating_2,
                                            'name' => '2 Star'
                                        );
                                        $ratinData[] = array(
                                            'y' => $rating_3,
                                            'name' => '3 Star'
                                        );
                                        $ratinData[] = array(
                                            'y' => $rating_4,
                                            'name' => '4 Star'
                                        );
                                        $ratinData[] = array(
                                            'y' => $rating_5,
                                            'name' => '5 Star'
                                        );
                                        $monthlyVal = $ratinData;

                                        $resp = array(
                                            'data_value' => $monthlyVal

                                        );
                                        return response()->json($resp);
                                    }
                                    //getPIEChartDataByCountrySchoolAdmin

                                    //getPIEChartDataByCountrySchool
                                    public function getPIEChartDataByCountrySchool(Request $request)
                                    {
                                        $monthlyVal = array();

                                        if (empty($request->country_id)) {
                                            $rating_1 = DB::table('school_user_rating')
                                                ->where('rating_val', 1)
                                                ->count();
                                            $rating_2 = DB::table('school_user_rating')
                                                ->where('rating_val', 2)
                                                ->count();
                                            $rating_3 = DB::table('school_user_rating')
                                                ->where('rating_val', 3)
                                                ->count();
                                            $rating_4 = DB::table('school_user_rating')
                                                ->where('rating_val', 4)
                                                ->count();
                                            $rating_5 = DB::table('school_user_rating')
                                                ->where('rating_val', 5)
                                                ->count();
                                        } else {
                                            if (isset($request->sid)) {

                                                $rating_1 = DB::table('school_user_rating')
                                                    ->where('rating_val', 1)
                                                    ->where('country', $request->country_id)
                                                    ->where('sid', $request->sid)
                                                    ->count();
                                                $rating_2 = DB::table('school_user_rating')
                                                    ->where('rating_val', 2)
                                                    ->where('country', $request->country_id)
                                                    ->where('sid', $request->sid)
                                                    ->count();
                                                $rating_3 = DB::table('school_user_rating')
                                                    ->where('rating_val', 3)
                                                    ->where('country', $request->country_id)
                                                    ->where('sid', $request->sid)
                                                    ->count();
                                                $rating_4 = DB::table('school_user_rating')
                                                    ->where('rating_val', 4)
                                                    ->where('country', $request->country_id)
                                                    ->where('sid', $request->sid)
                                                    ->count();
                                                $rating_5 = DB::table('school_user_rating')
                                                    ->where('country', $request->country_id)
                                                    ->where('rating_val', 5)
                                                    ->where('sid', $request->sid)
                                                    ->count();
                                            } else {
                                                $rating_1 = DB::table('school_user_rating')
                                                    ->where('rating_val', 1)
                                                    ->where('country', $request->country_id)
                                                    ->count();
                                                $rating_2 = DB::table('school_user_rating')
                                                    ->where('rating_val', 2)
                                                    ->where('country', $request->country_id)
                                                    ->count();
                                                $rating_3 = DB::table('school_user_rating')
                                                    ->where('rating_val', 3)
                                                    ->where('country', $request->country_id)
                                                    ->count();
                                                $rating_4 = DB::table('school_user_rating')
                                                    ->where('rating_val', 4)
                                                    ->where('country', $request->country_id)
                                                    ->count();
                                                $rating_5 = DB::table('school_user_rating')
                                                    ->where('rating_val', 5)
                                                    ->where('country', $request->country_id)
                                                    ->count();
                                            }
                                        }


                                        $ratinData[] = array(
                                            'y' => $rating_1,
                                            'name' => '1 Star'
                                        );
                                        $ratinData[] = array(
                                            'y' => $rating_2,
                                            'name' => '2 Star'
                                        );
                                        $ratinData[] = array(
                                            'y' => $rating_3,
                                            'name' => '3 Star'
                                        );
                                        $ratinData[] = array(
                                            'y' => $rating_4,
                                            'name' => '4 Star'
                                        );
                                        $ratinData[] = array(
                                            'y' => $rating_5,
                                            'name' => '5 Star'
                                        );
                                        $monthlyVal = $ratinData;

                                        $resp = array(
                                            'data_value' => $monthlyVal

                                        );
                                        return response()->json($resp);
                                    }
                                    //getPIEChartDataByCountrySchool

                                    //getHighcartUsersWeeklyEn
                                    //getHighcartUsersWeeklyEnAdmin
                                    public function getHighcartUsersWeeklyEnAdmin(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $country_id = $request->country_id;
                                        $sid = $request->sid;
                                        $courseID = $request->courseID;




                                        $now = \Carbon\Carbon::now();
                                        $weekStartDate = $now->startOfWeek()->format('Y-m-d');

                                        $monthlyVal = array();
                                        $monthName = array();

                                        for ($x = 0; $x <= 6; $x++) {



                                            $date = date('j-m-Y', strtotime($weekStartDate . ' + ' . $x . ' days'));
                                            $byDate = date('Y-m-d', strtotime($weekStartDate . ' + ' . $x . ' days'));

                                            $data_output = getUserCountByDateEn($byDate, $ActionType, $country_id, $sid, $courseID);


                                            $monthlyVal[] = (float)$data_output;






                                            $monthName[] = $date;
                                        }






                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }

                                    //getHighcartUsersWeeklyEnAdmin

                                    public function getHighcartUsersWeeklyEn(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $country_id = $request->country_id;
                                        $sid = $request->sid;
                                        $courseID = $request->courseID;




                                        $now = \Carbon\Carbon::now();
                                        $weekStartDate = $now->startOfWeek()->format('Y-m-d');

                                        $monthlyVal = array();
                                        $monthName = array();

                                        for ($x = 0; $x <= 6; $x++) {



                                            $date = date('j-m-Y', strtotime($weekStartDate . ' + ' . $x . ' days'));
                                            $byDate = date('Y-m-d', strtotime($weekStartDate . ' + ' . $x . ' days'));

                                            $data_output = getUserCountByDateEn($byDate, $ActionType, $country_id, $sid, $courseID);


                                            $monthlyVal[] = (float)$data_output;






                                            $monthName[] = $date;
                                        }






                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }

                                    //getHighcartUsersWeeklyAdminPay_super
                                    public function getHighcartUsersWeeklyAdminPay_super(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $course_id = $request->course_id;


                                        $now = \Carbon\Carbon::now();
                                        $weekStartDate = $now->startOfWeek()->format('Y-m-d');

                                        $monthlyVal = array();
                                        $monthName = array();

                                        for ($x = 0; $x <= 6; $x++) {



                                            $date = date('j-m-Y', strtotime($weekStartDate . ' + ' . $x . ' days'));
                                            $byDate = date('Y-m-d', strtotime($weekStartDate . ' + ' . $x . ' days'));

                                            $data_output = getUserCountByDateAdminPay($byDate, $ActionType, $course_id);


                                            $monthlyVal[] = (float)$data_output;






                                            $monthName[] = $date;
                                        }






                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }

                                    //getHighcartUsersWeeklyAdminPay_super

                                    //getHighcartUsersWeeklyEn
                                    public function getHighcartUsersWeeklyAdminPay(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $course_id = $request->course_id;


                                        $now = \Carbon\Carbon::now();
                                        $weekStartDate = $now->startOfWeek()->format('Y-m-d');

                                        $monthlyVal = array();
                                        $monthName = array();

                                        for ($x = 0; $x <= 6; $x++) {



                                            $date = date('j-m-Y', strtotime($weekStartDate . ' + ' . $x . ' days'));
                                            $byDate = date('Y-m-d', strtotime($weekStartDate . ' + ' . $x . ' days'));

                                            $data_output = getUserCountByDateAdminPay($byDate, $ActionType, $course_id);


                                            $monthlyVal[] = (float)$data_output;






                                            $monthName[] = $date;
                                        }






                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }
                                    //getHighcartUsersWeeklyAdmin
                                    public function getHighcartUsersWeeklyAdmin(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $course_id = $request->course_id;


                                        $now = \Carbon\Carbon::now();
                                        $weekStartDate = $now->startOfWeek()->format('Y-m-d');

                                        $monthlyVal = array();
                                        $monthName = array();

                                        for ($x = 0; $x <= 6; $x++) {



                                            $date = date('j-m-Y', strtotime($weekStartDate . ' + ' . $x . ' days'));
                                            $byDate = date('Y-m-d', strtotime($weekStartDate . ' + ' . $x . ' days'));

                                            $data_output = getUserCountByDateAdmin($byDate, $ActionType, $course_id);


                                            $monthlyVal[] = (float)$data_output;






                                            $monthName[] = $date;
                                        }






                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }
                                    //getHighcartUsersWeeklyAdmin

                                    //getHighcartUsersWeekly
                                    public function getHighcartUsersWeekly(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $country_id = $request->country_id;


                                        $now = \Carbon\Carbon::now();
                                        $weekStartDate = $now->startOfWeek()->format('Y-m-d');

                                        $monthlyVal = array();
                                        $monthName = array();

                                        for ($x = 0; $x <= 6; $x++) {



                                            $date = date('j-m-Y', strtotime($weekStartDate . ' + ' . $x . ' days'));
                                            $byDate = date('Y-m-d', strtotime($weekStartDate . ' + ' . $x . ' days'));

                                            $data_output = getUserCountByDate($byDate, $ActionType, $country_id);


                                            $monthlyVal[] = (float)$data_output;






                                            $monthName[] = $date;
                                        }






                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }

                                    //getHighcartUsersMonthlyEnAdmin
                                    public function getHighcartUsersMonthlyEnAdmin(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $country_id = $request->country_id;
                                        $sid = $request->sid;
                                        $courseID = $request->courseID;
                                        $now = \Carbon\Carbon::now();
                                        $weekStartDate = $now->firstOfMonth()->format('Y-m-d');
                                        $endDay = date("t") - 1;

                                        $monthlyVal = array();
                                        $monthName = array();

                                        for ($x = 0; $x <= $endDay; $x++) {



                                            $date = date('j-M-Y', strtotime($weekStartDate . ' + ' . $x . ' days'));




                                            $date = date('j-m-y', strtotime($weekStartDate . ' + ' . $x . ' days'));
                                            $byDate = date('Y-m-d', strtotime($weekStartDate . ' + ' . $x . ' days'));

                                            //$data_output = getUserCountByDate($byDate,$ActionType,$country_id);
                                            $data_output = getUserCountByDateEn($byDate, $ActionType, $country_id, $sid, $courseID);


                                            $monthlyVal[] = (float)$data_output;






                                            $monthName[] = $date;
                                        }






                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }

                                    //getHighcartUsersMonthlyEnAdmin

                                    //getHighcartUsersMonthlyEn
                                    public function getHighcartUsersMonthlyEn(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $country_id = $request->country_id;
                                        $sid = $request->sid;
                                        $courseID = $request->courseID;
                                        $now = \Carbon\Carbon::now();
                                        $weekStartDate = $now->firstOfMonth()->format('Y-m-d');
                                        $endDay = date("t") - 1;

                                        $monthlyVal = array();
                                        $monthName = array();

                                        for ($x = 0; $x <= $endDay; $x++) {



                                            $date = date('j-M-Y', strtotime($weekStartDate . ' + ' . $x . ' days'));




                                            $date = date('j-m-y', strtotime($weekStartDate . ' + ' . $x . ' days'));
                                            $byDate = date('Y-m-d', strtotime($weekStartDate . ' + ' . $x . ' days'));

                                            //$data_output = getUserCountByDate($byDate,$ActionType,$country_id);
                                            $data_output = getUserCountByDateEn($byDate, $ActionType, $country_id, $sid, $courseID);


                                            $monthlyVal[] = (float)$data_output;






                                            $monthName[] = $date;
                                        }






                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }

                                    //getHighcartUsersMonthlyEn

                                    //getHighcartUsersMonthlyAdminPay_super
                                    public function getHighcartUsersMonthlyAdminPay_super(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $country_id = $request->country_id;
                                        $now = \Carbon\Carbon::now();
                                        $weekStartDate = $now->firstOfMonth()->format('Y-m-d');
                                        $endDay = date("t") - 1;

                                        $monthlyVal = array();
                                        $monthName = array();

                                        for ($x = 0; $x <= $endDay; $x++) {



                                            $date = date('j-M-Y', strtotime($weekStartDate . ' + ' . $x . ' days'));




                                            $date = date('j-m-y', strtotime($weekStartDate . ' + ' . $x . ' days'));
                                            $byDate = date('Y-m-d', strtotime($weekStartDate . ' + ' . $x . ' days'));

                                            $data_output = getUserCountByDateAdminPay($byDate, $ActionType, $country_id);



                                            $monthlyVal[] = (float)$data_output;






                                            $monthName[] = $date;
                                        }






                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }
                                    //getHighcartUsersMonthlyAdminPay_super

                                    //getHighcartUsersMonthlyAdminPay
                                    public function getHighcartUsersMonthlyAdminPay(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $country_id = $request->country_id;
                                        $now = \Carbon\Carbon::now();
                                        $weekStartDate = $now->firstOfMonth()->format('Y-m-d');
                                        $endDay = date("t") - 1;

                                        $monthlyVal = array();
                                        $monthName = array();

                                        for ($x = 0; $x <= $endDay; $x++) {



                                            $date = date('j-M-Y', strtotime($weekStartDate . ' + ' . $x . ' days'));




                                            $date = date('j-m-y', strtotime($weekStartDate . ' + ' . $x . ' days'));
                                            $byDate = date('Y-m-d', strtotime($weekStartDate . ' + ' . $x . ' days'));

                                            $data_output = getUserCountByDateAdminPay($byDate, $ActionType, $country_id);



                                            $monthlyVal[] = (float)$data_output;






                                            $monthName[] = $date;
                                        }






                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }
                                    //getHighcartUsersMonthlyAdminPay

                                    //getHighcartUsersMonthlyAdmin
                                    public function getHighcartUsersMonthlyAdmin(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $country_id = $request->country_id;
                                        $now = \Carbon\Carbon::now();
                                        $weekStartDate = $now->firstOfMonth()->format('Y-m-d');
                                        $endDay = date("t") - 1;

                                        $monthlyVal = array();
                                        $monthName = array();

                                        for ($x = 0; $x <= $endDay; $x++) {



                                            $date = date('j-M-Y', strtotime($weekStartDate . ' + ' . $x . ' days'));




                                            $date = date('j-m-y', strtotime($weekStartDate . ' + ' . $x . ' days'));
                                            $byDate = date('Y-m-d', strtotime($weekStartDate . ' + ' . $x . ' days'));

                                            $data_output = getUserCountByDateAdmin($byDate, $ActionType, $country_id);



                                            $monthlyVal[] = (float)$data_output;






                                            $monthName[] = $date;
                                        }






                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }

                                    //getHighcartUsersMonthlyAdmin

                                    public function getHighcartUsersMonthly(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $country_id = $request->country_id;
                                        $now = \Carbon\Carbon::now();
                                        $weekStartDate = $now->firstOfMonth()->format('Y-m-d');
                                        $endDay = date("t") - 1;

                                        $monthlyVal = array();
                                        $monthName = array();

                                        for ($x = 0; $x <= $endDay; $x++) {



                                            $date = date('j-M-Y', strtotime($weekStartDate . ' + ' . $x . ' days'));




                                            $date = date('j-m-y', strtotime($weekStartDate . ' + ' . $x . ' days'));
                                            $byDate = date('Y-m-d', strtotime($weekStartDate . ' + ' . $x . ' days'));

                                            $data_output = getUserCountByDate($byDate, $ActionType, $country_id);



                                            $monthlyVal[] = (float)$data_output;






                                            $monthName[] = $date;
                                        }






                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }

                                    //getHighcartUsersWeekly
                                    //getHighcartUsersYearlyYearEnAdmin
                                    public function getHighcartUsersYearlyYearEnAdmin(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $country_id = $request->country_id;
                                        $sid = $request->sid;
                                        $courseID = $request->courseID;

                                        $monthlyVal = array();
                                        $monthName = array();
                                        $year_digit = "2021";
                                        $currYear = date('Y') - 12;

                                        for ($x = 1; $x <= 12; $x++) {


                                            $yrVal = ($currYear + $x);



                                            //$yrVal = ($currYear - $x);
                                            //$data_output = getUserCountByMonthYear($yrVal,$ActionType,$country_id);

                                            $data_output = getUserCountByMonthYearEn($yrVal, $ActionType, $country_id, $sid, $courseID);


                                            $monthlyVal[] = (float)$data_output;

                                            $month = $x;


                                            $month = substr($month, -2, 2);
                                            $mN = date('M-' . $year_digit, strtotime(date('Y-' . $month . '-d')));
                                            $monthName[] = "Yr-" . $yrVal;
                                        }







                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }

                                    //getHighcartUsersYearlyYearEnAdmin

                                    //getHighcartUsersYearlyYearEn
                                    public function getHighcartUsersYearlyYearEn(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $country_id = $request->country_id;
                                        $sid = $request->sid;
                                        $courseID = $request->courseID;

                                        $monthlyVal = array();
                                        $monthName = array();
                                        $year_digit = "2021";
                                        $currYear = date('Y') - 12;

                                        for ($x = 1; $x <= 12; $x++) {


                                            $yrVal = ($currYear + $x);



                                            //$yrVal = ($currYear - $x);
                                            //$data_output = getUserCountByMonthYear($yrVal,$ActionType,$country_id);

                                            $data_output = getUserCountByMonthYearEn($yrVal, $ActionType, $country_id, $sid, $courseID);


                                            $monthlyVal[] = (float)$data_output;

                                            $month = $x;


                                            $month = substr($month, -2, 2);
                                            $mN = date('M-' . $year_digit, strtotime(date('Y-' . $month . '-d')));
                                            $monthName[] = "Yr-" . $yrVal;
                                        }







                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }


                                    //getHighcartUsersYearlyYearEn
                                    //getHighcartUsersYearlyYearAdminPay_super
                                    public function getHighcartUsersYearlyYearAdminPay_super(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $course_id = $request->course_id;

                                        $monthlyVal = array();
                                        $monthName = array();
                                        $year_digit = "2021";
                                        $currYear = date('Y') - 12;

                                        for ($x = 1; $x <= 12; $x++) {


                                            $yrVal = ($currYear + $x);



                                            //$yrVal = ($currYear - $x);
                                            $data_output = getUserCountByMonthYearAdminPay($yrVal, $ActionType, $course_id);



                                            $monthlyVal[] = (float)$data_output;

                                            $month = $x;


                                            $month = substr($month, -2, 2);
                                            $mN = date('M-' . $year_digit, strtotime(date('Y-' . $month . '-d')));
                                            $monthName[] = "Yr-" . $yrVal;
                                        }







                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }
                                    //getHighcartUsersYearlyYearAdminPay_super

                                    //getHighcartUsersYearlyYearAdminPay
                                    public function getHighcartUsersYearlyYearAdminPay(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $course_id = $request->course_id;

                                        $monthlyVal = array();
                                        $monthName = array();
                                        $year_digit = "2021";
                                        $currYear = date('Y') - 12;

                                        for ($x = 1; $x <= 12; $x++) {


                                            $yrVal = ($currYear + $x);



                                            //$yrVal = ($currYear - $x);
                                            $data_output = getUserCountByMonthYearAdminPay($yrVal, $ActionType, $course_id);



                                            $monthlyVal[] = (float)$data_output;

                                            $month = $x;


                                            $month = substr($month, -2, 2);
                                            $mN = date('M-' . $year_digit, strtotime(date('Y-' . $month . '-d')));
                                            $monthName[] = "Yr-" . $yrVal;
                                        }







                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }

                                    //getHighcartUsersYearlyYearAdminPay

                                    //getHighcartUsersYearlyYearAdmin
                                    public function getHighcartUsersYearlyYearAdmin(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $country_id = $request->country_id;

                                        $monthlyVal = array();
                                        $monthName = array();
                                        $year_digit = "2021";
                                        $currYear = date('Y') - 12;

                                        for ($x = 1; $x <= 12; $x++) {


                                            $yrVal = ($currYear + $x);



                                            //$yrVal = ($currYear - $x);
                                            $data_output = getUserCountByMonthYearAdmin($yrVal, $ActionType, $country_id);



                                            $monthlyVal[] = (float)$data_output;

                                            $month = $x;


                                            $month = substr($month, -2, 2);
                                            $mN = date('M-' . $year_digit, strtotime(date('Y-' . $month . '-d')));
                                            $monthName[] = "Yr-" . $yrVal;
                                        }







                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }

                                    //getHighcartUsersYearlyYearAdmin

                                    //getHighcartUsersYearlyYear
                                    public function getHighcartUsersYearlyYear(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $country_id = $request->country_id;

                                        $monthlyVal = array();
                                        $monthName = array();
                                        $year_digit = "2021";
                                        $currYear = date('Y') - 12;

                                        for ($x = 1; $x <= 12; $x++) {


                                            $yrVal = ($currYear + $x);



                                            //$yrVal = ($currYear - $x);
                                            $data_output = getUserCountByMonthYear($yrVal, $ActionType, $country_id);



                                            $monthlyVal[] = (float)$data_output;

                                            $month = $x;


                                            $month = substr($month, -2, 2);
                                            // $mN = date('M-' . $year_digit, strtotime(date('Y-' . $month . '-d')));
                                            $monthName[] = "Yr-" . $yrVal;
                                        }







                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }

                                    //getHighcartUsersYearlyYear
                                    //getHighcartUsersYearlyEnAdmin
                                    public function getHighcartUsersYearlyEnAdmin(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $country_id = $request->country_id;
                                        $sid = $request->sid;
                                        $courseID = $request->courseID;

                                        $monthlyVal = array();
                                        $monthName = array();
                                        $year_digit = "2021";

                                        for ($x = 1; $x <= 12; $x++) {






                                            $data_output = getUserCountByMonthEn($x, $ActionType, $country_id, $sid, $courseID);



                                            $monthlyVal[] = (float)$data_output;

                                            $month = $x;


                                            $month = substr($month, -2, 2);
                                            $mN = date('M-' . $year_digit, strtotime(date('Y-' . $month . '-d')));
                                            $monthName[] = $mN;
                                        }







                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }
                                    //getHighcartUsersYearlyEnAdmin

                                    //getHighcartUsersYearlyEn
                                    public function getHighcartUsersYearlyEn(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $country_id = $request->country_id;
                                        $sid = $request->sid;
                                        $courseID = $request->courseID;

                                        $monthlyVal = array();
                                        $monthName = array();
                                        $year_digit = "2021";

                                        for ($x = 1; $x <= 12; $x++) {






                                            $data_output = getUserCountByMonthEn($x, $ActionType, $country_id, $sid, $courseID);



                                            $monthlyVal[] = (float)$data_output;

                                            $month = $x;


                                            $month = substr($month, -2, 2);
                                            $mN = date('M-' . $year_digit, strtotime(date('Y-' . $month . '-d')));
                                            $monthName[] = $mN;
                                        }







                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }

                                    //getHighcartUsersYearlyEn
                                    //getHighcartUsersYearlyAdminPay_super
                                    public function getHighcartUsersYearlyAdminPay_super(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $course_id = $request->course_id;



                                        $monthlyVal = array();
                                        $monthName = array();
                                        $year_digit = "2021";

                                        for ($x = 1; $x <= 12; $x++) {






                                            $data_output = getUserCountByMonthAdminPay($x, $ActionType, $course_id);



                                            $monthlyVal[] = (float)$data_output;

                                            $month = $x;


                                            $month = substr($month, -2, 2);
                                            $mN = date('M-' . $year_digit, strtotime(date('Y-' . $month . '-d')));
                                            $monthName[] = $mN;
                                        }







                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }
                                    //getHighcartUsersYearlyAdminPay_super

                                    //getHighcartUsersYearlyAdminPay
                                    public function getHighcartUsersYearlyAdminPay(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $course_id = $request->course_id;



                                        $monthlyVal = array();
                                        $monthName = array();
                                        $year_digit = "2021";

                                        for ($x = 1; $x <= 12; $x++) {






                                            $data_output = getUserCountByMonthAdminPay($x, $ActionType, $course_id);



                                            $monthlyVal[] = (float)$data_output;

                                            $month = $x;


                                            $month = substr($month, -2, 2);
                                            $mN = date('M-' . $year_digit, strtotime(date('Y-' . $month . '-d')));
                                            $monthName[] = $mN;
                                        }







                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }
                                    //getHighcartUsersYearlyAdminPay

                                    //getHighcartUsersYearlyAdmin
                                    public function getHighcartUsersYearlyAdmin(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $country_id = $request->country_id;



                                        $monthlyVal = array();
                                        $monthName = array();
                                        $year_digit = "2021";

                                        for ($x = 1; $x <= 12; $x++) {






                                            $data_output = getUserCountByMonthAdmin($x, $ActionType, $country_id);



                                            $monthlyVal[] = (float)$data_output;

                                            $month = $x;


                                            $month = substr($month, -2, 2);
                                            $mN = date('M-' . $year_digit, strtotime(date('Y-' . $month . '-d')));
                                            $monthName[] = $mN;
                                        }







                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }

                                    //getHighcartUsersYearlyAdmin

                                    public function getHighcartUsersYearly(Request $request)
                                    {
                                        $ActionType = $request->ActionType;
                                        $country_id = $request->country_id;



                                        $monthlyVal = array();
                                        $monthName = array();
                                        $year_digit = "2021";

                                        for ($x = 1; $x <= 12; $x++) {






                                            $data_output = getUserCountByMonth($x, $ActionType, $country_id);



                                            $monthlyVal[] = (float)$data_output;

                                            $month = $x;


                                            $month = substr($month, -2, 2);
                                            $mN = date('M-' . $year_digit, strtotime(date('Y-' . $month . '-d')));
                                            $monthName[] = $mN;
                                        }







                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }
                                    //getHighcartUsersAddedMonthly
                                    public function getHighcartUsersAddedMonthly(Request $request)
                                    {
                                        $monthlyVal = array();
                                        $monthName = array();
                                        $year_digit = "2021";

                                        for ($x = 1; $x <= 12; $x++) {




                                            $data_output = 13;

                                            $monthlyVal[] = (float)$data_output;

                                            $month = $x;


                                            $month = substr($month, -2, 2);
                                            $mN = date('M-' . $year_digit, strtotime(date('Y-' . $month . '-d')));
                                            $monthName[] = $mN;
                                        }






                                        $resp = array(
                                            'monthlyValue' => $monthlyVal,
                                            'MonthName' => $monthName,

                                        );
                                        return response()->json($resp);
                                    }
                                    //getHighcartUsersAddedMonthly


                                    public function SchoolRequestList(Request $request)
                                    {
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('school_list_requested', $data)->render();
                                    }
                                    //UserResetPasswordV2
                                    public function UserResetPasswordV2(Request $request)
                                    {

                                        if (!(Hash::check($request->get('current'), Auth::user()->password))) {
                                            // The passwords matches
                                            $res_arr = array(
                                                'status' => 2,
                                                'Message' => 'Your current password does not matches with the password you provided. Please try again..',
                                            );
                                            return response()->json($res_arr);
                                        }
                                        if (strcmp($request->get('current'), $request->get('password')) == 0) {
                                            //Current password and new password are same
                                            $res_arr = array(
                                                'status' => 3,
                                                'Message' => 'New Password cannot be same as your current password. Please choose a different password..',
                                            );
                                            return response()->json($res_arr);
                                        }

                                        //  $id = $request->user_id;
                                        // $user = User::find($id);
                                        // $this->validate($request, [
                                        //   'password' => 'required'
                                        // ]);

                                        // $input = $request->only(['password']);
                                        // $user->fill($input)->save();
                                        User::find(auth()->user()->id)->update(['password' => bcrypt($request->get('password'))]);

                                        Auth::logout();

                                        $res_arr = array(
                                            'status' => 1,
                                            'Message' => 'Password saved successfully.',
                                        );
                                        return response()->json($res_arr);
                                    }
                                    //UserResetPasswordV2

                                    public function UserResetPassword(Request $request)
                                    {

                                        if (!(Hash::check($request->get('current'), Auth::user()->password))) {
                                            // The passwords matches
                                            $res_arr = array(
                                                'status' => 2,
                                                'Message' => 'Your current password does not matches with the password you provided. Please try again..',
                                            );
                                            return response()->json($res_arr);
                                        }
                                        if (strcmp($request->get('current'), $request->get('password')) == 0) {
                                            //Current password and new password are same
                                            $res_arr = array(
                                                'status' => 3,
                                                'Message' => 'New Password cannot be same as your current password. Please choose a different password..',
                                            );
                                            return response()->json($res_arr);
                                        }

                                        //  $id = $request->user_id;
                                        // $user = User::find($id);
                                        // $this->validate($request, [
                                        //   'password' => 'required'
                                        // ]);

                                        // $input = $request->only(['password']);
                                        // $user->fill($input)->save();
                                        User::find(auth()->user()->id)->update(['password' => bcrypt($request->get('password'))]);

                                        Auth::logout();

                                        $res_arr = array(
                                            'status' => 1,
                                            'Message' => 'Password saved successfully.',
                                        );
                                        return response()->json($res_arr);
                                    }

                                    public function saveAdminProfile(Request $request)
                                    {

                                        $affected = DB::table('users')
                                            ->where('id', $request->txtSID)
                                            ->update([
                                                'name' => $request->name,
                                                'phone' => $request->phone,
                                                //   'gender' => $request->gender,
                                                //   'location_address' => $request->location,

                                            ]);

                                        $data = array(
                                            'msg' => 'Data saved Successfully',
                                            'status' => 1
                                        );
                                        return response()->json($data);
                                    }
                                    //saveUserEdit
                                    public function saveUserEdit(Request $request)
                                    {

                                        $affected = DB::table('users')
                                            ->where('id', $request->txtSID)
                                            ->update([
                                                'name' => $request->name,
                                                'phone' => $request->phone,
                                                'gender' => $request->gender,
                                                'location_address' => $request->location,

                                            ]);

                                        $data = array(
                                            'msg' => 'Data saved Successfully',
                                            'status' => 1
                                        );
                                        return response()->json($data);
                                    }
                                    //saveUserEdit

                                    public function saveSportIntrest(Request $request)
                                    {
                                        if ($request->txtAction == 1) {
                                            DB::table('sports')->insert([
                                                'name' => $request->txtSport

                                            ]);
                                            $data = array(
                                                'msg' => 'Data saved Successfully',
                                                'status' => 1
                                            );
                                        } else {
                                            DB::table('interest')->insert([
                                                'name' => $request->txtInterest

                                            ]);
                                            $data = array(
                                                'msg' => 'Data saved Successfully',
                                                'status' => 1
                                            );
                                        }
                                        return response()->json($data);
                                    }
                                    public function basicSettings()
                                    {


                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["data" => ''];
                                        return $theme->scope('basic_settings', $data)->render();
                                    }
                                    public function deletImage(Request $request)
                                    {
                                        switch ($request->action) {
                                            case 1:
                                                $affected = DB::table('schools')
                                                    ->where('id', $request->rowid)
                                                    ->update(['school_logo' => NULL]);

                                                $data = array(
                                                    'msg' => 'Submitted Successfully ..',
                                                    'status' => 1
                                                );
                                                return response()->json($data);

                                                break;
                                            case 2:
                                                DB::table('schools_slider_img')->where('id', $request->rowid)->delete();

                                                $data = array(
                                                    'msg' => 'Submitted Successfully',
                                                    'status' => 1
                                                );
                                                return response()->json($data);

                                                break;


                                            default:
                                                # code...
                                                break;
                                        }
                                    }
                                    //useractionUserIsActive
                                    public function useractionUserIsActive(Request $request)
                                    {
                                        $txtSID = $request->txtSID;
                                        $statusAction = $request->statusAction;
                                        if ($statusAction == 1) {
                                            $affected = DB::table('users')
                                                ->where('id', $request->txtSID)
                                                ->update(['is_active' => 1]);
                                            $data = array(
                                                'msg' => 'Activated Successfully',
                                                'status' => 1
                                            );
                                            return response()->json($data);
                                        } else {
                                            $affected = DB::table('users')
                                                ->where('id', $request->txtSID)
                                                ->update(['is_active' => 2]);
                                            $data = array(
                                                'msg' => 'De-Activated Successfully',
                                                'status' => 1
                                            );
                                            return response()->json($data);
                                        }
                                    }

                                    //useractionUserIsActive

                                    public function useractionSchoolAccount(Request $request)
                                    {
                                        
                                        $txtSID = $request->txtSID;
                                        $scoolArrData = DB::table('schools_slider_img')->where('sid', $txtSID)->get();
                                    $is_school_img=0;
                                    if (count($scoolArrData) > 0) {
                                        $is_school_img=1;
                                    }

                                        $statusAction = $request->statusAction;
                                        if ($statusAction == 1) {

                                            $schArrData = DB::table('schools')
                                            ->where('id',$txtSID)
                                            ->first();

                                            if($is_school_img==0){
                                                $data = array(
                                                    'msg' => 'School Gallery missing',
                                                    'status' => 2
                                                );
                                                return response()->json($data);

                                            }
                                            if(empty($schArrData->school_logo)){
                                                $data = array(
                                                    'msg' => 'School Logo missing',
                                                    'status' => 2
                                                );
                                                return response()->json($data);

                                            }


                                            if(empty($schArrData->title)){
                                                $data = array(
                                                    'msg' => 'Title missing',
                                                    'status' => 2
                                                );
                                                return response()->json($data);

                                            }
                                            if(empty($schArrData->reg_no)){
                                                $data = array(
                                                    'msg' => 'Registration No missing',
                                                    'status' => 2
                                                );
                                                return response()->json($data);

                                            }
                                            if(empty($schArrData->country_id)){
                                                $data = array(
                                                    'msg' => 'Country  missing',
                                                    'status' => 2
                                                );
                                                return response()->json($data);

                                            }
                                            if(empty($schArrData->state_id)){
                                                $data = array(
                                                    'msg' => 'State  missing',
                                                    'status' => 2
                                                );
                                                return response()->json($data);

                                            }
                                            if(empty($schArrData->city_id)){
                                                $data = array(
                                                    'msg' => 'City missing',
                                                    'status' => 2
                                                );
                                                return response()->json($data);

                                            }
                                            if(empty($schArrData->phone)){
                                                $data = array(
                                                    'msg' => 'Phone missing',
                                                    'status' => 2
                                                );
                                                return response()->json($data);

                                            }
                                            if(empty($schArrData->admin_comm)){
                                                $data = array(
                                                    'msg' => 'Admin Commission',
                                                    'status' => 2
                                                );
                                                return response()->json($data);

                                            }



                                            $affected = DB::table('schools')
                                                ->where('id', $request->txtSID)
                                                ->update(['status' => 1]);
                                            $data = array(
                                                'msg' => 'Activated Successfully',
                                                'status' => 1
                                            );
                                            return response()->json($data);
                                        } else {
                                            $affected = DB::table('schools')
                                                ->where('id', $request->txtSID)
                                                ->update(['status' => 2]);
                                            $data = array(
                                                'msg' => 'De-Activated Successfully',
                                                'status' => 1
                                            );
                                            return response()->json($data);
                                        }
                                    }
                                    public function createOrSentSchoolAccount(Request $request)
                                    {

                                        $txtSID = $request->txtSID;
                                        $statusAction = $request->statusAction;

                                        if ($statusAction == 1) {

                                            $schoolArr = DB::table('schools')
                                                ->where('id', $txtSID)
                                                ->first();
                                            // print_r($schoolArr);
                                            // die;
                                            $sid = $schoolArr->id;

                                            $users = DB::table('users')
                                                ->where('email', $schoolArr->email)
                                                ->first();

                                            if ($users == null) {
                                                $dev_role = Role::where('slug', 'admin')->first();
                                                $dev_perm = Permission::where('slug', 'create-tasks')->first();
                                                $developer = new User();
                                                $developer->name = $schoolArr->title;
                                                $developer->email = $schoolArr->email;
                                                $developer->password = bcrypt('123456');
                                                $developer->sid = $sid;
                                                $developer->save();
                                                $developer->roles()->attach($dev_role);
                                                $developer->permissions()->attach($dev_perm);
                                                //send email to user
                                                $sent_to = $schoolArr->email;
                                                $subLine = "Login credential of ZELOS";

                                                $data = array(
                                                    'title' => $schoolArr->title,
                                                    'email' => $schoolArr->email,
                                                    'password' => '123456'

                                                );

                                                Mail::send('mail_school', $data, function ($message) use ($sent_to,  $subLine) {

                                                    $message->to($sent_to, 'Bo')->subject($subLine);
                                                    //$message->cc($use_data->email, $use_data->name = null);
                                                    //$message->bcc('udita.bointl@gmail.com', 'UDITA');
                                                    $message->from('codexage@gmail.com', 'Zelos');
                                                });

                                                //send email to user
                                                $data = array(
                                                    'msg' => 'Submitted Successfully',
                                                    'status' => 1
                                                );
                                                return response()->json($data);
                                            } else {
                                                //send email to user
                                                $sent_to = $schoolArr->email;
                                                $subLine = "Login credential of ZELOS";

                                                $data = array(
                                                    'title' => $schoolArr->title,
                                                    'email' => $schoolArr->email,
                                                    'password' => '123456'
                                                );

                                                Mail::send('mail_school', $data, function ($message) use ($sent_to,  $subLine) {

                                                    $message->to($sent_to, 'Bo')->subject($subLine);
                                                    //$message->cc($use_data->email, $use_data->name = null);
                                                    //$message->bcc('udita.bointl@gmail.com', 'UDITA');
                                                    $message->from('codexage@gmail.com', 'Zelos');
                                                });

                                                //send email to user

                                                $data = array(
                                                    'msg' => 'Submitted Successfully ..',
                                                    'status' => 1
                                                );
                                                return response()->json($data);
                                            }
                                        } else {

                                            $data = array(
                                                'msg' => 'Its okey',
                                                'status' => 1
                                            );
                                            return response()->json($data);
                                        }
                                    }
                                    public function editStaticContent($id)
                                    {
                                        $users = DB::table('cms_contents')->where('id', $id)->first();
                                        $theme = Theme::uses('adminsuper')->layout('layout');
                                        $data = ["data" => $users];
                                        return $theme->scope('edit_static_content', $data)->render();
                                    }

                                    //addNotify
                                    public function addNotify()
                                    {
                                        $theme = Theme::uses('adminsuper')->layout('layout');
                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('add_notify_content', $data)->render();
                                    }



                                    public function addStaticContent()
                                    {
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('add_static_content', $data)->render();
                                    }
                                    //schoolCourseList
                                    public function schoolCourseList()
                                    {
                                        $theme = Theme::uses('admin')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('courseList', $data)->render();
                                    }
                                    //schoolCourseList
                                    //AdminUserList
                                    public function AdminUserList()
                                    {
                                        $theme = Theme::uses('admin')->layout('layout');
                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('admin_userList', $data)->render();
                                    }
                                    //AdminUserList
                                    public function userList()
                                    {
                                        $theme = Theme::uses('adminsuper')->layout('layout');
                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('userList', $data)->render();
                                    }

                                    //uploadChatFileAdmin
                                    public function uploadChatFileAdmin(Request $request)
                                    {

                                        $options = array(

                                            'cluster' => 'ap2',

                                            'encrypted' => true

                                        );

                                        $pusher = new Pusher\Pusher(

                                            '4c1630c06a480f5f76de',

                                            '6c1b90583017db3293f9',

                                            '1239507',

                                            $options

                                        );
                                        $to_id = $request->to_id_model;

                                        $zelosEvent = 'zelosUser_' . $to_id;


                                        if ($request->hasFile('photo')) {

                                            $file = $request->file('photo');
                                            $filename = $request->to_id_model . "_user_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
                                            // save to local/public/uploads/photo/ as the new $filename
                                            //var/www/larachat/local/public/storage/users-avatar

                                            $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);


                                            $messageID = Auth::user()->id . mt_rand(9, 999999999) ;

                                            switch ($file->getClientOriginalExtension()) {
                                                case 'mp4':
                                                    $attype = 2;
                                                    break;
                                                case 'doc':
                                                    $attype = 3;
                                                    break;
                                                case 'docx':
                                                    $attype = 3;
                                                    break;
                                                case 'pdf':
                                                    $attype = 3;
                                                    break;
                                                case 'xls':
                                                    $attype = 3;
                                                    break;
                                                case 'xlsx':
                                                    $attype = 3;
                                                    break;
                                                default:
                                                    $attype = 1;
                                                    break;
                                            }
                                            $recordsUSer = \App\Models\User::where('id', $to_id)->first();

                                            if ($recordsUSer->avatar == null) {
                                                $schLogoU = NoImage();
                                            } else {
                                                $schLogoU = asset('/local/public/upload/') . "/" . $recordsUSer->avatar;
                                            }
                                            $bodyMesage = $request->message;

                                            $HTML = "";
                                            //message for document 
                                            if ($attype == 1) {
                                                $imgDoc = asset('/local/public/upload/') . "/" . $filename;


                                                $HTML .= '<div class="d-flex flex-column mb-5 align-items-start">
                                                <div class="d-flex align-items-center">
                                                <div class="symbol symbol-circle symbol-40 mr-3">
                                                    <img alt="Pic" src="' . $schLogoU . '" />
                                                </div>
                                                <div>
                                                    <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">' . $recordsUSer->name . '</a>
                                                    <span class="text-muted font-size-sm">just now</span>
                                                </div>
                                            </div>
                                                    <div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
                                                        <a href="' . $imgDoc . '" target="_blank" title="" class=" text-dark-50 font-weight-bold font-size-lg">
                                                            <img src="' . $imgDoc . '" class="w-100 rounded" alt=""> 
                                                        </a>
                                                        <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">' . $bodyMesage . '</span>
                                                    </div>
                                                </div>';
                                            }
                                            if ($attype == 2) {
                                                $imgDoc = asset('/local/public/upload/') . "/" . $filename;

                                                $HTML .= '<div class="d-flex flex-column mb-5 align-items-start">
                                                <div class="d-flex align-items-center">
                                                <div class="symbol symbol-circle symbol-40 mr-3">
                                                    <img alt="Pic" src="' . $schLogoU . '" />
                                                </div>
                                                <div>
                                                    <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">' . $recordsUSer->name . '</a>
                                                    <span class="text-muted font-size-sm">just now</span>
                                                </div>
                                            </div>
                                                    <div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
                                                        <a href="' . $imgDoc . '" target="_blank" title="" class=" text-dark-50 font-weight-bold font-size-lg">
                                                            <video width="250" height="150" src="' . $imgDoc . '" controls>
                                                                Sorry, your browser does nt support HTML5 <code>video</code>
                                                              </video>
                                                        </a>
                                                        <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">' . $bodyMesage . '</span>
                                                    </div>
                                                </div>';
                                            }
                                            if ($attype == 3) {
                                                $imgDoc = asset('/local/public/upload/') . "/" . $filename;

                                                $HTML .= '<div class="d-flex flex-column mb-5 align-items-start">
                                                <div class="d-flex align-items-center">
                                                <div class="symbol symbol-circle symbol-40 mr-3">
                                                    <img alt="Pic" src="' . $schLogoU . '" />
                                                </div>
                                                <div>
                                                    <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">' . $recordsUSer->name . '</a>
                                                    <span class="text-muted font-size-sm">just now</span>
                                                </div>
                                            </div>
                                                    <div class="msg_time_mn rounded ml-5 p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px position-relative">
                                                        <a href="' . $imgDoc . '" target="_blank" title="" style="width: 300px;" class="d-flex align-items-center justify-content-between bg-white p-4 rounded">
                                                            <p class="mb-0">abcd.docx</p>
                                                            <i class="icon-xl fas fa-arrow-alt-circle-down ml-5 text-primary"></i>
                                                        </a>
                                                        
                                                        <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">' . $bodyMesage . '</span>
                                                    </div>
                                                </div>';
                                            }

                                            //message for doucment 

                                            if ($pusher->trigger('test_channel', $zelosEvent, $HTML)) {


                                                DB::table('messages')->insert([
                                                    'message_id' => $messageID,
                                                    'type' => 'user',
                                                    'from_id' => Auth::user()->id,
                                                    'to_id' => $request->to_id_model,
                                                    'body' => $request->message,

                                                    'created_at' => date('Y-m-d H:i:s'),

                                                ]);
                                                $lid = DB::getPdo()->lastInsertId();



                                                $affected = DB::table('messages')
                                                    ->where('id', $lid,)
                                                    ->update([
                                                        'attachment' => $filename,
                                                        'attachment_type' => $attype,

                                                    ]);
                                            }
                                        }
                                        $data = array(
                                            'msg' => 'Uploaded Successfully',
                                            'status' => 1
                                        );
                                        return response()->json($data);
                                    }

                                    public function uploadChatFileAdminOO(Request $request)
                                    {
                                        $options = array(

                                            'cluster' => 'ap2',

                                            'encrypted' => true

                                        );

                                        $pusher = new Pusher\Pusher(

                                            '4c1630c06a480f5f76de',

                                            '6c1b90583017db3293f9',

                                            '1239507',

                                            $options

                                        );
                                        $to_id = $request->to_id_model;

                                        $zelosEvent = 'zelosUser_' . $to_id;


                                        if ($request->hasFile('file')) {

                                            $file = $request->file('file');
                                            $filename = $request->txtSID . "_user_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
                                            // save to local/public/uploads/photo/ as the new $filename
                                            //var/www/larachat/local/public/storage/users-avatar

                                            $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);

                                            $messageID = Auth::user()->id . mt_rand(9, 999999999) ;
                                            if ($pusher->trigger('test_channel', $zelosEvent, $filename)) {

                                                DB::table('messages')->insert([
                                                    'message_id' => $messageID,
                                                    'type' => 'user',
                                                    'from_id' => Auth::user()->id,
                                                    'to_id' => $request->to_id_model,
                                                    'body' => $request->message,

                                                    'created_at' => date('Y-m-d H:i:s'),

                                                ]);
                                                $lid = DB::getPdo()->lastInsertId();

                                                switch ($file->getClientOriginalExtension()) {
                                                    case 'mp4':
                                                        $attype = 2;
                                                        break;
                                                    case 'doc':
                                                        $attype = 3;
                                                        break;
                                                    case 'docx':
                                                        $attype = 3;
                                                        break;
                                                    case 'pdf':
                                                        $attype = 3;
                                                        break;
                                                    case 'xls':
                                                        $attype = 3;
                                                        break;
                                                    case 'xlsx':
                                                        $attype = 3;
                                                        break;
                                                    default:
                                                        $attype = 1;
                                                        break;
                                                }

                                                $affected = DB::table('messages')
                                                    ->where('id', $lid,)
                                                    ->update([
                                                        'attachment' => $filename,
                                                        'attachment_type' => $attype,

                                                    ]);
                                            }
                                        }
                                        $data = array(
                                            'msg' => 'Uploaded Successfull4y',
                                            'status' => 1
                                        );
                                        return response()->json($data);
                                    }

                                    //uploadChatFileAdmin
                                    public function uploadChatFile(Request $request)
                                    {
                                        $options = array(

                                            'cluster' => 'ap2',

                                            'encrypted' => true

                                        );

                                        $pusher = new Pusher\Pusher(

                                            '4c1630c06a480f5f76de',

                                            '6c1b90583017db3293f9',

                                            '1239507',

                                            $options

                                        );
                                        $to_id = $request->to_id_model;
                                        $group_id = $request->to_id_model;
                                        $zelosEvent = 'zelosUser_' . $to_id;
                                        if ($request->hasFile('file')) {
                                            $file = $request->file('file');
                                            $filename = $request->txtSID . "_user_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
                                            // save to local/public/uploads/photo/ as the new $filename
                                            //var/www/larachat/local/public/storage/users-avatar

                                            $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);

                                            $messageID = Auth::user()->id . mt_rand(9, 999999999) ;

                                            switch ($file->getClientOriginalExtension()) {
                                                case 'mp4':
                                                    $attype = 2;
                                                    break;
                                                case 'doc':
                                                    $attype = 3;
                                                    break;
                                                case 'docx':
                                                    $attype = 3;
                                                    break;
                                                case 'pdf':
                                                    $attype = 3;
                                                    break;
                                                case 'xls':
                                                    $attype = 3;
                                                    break;
                                                case 'xlsx':
                                                    $attype = 3;
                                                    break;
                                                default:
                                                    $attype = 1;
                                                    break;
                                            }
                                            $recordsUSer = \App\Models\User::where('id', $to_id)->first();

                                            if ($recordsUSer->avatar == null) {
                                                $schLogoU = NoImage();
                                            } else {
                                                $schLogoU = asset('/local/public/upload/') . "/" . $recordsUSer->avatar;
                                            }
                                            $bodyMesage = $request->message;
                                            $imgDoc = asset('/local/public/upload/') . "/" . $filename;



                                            $chat_data = array(
                                                'name' => $recordsUSer->name,
                                                'user_id' => $recordsUSer->id,
                                                "g_id" => $to_id,
                                                "to_id" => $to_id,
                                                'msg' => $bodyMesage,
                                                'photo' => $schLogoU,
                                                'doc_type' => $attype,
                                                'doc_url' => $imgDoc,


                                            );

                                            //message for doucment 

                                            if ($pusher->trigger('test_channel', $zelosEvent, $chat_data)) {


                                                DB::table('messages')->insert([
                                                    'message_id' => $messageID,
                                                    'type' => 'user',
                                                    'from_id' => Auth::user()->id,
                                                    'to_id' => $request->to_id_model,
                                                    'body' => $request->message,

                                                    'created_at' => date('Y-m-d H:i:s'),

                                                ]);
                                                $lid = DB::getPdo()->lastInsertId();



                                                $affected = DB::table('messages')
                                                    ->where('id', $lid,)
                                                    ->update([
                                                        'attachment' => $filename,
                                                        'attachment_type' => $attype,

                                                    ]);
                                            }


                                            //--------------------------


                                        }


                                        $data = array(
                                            'msg' => 'Uploaded Successfully',
                                            'status' => 1
                                        );
                                        return response()->json($data);
                                    }
                                    //uploadChatFile
                                    public function uploadChatFile_1(Request $request)
                                    {

                                        $options = array(

                                            'cluster' => 'ap2',

                                            'encrypted' => true

                                        );

                                        $pusher = new Pusher\Pusher(

                                            '4c1630c06a480f5f76de',

                                            '6c1b90583017db3293f9',

                                            '1239507',

                                            $options

                                        );
                                        $to_id = $request->to_id_model;

                                        $zelosEvent = 'zelosUser_' . $to_id;


                                        if ($request->hasFile('file')) {
                                            $file = $request->file('file');
                                            $filename = $request->txtSID . "_user_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
                                            // save to local/public/uploads/photo/ as the new $filename
                                            //var/www/larachat/local/public/storage/users-avatar

                                            $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);

                                            $messageID = Auth::user()->id . mt_rand(9, 999999999) ;

                                            switch ($file->getClientOriginalExtension()) {
                                                case 'mp4':
                                                    $attype = 2;
                                                    break;
                                                case 'doc':
                                                    $attype = 3;
                                                    break;
                                                case 'docx':
                                                    $attype = 3;
                                                    break;
                                                case 'pdf':
                                                    $attype = 3;
                                                    break;
                                                case 'xls':
                                                    $attype = 3;
                                                    break;
                                                case 'xlsx':
                                                    $attype = 3;
                                                    break;
                                                default:
                                                    $attype = 1;
                                                    break;
                                            }
                                            $recordsUSer = \App\Models\User::where('id', $to_id)->first();

                                            if ($recordsUSer->avatar == null) {
                                                $schLogoU = NoImage();
                                            } else {
                                                $schLogoU = asset('/local/public/upload/') . "/" . $recordsUSer->avatar;
                                            }
                                            $bodyMesage = $request->message;

                                            $HTML = "";
                                            //message for document 
                                            if ($attype == 1) {
                                                $imgDoc = asset('/local/public/upload/') . "/" . $filename;


                                                $HTML .= '<div class="d-flex flex-column mb-5 align-items-start">
                                                <div class="d-flex align-items-center">
                                                <div class="symbol symbol-circle symbol-40 mr-3">
                                                    <img alt="Pic" src="' . $schLogoU . '" />
                                                </div>
                                                <div>
                                                    <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">' . $recordsUSer->name . '</a>
                                                    <span class="text-muted font-size-sm">just now</span>
                                                </div>
                                            </div>
                                                    <div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
                                                        <a href="' . $imgDoc . '" target="_blank" title="" class=" text-dark-50 font-weight-bold font-size-lg">
                                                            <img src="' . $imgDoc . '" class="w-100 rounded" alt=""> 
                                                        </a>
                                                        <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">' . $bodyMesage . '</span>
                                                    </div>
                                                </div>';
                                            }
                                            if ($attype == 2) {
                                                $imgDoc = asset('/local/public/upload/') . "/" . $filename;

                                                $HTML .= '<div class="d-flex flex-column mb-5 align-items-start">
                                                <div class="d-flex align-items-center">
                                                <div class="symbol symbol-circle symbol-40 mr-3">
                                                    <img alt="Pic" src="' . $schLogoU . '" />
                                                </div>
                                                <div>
                                                    <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">' . $recordsUSer->name . '</a>
                                                    <span class="text-muted font-size-sm">just now</span>
                                                </div>
                                            </div>
                                                    <div class="msg_time_mn rounded ml-5 px-4 py-2 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-300px position-relative">
                                                        <a href="' . $imgDoc . '" target="_blank" title="" class=" text-dark-50 font-weight-bold font-size-lg">
                                                            <video width="250" height="150" src="' . $imgDoc . '" controls>
                                                                Sorry, your browser does nt support HTML5 <code>video</code>
                                                              </video>
                                                        </a>
                                                        <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-1 text-right">' . $bodyMesage . '</span>
                                                    </div>
                                                </div>';
                                            }
                                            if ($attype == 3) {
                                                $imgDoc = asset('/local/public/upload/') . "/" . $filename;

                                                $HTML .= '<div class="d-flex flex-column mb-5 align-items-start">
                                                <div class="d-flex align-items-center">
                                                <div class="symbol symbol-circle symbol-40 mr-3">
                                                    <img alt="Pic" src="' . $schLogoU . '" />
                                                </div>
                                                <div>
                                                    <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">' . $recordsUSer->name . '</a>
                                                    <span class="text-muted font-size-sm">just now</span>
                                                </div>
                                            </div>
                                                    <div class="msg_time_mn rounded ml-5 p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px position-relative">
                                                        <a href="' . $imgDoc . '" target="_blank" title="" style="width: 300px;" class="d-flex align-items-center justify-content-between bg-white p-4 rounded">
                                                            <p class="mb-0">abcd.docx</p>
                                                            <i class="icon-xl fas fa-arrow-alt-circle-down ml-5 text-primary"></i>
                                                        </a>
                                                        
                                                        <span class="msg_send_time_mn text-muted font-size-sm d-inline-block w-100 mt-2 text-right">' . $bodyMesage . '</span>
                                                    </div>
                                                </div>';
                                            }

                                            //message for doucment 

                                            if ($pusher->trigger('test_channel', $zelosEvent, $HTML)) {


                                                DB::table('messages')->insert([
                                                    'message_id' => $messageID,
                                                    'type' => 'user',
                                                    'from_id' => Auth::user()->id,
                                                    'to_id' => $request->to_id_model,
                                                    'body' => $request->message,

                                                    'created_at' => date('Y-m-d H:i:s'),

                                                ]);
                                                $lid = DB::getPdo()->lastInsertId();



                                                $affected = DB::table('messages')
                                                    ->where('id', $lid,)
                                                    ->update([
                                                        'attachment' => $filename,
                                                        'attachment_type' => $attype,

                                                    ]);
                                            }
                                        }
                                        $data = array(
                                            'msg' => 'Uploaded Successfully',
                                            'status' => 1
                                        );
                                        return response()->json($data);
                                    }
                                    //uploadChatFile

                                    //uploadUserPhoto
                                    public function uploadUserPhoto(Request $request)
                                    {
                                        if ($request->hasFile('file')) {
                                            $file = $request->file('file');
                                            $filename = $request->txtSID . "_user_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
                                            // save to local/public/uploads/photo/ as the new $filename
                                            //var/www/larachat/local/public/storage/users-avatar

                                            $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);


                                            $affected = DB::table('users')
                                                ->where('id', $request->txtSID,)
                                                ->update(['avatar' => $filename]);
                                        }
                                        $data = array(
                                            'msg' => 'Uploaded Successfully',
                                            'status' => 1
                                        );
                                        return response()->json($data);
                                    }

                                    //uploadUserPhoto

                                    //uploadSchoolSlider
                                    public function uploadSchoolSlider(Request $request)
                                    {
                                        if ($request->hasFile('file')) {
                                            $file = $request->file('file');
                                            $filename = $request->txtSID . "_logo_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
                                            // save to local/uploads/photo/ as the new $filename

                                            $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);

                                            DB::table('schools_slider_img')->insert([
                                                'sid' => $request->txtSID,
                                                'slider_img' => $filename

                                            ]);
                                        }
                                        $data = array(
                                            'msg' => 'Uploaded Successfully',
                                            'status' => 1
                                        );
                                        return response()->json($data);
                                    }

                                    //uploadSchoolSlider

                                    //=======================================
                                    public function uploadSchoolLogo(Request $request)
                                    {
                                        //upload avator phone
                                        if ($request->action == 11) {
                                            if ($request->hasFile('file')) {
                                                $file = $request->file('file');
                                                $filename = $request->txtSID . "_user_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
                                                // save to local/public/uploads/photo/ as the new $filename
                                                //var/www/larachat/local/public/storage/users-avatar

                                                $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);


                                                $affected = DB::table('users')
                                                    ->where('id', $request->txtSID,)
                                                    ->update(['avatar' => $filename]);
                                            }
                                            $data = array(
                                                'msg' => 'Uploaded Successfully',
                                                'status' => 1
                                            );
                                            return response()->json($data);
                                        }
                                        //upload avator phone
                                        if ($request->hasFile('file')) {
                                            $file = $request->file('file');
                                            $filename = $request->txtSID . "_logo_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
                                            // save to local/uploads/photo/ as the new $filename

                                            $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);

                                            $affected = DB::table('schools')
                                                ->where('id', $request->txtSID)
                                                ->update(['school_logo' => $filename]);
                                        }
                                        $data = array(
                                            'msg' => 'Uploaded Successfully',
                                            'status' => 1
                                        );
                                        return response()->json($data);
                                    }
                                    //uploadSchoolCourseDoc
                                    public function uploadSchoolCourseDoc(Request $request)
                                    {


                                        if ($request->action_uploadv2 == 22) {
                                            if ($request->hasFile('file')) {
                                                $file = $request->file('file');
                                                $filename = Auth::user()->sid . "_doc_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
                                                // save to local/uploads/photo/ as the new $filename

                                                $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);
                                                $affected = DB::table('users')
                                                    ->where('sid', Auth::user()->sid)
                                                    ->update(['avatar' => $filename]);
                                            }
                                        }
                                        //  print_r($request->all());
                                        DB::table('school_documents_course')->insert([
                                            'sid' => $request->txtSID,
                                            'doc_info' => $request->doc_info
                                        ]);
                                        $lid = DB::getPdo()->lastInsertId();


                                        if ($request->hasFile('file')) {
                                            $file = $request->file('file');
                                            $filename = $request->txtSID . "_doc_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
                                            // save to local/uploads/photo/ as the new $filename

                                            $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);
                                            $affected = DB::table('school_documents_course')
                                                ->where('id', $lid)
                                                ->update(['doc_name' => $filename]);
                                        }
                                    }


                                    //uploadSchoolCourseDoc

                                    public function uploadSchoolDoc(Request $request)
                                    {
                                        if ($request->action_upload == "_upload_Avatar") {

                                            if ($request->hasFile('file')) {
                                                $file = $request->file('file');
                                                $filename = $request->txtSID . "_doc_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
                                                // save to local/uploads/photo/ as the new $filename

                                                $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);

                                                $affected = DB::table('users')
                                                    ->where('id', $request->txtSID)
                                                    ->update(['avatar' => $filename]);
                                            }
                                        }
                                        //  print_r($request->all());
                                        DB::table('school_documents')->insert([
                                            'sid' => $request->txtSID,
                                            'doc_info' => $request->doc_info
                                        ]);
                                        $lid = DB::getPdo()->lastInsertId();


                                        if ($request->hasFile('file')) {
                                            $file = $request->file('file');
                                            $filename = $request->txtSID . "_doc_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
                                            // save to local/uploads/photo/ as the new $filename

                                            $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);
                                            $affected = DB::table('school_documents')
                                                ->where('id', $lid)
                                                ->update(['doc_name' => $filename]);
                                        }
                                    }

                                    //schoolAcceptedRejectAction
                                    public function schoolAcceptedRejectAction(Request $request)
                                    {
                                        if ($request->action == 1) {  //static detete conted 
                                            $affected = DB::table('schools')
                                                ->where('id', $request->rowid)
                                                ->update(['added_from_status' => 1, 'is_approved' => 1]);

                                            $data = array(
                                                'msg' => 'Deleted Successfullyy',
                                                'status' => 1
                                            );
                                        }
                                        if ($request->action == 2) {  //static detete conted 
                                            $affected = DB::table('schools')
                                                ->where('id', $request->rowid)
                                                ->update(['added_from_status' => 2, 'is_approved' => 2, 'action_remarks' => $request->msg]);
                                            //send email
                                            $schArr = DB::table('schools')
                                                ->where('id', $request->rowid)
                                                ->first();


                                            Mail::send('auth.rejectSchoool', ['token' => $request->msg], function ($message) use ($request, $schArr) {
                                                $message->to($schArr->email);
                                                $message->subject('School Rejectioion');
                                            });

                                            //send email
                                            $data = array(
                                                'msg' => 'Deleted Successfullyy',
                                                'status' => 1
                                            );
                                        }
                                        return response()->json($data);
                                    }
                                    //schoolAcceptedRejectAction

                                    //deletebyAction
                                    public function deletebyAction(Request $request)
                                    {
                                        if ($request->action == 1) {  //static detete conted 
                                            $affected = DB::table('cms_contents')
                                                ->where('id', $request->rowid)
                                                ->update(['is_deleted' => 1]);

                                            $data = array(
                                                'msg' => 'Deleted Successfullyy',
                                                'status' => 1
                                            );
                                        }
                                        return response()->json($data);
                                    }
                                    //deletebyAction

                                    public function deleteSportInterst(Request $request)
                                    {
                                        if ($request->action == 1) {
                                            $affected = DB::table('sports')
                                                ->where('id', $request->rowid)
                                                ->update(['is_deleted' => 1]);

                                            $data = array(
                                                'msg' => 'Deleted Successfully',
                                                'status' => 1
                                            );
                                        } else {
                                            $affected = DB::table('interest')
                                                ->where('id', $request->rowid)
                                                ->update(['is_deleted' => 1]);

                                            $data = array(
                                                'msg' => 'Deleted Successfully',
                                                'status' => 1
                                            );
                                        }

                                        return response()->json($data);
                                    }

                                    //
                                    //deleteUser
                                    public function deleteSchoolCouse(Request $request)
                                    {
                                        $affected = DB::table('school_course')
                                            ->where('id', $request->rowid)
                                            ->update(['is_deleted' => 1]);

                                        $data = array(
                                            'msg' => 'Deleted Successfully',
                                            'status' => 1
                                        );
                                        return response()->json($data);
                                    }

                                    //
                                    //deleteUser
                                    public function deleteUser(Request $request)
                                    {
                                        $affected = DB::table('users')
                                            ->where('id', $request->rowid)
                                            ->update(['is_deleted' => 1]);

                                        $data = array(
                                            'msg' => 'Deleted Successfully',
                                            'status' => 1
                                        );
                                        return response()->json($data);
                                    }
                                    //sendNoify
                                    public function deleteNoify(Request $request)
                                    {
                                        $affected = DB::table('tbl_notify')
                                            ->where('id', $request->rowid)
                                            ->update(['is_deleted' => 1]);

                                        $data = array(
                                            'msg' => 'Deleted Successfully',
                                            'status' => 1
                                        );
                                        return response()->json($data);
                                    }

                                    //sendNoify

                                    //deleteNoify
                                    public function sendNoify(Request $request)
                                    {
                                        $arrData = DB::table('tbl_notify')
                                            ->where('id', $request->rowid)
                                            ->first();


                                        DB::table('tbl_notify_users')->insert([
                                            'noti_id' => $request->rowid,
                                            'sent_on' => date('Y-m-d h:i:s'),
                                            'is_read' => 0,
                                            'note_type' => $arrData->noti_type,
                                        ]);

                                        $affected = DB::table('tbl_notify')
                                            ->where('id', $request->rowid)
                                            ->update(['is_send' => 2]);

                                        $data = array(
                                            'msg' => 'Send Successfully',
                                            'status' => 1
                                        );
                                        return response()->json($data);
                                    }

                                    //deleteNoify

                                    //deleteUser
                                    public function deleteSchool(Request $request)
                                    {
                                        $affected = DB::table('schools')
                                            ->where('id', $request->rowid)
                                            ->update(['is_deleted' => 1]);

                                        $data = array(
                                            'msg' => 'Deleted Successfully',
                                            'status' => 1
                                        );
                                        return response()->json($data);
                                    }
                                    //saveSettleDuePaymetOfCouser
                                    public function saveSettleDuePaymetOfCouser(Request $request)
                                    {

                                        $txtUID = $request->txtUID;
                                        $txtCouserID = $request->txtCouserID;
                                        $txtSID = $request->txtSID;
                                        $txtCourseAmt = $request->txtCourseAmt;
                                        $txtTotalPaidAmt = $request->txtTotalPaidAmt;
                                        $txtdueAmt = $request->txtdueAmt;
                                        $txtRemarks = $request->txtRemarks;

                                        DB::table('course_payment')->insert([
                                            'course_id' => $txtCouserID,
                                            'course_amt' => $txtCourseAmt,
                                            'payment_amt' => $txtdueAmt,
                                            'paid_on' => date('Y-m-d'),
                                            'pay_mode' => 2,
                                            'user_id' => $txtUID,
                                            'sid' => $txtSID,
                                            'pay_id' => 44

                                        ]);

                                        return redirect()->back()->with('success', 'Payment Updated Successfully');

                                        //course_payment

                                    }
                                    //saveSettleDuePaymetOfCouser
                                    //getEnrolledFilter
                                    public function getEnrolledFilter(Request $request)
                                    {
                                        print_r($request->all());
                                    }
                                    //getEnrolledFilter
                                    //
                                    //getCertifyByIDAdmin
                                    public function getCertifyByIDAdmin(Request $request)
                                    {
                                        $rowID = $request->rowID;
                                        $users_arrArr = DB::table('school_course')->where('sid', $rowID)->where('is_deleted', 0)->orderBy('id', 'DESC')->first();

                                        $HTML = '<table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Course Title</th>
                                                <th scope="col">Course Amount</th>
                                                <th scope="col">Course Information</th>
                                                <th scope="col">Duration</th>
                                            </tr>
                                        </thead>
                                        <tbody>';


                                        $HTML .= ' <tr>
          <th scope="row">1</th>
          <td>' . @$users_arrArr->certificate_title . '</td>
          <td>' . @$users_arrArr->course_amt . '</td>
          <td>' . @$users_arrArr->course_info . '</td>
          <td>' . @$users_arrArr->duration . '</td>
         
         
         
      </tr>';
      echo $HTML;


                                    }
                                    //getCertifyByIDAdmin


                                    public function getPaymentHistoryAdmin(Request $request)
                                    {
                                        $rowID = $request->rowID;
                                        $users_arrArr = DB::table('school_course_student')->where('id', $rowID)->where('is_deleted', 0)->orderBy('id', 'DESC')->first();
                                        $student_id = $users_arrArr->student_id;
                                        $course_id = $users_arrArr->course_id;
                                        $payment_arrArr = DB::table('course_payment')->where('course_id', $course_id)->where('user_id', $student_id)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                        $HTML = '<table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Payment Amount</th>
                <th scope="col">Mode</th>
                <th scope="col">Paid On</th>
                <th scope="col">Remarks </th>
            </tr>
        </thead>
        <tbody>';
                                        $i = 0;
                                        foreach ($payment_arrArr as $key => $rowData) {

                                            $i++;
                                            if ($rowData->pay_mode == 1) {
                                                $pmode = 'ONLINE APP';
                                            } else {
                                                $pmode = 'OFFLINE';
                                            }
                                            $paidOn = date('j F Y H:iA', strtotime($rowData->paid_on));
                                            $remarks = $rowData->remarks;
                                            $HTML .= ' <tr>
          <th scope="row">' . $i . '</th>
          <td>' . $rowData->payment_amt . '</td>
         
          <td>
              <span class="label label-inline label-light-primary font-weight-bold">' . $pmode . '</span>
          </td>
          <td>
          <span class="label label-inline label-light-waring font-weight-bold">' . $paidOn . '</span>
      </td>
      <td>
     ' . $remarks . '
  </td>

      </tr>';
                                        }



                                        $HTML . '
        </tbody>
    </table>';

                                        echo $HTML;
                                    }


                                    //getPaymentHistory
                                    public function getPaymentHistory(Request $request)
                                    {
                                        $rowID = $request->rowID;
                                        $users_arrArr = DB::table('school_course_student')->where('id', $rowID)->where('is_deleted', 0)->orderBy('id', 'DESC')->first();
                                        $student_id = $users_arrArr->student_id;
                                        $course_id = $users_arrArr->course_id;
                                        $payment_arrArr = DB::table('course_payment')->where('course_id', $course_id)->where('user_id', $student_id)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                        $HTML = '<table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Payment Amount</th>
                <th scope="col">Mode</th>
                <th scope="col">Paid On</th>
                <th scope="col">Remarks </th>
            </tr>
        </thead>
        <tbody>';
                                        $i = 0;
                                        foreach ($payment_arrArr as $key => $rowData) {

                                            $i++;
                                            if ($rowData->pay_mode == 1) {
                                                $pmode = 'ONLINE APP';
                                            } else {
                                                $pmode = 'OFFLINE';
                                            }
                                            $paidOn = date('j F Y H:iA', strtotime($rowData->paid_on));
                                            $remarks = $rowData->remarks;
                                            $HTML .= ' <tr>
          <th scope="row">' . $i . '</th>
          <td>' . $rowData->payment_amt . '</td>
         
          <td>
              <span class="label label-inline label-light-primary font-weight-bold">' . $pmode . '</span>
          </td>
          <td>
          <span class="label label-inline label-light-waring font-weight-bold">' . $paidOn . '</span>
      </td>
      <td>
     ' . $remarks . '
  </td>

      </tr>';
                                        }



                                        $HTML . '
        </tbody>
    </table>';

                                        echo $HTML;
                                    }
                                    //getPaymentHistory
                                    //getDatatableAdminUserEnrolledCouseListFilterSuper
                                    public function getDatatableAdminUserEnrolledCouseListFilterSuper(Request $request)
                                    {
                                        $data_arr = array();
                                        $sid = $request->txtSID;

                                        $couserID = $request->couserID;
                                        $getWeelData = $request->getWeelData;
                                        $myDate = $request->startDate;
                                        $myDateA = $request->endDate;
                                        $paymentType = $request->paymentType;
                                        if (empty($paymentType)) {

                                            if (empty($couserID)) {
                                                if (empty($getWeelData)) {
                                                    $users_arrArr = DB::table('school_course_student')->where('sid', $sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                                } else {


                                                    $weekStartDate = Carbon::createFromFormat('m/d/Y', $myDate)
                                                        ->format('Y-m-d');
                                                    $weekEndDate = Carbon::createFromFormat('m/d/Y', $myDateA)
                                                        ->format('Y-m-d');

                                                    $users_arrArr = DB::table('school_course_student')->whereBetween('created_at', [$weekStartDate, $weekEndDate])->where('sid', $sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                                }
                                            } else {
                                                if (empty($getWeelData)) {
                                                    $users_arrArr = DB::table('school_course_student')->where('course_id', $couserID)->where('sid', $sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                                } else {
                                                    $weekStartDate = Carbon::createFromFormat('m/d/Y', $myDate)
                                                        ->format('Y-m-d');
                                                    $weekEndDate = Carbon::createFromFormat('m/d/Y', $myDateA)
                                                        ->format('Y-m-d');

                                                    $users_arrArr = DB::table('school_course_student')->whereBetween('created_at', [$weekStartDate, $weekEndDate])->where('course_id', $couserID)->where('sid', $sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                                }
                                            }
                                        } else {
                                            if (empty($couserID)) {
                                                if (empty($getWeelData)) {
                                                    $users_arrArr = DB::table('school_course_student')->where('payment_status', $paymentType)->where('sid', $sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                                } else {


                                                    $weekStartDate = Carbon::createFromFormat('m/d/Y', $myDate)
                                                        ->format('Y-m-d');
                                                    $weekEndDate = Carbon::createFromFormat('m/d/Y', $myDateA)
                                                        ->format('Y-m-d');

                                                    $users_arrArr = DB::table('school_course_student')->where('payment_status', $paymentType)->whereBetween('created_at', [$weekStartDate, $weekEndDate])->where('sid', $sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                                }
                                            } else {
                                                if (empty($getWeelData)) {
                                                    $users_arrArr = DB::table('school_course_student')->where('payment_status', $paymentType)->where('course_id', $couserID)->where('sid', $sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                                } else {
                                                    $weekStartDate = Carbon::createFromFormat('m/d/Y', $myDate)
                                                        ->format('Y-m-d');
                                                    $weekEndDate = Carbon::createFromFormat('m/d/Y', $myDateA)
                                                        ->format('Y-m-d');

                                                    $users_arrArr = DB::table('school_course_student')->where('payment_status', $paymentType)->whereBetween('created_at', [$weekStartDate, $weekEndDate])->where('course_id', $couserID)->where('sid', $sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                                }
                                            }
                                        }









                                        $i = 0;
                                        foreach ($users_arrArr as $key => $value) {
                                            $i++;

                                            $schoolArr = DB::table('users')->where('id', $value->student_id)->whereNotNull('avatar')->first();
                                            if ($schoolArr == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
                                            }

                                            $schoolCourseArr = DB::table('school_course')->where('id', $value->course_id)->where('sid', $sid)->where('is_deleted', 0)->first();
                                            $schoolCoursePaymentArr = DB::table('course_payment')->where('course_id', $value->course_id)->where('user_id', $value->student_id)->where('is_deleted', 0)->sum('payment_amt');

                                            $dueAMT = ($schoolCourseArr->course_amt) - ($schoolCoursePaymentArr);
                                            if ($dueAMT > 0) {
                                                $payment_status = 2;
                                                $affected = DB::table('school_course_student')
                                                    ->where('id', $value->id)
                                                    ->update(['payment_status' => 2]);
                                            } else {
                                                $payment_status = 1;
                                                $affected = DB::table('school_course_student')
                                                    ->where('id', $value->id)
                                                    ->update(['payment_status' => 1]);
                                            }

                                            //---------------------------------------

                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'photo' => $schLogo,
                                                'course_name' => $schoolCourseArr->certificate_title . "-" . $schoolCourseArr->course_amt,
                                                'join_date' =>  $value->created_at,
                                                'payment_status' => $payment_status,
                                                'payment_amount' => $schoolCoursePaymentArr,
                                                'dueAMT' => $dueAMT,
                                                'name' => $schoolArr->name,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'photo'  => true,
                                            'name'      => true,
                                            'course_name'      => true,
                                            'join_date'      => true,
                                            'payment_status'      => true,
                                            'payment_amount'      => true,
                                            'dueAMT'      => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getDatatableAdminUserEnrolledCouseListFilterSuper

                                    //getDatatableAdminUserEnrolledCouseListFilter
                                    public function getDatatableAdminUserEnrolledCouseListFilter(Request $request)
                                    {
                                        $data_arr = array();

                                        $couserID = $request->couserID;
                                        $getWeelData = $request->getWeelData;
                                        $myDate = $request->startDate;
                                        $myDateA = $request->endDate;
                                        $paymentType = $request->paymentType;
                                        if (empty($paymentType)) {

                                            if (empty($couserID)) {
                                                if (empty($getWeelData)) {
                                                    $users_arrArr = DB::table('school_course_student')->where('sid', Auth::user()->sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                                } else {


                                                    $weekStartDate = Carbon::createFromFormat('m/d/Y', $myDate)
                                                        ->format('Y-m-d');
                                                    $weekEndDate = Carbon::createFromFormat('m/d/Y', $myDateA)
                                                        ->format('Y-m-d');

                                                    $users_arrArr = DB::table('school_course_student')->whereBetween('created_at', [$weekStartDate, $weekEndDate])->where('sid', Auth::user()->sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                                }
                                            } else {
                                                if (empty($getWeelData)) {
                                                    $users_arrArr = DB::table('school_course_student')->where('course_id', $couserID)->where('sid', Auth::user()->sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                                } else {
                                                    $weekStartDate = Carbon::createFromFormat('m/d/Y', $myDate)
                                                        ->format('Y-m-d');
                                                    $weekEndDate = Carbon::createFromFormat('m/d/Y', $myDateA)
                                                        ->format('Y-m-d');

                                                    $users_arrArr = DB::table('school_course_student')->whereBetween('created_at', [$weekStartDate, $weekEndDate])->where('course_id', $couserID)->where('sid', Auth::user()->sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                                }
                                            }
                                        } else {
                                            if (empty($couserID)) {
                                                if (empty($getWeelData)) {
                                                    $users_arrArr = DB::table('school_course_student')->where('payment_status', $paymentType)->where('sid', Auth::user()->sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                                } else {


                                                    $weekStartDate = Carbon::createFromFormat('m/d/Y', $myDate)
                                                        ->format('Y-m-d');
                                                    $weekEndDate = Carbon::createFromFormat('m/d/Y', $myDateA)
                                                        ->format('Y-m-d');

                                                    $users_arrArr = DB::table('school_course_student')->where('payment_status', $paymentType)->whereBetween('created_at', [$weekStartDate, $weekEndDate])->where('sid', Auth::user()->sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                                }
                                            } else {
                                                if (empty($getWeelData)) {
                                                    $users_arrArr = DB::table('school_course_student')->where('payment_status', $paymentType)->where('course_id', $couserID)->where('sid', Auth::user()->sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                                } else {
                                                    $weekStartDate = Carbon::createFromFormat('m/d/Y', $myDate)
                                                        ->format('Y-m-d');
                                                    $weekEndDate = Carbon::createFromFormat('m/d/Y', $myDateA)
                                                        ->format('Y-m-d');

                                                    $users_arrArr = DB::table('school_course_student')->where('payment_status', $paymentType)->whereBetween('created_at', [$weekStartDate, $weekEndDate])->where('course_id', $couserID)->where('sid', Auth::user()->sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();
                                                }
                                            }
                                        }









                                        $i = 0;
                                        foreach ($users_arrArr as $key => $value) {
                                            $i++;

                                            $schoolArr = DB::table('users')->where('id', $value->student_id)->whereNotNull('avatar')->first();
                                            if ($schoolArr == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
                                            }

                                            $schoolCourseArr = DB::table('school_course')->where('id', $value->course_id)->where('sid', Auth::user()->sid)->where('is_deleted', 0)->first();
                                            $schoolCoursePaymentArr = DB::table('course_payment')->where('course_id', $value->course_id)->where('user_id', $value->student_id)->where('is_deleted', 0)->sum('payment_amt');

                                            $dueAMT = ($schoolCourseArr->course_amt) - ($schoolCoursePaymentArr);
                                            if ($dueAMT > 0) {
                                                $payment_status = 2;
                                                $affected = DB::table('school_course_student')
                                                    ->where('id', $value->id)
                                                    ->update(['payment_status' => 2]);
                                            } else {
                                                $payment_status = 1;
                                                $affected = DB::table('school_course_student')
                                                    ->where('id', $value->id)
                                                    ->update(['payment_status' => 1]);
                                            }

                                            //---------------------------------------

                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'photo' => $schLogo,
                                                'course_name' => $schoolCourseArr->certificate_title . "-" . $schoolCourseArr->course_amt,
                                                'join_date' =>  $value->created_at,
                                                'payment_status' => $payment_status,
                                                'payment_amount' => $schoolCoursePaymentArr,
                                                'dueAMT' => $dueAMT,
                                                'name' => $schoolArr->name,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'photo'  => true,
                                            'name'      => true,
                                            'course_name'      => true,
                                            'join_date'      => true,
                                            'payment_status'      => true,
                                            'payment_amount'      => true,
                                            'dueAMT'      => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getDatatableAdminUserEnrolledCouseListFilter


                                    //getDatatableAdminGroupChatList
                                    public function getDatatableAdminGroupChatList(Request $request)
                                    {
                                        $data_arr = array();

                                        $users_arrArr = DB::table('chat_group')->where('is_deleted', 0)->where('status', 1)->orderBy('id', 'DESC')->get();


                                        $i = 0;
                                        foreach ($users_arrArr as $key => $value) {
                                            $i++;
                                            if ($value->photo == null) {
                                                $photo = NoImage();
                                            } else {
                                                $photo = asset('/local/public/upload/') . "/" . $value->photo;
                                            }

                                            $schoolArr = DB::table('users')->where('id', $value->created_by)->first();


                                            $gUserCount = DB::table('group_chat_users')->where('is_deleted', 0)->where('gid', $value->id)->count();

                                            //---------------------------------------

                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'group_name' => $value->group_name,
                                                'group_title' => $value->group_title,
                                                'created_at' =>  $value->created_at,
                                                'created_by' => $schoolArr->name,
                                                'status' =>  $value->status,
                                                'membercount' =>  $gUserCount,
                                                'photo' =>  $photo,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'photo'  => true,
                                            'name'      => true,
                                            'course_name'      => true,
                                            'join_date'      => true,
                                            'payment_status'      => true,
                                            'payment_amount'      => true,
                                            'dueAMT'      => true,
                                            'photo'      => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getDatatableAdminGroupChatList
                                    //getDatatableAdminUserEnrolledCouseListSuper
                                    public function getDatatableAdminUserEnrolledCouseListSuper(Request $request)
                                    {
                                        $data_arr = array();

                                        $sid = $request->sid;

                                        $users_arrArr = DB::table('school_course_student')->where('sid', $sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();


                                        $i = 0;
                                        foreach ($users_arrArr as $key => $value) {
                                            $i++;

                                            $schoolArr = DB::table('users')->where('id', $value->student_id)->whereNotNull('avatar')->first();
                                            if ($schoolArr == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
                                            }

                                            $schoolCourseArr = DB::table('school_course')->where('id', $value->course_id)->where('sid', $sid)->where('is_deleted', 0)->first();
                                            $schoolCoursePaymentArr = DB::table('course_payment')->where('course_id', $value->course_id)->where('user_id', $value->student_id)->where('is_deleted', 0)->sum('payment_amt');

                                            $dueAMT = ($schoolCourseArr->course_amt) - ($schoolCoursePaymentArr);
                                            if ($dueAMT > 0) {
                                                $payment_status = 2;
                                                $affected = DB::table('school_course_student')
                                                    ->where('id', $value->id)
                                                    ->update(['payment_status' => 2]);
                                            } else {
                                                $payment_status = 1;
                                                $affected = DB::table('school_course_student')
                                                    ->where('id', $value->id)
                                                    ->update(['payment_status' => 1]);
                                            }

                                            //---------------------------------------

                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'photo' => $schLogo,
                                                'course_name' => $schoolCourseArr->certificate_title . "-" . $schoolCourseArr->course_amt,
                                                'join_date' =>  $value->created_at,
                                                'payment_status' => $payment_status,
                                                'payment_amount' => $schoolCoursePaymentArr,
                                                'dueAMT' => $dueAMT,
                                                'name' => $schoolArr->name,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'photo'  => true,
                                            'name'      => true,
                                            'course_name'      => true,
                                            'join_date'      => true,
                                            'payment_status'      => true,
                                            'payment_amount'      => true,
                                            'dueAMT'      => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getDatatableAdminUserEnrolledCouseListSuper

                                    //getDatatableAdminUserEnrolledCouseList
                                    public function getDatatableAdminUserEnrolledCouseList(Request $request)
                                    {
                                        $data_arr = array();

                                        $users_arrArr = DB::table('school_course_student')->where('sid', Auth::user()->sid)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();


                                        $i = 0;
                                        foreach ($users_arrArr as $key => $value) {
                                            $i++;

                                            $schoolArr = DB::table('users')->where('id', $value->student_id)->whereNotNull('avatar')->first();
                                            if ($schoolArr == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
                                            }

                                            $schoolCourseArr = DB::table('school_course')->where('id', $value->course_id)->where('sid', Auth::user()->sid)->where('is_deleted', 0)->first();
                                            $schoolCoursePaymentArr = DB::table('course_payment')->where('course_id', $value->course_id)->where('user_id', $value->student_id)->where('is_deleted', 0)->sum('payment_amt');

                                            $dueAMT = (@$schoolCourseArr->course_amt) - ($schoolCoursePaymentArr);
                                            if ($dueAMT > 0) {
                                                $payment_status = 2;
                                                $affected = DB::table('school_course_student')
                                                    ->where('id', $value->id)
                                                    ->update(['payment_status' => 2]);
                                            } else {
                                                $payment_status = 1;
                                                $affected = DB::table('school_course_student')
                                                    ->where('id', $value->id)
                                                    ->update(['payment_status' => 1]);
                                            }

                                            //---------------------------------------

                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'photo' => $schLogo,
                                                'course_name' => optional($schoolCourseArr)->certificate_title . "-" . @$schoolCourseArr->course_amt,
                                                'join_date' =>  $value->created_at,
                                                'payment_status' => $payment_status,
                                                'payment_amount' => $schoolCoursePaymentArr,
                                                'dueAMT' => $dueAMT,
                                                'name' => $schoolArr->name,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'photo'  => true,
                                            'name'      => true,
                                            'course_name'      => true,
                                            'join_date'      => true,
                                            'payment_status'      => true,
                                            'payment_amount'      => true,
                                            'dueAMT'      => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getDatatableAdminUserEnrolledCouseList

                                    //getDatatableAdminUserList
                                    public function getDatatableAdminUserList(Request $request)
                                    {
                                        $data_arr = array();

                                        $users_arrArr = DB::table('users')->where('sid', Auth::user()->id)->where('user_type', 3)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();


                                        $i = 0;
                                        foreach ($users_arrArr as $key => $value) {
                                            $i++;

                                            $schoolArr = DB::table('users')->where('id', $value->id)->whereNotNull('avatar')->first();
                                            if ($schoolArr == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
                                            }

                                            //---------------------------------------

                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'photo' => $schLogo,
                                                'name' => $value->name,
                                                'email' =>  $value->email,
                                                'phone' => $value->phone,
                                                'gender' => $value->gender,
                                                'status' => $value->is_active,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'photo'  => true,
                                            'name'      => true,
                                            'email'      => true,
                                            'phone'      => true,
                                            'gender'      => true,
                                            'status'      => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getDatatableAdminUserList

                                    public function getDatatableUserList(Request $request)
                                    {
                                        $data_arr = array();

                                        $users_arrArr = DB::table('users')->where('user_type', 3)->where('is_deleted', 0)->orderBy('id', 'DESC')->get();


                                        $i = 0;
                                        foreach ($users_arrArr as $key => $value) {
                                            $i++;

                                            $schoolArr = DB::table('users')->where('id', $value->id)->whereNotNull('avatar')->first();
                                            if ($schoolArr == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
                                            }

                                            //$value->country_id,
                                            $countryData = DB::table('countries')
                                            ->where('id', $value->country_id)
                                            ->first();


                                            //---------------------------------------

                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'photo' => $schLogo,
                                                'name' => $value->name,
                                                'email' =>  $value->email,
                                                'phone' => $value->phone,
                                                'gender' => $value->gender,
                                                'status' => $value->is_active,
                                                'country' => $countryData->name,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'photo'  => true,
                                            'name'      => true,
                                            'email'      => true,
                                            'phone'      => true,
                                            'gender'      => true,
                                            'status'      => true,
                                            'country'      => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getSchoolRatingCommentsBySchoolMyCourse
                                    public function getSchoolRatingCommentsBySchoolMyCourse(Request $request)
                                    {


                                        if ($request->starRadioVal == 6) {
                                            $scArr_arr = DB::table('school_user_rating_course')->where('sid', Auth::user()->sid)->get();
                                        } else {
                                            $scArr_arr = DB::table('school_user_rating_course')->where('rating_val', $request->starRadioVal)->where('sid', Auth::user()->sid)->get();
                                        }




                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($scArr_arr as $key => $value) {
                                            $i++;
                                            $schorArrData = DB::table('schools')->where('id', $value->sid)->first();
                                            $schorUserArrData = DB::table('users')->where('id', $value->user_id)->first();

                                            $schoolArr = DB::table('users')->where('id', $value->user_id)->whereNotNull('avatar')->first();
                                            if ($schoolArr == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
                                            }



                                            //---------------------------------------

                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'user_name' => $schorUserArrData->name,
                                                'user_pic' => $schLogo,
                                                'user_id' => $schoolArr->id,
                                                'rating' => $value->rating_val,
                                                'comment' => $value->comment,
                                                'created_at' => date('Y-m-d H:iA', strtotime($value->created_at)),
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,

                                            'user_name'    => true,
                                            'user_pic'     => true,
                                            'user_id' => true,
                                            'rating'       => true,
                                            'comment'      => true,
                                            'created_at'   => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getSchoolRatingCommentsBySchoolMyCourse

                                    //getSchoolRatingCommentsBySchoolMy
                                    public function getSchoolRatingCommentsBySchoolMy(Request $request)
                                    {


                                        if ($request->starRadioVal == 6) {
                                            $scArr_arr = DB::table('school_user_rating')->where('sid', Auth::user()->sid)->get();
                                        } else {
                                            $scArr_arr = DB::table('school_user_rating')->where('rating_val', $request->starRadioVal)->where('sid', Auth::user()->sid)->get();
                                        }




                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($scArr_arr as $key => $value) {
                                            $i++;
                                            $schorArrData = DB::table('schools')->where('id', $value->sid)->first();
                                            $schorUserArrData = DB::table('users')->where('id', $value->user_id)->first();

                                            $schoolArr = DB::table('users')->where('id', $value->user_id)->whereNotNull('avatar')->first();
                                            if ($schoolArr == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
                                            }



                                            //---------------------------------------

                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'user_name' => $schorUserArrData->name,
                                                'user_pic' => $schLogo,
                                                'user_id' => $schoolArr->id,
                                                'rating' => $value->rating_val,
                                                'comment' => $value->comment,
                                                'created_at' => date('Y-m-d H:iA', strtotime($value->created_at)),
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,

                                            'user_name'    => true,
                                            'user_pic'     => true,
                                            'user_id' => true,
                                            'rating'       => true,
                                            'comment'      => true,
                                            'created_at'   => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getSchoolRatingCommentsBySchoolMy

                                    //getSchoolRatingCommentsBySchool
                                    public function getSchoolRatingCommentsBySchool(Request $request)
                                    {


                                        if ($request->starRadioVal == 6) {
                                            $scArr_arr = DB::table('school_user_rating')->where('sid', $request->sid)->get();
                                        } else {
                                            $scArr_arr = DB::table('school_user_rating')->where('rating_val', $request->starRadioVal)->where('sid', $request->sid)->get();
                                        }




                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($scArr_arr as $key => $value) {
                                            $i++;
                                            $schorArrData = DB::table('schools')->where('id', $value->sid)->first();
                                            $schorUserArrData = DB::table('users')->where('id', $value->user_id)->first();

                                            $schoolArr = DB::table('users')->where('id', $value->user_id)->whereNotNull('avatar')->first();
                                            if ($schoolArr == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
                                            }



                                            //---------------------------------------

                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'user_name' => $schorUserArrData->name,
                                                'user_pic' => $schLogo,
                                                'user_id' => $schoolArr->id,
                                                'rating' => $value->rating_val,
                                                'comment' => $value->comment,
                                                'created_at' => date('Y-m-d H:iA', strtotime($value->created_at)),
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,

                                            'user_name'    => true,
                                            'user_pic'     => true,
                                            'user_id' => true,
                                            'rating'       => true,
                                            'comment'      => true,
                                            'created_at'   => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getSchoolRatingCommentsBySchool
                                    //deleteGroupChat
                                    public function deleteGroupChat(Request $request)
                                    {
                                        $affected = DB::table('chat_group')
                                            ->where('id', $request->rowid)
                                            ->update(['is_deleted' => 1]);

                                        $data = array(
                                            'msg' => 'Deleted Successfully',
                                            'status' => 1
                                        );
                                        return response()->json($data);
                                    }

                                    //deleteGroupChat

                                    //removeGroupchatUser
                                    public function removeGroupchatUser(Request $request)
                                    {
                                        $affected = DB::table('group_chat_users')
                                            ->where('id', $request->rowid)
                                            ->update(['is_deleted' => 1]);

                                        $data = array(
                                            'msg' => 'Deleted Successfully',
                                            'status' => 1
                                        );
                                        return response()->json($data);
                                    }
                                    //removeGroupchatUser

                                    //getSchoolGroupchatUsers
                                    public function getSchoolGroupchatUsers(Request $request)
                                    {

                                        $scArr_arr = DB::table('group_chat_users')->where('gid', $request->rowID)->where('is_deleted', 0)->where('sid', Auth::user()->sid)->get();


                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($scArr_arr as $key => $value) {

                                            $schoolArr = DB::table('users')->where('id', $value->user_id)->first();
                                            if (@$schoolArr->avatar == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
                                            }

                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'user_name' => @$schoolArr->name,
                                                'user_pic' => $schLogo,
                                                'user_id' => $schoolArr->id,
                                                'created_at' => date('j F Y', strtotime($value->created_at)),
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'user_name'    => true,
                                            'user_pic'     => true,
                                            'user_id' => true,
                                            'created_at'   => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }
                                    //getSchoolGroupchatUsers

                                    //getSchoolRatingCommentsMyCourse
                                    public function getSchoolRatingCommentsMyCourse(Request $request)
                                    {


                                        $scArr_arr = DB::table('school_user_rating_course')->where('sid', Auth::user()->sid)->get();


                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($scArr_arr as $key => $value) {
                                            $i++;
                                            $schorArrData = DB::table('schools')->where('id', $value->sid)->first();
                                            $schorUserArrData = DB::table('users')->where('id', $value->user_id)->first();

                                            $schoolArr = DB::table('users')->where('id', $value->user_id)->whereNotNull('avatar')->first();
                                            if ($schoolArr == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
                                            }



                                            //---------------------------------------

                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'user_name' => $schorUserArrData->name,
                                                'user_pic' => $schLogo,
                                                'user_id' => $schoolArr->id,
                                                'rating' => $value->rating_val,
                                                'comment' => $value->comment,
                                                'created_at' => date('Y-m-d H:iA', strtotime($value->created_at)),
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,

                                            'user_name'    => true,
                                            'user_pic'     => true,
                                            'user_id' => true,
                                            'rating'       => true,
                                            'comment'      => true,
                                            'created_at'   => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getSchoolRatingCommentsMyCourse

                                    //getSchoolRatingCommentsMy
                                    public function getSchoolRatingCommentsMy(Request $request)
                                    {


                                        $scArr_arr = DB::table('school_user_rating')->where('sid', Auth::user()->sid)->get();


                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($scArr_arr as $key => $value) {
                                            $i++;
                                            $schorArrData = DB::table('schools')->where('id', $value->sid)->first();
                                            $schorUserArrData = DB::table('users')->where('id', $value->user_id)->first();

                                            $schoolArr = DB::table('users')->where('id', $value->user_id)->whereNotNull('avatar')->first();
                                            if ($schoolArr == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
                                            }



                                            //---------------------------------------

                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'user_name' => $schorUserArrData->name,
                                                'user_pic' => $schLogo,
                                                'user_id' => $schoolArr->id,
                                                'rating' => $value->rating_val,
                                                'comment' => $value->comment,
                                                'created_at' => date('Y-m-d H:iA', strtotime($value->created_at)),
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,

                                            'user_name'    => true,
                                            'user_pic'     => true,
                                            'user_id' => true,
                                            'rating'       => true,
                                            'comment'      => true,
                                            'created_at'   => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getSchoolRatingCommentsMy

                                    //getSchoolRatingComments
                                    public function getSchoolRatingComments(Request $request)
                                    {


                                        $scArr_arr = DB::table('school_user_rating')->where('sid', $request->sid)->get();


                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($scArr_arr as $key => $value) {
                                            $i++;
                                            $schorArrData = DB::table('schools')->where('id', $value->sid)->first();
                                            $schorUserArrData = DB::table('users')->where('id', $value->user_id)->first();

                                            $schoolArr = DB::table('users')->where('id', $value->user_id)->whereNotNull('avatar')->first();
                                            if ($schoolArr == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
                                            }



                                            //---------------------------------------

                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'user_name' => $schorUserArrData->name,
                                                'user_pic' => $schLogo,
                                                'user_id' => $schoolArr->id,
                                                'rating' => $value->rating_val,
                                                'comment' => $value->comment,
                                                'created_at' => date('Y-m-d H:iA', strtotime($value->created_at)),
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,

                                            'user_name'    => true,
                                            'user_pic'     => true,
                                            'user_id' => true,
                                            'rating'       => true,
                                            'comment'      => true,
                                            'created_at'   => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }
                                    //getSchoolRatingComments

                                    //getSchoolPerformanceFilterTop
                                    public function getSchoolPerformanceFilterTop(Request $request)
                                    {

                                        if ($request->starRadioVal == 1) {
                                            $scArr_arr = DB::table('school_rating')->where('is_deleted', 0)->orderBy('avg_rating', 'asc')->get();

                                            $data_arr = array();
                                            $i = 0;
                                            foreach ($scArr_arr as $key => $value) {
                                                $i++;
                                                $schorArrData = DB::table('schools')->where('id', $value->sid)->first();

                                                //---------------------------------------

                                                $data_arr[] = array(
                                                    'RecordID' => $value->id,
                                                    'IndexID' => $i,
                                                    'school_title' => $schorArrData->title,
                                                    'rating' => $value->rating,
                                                    'avg_rating' => $value->avg_rating,
                                                    'country_name' => 'India',
                                                    'Actions' => ''

                                                );
                                            }

                                            $JSON_Data = json_encode($data_arr);
                                            $columnsDefault = [
                                                'RecordID'  => true,
                                                'IndexID' => true,
                                                'school_title'  => true,
                                                'rating'      => true,
                                                'avg_rating'      => true,
                                                'country_name'      => true,

                                                'Actions'      => true,
                                            ];
                                        }
                                        if ($request->starRadioVal == 2) {
                                            $scArr_arr = DB::table('school_rating')->where('is_deleted', 0)->orderBy('avg_rating', 'desc')->get();

                                            $data_arr = array();
                                            $i = 0;
                                            foreach ($scArr_arr as $key => $value) {
                                                $i++;
                                                $schorArrData = DB::table('schools')->where('id', $value->sid)->first();

                                                //---------------------------------------

                                                $data_arr[] = array(
                                                    'RecordID' => $value->id,
                                                    'IndexID' => $i,
                                                    'school_title' => $schorArrData->title,
                                                    'rating' => $value->rating,
                                                    'avg_rating' => $value->avg_rating,
                                                    'country_name' => 'India',
                                                    'Actions' => ''

                                                );
                                            }

                                            $JSON_Data = json_encode($data_arr);
                                            $columnsDefault = [
                                                'RecordID'  => true,
                                                'IndexID' => true,
                                                'school_title'  => true,
                                                'rating'      => true,
                                                'avg_rating'      => true,
                                                'country_name'      => true,

                                                'Actions'      => true,
                                            ];
                                        }





                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getSchoolPerformanceFilterTop

                                    //getSchoolPerformanceFilter
                                    public function getSchoolPerformanceA(Request $request)
                                    {


                                        $scArr_arr = DB::table('school_rating')->where('rating', $request->starRadioVal)->where('is_deleted', 0)->get();


                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($scArr_arr as $key => $value) {
                                            $i++;
                                            $schorArrData = DB::table('schools')->where('id', $value->sid)->first();

                                            //---------------------------------------

                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'school_title' => $schorArrData->title,
                                                'rating' => $value->rating,
                                                'avg_rating' => $value->avg_rating,
                                                'country_name' => 'India',
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'school_title'  => true,
                                            'rating'      => true,
                                            'avg_rating'      => true,
                                            'country_name'      => true,

                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getSchoolPerformanceFilter
                                    public function calcAverageRating($ratings)
                                    {

                                        $totalWeight = 0;
                                        $totalReviews = 0;

                                        foreach ($ratings as $weight => $numberofReviews) {
                                            $WeightMultipliedByNumber = $weight * $numberofReviews;
                                            $totalWeight += $WeightMultipliedByNumber;
                                            $totalReviews += $numberofReviews;
                                        }

                                        //divide the total weight by total number of reviews
                                        $averageRating = $totalWeight / $totalReviews;

                                        return intVal($averageRating);
                                    }


                                    //getSchoolPerformanceFilterMy
                                    public function getSchoolPerformanceFilterMy(Request $request)
                                    {

                                        if ($request->starRadioVal == 6) {
                                            $scArr_arr = DB::table('school_rating')->where('sid', Auth::user()->sid)->where('is_deleted', 0)->orderBy('school_name', 'asc')->get();
                                        } else {
                                            $scArr_arr = DB::table('school_rating')->where('sid', Auth::user()->sid)->where('avg_rating', $request->starRadioVal)->where('is_deleted', 0)->orderBy('school_name', 'asc')->get();
                                        }



                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($scArr_arr as $key => $value) {
                                            $schorArrData = DB::table('schools')->where('id', $value->sid)->first();
                                            $scArr_arrcount = DB::table('school_user_rating')->where('sid', $value->sid)->count();


                                            $scArr_arrAVG_1 = DB::table('school_user_rating')->where('sid', $value->sid)->where('is_deleted', 0)->where('rating_val', 1)->count();
                                            $scArr_arrAVG_2 = DB::table('school_user_rating')->where('sid', $value->sid)->where('is_deleted', 0)->where('rating_val', 2)->count();
                                            $scArr_arrAVG_3 = DB::table('school_user_rating')->where('sid', $value->sid)->where('is_deleted', 0)->where('rating_val', 3)->count();
                                            $scArr_arrAVG_4 = DB::table('school_user_rating')->where('sid', $value->sid)->where('is_deleted', 0)->where('rating_val', 4)->count();
                                            $scArr_arrAVG_5 = DB::table('school_user_rating')->where('sid', $value->sid)->where('is_deleted', 0)->where('rating_val', 5)->count();



                                            $ratings = array(
                                                5 => $scArr_arrAVG_5,
                                                4 => $scArr_arrAVG_4,
                                                3 => $scArr_arrAVG_3,
                                                2 => $scArr_arrAVG_2,
                                                1 => $scArr_arrAVG_1,
                                            );

                                            $avgRating = $this->calcAverageRating($ratings);

                                            $affected = DB::table('school_rating')
                                                ->where('sid', $value->sid)
                                                ->update([
                                                    'total_rating' => $scArr_arrcount,
                                                    'avg_rating' => $avgRating
                                                ]);

                                            $i++;

                                            $data_arr[] = array(
                                                'RecordID' => $value->sid,
                                                'IndexID' => $i,
                                                'school_title' => $schorArrData->title,
                                                'rating' => $scArr_arrcount,
                                                'avg_rating' => $avgRating,
                                                'country_name' => @$schorArrDataCountry->name,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'school_title'  => true,
                                            'rating'      => true,
                                            'avg_rating'      => true,
                                            'country_name'      => true,

                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getSchoolPerformanceFilterMy

                                    //getSchoolPerformanceFilterCountry
                                    public function getSchoolPerformanceFilterCountry(Request $request)
                                    {

                                        if ($request->starRadioTOPVal == 1) { //top //desc
                                            if ($request->starRadioVal == 6) {
                                                if (empty($request->countryID)) {
                                                    $scArr_arr = DB::table('school_rating')->where('is_deleted', 0)->orderBy('avg_rating', 'desc')->get();
                                                } else {
                                                    $scArr_arr = DB::table('school_rating')->where('country_id', $request->countryID)->where('is_deleted', 0)->orderBy('avg_rating', 'desc')->get();
                                                }
                                            } else {
                                                if (empty($request->countryID)) {
                                                    $scArr_arr = DB::table('school_rating')->where('avg_rating', $request->starRadioVal)->where('is_deleted', 0)->orderBy('avg_rating', 'desc')->get();
                                                } else {
                                                    $scArr_arr = DB::table('school_rating')->where('country_id', $request->countryID)->where('avg_rating', $request->starRadioVal)->where('is_deleted', 0)->orderBy('avg_rating', 'desc')->get();
                                                }
                                            }
                                        }
                                        if ($request->starRadioTOPVal == 2) { //worst //asc
                                            if ($request->starRadioVal == 6) {
                                                if (empty($request->countryID)) {
                                                    $scArr_arr = DB::table('school_rating')->where('is_deleted', 0)->orderBy('avg_rating', 'asc')->get();
                                                } else {
                                                    $scArr_arr = DB::table('school_rating')->where('country_id', $request->countryID)->where('is_deleted', 0)->orderBy('avg_rating', 'asc')->get();
                                                }
                                            } else {
                                                if (empty($request->countryID)) {
                                                    $scArr_arr = DB::table('school_rating')->where('avg_rating', $request->starRadioVal)->where('is_deleted', 0)->orderBy('avg_rating', 'asc')->get();
                                                } else {
                                                    $scArr_arr = DB::table('school_rating')->where('country_id', $request->countryID)->where('avg_rating', $request->starRadioVal)->where('is_deleted', 0)->orderBy('avg_rating', 'asc')->get();
                                                }
                                            }
                                        }
                                        if ($request->starRadioTOPVal == 3) { //all with out asc and desc
                                            if ($request->starRadioVal == 6) {
                                                if (empty($request->countryID)) {
                                                    $scArr_arr = DB::table('school_rating')->where('is_deleted', 0)->orderBy('school_name', 'asc')->get();
                                                } else {
                                                    $scArr_arr = DB::table('school_rating')->where('country_id', $request->countryID)->where('is_deleted', 0)->orderBy('school_name', 'asc')->get();
                                                }
                                            } else {
                                                if (empty($request->countryID)) {
                                                    $scArr_arr = DB::table('school_rating')->where('avg_rating', $request->starRadioVal)->where('is_deleted', 0)->orderBy('school_name', 'asc')->get();
                                                } else {
                                                    $scArr_arr = DB::table('school_rating')->where('country_id', $request->countryID)->where('avg_rating', $request->starRadioVal)->where('is_deleted', 0)->orderBy('school_name', 'asc')->get();
                                                }
                                            }
                                        }


                                        // $scArr_arr = DB::table('school_rating')->where('is_deleted', 0)->get();
                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($scArr_arr as $key => $value) {
                                            $schorArrData = DB::table('schools')->where('id', $value->sid)->first();
                                            $scArr_arrcount = DB::table('school_user_rating')->where('sid', $value->sid)->count();
                                            $schorArrDataCountry = DB::table('countries')->where('id', $schorArrData->country_id)->first();

                                            $scArr_arrAVG_1 = DB::table('school_user_rating')->where('id', $value->sid)->where('is_deleted', 0)->where('rating_val', 1)->count();
                                            $scArr_arrAVG_2 = DB::table('school_user_rating')->where('id', $value->sid)->where('is_deleted', 0)->where('rating_val', 2)->count();
                                            $scArr_arrAVG_3 = DB::table('school_user_rating')->where('id', $value->sid)->where('is_deleted', 0)->where('rating_val', 3)->count();
                                            $scArr_arrAVG_4 = DB::table('school_user_rating')->where('id', $value->sid)->where('is_deleted', 0)->where('rating_val', 4)->count();
                                            $scArr_arrAVG_5 = DB::table('school_user_rating')->where('id', $value->sid)->where('is_deleted', 0)->where('rating_val', 5)->count();



                                            $ratings = array(
                                                5 => $scArr_arrAVG_5,
                                                4 => $scArr_arrAVG_4,
                                                3 => $scArr_arrAVG_3,
                                                2 => $scArr_arrAVG_2,
                                                1 => $scArr_arrAVG_1,
                                            );

                                            $avgRating = $this->calcAverageRating($ratings);

                                            $affected = DB::table('school_rating')
                                                ->where('sid', $value->sid)
                                                ->update([
                                                    'total_rating' => $scArr_arrcount,
                                                    'avg_rating' => $avgRating
                                                ]);

                                            $i++;

                                            $data_arr[] = array(
                                                'RecordID' => $value->sid,
                                                'IndexID' => $i,
                                                'school_title' => $schorArrData->title,
                                                'rating' => $scArr_arrcount,
                                                'avg_rating' => $avgRating,
                                                'country_name' => @$schorArrDataCountry->name,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'school_title'  => true,
                                            'rating'      => true,
                                            'avg_rating'      => true,
                                            'country_name'      => true,

                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getSchoolPerformanceFilterCountry
                                    //getSchoolPerformanceMy
                                    public function getSchoolPerformanceMy(Request $request)
                                    {
                                        $scArr_arr = DB::table('school_rating')->where('sid', Auth::user()->sid)->where('is_deleted', 0)->orderBy('school_name', 'asc')->get();

                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($scArr_arr as $key => $value) {

                                            $schorArrData = DB::table('schools')->where('id', $value->sid)->first();
                                            $scArr_arrcount = DB::table('school_user_rating')->where('sid', $value->sid)->count();
                                            $schorArrDataCountry = DB::table('countries')->where('id', $schorArrData->country_id)->first();

                                            $scArr_arrAVG_1 = DB::table('school_user_rating')->where('sid', $value->sid)->where('is_deleted', 0)->where('rating_val', 1)->count();
                                            $scArr_arrAVG_2 = DB::table('school_user_rating')->where('sid', $value->sid)->where('is_deleted', 0)->where('rating_val', 2)->count();
                                            $scArr_arrAVG_3 = DB::table('school_user_rating')->where('sid', $value->sid)->where('is_deleted', 0)->where('rating_val', 3)->count();
                                            $scArr_arrAVG_4 = DB::table('school_user_rating')->where('sid', $value->sid)->where('is_deleted', 0)->where('rating_val', 4)->count();
                                            $scArr_arrAVG_5 = DB::table('school_user_rating')->where('sid', $value->sid)->where('is_deleted', 0)->where('rating_val', 5)->count();



                                            $ratings = array(
                                                5 => $scArr_arrAVG_5,
                                                4 => $scArr_arrAVG_4,
                                                3 => $scArr_arrAVG_3,
                                                2 => $scArr_arrAVG_2,
                                                1 => $scArr_arrAVG_1,
                                            );



                                            $avgRating = $this->calcAverageRating($ratings);
                                            $affected = DB::table('school_rating')
                                                ->where('sid', $value->sid)
                                                ->update([
                                                    'total_rating' => $scArr_arrcount,
                                                    'avg_rating' => $avgRating,
                                                    'country_id' => $schorArrData->country_id,
                                                    'school_name' => $schorArrData->title,
                                                ]);

                                            $i++;
                                            $data_arr[] = array(
                                                'RecordID' => $value->sid,
                                                'IndexID' => $i,
                                                'school_title' => $schorArrData->title,
                                                'rating' => $scArr_arrcount,
                                                'avg_rating' => $avgRating,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'school_title'  => true,
                                            'rating'      => true,
                                            'avg_rating'      => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getSchoolPerformanceMy

                                    public function getSchoolPerformance(Request $request)
                                    {
                                        $scArr_arr = DB::table('school_rating')->where('is_deleted', 0)->orderBy('school_name', 'asc')->get();
                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($scArr_arr as $key => $value) {

                                            $schorArrData = DB::table('schools')->where('id', $value->sid)->first();
                                            $scArr_arrcount = DB::table('school_user_rating')->where('sid', $value->sid)->count();
                                            $schorArrDataCountry = DB::table('countries')->where('id', $schorArrData->country_id)->first();

                                            $scArr_arrAVG_1 = DB::table('school_user_rating')->where('sid', $value->sid)->where('is_deleted', 0)->where('rating_val', 1)->count();
                                            $scArr_arrAVG_2 = DB::table('school_user_rating')->where('sid', $value->sid)->where('is_deleted', 0)->where('rating_val', 2)->count();
                                            $scArr_arrAVG_3 = DB::table('school_user_rating')->where('sid', $value->sid)->where('is_deleted', 0)->where('rating_val', 3)->count();
                                            $scArr_arrAVG_4 = DB::table('school_user_rating')->where('sid', $value->sid)->where('is_deleted', 0)->where('rating_val', 4)->count();
                                            $scArr_arrAVG_5 = DB::table('school_user_rating')->where('sid', $value->sid)->where('is_deleted', 0)->where('rating_val', 5)->count();



                                            $ratings = array(
                                                5 => $scArr_arrAVG_5,
                                                4 => $scArr_arrAVG_4,
                                                3 => $scArr_arrAVG_3,
                                                2 => $scArr_arrAVG_2,
                                                1 => $scArr_arrAVG_1,
                                            );

                                            $avgRating = $this->calcAverageRating($ratings);
                                            $affected = DB::table('school_rating')
                                                ->where('sid', $value->sid)
                                                ->update([
                                                    'total_rating' => $scArr_arrcount,
                                                    'avg_rating' => $avgRating,
                                                    'country_id' => $schorArrData->country_id,
                                                    'school_name' => $schorArrData->title,
                                                ]);

                                            $i++;
                                            $data_arr[] = array(
                                                'RecordID' => $value->sid,
                                                'IndexID' => $i,
                                                'school_title' => $schorArrData->title,
                                                'rating' => $scArr_arrcount,
                                                'avg_rating' => $avgRating,
                                                'country_name' => @$schorArrDataCountry->name,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'school_title'  => true,
                                            'rating'      => true,
                                            'avg_rating'      => true,
                                            'country_name'      => true,

                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }



                                    //getCousePaymentByFilterThisWeek
                                    public function getCousePaymentByFilterThisWeek(Request $request)
                                    {
                                        $selectVal = $request->optN;
                                        $data = explode('@', $selectVal);
                                        $now = \Carbon\Carbon::now();
                                        $isd = $data[1];
                                        $selectVal = $request->payOPT;
                                        $dataPay = explode('@', $selectVal);
                                        $dataPayOption = $dataPay[0];
                                        switch ($dataPayOption) {
                                            case 1:
                                                # code...
                                                break;

                                            default:
                                                # code...
                                                break;
                                        }


                                        if ($data[0] == 1) {
                                            $weekStartDate = $now->startOfWeek()->format('Y-m-d');
                                            $weekEndDate = $now->endOfWeek()->format('Y-m-d');
                                            $schoolCourse = DB::table('school_course_student')
                                                ->where('is_deleted', 0)
                                                ->where('is_active', 1)
                                                ->where('course_id', $isd)
                                                ->whereBetween('created_at', [$weekStartDate, $weekEndDate])
                                                ->get();
                                            // echo "<pre>";
                                            // print_r($schoolCourse);
                                            // die;
                                        }
                                        if ($data[0] == 2) {
                                            $weekStartDate = $now->startOfMonth()->format('Y-m-d');
                                            $weekEndDate = $now->endOfMonth()->format('Y-m-d');

                                            $schoolCourse = DB::table('school_course_student')
                                                ->where('is_deleted', 0)
                                                ->where('is_active', 1)
                                                ->where('course_id', $isd)
                                                ->whereBetween('created_at', [$weekStartDate, $weekEndDate])
                                                ->get();
                                        }
                                        if ($data[0] == 3) {
                                            $myDate = '01/01/' . date('Y');
                                            $myDateA = '12/31/' . date('Y');

                                            $weekStartDate = Carbon::createFromFormat('m/d/Y', $myDate)
                                                ->startOfMonth()
                                                ->format('Y-m-d');
                                            $weekEndDate = Carbon::createFromFormat('m/d/Y', $myDateA)
                                                ->endOfMonth()
                                                ->format('Y-m-d');

                                            $schoolCourse = DB::table('school_course_student')
                                                ->where('is_deleted', 0)
                                                ->where('is_active', 1)
                                                ->where('course_id', $isd)
                                                ->whereBetween('created_at', [$weekStartDate, $weekEndDate])
                                                ->get();
                                        }
                                        $HTML = '';



                                        $i = 0;
                                        foreach ($schoolCourse as $key => $rowData) {
                                            $student_id = $rowData->student_id;
                                            $course_id = $rowData->course_id;
                                            $CourseArr = DB::table('school_course')
                                                ->where('id', $course_id)
                                                ->first();

                                            $users_arrArr = DB::table('users')->where('id', $student_id)->first();


                                            $schoolArr = DB::table('users')->where('id', $student_id)->whereNotNull('avatar')->first();
                                            if ($schoolArr == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
                                            }

                                            switch ($rowData->payment_status) {
                                                case 1:
                                                    $paym = "Done";
                                                    break;
                                                case 1:
                                                    $paym = "downpayment";
                                                    break;

                                                default:
                                                    $paym = "NA";
                                                    break;
                                            }





                                            $i++;
                                            $uURL = getBaseURL() . '/view-user/' . $users_arrArr->id;
                                            $cdate = date('Y-m-d H:iA', strtotime($rowData->created_at));

                                            $HTML .= '<tr>
                        <td>' . $i . '</td>
                        <td>
                            <div class="symbol symbol-circle symbol-lg-50">
                                <img src="' . $schLogo . '" alt="image">
                            </div>
                        </td>
                        <td><a href="' . $uURL . '">' . $users_arrArr->name . '</a></td>
                        <td>' . $CourseArr->certificate_title . '</td>
                        <td>' . $cdate . '</td>
                        <td>' . $paym . '</td>
                        </tr>';
                                        }
                                        echo $HTML;
                                    }

                                    //getCousePaymentByFilterThisWeek
                                    //getCousePaymentByFilter
                                    public function getCousePaymentByFilter(Request $request)
                                    {
                                        $selectVal = $request->optN;
                                        $data = explode('@', $selectVal);

                                        $isd = $data[1];



                                        if ($data[0] == 1) {
                                            $schoolCourse = DB::table('school_course_student')
                                                ->where('is_deleted', 0)
                                                ->where('is_active', 1)
                                                ->where('course_id', $isd)
                                                ->where('payment_status', 1)
                                                ->get();
                                            // echo "<pre>";
                                            // print_r($schoolCourse);
                                            // die;
                                        }
                                        if ($data[0] == 2) {
                                            $schoolCourse = DB::table('school_course_student')
                                                ->where('is_deleted', 0)
                                                ->where('is_active', 1)
                                                ->where('course_id', $isd)
                                                ->where('payment_status', 2)
                                                ->get();
                                        }
                                        if ($data[0] == 3) {
                                            $schoolCourse = DB::table('school_course_student')
                                                ->where('is_deleted', 0)
                                                ->where('is_active', 1)
                                                ->where('course_id', $isd)
                                                ->get();
                                        }
                                        $HTML = '';



                                        $i = 0;
                                        foreach ($schoolCourse as $key => $rowData) {
                                            $student_id = $rowData->student_id;
                                            $course_id = $rowData->course_id;
                                            $CourseArr = DB::table('school_course')
                                                ->where('id', $course_id)
                                                ->first();

                                            $users_arrArr = DB::table('users')->where('id', $student_id)->first();


                                            $schoolArr = DB::table('users')->where('id', $student_id)->whereNotNull('avatar')->first();
                                            if ($schoolArr == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
                                            }

                                            switch ($rowData->payment_status) {
                                                case 1:
                                                    $paym = "Done";
                                                    break;
                                                case 1:
                                                    $paym = "downpayment";
                                                    break;

                                                default:
                                                    $paym = "NA";
                                                    break;
                                            }





                                            $i++;
                                            $uURL = getBaseURL() . '/view-user/' . $users_arrArr->id;
                                            $cdate = date('Y-m-d H:iA', strtotime($rowData->created_at));

                                            $HTML .= '<tr>
                        <td>' . $i . '</td>
                        <td>
                            <div class="symbol symbol-circle symbol-lg-50">
                                <img src="' . $schLogo . '" alt="image">
                            </div>
                        </td>
                        <td><a href="' . $uURL . '">' . $users_arrArr->name . '</a></td>
                        <td>' . $CourseArr->certificate_title . '</td>
                        <td>' . $cdate . '</td>
                        <td>' . $paym . '</td>
                        </tr>';
                                        }
                                        echo $HTML;
                                    }
                                    //getCousePaymentByFilter

                                    public function getSchoolCertificatesByWeek(Request $request)
                                    {


                                        $now = \Carbon\Carbon::now();
                                        if ($request->selectedVal == 1) {
                                            $weekStartDate = $now->startOfWeek()->format('m/d/Y');
                                            $weekEndDate = $now->endOfWeek()->format('m/d/Y');
                                        }
                                        if ($request->selectedVal == 2) {
                                            $weekStartDate = $now->startOfMonth()->format('m/d/Y');
                                            $weekEndDate = $now->endOfMonth()->format('m/d/Y');
                                        }
                                        if ($request->selectedVal == 3) {
                                            $myDate = '01/01/' . date('Y');
                                            $myDateA = '12/31/' . date('Y');

                                            $weekStartDate = Carbon::createFromFormat('m/d/Y', $myDate)
                                                ->startOfMonth()
                                                ->format('m/d/Y');
                                            $weekEndDate = Carbon::createFromFormat('m/d/Y', $myDateA)
                                                ->endOfMonth()
                                                ->format('m/d/Y');
                                        }
                                        $dataArr = array(
                                            'start_date' => $weekStartDate,
                                            'end_date' => $weekEndDate,
                                        );

                                        $data = array(
                                            'data' => $dataArr,
                                            'status' => 1
                                        );



                                        return response()->json($data);
                                    }
                                    //getSchoolCertificatesByWeek
                                    public function getSchoolCertificatesByWeekA(Request $request)
                                    {


                                        $now = \Carbon\Carbon::now();
                                        if ($request->selectedVal == 1) {
                                            $weekStartDate = $now->startOfWeek()->format('Y-m-d');
                                            $weekEndDate = $now->endOfWeek()->format('Y-m-d');
                                        }
                                        if ($request->selectedVal == 2) {
                                            $weekStartDate = $now->startOfMonth()->format('Y-m-d');
                                            $weekEndDate = $now->endOfMonth()->format('Y-m-d');
                                        }
                                        if ($request->selectedVal == 3) {
                                            $myDate = '01/01/' . date('Y');
                                            $myDateA = '12/31/' . date('Y');

                                            $weekStartDate = Carbon::createFromFormat('m/d/Y', $myDate)
                                                ->startOfMonth()
                                                ->format('Y-m-d');
                                            $weekEndDate = Carbon::createFromFormat('m/d/Y', $myDateA)
                                                ->endOfMonth()
                                                ->format('Y-m-d');
                                        }
                                        if ($request->selectedVal == 4) {
                                            $myDate = $request->startDate;
                                            $myDateA = $request->endDate;

                                            $weekStartDate = Carbon::createFromFormat('m/d/Y', $myDate)
                                                ->format('Y-m-d');
                                            $weekEndDate = Carbon::createFromFormat('m/d/Y', $myDateA)
                                                ->format('Y-m-d');
                                        }





                                        $contentArr = DB::table('school_course')
                                            ->where('is_deleted', 0)
                                            ->orderBy('id', 'desc')
                                            ->whereBetween('course_date', [$weekStartDate, $weekEndDate])
                                            ->get();
                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($contentArr as $key => $rowData) {
                                            $i++;
                                            $schoolArr = DB::table('schools')
                                                ->where('id', $rowData->sid)
                                                ->first();
                                            $URL = getBaseURL() . "/view-school-details/" . $rowData->sid;
                                            $cid = $rowData->id;
                                            $stuCount = DB::table('school_course_student')
                                                ->where('course_id', $cid)
                                                ->count();

                                            $data_arr[] = array(
                                                'RecordID' => $rowData->id,
                                                'IndexID' => $i,
                                                'school_title' => $schoolArr->title,
                                                'certificate_title' => $rowData->certificate_title,
                                                'total_student' => $stuCount,
                                                'course_date' => $rowData->course_date,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'school_title'  => true,
                                            'certificate_title'      => true,
                                            'total_student'      => true,
                                            'course_date'      => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getSchoolCertificatesByWeek
                                    //getSchoolCertificatesBYFilter
                                    public function getSchoolCertificatesBYFilter(Request $request)
                                    {

                                        $startDate = date('Y-m-d', strtotime($request->startDate));
                                        $end = date('Y-m-d', strtotime($request->endDate));


                                        if (isset($request->course)) {

                                            if (empty($request->sid)) {
                                                if (empty($request->startDate)) {
                                                    $contentArr = DB::table('school_course')
                                                        ->where('is_deleted', 0)
                                                        ->orderBy('school_name', 'asc')
                                                        ->where('certificate_title', $request->course)
                                                        // ->where('sid', $request->sid)
                                                        ->where('is_deleted', 0)
                                                        ->get();
                                                } else {


                                                    $contentArr = DB::table('school_course')
                                                        ->where('is_deleted', 0)
                                                        ->orderBy('school_name', 'asc')
                                                        ->where('certificate_title', $request->course)
                                                        ->whereBetween('course_date', [$startDate, $end])
                                                        ->where('is_deleted', 0)
                                                        ->get();
                                                }
                                            } else {
                                                if (empty($request->startDate)) {
                                                    $contentArr = DB::table('school_course')
                                                        ->where('is_deleted', 0)
                                                        ->orderBy('school_name', 'asc')
                                                        ->where('certificate_title', $request->course)
                                                        ->where('sid', $request->sid)
                                                        ->where('is_deleted', 0)
                                                        ->get();
                                                } else {


                                                    $contentArr = DB::table('school_course')
                                                        ->where('is_deleted', 0)
                                                        ->orderBy('school_name', 'asc')
                                                        ->where('certificate_title', $request->course)
                                                        ->whereBetween('course_date', [$startDate, $end])
                                                        ->where('is_deleted', 0)
                                                        ->where('sid', $request->sid)
                                                        ->get();
                                                }
                                            }
                                            // is data 

                                        } else {

                                            if (empty($request->sid)) {

                                                if (empty($request->startDate)) {
                                                    $contentArr = DB::table('school_course')
                                                        ->where('is_deleted', 0)
                                                        ->orderBy('school_name', 'asc')
                                                        ->where('is_deleted', 0)
                                                        ->get();
                                                } else {
                                                    $contentArr = DB::table('school_course')
                                                        ->where('is_deleted', 0)
                                                        ->orderBy('school_name', 'asc')
                                                        ->whereBetween('course_date', [$startDate, $end])
                                                        ->where('is_deleted', 0)
                                                        ->get();
                                                }
                                            } else {

                                                if (empty($request->startDate)) {
                                                    $contentArr = DB::table('school_course')
                                                        ->where('is_deleted', 0)
                                                        ->where('sid', $request->sid)
                                                        ->orderBy('school_name', 'asc')
                                                        ->where('is_deleted', 0)
                                                        ->get();
                                                } else {
                                                    $contentArr = DB::table('school_course')
                                                        ->where('is_deleted', 0)
                                                        ->where('sid', $request->sid)
                                                        ->orderBy('school_name', 'asc')
                                                        ->whereBetween('course_date', [$startDate, $end])
                                                        ->where('is_deleted', 0)
                                                        ->get();
                                                }
                                            }
                                        }



                                        $data_arr = array();
                                        $i = 0;
                                        $stcount = count($contentArr);

                                        foreach ($contentArr as $key => $rowData) {
                                            $i++;
                                            $schoolArr = DB::table('schools')
                                                ->where('id', $rowData->sid)
                                                ->first();
                                            $URL = getBaseURL() . "/view-school-details/" . $rowData->sid;
                                            $cid = $rowData->id;
                                            $stuCount = DB::table('school_course_student')
                                                ->where('course_id', $cid)
                                                ->count();


                                            $affected = DB::table('school_course')
                                                ->where('sid', $rowData->sid)
                                                ->update([
                                                    'school_name' => $schoolArr->title


                                                ]);

                                            $data_arr[] = array(
                                                'RecordID' => $rowData->id,
                                                'IndexID' => $i,
                                                'school_title' => $schoolArr->title,
                                                'certificate_title' => $rowData->certificate_title,
                                                'total_student' => $stuCount,
                                                'course_date' => $rowData->course_date,
                                                'stcount' => $stcount,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'school_title'  => true,
                                            'certificate_title'      => true,
                                            'total_student'      => true,
                                            'course_date'      => true,
                                            'stcount'      => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getSchoolCertificatesBYFilter

                                    // getSchoolEntrollPaymentFilter
                                    public function getSchoolEntrollPaymentFilter(Request $request)
                                    {
                                        $startDate = date('Y-m-d', strtotime($request->startDate));
                                        $end = date('Y-m-d', strtotime($request->endDate));

                                        $isd = $request->sid;
                                        if (isset($request->paymentStatusRadio)) {
                                            if (empty($request->startDate)) {
                                                $schoolCourse = DB::table('school_course_student')
                                                    ->where('is_deleted', 0)
                                                    ->where('is_active', 1)
                                                    ->where('course_id', $isd)
                                                    ->where('payment_status', $request->paymentStatusRadio)
                                                    ->get();
                                            } else {
                                                $schoolCourse = DB::table('school_course_student')
                                                    ->where('is_deleted', 0)
                                                    ->where('is_active', 1)
                                                    ->where('course_id', $isd)
                                                    ->whereBetween('created_at', [$startDate, $end])
                                                    ->where('payment_status', $request->paymentStatusRadio)
                                                    ->get();
                                            }
                                        } else {

                                            if (empty($request->startDate)) {
                                                $schoolCourse = DB::table('school_course_student')
                                                    ->where('is_deleted', 0)
                                                    ->where('is_active', 1)
                                                    ->where('course_id', $isd)
                                                    ->get();
                                            } else {
                                                $schoolCourse = DB::table('school_course_student')
                                                    ->where('is_deleted', 0)
                                                    ->where('is_active', 1)
                                                    ->where('course_id', $isd)
                                                    ->whereBetween('created_at', [$startDate, $end])
                                                    ->get();
                                            }
                                        }

                                        $data_arr = array();
                                        $i = 0;

                                        foreach ($schoolCourse as $key => $rowData) {
                                            $i++;
                                            $student_id = $rowData->student_id;
                                            $course_id = $rowData->course_id;
                                            $CourseArr = DB::table('school_course')
                                                ->where('id', $course_id)
                                                ->first();

                                            $schoolArr = DB::table('users')->where('id', $student_id)->whereNotNull('avatar')->first();
                                            if ($schoolArr == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
                                            }

                                            switch ($rowData->payment_status) {
                                                case 1:
                                                    $paym = "Full";
                                                    break;
                                                case 2:
                                                    $paym = "Partial";
                                                    break;

                                                default:
                                                    $paym = "NA";
                                                    break;
                                            }



                                            $data_arr[] = array(
                                                'RecordID' => $rowData->id,
                                                'IndexID' => $i,
                                                'user_pic' => $schLogo,
                                                'name' => $schoolArr->name,
                                                'user_id' => $schoolArr->id,
                                                'course_name' => $CourseArr->certificate_title,
                                                'course_date' => date('Y-m-d H:iA', strtotime($rowData->created_at)),
                                                'payement_status' => $paym,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'user_pic'  => true,
                                            'name'      => true,
                                            'course_name'      => true,
                                            'course_date'      => true,
                                            'payement_status'  => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getSchoolEntrollPaymentFilter
                                    //getSchoolEntrollPayment
                                    public function getSchoolEntrollPayment(Request $request)
                                    {
                                        $isd = $request->sid;

                                        $schoolCourse = DB::table('school_course_student')
                                            ->where('is_deleted', 0)
                                            ->where('is_active', 1)
                                            ->where('course_id', $isd)
                                            ->get();
                                        $data_arr = array();
                                        $i = 0;

                                        foreach ($schoolCourse as $key => $rowData) {
                                            $i++;
                                            $student_id = $rowData->student_id;
                                            $course_id = $rowData->course_id;
                                            $CourseArr = DB::table('school_course')
                                                ->where('id', $course_id)
                                                ->first();

                                            $schoolArr = DB::table('users')->where('id', $student_id)->whereNotNull('avatar')->first();
                                            if ($schoolArr == null) {
                                                $schLogo = NoImage();
                                            } else {
                                                $schLogo = asset('/local/public/upload/') . "/" . $schoolArr->avatar;
                                            }

                                            switch ($rowData->payment_status) {
                                                case 1:
                                                    $paym = "Full";
                                                    break;
                                                case 2:
                                                    $paym = "Partial";
                                                    break;

                                                default:
                                                    $paym = "NA";
                                                    break;
                                            }



                                            $data_arr[] = array(
                                                'RecordID' => $rowData->id,
                                                'IndexID' => $i,
                                                'user_pic' => $schLogo,
                                                'name' => $schoolArr->name,
                                                'user_id' => $schoolArr->id,
                                                'course_name' => $CourseArr->certificate_title,
                                                'course_date' => date('Y-m-d H:iA', strtotime($rowData->created_at)),
                                                'payement_status' => $paym,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'user_pic'  => true,
                                            'name'      => true,
                                            'course_name'      => true,
                                            'course_date'      => true,
                                            'payement_status'  => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getSchoolEntrollPayment

                                    //getSchoolCertificates
                                    public function getSchoolCertificates(Request $request)
                                    {


                                        $contentArr = DB::table('school_course')
                                            ->where('is_deleted', 0)
                                            ->orderBy('school_name', 'asc')
                                            ->get();
                                        $data_arr = array();
                                        $i = 0;
                                        $stcount = count($contentArr);

                                        foreach ($contentArr as $key => $rowData) {
                                            $i++;
                                            $schoolArr = DB::table('schools')
                                                ->where('id', $rowData->sid)
                                                ->first();
                                            $URL = getBaseURL() . "/view-school-details/" . $rowData->sid;
                                            $cid = $rowData->id;
                                            $stuCount = DB::table('school_course_student')
                                                ->where('course_id', $cid)
                                                ->count();


                                            $affected = DB::table('school_course')
                                                ->where('sid', $rowData->sid)
                                                ->update([
                                                    'school_name' => $schoolArr->title


                                                ]);

                                            $data_arr[] = array(
                                                'RecordID' => $rowData->id,
                                                'IndexID' => $i,
                                                'school_title' => $schoolArr->title,
                                                'certificate_title' => $rowData->certificate_title,
                                                'total_student' => $stuCount,
                                                'stcount' => $stcount,
                                                'course_date' => $rowData->course_date,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'school_title'  => true,
                                            'certificate_title'      => true,
                                            'total_student'      => true,
                                            'course_date'      => true,
                                            'stcount' => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getSchoolCertificates
                                    //getDatatableNotifyList
                                    public function getDatatableNotifyList(Request $request)
                                    {


                                        $users_arr = DB::table('tbl_notify')->where('is_deleted', 0)->orderBy('id', 'DESC')->get();


                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($users_arr as $key => $value) {
                                            $i++;

                                            //---------------------------------------
                                            $proVal = is_school_completed($value->id);
                                            if ($proVal == 7) {
                                                $profile_status = 1;
                                            } else {
                                                $profile_status = 2;
                                            }
                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'noti_name' => $value->noti_name,
                                                'noti_type' => $value->noti_type,
                                                'noti_message' => $value->noti_message,
                                                'created_at' => $value->created_at,
                                                'is_send' => $value->is_send,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'noti_name'  => true,
                                            'noti_type'      => true,
                                            'noti_message'      => true,
                                            'created_at'      => true,
                                            'is_send'      => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getDatatableNotifyList
                                    //getDatatableSchoolCouserList
                                    public function getDatatableSchoolCouserList(Request $request)
                                    {

                                        $users_arr = DB::table('school_course')->where('is_deleted', 0)->where('sid', Auth::user()->sid)->orderBy('id', 'DESC')->get();

                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($users_arr as $key => $value) {
                                            $i++;


                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'certificate_title' => $value->certificate_title,
                                                'course_date' => $value->course_date,
                                                'regno' => $value->regno,
                                                'status' => $value->is_active,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'certificate_title'  => true,
                                            'course_date'      => true,
                                            'regno'      => true,
                                            'status'      => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getDatatableSchoolCouserList


                                    //getDatatableSchoolListData
                                    public function getDatatableSchoolListData(Request $request)
                                    {

                                        if ($request->schoolListFrom == 1) {
                                            $users_arr = DB::table('schools')->where('is_deleted', 0)->where('added_from_status', 1)->orderBy('id', 'DESC')->get();
                                        } else {
                                            $users_arr = DB::table('schools')->where('is_deleted', 0)->where('added_from_status', 2)->orderBy('id', 'DESC')->get();
                                        }


                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($users_arr as $key => $value) {
                                            $i++;

                                            //---------------------------------------
                                            $proVal = is_school_completed($value->id);
                                            if ($proVal == 7) {
                                                $profile_status = 1;
                                            } else {
                                                $profile_status = 2;
                                            }
                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'school_title' => $value->title,
                                                'rating' => $value->rating,
                                                'city' => optional(getCityData($value->city_id))->name,
                                                'email' => $value->email,
                                                'status' => $value->status,
                                                'profile_status' => $profile_status,
                                                'is_approved' => $value->is_approved,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'school_title'  => true,
                                            'rating'      => true,
                                            'email'      => true,
                                            'phone'      => true,
                                            'status'      => true,
                                            'profile_status'      => true,
                                            'is_approved' => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }
                                    //viewSchoolPaymentDetails
                                    public function viewSchoolPaymentDetails()
                                    {
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('super_admin_userList', $data)->render();
                                    }
                                    //viewSchoolPaymentDetails

                                    //getDatatableSchoolPaymentList
                                    public function getDatatableSchoolPaymentList(Request $request)
                                    {

                                        if ($request->schoolListFrom == 1) {
                                            $users_arr = DB::table('schools')->where('is_deleted', 0)->where('added_from_status', 1)->orderBy('id', 'DESC')->get();
                                        } else {
                                            $users_arr = DB::table('schools')->where('is_deleted', 0)->where('added_from_status', 2)->orderBy('id', 'DESC')->get();
                                        }

                                         $users_arr = DB::table('schools')->where('is_deleted', 0)->where('added_from_status', 1)->orderBy('id', 'DESC')->get();



                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($users_arr as $key => $value) {
                                            $i++;

                                            //---------------------------------------
                                            $proVal = is_school_completed($value->id);
                                            if ($proVal == 7) {
                                                $profile_status = 1;
                                            } else {
                                                $profile_status = 2;
                                            }
                                            $schoolCoursePaymentData = DB::table('course_payment')->where('sid', $value->id)->where('is_deleted', 0)->sum('payment_amt');
                                            $schoolAmt = ($schoolCoursePaymentData * $value->admin_comm) / 100;

                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'school_title' => $value->title,
                                                'school_regno' => $value->reg_no,
                                                'school_total_rec' => $schoolCoursePaymentData,
                                                'admin_comm' => is_null($value->admin_comm) ? '0 %' : $value->admin_comm."%",
                                                'admin_total_rec' => $schoolAmt,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'school_title'  => true,
                                            'rating'      => true,
                                            'email'      => true,
                                            'phone'      => true,
                                            'status'      => true,
                                            'profile_status'      => true,
                                            'is_approved' => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getDatatableSchoolPaymentList
                                    //getDatatableSchoolListFilter
                                    public function getDatatableSchoolListFilter(Request $request)
                                    {
                                        $selectedCountry = $request->selectedCountry;
                                        
                                        if ($request->schoolListFrom == 1) {
                                            if($selectedCountry=='ALL'){ 
                                                $users_arr = DB::table('schools')->where('is_deleted', 0)->where('added_from_status', 1)->orderBy('id', 'DESC')->get();
                                            }else{
                                                $users_arr = DB::table('schools')->where('country_id', $selectedCountry)->where('is_deleted', 0)->where('added_from_status', 1)->orderBy('id', 'DESC')->get();
                                            }
                                            
                                        } else {
                                            if($selectedCountry=='ALL'){ 
                                                $users_arr = DB::table('schools')->where('is_deleted', 0)->where('added_from_status', 2)->orderBy('id', 'DESC')->get();

                                            }else{
                                                $users_arr = DB::table('schools')->where('country_id', $selectedCountry)->where('is_deleted', 0)->where('added_from_status', 2)->orderBy('id', 'DESC')->get();
                                            }
                                            
                                        }


                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($users_arr as $key => $value) {
                                            $i++;

                                            //---------------------------------------
                                            $proVal = is_school_completed($value->id);
                                            if ($proVal == 7) {
                                                $profile_status = 1;
                                            } else {
                                                $profile_status = 2;
                                            }
                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'school_title' => $value->title,
                                                'rating' => $value->rating,
                                                'city' => optional(getCityData($value->city_id))->name,
                                                'email' => $value->email,
                                                'status' => $value->status,
                                                'profile_status' => $profile_status,
                                                'is_approved' => $value->is_approved,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'school_title'  => true,
                                            'rating'      => true,
                                            'email'      => true,
                                            'phone'      => true,
                                            'status'      => true,
                                            'profile_status'      => true,
                                            'is_approved' => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }

                                    //getDatatableSchoolListFilter


                                    //getDatatableSchoolListData
                                    public function getDatatableSchoolList(Request $request)
                                    {

                                        if ($request->schoolListFrom == 1) {
                                            $users_arr = DB::table('schools')->where('is_deleted', 0)->where('added_from_status', 1)->orderBy('id', 'DESC')->get();
                                        } else {
                                            $users_arr = DB::table('schools')->where('is_deleted', 0)->where('added_from_status', 2)->orderBy('id', 'DESC')->get();
                                        }


                                        $data_arr = array();
                                        $i = 0;
                                        foreach ($users_arr as $key => $value) {
                                            $i++;

                                            //---------------------------------------
                                            $proVal = is_school_completed($value->id);
                                            if ($proVal == 7) {
                                                $profile_status = 1;
                                            } else {
                                                $profile_status = 2;
                                            }
                                            $data_arr[] = array(
                                                'RecordID' => $value->id,
                                                'IndexID' => $i,
                                                'school_title' => $value->title,
                                                'rating' => $value->rating,
                                                'city' => optional(getCityData($value->city_id))->name,
                                                'email' => $value->email,
                                                'status' => $value->status,
                                                'profile_status' => $profile_status,
                                                'is_approved' => $value->is_approved,
                                                'Actions' => ''

                                            );
                                        }

                                        $JSON_Data = json_encode($data_arr);
                                        $columnsDefault = [
                                            'RecordID'  => true,
                                            'IndexID' => true,
                                            'school_title'  => true,
                                            'rating'      => true,
                                            'email'      => true,
                                            'phone'      => true,
                                            'status'      => true,
                                            'profile_status'      => true,
                                            'is_approved' => true,
                                            'Actions'      => true,
                                        ];

                                        $this->DataGridResponse($JSON_Data, $columnsDefault);
                                    }
                                    //getSchoolCourseEn
                                    public function getSchoolCourseEn(Request $request)
                                    {

                                        $selectedSchool = $request->selectedSchool;
                                        $schArr = DB::table('schools')->where('id', $selectedSchool)->first();

                                        $schArrData = DB::table('school_course')->where('sid', $schArr->id)->get();
                                        $HTML = '<option value="">-SELECT-</option>';

                                        foreach ($schArrData as $key => $row) {

                                            $HTML .= '<option value="' . $row->id . '">' . $row->certificate_title . '</option>';
                                        }

                                        echo $HTML;



                                        //school_course

                                    }

                                    //getSchoolCourseEn

                                    //getSchoolCourse
                                    public function getSchoolCourse(Request $request)
                                    {

                                        $selectedSchool = $request->selectedSchool;
                                        $schArr = DB::table('schools')->where('id', $selectedSchool)->first();

                                        $schArrData = DB::table('school_course')->where('sid', $schArr->id)->get();
                                        $HTML = '<option value="">-SELECT-</option>';

                                        foreach ($schArrData as $key => $row) {

                                            $HTML .= '<option value="' . $row->certificate_title . '">' . $row->certificate_title . '</option>';
                                        }

                                        echo $HTML;



                                        //school_course

                                    }
                                    //getSchoolCourse
                                    //getMoreCertificate
                                    public function getMoreCertificate(Request $request)
                                    {
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('get_more_certificate', $data)->render();
                                    }

                                    //getMoreCertificate
                                    //getEnrollSchool
                                    public function getEnrollSchool($id)
                                    {
                                        $users = DB::table('school_course_student')->where('course_id', $id)->get();

                                        $theme = Theme::uses('adminsuper')->layout('layout');
                                        $data = ["data" => $users];
                                        return $theme->scope('get_coursewiseAllStudent', $data)->render();
                                    }

                                    //getEnrollSchool

                                    // updateSchoolInstructor
                                    public function updateSchoolInstructor(Request $request)
                                    {
                                        //print_r($request->all());
                                        $schoolHistoryArr = $request->instructor;
                                        $sid = $request->txtSValue;
                                        DB::table('school_instructor')->where('sid', $sid)->delete();
                                        foreach ($schoolHistoryArr as $key => $rowData) {
                                            DB::table('school_instructor')->insert([
                                                'sid' => $sid,
                                                'name' => $schoolHistoryArr[$key]['txtInstName'],
                                                'profile_url' => $schoolHistoryArr[$key]['txtInstProfileURL']


                                            ]);
                                        }
                                        $data = array(
                                            'msg' => 'Data saved Successfully',
                                            'status' => 1
                                        );
                                    }
                                    //updateUserInterest
                                    public function updateUserInterest(Request $request)
                                    {
                                        //print_r($request->all());
                                        $user_inerestArr = $request->user_inerest;
                                        $uid = $request->uid;
                                        DB::table('app_users_interest')->where('user_id', $uid)->delete();
                                        foreach ($user_inerestArr as $key => $rowData) {
                                            DB::table('app_users_interest')->insert([
                                                'user_id' => $uid,
                                                'interest_id' => $user_inerestArr[$key]


                                            ]);
                                        }
                                        $data = array(
                                            'msg' => 'Data saved Successfully',
                                            'status' => 1
                                        );
                                    }
                                    public function updateUserSport(Request $request)
                                    {
                                        //print_r($request->all());
                                        $user_inerestArr = $request->user_sport;
                                        $uid = $request->uid;
                                        DB::table('app_users_sports')->where('user_id', $uid)->delete();
                                        foreach ($user_inerestArr as $key => $rowData) {
                                            DB::table('app_users_sports')->insert([
                                                'user_id' => $uid,
                                                'sport_id' => $user_inerestArr[$key]


                                            ]);
                                        }
                                        $data = array(
                                            'msg' => 'Data saved Successfully',
                                            'status' => 1
                                        );
                                    }

                                    //updateUserInterest
                                    //updateSchoolInstructor
                                    public function updateSchoolHistory(Request $request)
                                    {
                                        //print_r($request->all());
                                        $schoolHistoryArr = $request->schoolHistory;
                                        $sid = $request->txtSIDValue;
                                        DB::table('school_history')->where('sid', $sid)->delete();
                                        foreach ($schoolHistoryArr as $key => $rowData) {
                                            DB::table('school_history')->insert([
                                                'sid' => $sid,
                                                'school_year' => $schoolHistoryArr[$key]['txtSchoolYear'],
                                                'school_students' => $schoolHistoryArr[$key]['txtSchoolStudent'],
                                                'school_notes' => $schoolHistoryArr[$key]['txtSchoolNotes'],

                                            ]);
                                        }
                                        $data = array(
                                            'msg' => 'Data saved Successfully',
                                            'status' => 1
                                        );
                                    }
                                    //saveAdminNotify
                                    public function saveAdminNotify(Request $request)
                                    {
                                        if ($request->staticAction == "_add") {
                                            DB::table('tbl_notify')->insert([
                                                'noti_type' => $request->whomto,
                                                'noti_name' => $request->title,
                                                'noti_message' => $request->content,
                                                'created_at' => date('Y-m-d h:i:s'),
                                                'is_view' => 0,
                                                'created_by' => Auth::user()->id,
                                                'is_send' => $request->isactive,

                                            ]);
                                            $data = array(
                                                'msg' => 'Data Saved Successfully',
                                                'status' => 1
                                            );
                                            return response()->json($data);
                                        }
                                        if ($request->staticAction == '_edit') {

                                            $affected = DB::table('cms_contents')
                                                ->where('id', $request->txtStaticID)
                                                ->update([
                                                    'title' => $request->title,
                                                    'content' => $request->content,
                                                    'is_active' => $request->isactive,


                                                ]);

                                            $data = array(
                                                'msg' => 'Data saved Successfully',
                                                'status' => 1
                                            );
                                        }
                                    }
                                    //saveAdminNotify

                                    public function saveStaticContent(Request $request)
                                    {
                                        //dd($request->all());exit;
                                        if ($request->staticAction == "_add") {
                                            DB::table('cms_contents')->insert([
                                                'title' => $request->title,
                                                'content' => $request->content,
                                                'is_active' => $request->isactive,

                                            ]);
                                            $data = array(
                                                'msg' => 'Data Saved Successfully',
                                                'status' => 1
                                            );
                                        }
                                        if ($request->staticAction == '_edit') {
                                          

                                            $affected = DB::table('cms_contents')
                                                ->where('id', $request->txtStaticID)
                                                ->update([
                                                    'title' => $request->title,
                                                    'content' => $request->content,
                                                    'is_active' => $request->isactive,


                                                ]);

                                            $data = array(
                                                'msg' => 'Data saved Successfully',
                                                'status' => 1
                                            );
                                        }


                                        return response()->json($data);
                                    }
                                    public function saveSchool(Request $request)
                                    {
                                        if ($request->txtAction == '_edit') {

                                            $affected = DB::table('schools')
                                                ->where('id', $request->txtSID)
                                                ->update([
                                                    'title' => $request->title,
                                                    'reg_no' => $request->regno,
                                                    // 'email' => $request->email,
                                                    'city_id' => $request->city,
                                                    'country_id' => $request->country,
                                                    'state_id' => $request->state,
                                                    'website' => $request->website,
                                                    'phone_code' => $request->phone_code,
                                                    'phone' => $request->phone,
                                                    'facebook' => $request->facebook,
                                                    'twitter' => $request->twitter,
                                                    'linkedin' => $request->linkedin,
                                                    'admin_comm' => $request->admin_comm,


                                                    'about' => $request->about,
                                                    'reg_body_1' => $request->reg_body_1,
                                                    'reg_body_2' => $request->reg_body_2,
                                                    'reg_body_3' => $request->reg_body_3,


                                                ]);

                                            $data = array(
                                                'msg' => 'Data saved Successfully',
                                                'status' => 1
                                            );
                                        }
                                        if ($request->txtAction == '_add') {
                                            $schoolsArr = DB::table('schools')
                                                ->where('email', $request->email)
                                                ->first();
                                            if ($schoolsArr != null) {
                                                $data = array(
                                                    'msg' => 'Already Created',
                                                    'status' => 0
                                                );
                                            } else {
                                                DB::table('schools')->insert([
                                                    'sid' => getSchoolCode(),
                                                    'title' => $request->title,
                                                    'reg_no' => $request->regno,
                                                    'email' => $request->email,
                                                    'city_id' => $request->city,
                                                    'country_id' => $request->country,
                                                    'state_id' => $request->state,
                                                    'phone_code' => $request->phone_code,
                                                    'phone' => $request->phone,

                                                ]);
                                                $data = array(
                                                    'msg' => 'Data Saved Successfully',
                                                    'status' => 1
                                                );
                                            }
                                        }




                                        return response()->json($data);
                                    }

                                    //getSchoolCourseCommentRatingMy
                                    public function getSchoolCourseCommentRatingMy()
                                    {
                                        $theme = Theme::uses('admin')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('my_course_comment_rating_list', $data)->render();
                                    }

                                    //getSchoolCourseCommentRatingMy

                                    //getSchoolCommentRatingMy
                                    public function getSchoolCommentRatingMy()
                                    {
                                        $theme = Theme::uses('admin')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('my_school_comment_rating_list', $data)->render();
                                    }
                                    //getSchoolCommentRatingMy

                                    public function getSchoolCommentRating()
                                    {
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('school_comment_rating_list', $data)->render();
                                    }


                                    //MyschoolPerformanceList
                                    public function MyschoolPerformanceList()
                                    {
                                        $theme = Theme::uses('admin')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('my_school_performance_list', $data)->render();
                                    }
                                    public function MyschoolCoursePerformanceList()
                                    {
                                        $theme = Theme::uses('admin')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('my_school_course_performance_list', $data)->render();
                                    }
                                    //MyschoolPerformanceList


                                    public function schoolPerformanceList()
                                    {
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('school_performance_list', $data)->render();
                                    }
                                    public function schoolCertificateList()
                                    {
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('school_certificate_list', $data)->render();
                                    }
                                    public function staticContentList()
                                    {
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('static_content_list', $data)->render();
                                    }
                                    //AddSchoolCourse
                                    public function AddSchoolCourse()
                                    {
                                        $theme = Theme::uses('admin')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('add_school_course', $data)->render();
                                    }

                                    //AddSchoolCourse

                                    public function add_school()
                                    {
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('add_school', $data)->render();
                                    }
                                    public function viewSchoolDetails($id)
                                    {
                                        $schoolsArr = DB::table('schools')
                                            ->where('id', $id)
                                            ->first();
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["data" => $schoolsArr];
                                        return $theme->scope('view_school_enroll_details', $data)->render();
                                    }
                                    //
                                    public function view_school_requested($id)
                                    {
                                        $schoolsArr = DB::table('schools')
                                            ->where('id', $id)
                                            ->first();
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["data" => $schoolsArr];
                                        return $theme->scope('view_school_requested', $data)->render();
                                    }


                                    public function view_school($id)
                                    {
                                        $schoolsArr = DB::table('schools')
                                            ->where('id', $id)
                                            ->first();
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["data" => $schoolsArr];
                                        return $theme->scope('view_school', $data)->render();
                                    }
                                    public function edit_school($id)
                                    {
                                        $schoolsArr = DB::table('schools')
                                            ->where('id', $id)
                                            ->first();
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["data" => $schoolsArr];
                                        return $theme->scope('edit_school', $data)->render();
                                    }

                                    //admin_profileV1
                                    public function admin_profileV1()
                                    {
                                        $schoolsArr = DB::table('users')
                                            ->where('id', Auth::user()->id)
                                            ->first();
                                        $theme = Theme::uses('admin')->layout('layout');

                                        $data = ["data" => $schoolsArr];
                                        return $theme->scope('view_adminprofile', $data)->render();
                                    }

                                    //admin_profileV1

                                    public function admin_profile()
                                    {
                                        $schoolsArr = DB::table('users')
                                            ->where('id', Auth::user()->id)
                                            ->first();
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["data" => $schoolsArr];
                                        return $theme->scope('view_adminprofile', $data)->render();
                                    }
                                    //Adminview_user
                                    public function Adminview_user($id)
                                    {
                                        $schoolsArr = DB::table('users')
                                            ->where('id', $id)
                                            ->first();
                                        $theme = Theme::uses('admin')->layout('layout');

                                        $data = ["data" => $schoolsArr];
                                        return $theme->scope('admin_view_user', $data)->render();
                                    }

                                    //Adminview_user
                                    //settleDueAmtSchoolAdmin
                                    public function settleDueAmtSchoolAdmin($id)
                                    {
                                        $schoolsArr = DB::table('school_course_student')
                                            ->where('id', $id)
                                            ->where('sid', Auth::user()->sid)
                                            ->first();

                                        $theme = Theme::uses('admin')->layout('layout');

                                        $data = ["data" => $schoolsArr];
                                        return $theme->scope('settle_school_school_admin', $data)->render();
                                    }

                                    //settleDueAmtSchoolAdmin

                                    //AddCouserToUserAdminview
                                    public function AddCouserToUserAdminview($id)
                                    {
                                        $schoolsArr = DB::table('users')
                                            ->where('id', $id)
                                            ->first();
                                        $theme = Theme::uses('admin')->layout('layout');

                                        $data = ["data" => $schoolsArr];
                                        return $theme->scope('add_courser_to_user', $data)->render();
                                    }

                                    //AddCouserToUserAdminview


                                    //view_userDetails
                                    public function view_userDetails($id)
                                    {
                                        $schoolsArr = DB::table('users')
                                            ->where('id', $id)
                                            ->first();
                                        $theme = Theme::uses('admin')->layout('layout');

                                        $data = ["data" => $schoolsArr];
                                        return $theme->scope('view_userAdmin', $data)->render();
                                    }
                                    //view_userDetails


                                    public function view_user($id)
                                    {
                                        $schoolsArr = DB::table('users')
                                            ->where('id', $id)
                                            ->first();
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["data" => $schoolsArr];
                                        return $theme->scope('view_user', $data)->render();
                                    }

                                    public function edit_user($id)
                                    {
                                        $schoolsArr = DB::table('users')
                                            ->where('id', $id)
                                            ->first();
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["data" => $schoolsArr];
                                        return $theme->scope('edit_user', $data)->render();
                                    }


                                    public function schoolList()
                                    {
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('school_list', $data)->render();
                                    }
                                    public function paymentHistAdminList()
                                    {
                                        $theme = Theme::uses('adminsuper')->layout('layout');

                                        $data = ["avatar_img" => ''];
                                        return $theme->scope('school_payment_list', $data)->render();
                                    }
                                }