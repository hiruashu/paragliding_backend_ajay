<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Theme;
use DB;
use Auth;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Mail;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Pusher;


class AdminController extends Controller
{
    //zelosChatAdminGroupChatEDIT
    public function zelosChatAdminGroupChatEDIT($id)
    {
    $theme = Theme::uses('admin')->layout('layout');
    $data = DB::table('chat_group')->where('id', $id)->first();

    $data = ["data" => $data];
    return $theme->scope('zelos_chat_admin_group_chatEDIT', $data)->render();
    }
    //zelosChatAdminGroupChatEDIT

    //zelosChatAdminGroupChat
    public function zelosChatAdminGroupChat(Request $request)
    {
    $theme = Theme::uses('admin')->layout('layout');

    $data = ["avatar_img" => ''];
    return $theme->scope('zelos_chat_admin_group_chat', $data)->render();
    }

    //zelosChatAdminGroupChat

    public function zelosChat(Request $request)
    {
    $theme = Theme::uses('admin')->layout('layout');

    $data = ["avatar_img" => ''];
    return $theme->scope('zelos_chat_admin', $data)->render();
    }
    public function zelosChatAdminGroupView(Request $request)
    {
    $theme = Theme::uses('admin')->layout('layout');

    $data = ["avatar_img" => ''];
    return $theme->scope('zelos_chat_admin_groupChatView', $data)->render();
    }

    

}
