<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\School;
use Illuminate\Http\Request;
use Validator;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Arr;
//Please use this file only sir 2:
use Pusher;

class AuthController extends Controller
{

    //     Get List of Groups API : (NOTE:which one created from the backend side)
    // Method : POST
    // Param : user_id
    // Response : 
    // Group_img_url, group name, user-list which are connected with that group in array(user_id, img_url, username), unread_message_count
    // -------------------------------------------------------------------------------------------------------------------------------
    // Get chat of particular group Groups API : (NOTE:which one created from the backend side)
    // Method : POST
    // Param : group_id
    // Response : 
    // Full chat user-wise with all the user’s data message in that group
    // -------------------------------------------------------------------------------------------------------------------------------
    // Send Message API :
    // Method : POST
    // Param : user_id, group_id, text/image, type (1-text,2-image)
    // Response : 
    // Full chat user-wise with all the user’s data message in that group

    //getCountryList
    public function getCountryList(Request $request)  //phase two  part afte school with user attachement 
    {
        
        $data = DB::table('countries')
            ->select(
                'id as country_id',
                 'name',
                 'emoji'
                 )
            ->get();

        $message = strtoupper('SUCCESS-Added');
                $message_action = "setUserPost";
                return $this->setSuccessResponse($data, $message, "Auth:Login", '', $message_action);


    }
    //getCountryList

    //getBestMatchUsers
    public function getBestMatchUsers(Request $request)  //phase two  part afte school with user attachement 
    {
        $validatedData = $request->only(
            'user_id'          


        );
        $rules = [
            'user_id' => 'required'
        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "getBestMatchUsers-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        }
        $usersData = DB::table('group_chat_users')
                ->join('users', 'group_chat_users.user_id', '=', 'users.id')
                ->select('users.name', 'users.id as user_id', 'users.avatar')
                ->where('group_chat_users.gid', $gid)
                ->get();


    }
    //getBestMatchUsers

    //saveMessageToGroup
    public function saveMessageToGroup(Request $request)
    {
        $validatedData = $request->only(
            'user_id',
            'group_id',
            'doc',
            'attachment_type',
            'body',
            'chat_group',


        );
        $rules = [
            'user_id' => 'required',
            'group_id' => 'required'




        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "getGroupchatBySID-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        }

        $messageID = $request->user_id . mt_rand(9, 999999999) + time();
        $to_id=$request->group_id;
        DB::table('messages')->insert([
            'type' => 'user',
            'from_id' => $request->user_id,
            'to_id' => $to_id,
            'group_id' => $request->group_id,
            'body' => $request->body,
            'attachment_type' => $request->attachment_type,
            'message_id' => $messageID,
            'created_at' => date('Y-m-d H:i:s')
            
        ]);
        $lid = DB::getPdo()->lastInsertId();
        $docURL="";
        if ($request->hasFile('doc')) {
            $file = $request->file('doc');
            $filename = $request->user_id . "_chatdoc_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
            $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);
            $affected = DB::table('messages')
                ->where('message_id', $messageID,)
                ->update([
                    'attachment' => $filename
                ]);
                $docURL = asset('/local/public/upload/') . "/" . $filename;
        }
        $data = DB::table('messages')
             ->join('users', 'messages.from_id', '=', 'users.id')
            ->where('messages.group_id', $request->group_id)
            ->select(
                'messages.from_id as user_id',
                'messages.body',
                'messages.attachment',
                'messages.seen',
                'messages.created_at',
                'messages.attachment_type',
                'messages.message_id',
                'messages.group_id',
                'users.name as name',


            )
            ->get();

            $options = array(

                'cluster' => 'ap2',
    
                'encrypted' => true
    
            );
    
            $pusher = new Pusher\Pusher(
    
                '4c1630c06a480f5f76de',
    
                '6c1b90583017db3293f9',
    
                '1239507',
    
                $options
    
            );

           
            $group_id = $request->to_id;
            $group_chat =1;
            if($group_chat==1){
                $group_id = $to_id;
            }else{
                $group_id = null;
            }
    
            $users = DB::table('users')
            ->where('id', $request->user_id)
            ->first();
            if ($users->avatar == null) {
                $schLogo = NoImage();
            } else {
                $schLogo = asset('/local/public/upload/') . "/" . $users->avatar;
            }
            $message = $request->body;
            $name = $users->name;
           
    
            $dataM = DB::table('messages')->where('id',$lid)->first();
    
            $chat_data = array(
                'user_id' => $users->id,
                'name' => $users->name,              
                'body' => $dataM->body,              
                'attachment' => $dataM->attachment,              
                'attachment_type' => $dataM->attachment_type,              
                'seen' => $dataM->seen,              
                'created_at' => $dataM->created_at,
                'doc_type' =>$request->doc_type,
                'message_id' =>$dataM->message_id,
                'group_id' =>$dataM->group_id,
                "g_id" => $to_id,
                "to_id" => $to_id,
                'msg' => $message,
                'photo' => $schLogo,                
                'doc_url' => $docURL,
                'group_chat' => 1,
               
                
            );
            $zelosEvent = 'zelosUser_' . $to_id;
            if ($pusher->trigger('test_channel_group', $zelosEvent, $chat_data)) {
                
                $message = strtoupper('SUCCESS-Added');
                $message_action = "setUserPost";
              
                


              // $datav= array_push($data,$ajdata);

                return $this->setSuccessResponse($chat_data, $message, "Auth:Login", '', $message_action);
            }


     
    }
    //sendMessageToGroup


    //getGroupchatByGroupID
    public function getGroupchatByGroupID(Request $request)
    {
        $validatedData = $request->only(
            'group_id'
        );
        $rules = [
            'group_id' => 'required'


        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "getGroupchatBySID-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        }

        $dataGroupArrData = DB::table('chat_group')
            ->where('id', $request->group_id)
            ->where('is_deleted', 0)
            ->select(
                'id as gid',
                'sid',
                'group_name',
                'group_title',
                'created_at',
                'created_by',
                'status',
                'photo'
            )
            ->first();

            $usersDataMessage = DB::table('messages')
            ->join('users', 'messages.from_id', '=', 'users.id')
            ->select(
                'messages.from_id as user_id',
                'users.name',
                'messages.body',
                'messages.attachment',
                'messages.seen',
                'messages.created_at',
                'messages.attachment_type',
                'messages.message_id',
                'messages.group_id'
                
            )
            ->where('messages.group_id', $request->group_id)                
            // ->where('messages.seen', 0)
            ->get();
            $dataA=array();
            foreach ($usersDataMessage as $key => $rowData) {
                if( is_null($rowData->attachment_type)){
                    $docT=55;
                }else{
                    $docT=$rowData->attachment_type;
                }
                $dataA[] = array(
                    'user_id' => $rowData->user_id,
                    'name' => $rowData->name,              
                    'body' => $rowData->body,              
                    'attachment' => $rowData->attachment,              
                    'attachment_type' => $rowData->attachment_type,              
                    'seen' => $rowData->seen,              
                    'created_at' => $rowData->created_at,
                    'doc_type' =>$docT,
                    'message_id' =>$rowData->message_id,
                    'group_id' =>$rowData->group_id,
                    "g_id" => $rowData->group_id,
                    "to_id" =>$rowData->group_id,
                    'msg' =>$rowData->body, 
                    'photo' => '',                
                    'doc_url' => $rowData->attachment, 
                    'group_chat' => 1,
                   
                    
                );

            }

        $data = array(
            'group_data' => $dataGroupArrData,
            'group_message'=>$dataA
        );
        
        $message = strtoupper('SUCCESS-Added');
        $message_action = "getGroupchatBySID";
        return $this->setSuccessResponse($data, $message, "getGroupchatBySID", '', $message_action);
    }
    //getGroupchatByGroupID


    //getGroupchatByUserID
    public function getGroupchatByUserID(Request $request)
    {
        $validatedData = $request->only(
            'user_id'
        );
        $rules = [
            'user_id' => 'required'


        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "getGroupchatBySID-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        }
           
        $userGroupArr = array();
        $userArr = array();
        $data = array();
        $ownArr = array();

        $dataGroupArr = DB::table('group_chat_users')
            ->where('user_id', $request->user_id)
            ->where('is_deleted', 0)           
            ->get();
       

        foreach ($dataGroupArr as $key => $rowData) {
            // print_r($rowData);
           // die;

              $gid = $rowData->gid;
           // echo $id = $rowData->id;

            


            $dataGroupArrData = DB::table('chat_group')
                ->where('id', $gid)
                ->where('is_deleted', 0)
                ->select(
                    'id as gid',
                    'sid',
                    'group_name',
                    'group_title',
                    'created_at',
                    'created_by',
                    'status',
                    'photo'
                )
                ->first();
                $usersData = DB::table('group_chat_users')
                ->join('users', 'group_chat_users.user_id', '=', 'users.id')
                ->select('users.name', 'users.id as user_id', 'users.avatar')
                ->where('group_chat_users.gid', $gid)
                ->get();
                $usersDataUnSeen = DB::table('messages')
                ->where('messages.group_id', $gid)
                ->where('messages.seen', 0)
                ->count();
                $usersDataSeen = DB::table('messages')
                ->where('messages.group_id', $gid)
                ->where('messages.seen', 1)
                ->count();



                $userGroupArr[]=array(
                    'group_data'=>$dataGroupArrData,
                    'group_users'=>$usersData,
                    'group_read_count'=>$usersDataSeen,
                    'group_unread_count'=>$usersDataUnSeen,

                );

         
           
        }

        $data = $userGroupArr;



        $message = strtoupper('SUCCESS-Added');
        $message_action = "getGroupchatBySID";
        return $this->setSuccessResponse($data, $message, "getGroupchatBySID", '', $message_action);
    }
    //getGroupchatByUserID

    //getGroupchatBySID
    public function getGroupchatBySID(Request $request)
    {
        $validatedData = $request->only(
            'sid'
        );
        $rules = [
            'sid' => 'required'


        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "getGroupchatBySID-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        }
        $dataGroupArr = DB::table('chat_group')
            ->where('sid', $request->sid)
            ->where('is_deleted', 0)
            // ->select(
            //     'sid',
            //     'group_name',
            //     'group_title',
            //     'created_at',
            //     'created_by',
            //     'status',
            //     'photo'
            // )
            ->get();
        $userGroupArr = array();
        $userArr = array();
        $data = array();
        $ownArr = array();

        foreach ($dataGroupArr as $key => $rowData) {
            $gid = $rowData->id;


            $dataGroupArrData = DB::table('chat_group')
                ->where('id', $gid)
                ->where('is_deleted', 0)
                ->select(
                    'id as gid',
                    'sid',
                    'group_name',
                    'group_title',
                    'created_at',
                    'created_by',
                    'status',
                    'photo'
                )
                ->first();

            $usersData = DB::table('group_chat_users')
                ->join('users', 'group_chat_users.user_id', '=', 'users.id')
                ->select('users.name', 'users.id as user_id', 'users.avatar')
                ->where('group_chat_users.gid', $gid)
                ->get();
            $usersDataSeen = DB::table('messages')
                ->where('messages.group_id', $gid)
                ->where('messages.seen', 0)
                ->count();



            $ownArr[] = array(
                'group_details' => $dataGroupArrData,
                'group_user' => $usersData,
                'group_unread_count' => $usersDataSeen,
            );


            // $userGroupArr[]=$dataGroupArrData;
            // $userArr[]=$user_data;


        }

        $data = $ownArr;



        $message = strtoupper('SUCCESS-Added');
        $message_action = "getGroupchatBySID";
        return $this->setSuccessResponse($data, $message, "getGroupchatBySID", '', $message_action);
    }
    //getGroupchatBySID

    //setFriendRequestAcceptDecline
    public function setFriendRequestAcceptDecline(Request $request)
    {
        $validatedData = $request->only(
            'user_id',
            'profile_user_id',
            'type',


        );
        $rules = [
            'user_id' => 'required',
            'profile_user_id' => 'required',
            'type' => 'required',

        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "setFriendRequestAcceptDecline-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        }

        $usersReqArr = DB::table('users_friend_request')
            ->where('user_id', $request->user_id)
            ->where('profile_user_id', $request->profile_user_id)
            ->first();
        if ($usersReqArr == null) {
            $message = strtoupper('Invalid Input');
            $message_action = "setFriendRequestAcceptDecline-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        } else {
            $affected = DB::table('users_friend_request')
                ->where('user_id', $request->user_id)
                ->where('profile_user_id', $request->profile_user_id)
                ->update([
                    'accepted_status' => $request->type,
                ]);
            $data = DB::table('users_friend_request')
                ->where('user_id', $request->user_id)
                ->where('profile_user_id', $request->profile_user_id)
                ->select(
                    'id as request_id',
                    'user_id',
                    'profile_user_id',
                    'created_at',

                    'accepted_status',
                    'accepted_status_at'
                )
                ->get();

            $message = strtoupper('SUCCESS-Added');
            $message_action = "setFriendRequestAcceptDecline";
            return $this->setSuccessResponse($data, $message, "setFriendRequestAcceptDecline", '', $message_action);
        }
    }
    //setFriendRequestAcceptDecline

    //setFriendRequest
    public function setFriendRequest(Request $request)
    {
        $validatedData = $request->only(
            'user_id',
            'profile_user_id',


        );
        $rules = [
            'user_id' => 'required',
            'profile_user_id' => 'required',

        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "setFriendRequest-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        }


        $usersReqArr = DB::table('users_friend_request')
            ->where('user_id', $request->user_id)
            ->where('profile_user_id', $request->profile_user_id)
            ->where('accepted_status', 1)
            ->first();
        if ($usersReqArr == null) {
            DB::table('users_friend_request')
                ->updateOrInsert(
                    [
                        'user_id' => $request->user_id,
                        'profile_user_id' => $request->profile_user_id,
                    ],
                    [
                        'user_id' => $request->user_id,
                        'profile_user_id' => $request->profile_user_id,
                        'created_at' => date('Y-m-d H:i:s')
                    ]
                );


            $data = DB::table('users_friend_request')
                ->where('user_id', $request->user_id)
                ->where('profile_user_id', $request->profile_user_id)
                ->select(
                    'id as request_id',
                    'user_id',
                    'profile_user_id',
                    'created_at',

                    'accepted_status',
                    'accepted_status_at'
                )
                ->get();
            $message = strtoupper('SUCCESS-Added');
            $message_action = "setFriendRequest";
            return $this->setSuccessResponse($data, $message, "setFriendRequest", '', $message_action);
        } else {
            $data = DB::table('users_friend_request')
                ->where('user_id', $request->user_id)
                ->where('profile_user_id', $request->profile_user_id)
                ->select(
                    'id as request_id',
                    'user_id',
                    'profile_user_id',
                    'created_at',

                    'accepted_status',
                    'accepted_status_at'
                )
                ->get();
            $message = strtoupper('SUCCESS-Added');
            $message_action = "Already Accepted ";
            return $this->setSuccessResponse($data, $message, "setFriendRequest", '', $message_action);
        }
    }
    //setFriendRequest
    //getUserPostDetailV1
    public function getUserPostDetailV1(Request $request)
    {
        $validatedData = $request->only(
            'user_id'
        );
        $rules = [
            'user_id' => 'required'
        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "getUserPostDetail-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        }
        $postData = array();
        $user_data = DB::table('users')
            ->select('id as user_id',  'avatar as user_img', 'name as user_name')
            ->where('id', $request->user_id)
            ->first();

        $user_posts_data = DB::table('users_post')
            ->where('created_by', $request->user_id)
            ->where('is_deleted', 0)
            ->orderBy('id','DESC')
            ->select(
                'id as post_id',
                'created_by as user_id',
                'content',
                'is_publish',
                'photo',
                'link',
                'created_at'
            )->get();
        foreach ($user_posts_data as $key => $rowPosts) {

            $user_posts_data = DB::table('users_post')
                ->join('users', 'users_post.created_by', '=', 'users.id')
                ->where('users_post.created_by', $request->user_id)
                ->where('users_post.id', $rowPosts->post_id)
                ->where('users_post.is_deleted', 0)
                ->select(
                    'users_post.id as post_id',
                    'users_post.created_by as user_id',
                    'users.name as user_name',
                    'users.avatar as user_img',
                    'content',
                    'is_publish',
                    'users_post.photo',
                    'link',
                    'users_post.created_at'
                )->first();

            $user_post_comments_data = DB::table('users_post_comment')
                ->join('users', 'users_post_comment.created_by', '=', 'users.id')
                ->where('users_post_comment.created_by', $request->user_id)
                ->where('users_post_comment.post_id', $rowPosts->post_id)
                ->where('users_post_comment.is_deleted', 0)
                ->select(
                    'users_post_comment.id as comment_id',
                    'users_post_comment.created_by as user_id',
                    'users.name as user_name',
                    'users.avatar as user_img',
                    'post_id',
                    'users_post_comment.created_at',
                    'comment'
                )
                ->get();
            $user_posts_likes_data = DB::table('users_post_likes')
                ->join('users', 'users_post_likes.created_by', '=', 'users.id')
                ->where('users_post_likes.created_by', $request->user_id)
                ->where('users_post_likes.post_id', $rowPosts->post_id)
                ->select(
                    'users_post_likes.id as like_id',
                    'users_post_likes.created_by as user_id',
                    'users.name as user_name',
                    'users.avatar as user_img',
                    'post_id',
                    'users_post_likes.created_at',
                    'users_post_likes.status'
                )
                ->get();



            $postData[] = array(
                'post_data' => $user_posts_data,
                'post_comment_data' => $user_post_comments_data,
                'post_likes_data' => $user_posts_likes_data,

            );
        }




        //664101503711
        //Displaying



        $data = array(
            'user_data' => $user_data,
            'user_posts' => $postData

        );

        $message = strtoupper('SUCCESS-Added');
        $message_action = "setUserPostLike";
        return $this->setSuccessResponse($data, $message, "Auth:Login", '', $message_action);
    }

    //getUserPostDetailV1


    //getUserPostDetail
    public function getUserPostDetail(Request $request)
    {
        $validatedData = $request->only(
            'user_id'
        );
        $rules = [
            'user_id' => 'required'
        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "getUserPostDetail-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        }

        $user_data = DB::table('users')
            ->select('id as user_id',  'avatar')
            ->where('id', $request->user_id)
            ->first();
        $user_posts_data = DB::table('users_post')
            ->where('created_by', $request->user_id)
            ->where('is_deleted', 0)
            ->select(
                'id as post_id',
                'created_by as user_id',
                'content',
                'is_publish',
                'photo',
                'link',
                'created_at'
            )->get();

        $user_post_comments_data = DB::table('users_post_comment')
            ->where('created_by', $request->user_id)
            // ->where('post_id', $request->post_id)
            ->where('is_deleted', 0)
            ->select(
                'id as comment_id',
                'created_by as user_id',
                'post_id',
                'created_at',
                'comment'
            )
            ->get();

        $user_posts_likes_data = DB::table('users_post_likes')
            ->where('created_by', $request->user_id)
            // ->where('post_id', $request->post_id)    
            ->select(
                'id as like_id',
                'created_by as user_id',
                'post_id',
                'created_at',
                'status'
            )
            ->get();
        //664101503711
        //Displaying



        $data = array(
            'user_data' => $user_data,
            'user_posts' => $user_posts_data,
            'user_post_comments' => $user_post_comments_data,
            'user_posts_likes' => $user_posts_likes_data,
        );

        $message = strtoupper('SUCCESS-Added');
        $message_action = "setUserPostLike";
        return $this->setSuccessResponse($data, $message, "Auth:Login", '', $message_action);
    }
    //getUserPostDetail

    //setUserPostLike
    public function setUserPostLike(Request $request)
    {
        $validatedData = $request->only(
            'user_id',
            'post_id',
            'status',


        );
        $rules = [
            'user_id' => 'required',
            'post_id' => 'required',
            'status' => 'required'


        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "setUserPostLike-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        }

        //7053664216  avdesh kumar   885



        DB::table('users_post_likes')
            ->updateOrInsert(
                ['post_id' => $request->post_id,],
                [
                    'created_by' => $request->user_id,
                    'post_id' => $request->post_id,
                    'status' => $request->status,
                    'created_at' => date('Y-m-d H:i:s')
                ]
            );


        $data = DB::table('users_post_likes')
            ->where('created_by', $request->user_id)
            ->select(
                'id as like_id',
                'created_by as user_id',
                'post_id',
                'created_at',
                'status'
            )
            ->get();

        $message = strtoupper('SUCCESS-Added');
        $message_action = "setUserPostLike";
        return $this->setSuccessResponse($data, $message, "Auth:Login", '', $message_action);
    }

    //setUserPostLike

    //setUserPostComment
    public function setUserPostComment(Request $request)
    {
        $validatedData = $request->only(
            'user_id',
            'post_id',
            'comment',


        );
        $rules = [
            'user_id' => 'required',
            'post_id' => 'required',
            'comment' => 'required'


        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "setUserPostComment-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        }

        DB::table('users_post_comment')->insert([
            'created_by' => $request->user_id,
            'post_id' => $request->post_id,
            'comment' => $request->comment,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        $data = DB::table('users_post_comment')
            ->where('created_by', $request->user_id)
            ->where('is_deleted', 0)
            ->select(
                'id as comment_id',
                'created_by as user_id',
                'post_id',
                'created_at',
                'comment'
            )
            ->get();

        $message = strtoupper('SUCCESS-Added');
        $message_action = "setUserPost";
        return $this->setSuccessResponse($data, $message, "Auth:Login", '', $message_action);
    }
    //setUserPostComment

    //setUserPost
    public function setUserPost(Request $request)
    {
        $validatedData = $request->only(
            'user_id',
            'content',
            'photo',
            'link',

        );
        $rules = [
            'user_id' => 'required',
            'content' => 'required',
            'photo' => 'required',
            'link' => 'required',

        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "setUserPost-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        }

        DB::table('users_post')->insert([
            'created_by' => $request->user_id,
            'content' => $request->content,
            'link' => $request->link,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        $lid = DB::getPdo()->lastInsertId();

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $filename = $request->user_id . "_logo_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
            $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);



            $affected = DB::table('users_post')
                ->where('id', $lid,)
                ->update([
                    'photo' => $filename
                ]);
        }


        $data = DB::table('users_post')
            ->where('created_by', $request->user_id)
            ->where('is_deleted', 0)
            ->orderBy('id','DESC')
            ->select(
                'id as post_id',
                'created_by as user_id',
                'content',
                'is_publish',
                'photo',
                'link',
                'created_at',


            )
            ->get();




        $message = strtoupper('SUCCESS-Added');
        $message_action = "setUserPost";
        return $this->setSuccessResponse($data, $message, "Auth:Login", '', $message_action);
    }
    //setUserPost

    //getFlyUserFeedback
    public function getFlyUserFeedback(Request $request)
    {
        $validatedData = $request->only(
            'user_id',
            'fly_id',
            'q1_id',
            'q1_ans',
            'q2_id',
            'q2_ans',
            'q3_id',
            'q3_ans',
            'q4_id',
            'q4_ans',
            'q5_id',
            'q5_ans'

        );
        $rules = [
            'user_id' => 'required',
            'fly_id' => 'required',
            'q1_id' => 'required',
            'q1_ans' => 'required',
            'q2_id' => 'required',
            'q2_ans' => 'required',
            'q3_id' => 'required',
            'q3_ans' => 'required',
            'q4_id' => 'required',
            'q4_ans' => 'required',
            'q5_id' => 'required',
            'q5_ans' => 'required'

        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "FlyFeecback:setFlyUserFeedback-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        }


        DB::table('users_fly_feedback')->insert([
            'user_id' => $request->user_id,
            'fly_id' => $request->fly_id,
            'q1_id' => $request->q1_id,
            'q1_ans' => $request->q1_ans,
            'q2_id' => $request->q2_id,
            'q2_ans' => $request->q2_ans,
            'q3_id' => $request->q3_id,
            'q3_ans' => $request->q3_ans,
            'q4_id' => $request->q4_id,
            'q4_ans' => $request->q4_ans,
            'q5_id' => $request->q5_id,
            'q5_ans' => $request->q5_ans,
            'created_at' => date('Y-m-d H:i:s'),



        ]);

        $dataFeeback = DB::table('users_fly_feedback')
            ->where('user_id', $request->user_id)
            ->select(
                'user_id',
                'fly_id',
                'q1_id',
                'q1_ans',
                'q2_id',
                'q2_ans',
                'q3_id',
                'q3_ans',
                'q4_id',
                'q4_ans',
                'q5_id',
                'q5_ans',
                'created_at',

            )
            ->get();
        $dataFly = DB::table('users_fly')
            ->where('user_id', $request->fly_id)
            ->select(
                'id as fly_id',
                'user_id',
                'altitude',
                'start_lat',
                'start_long',
                'end_lat',
                'end_long',
                'name',
                'max_altitude',
                'elevation',
                'wind',
                'temperature',
                'created_at',

            )
            ->first();


        $data = array(
            'fly_feedback' => $dataFeeback,
            'fly_data' => $dataFly,
        );


        $message = strtoupper('SUCCESS-LOGIN');
        $message_action = "Fly Feedback :FlyFeeback-004";
        return $this->setSuccessResponse($data, $message, "Auth:Login", '', $message_action);
    }
    //getFlyUserFeedback

    //setFlyUser
    public function setFlyUser(Request $request)
    {
        $validatedData = $request->only(
            'user_id',
            'altitude',
            'start_lat',
            'start_long',
            'end_lat',
            'end_long',
            'name',
            'max_altitude',
            'elevation',
            'wind',
            'temperature'

        );
        $rules = [

            'user_id' => 'required',
            'altitude' => 'required',
            'start_lat' => 'required',
            'start_long' => 'required',
            'end_lat' => 'required',
            'end_long' => 'required',
            'name' => 'required',
            'max_altitude' => 'required',
            'elevation' => 'required',
            'wind' => 'required',
            'temperature' => 'required',
            'created_at' => 'required',

        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "Fly:setFlyUser-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        }




        DB::table('users_fly')->insert([
            'user_id' => $request->user_id,
            'altitude' => $request->altitude,
            'start_lat' => $request->start_lat,
            'start_long' => $request->start_long,
            'end_lat' => $request->end_lat,
            'end_long' => $request->end_long,
            'name' => $request->name,
            'max_altitude' => $request->max_altitude,
            'elevation' => $request->elevation,
            'wind' => $request->wind,
            'temperature' => $request->temperature,
            'created_at' => date('Y-m-d H:i:s'),



        ]);

        $data = DB::table('users_fly')
            ->where('user_id', $request->user_id)
            ->select(
                'id as fly_id',
                'user_id',
                'altitude',
                'start_lat',
                'start_long',
                'end_lat',
                'end_long',
                'name',
                'max_altitude',
                'elevation',
                'wind',
                'temperature',
                'created_at',

            )
            ->get();




        $message = strtoupper('SUCCESS-LOGIN');
        $message_action = "Auth:Login-004";
        return $this->setSuccessResponse($data, $message, "Auth:Login", '', $message_action);
    }
    //setFlyUser

    //getSchoolList
    public function getSchoolList(Request $request, School $user)
    {

        // $users->where('is_deleted', 0);
        $user = $user->newQuery();
        if ($request->has('sid')) {
            $user->where('id', $request->sid);
        }
        if ($request->has('country_id')) {
            $user->where('country_id', $request->country_id);
        }


        $users = $user->get();




        $data = array();

        // $schoolArrData = DB::table('schools')
        //     ->where('is_deleted', 0)
        //     ->get();
        foreach ($users as $key => $rowData) {
            $path = asset('/local/public/upload/') . "/";
            $schoolData = array(
                'school_id' => $rowData->id,
                'title' => $rowData->title,
                'reg_no' => $rowData->reg_no,
                'phone_code' => $rowData->phone_code,
                'phone' => $rowData->phone,
                'email' => $rowData->email,
                'city_name' => $rowData->city_name,
                'country_name' => $rowData->country_name,
                'state_name' => $rowData->state_name,
                'school_logo' => $rowData->school_logo,
                'base_path' => $path,
                'website' => $rowData->website,
                'facebook' => $rowData->facebook,
                'twitter' => $rowData->twitter,
                'linkedin' => $rowData->linkedin
            );

            //schools_slider_img
            $schoolArrDataIMGArr = DB::table('schools_slider_img')
                ->where('sid', $rowData->id)
                ->get();

            $schoolArrDataHistoryArr = DB::table('school_history')
                ->where('sid', $rowData->id)
                ->get();
            $schoolArrDataInstructorArr = DB::table('school_instructor')
                ->where('sid', $rowData->id)
                ->get();
            $schoolArrDataDocumentArr = DB::table('school_documents')
                ->where('sid', $rowData->id)
                ->get();

            $schoolArrDataIMG = array();
            $schoolArrDataHistory = array();
            $schoolArrDataInstro = array();
            $schoolArrDatadocu = array();

            if (count($schoolArrDataIMGArr) >= 0) {


                foreach ($schoolArrDataIMGArr as $key => $rowIMG) {
                    $schoolArrDataIMG[] = array(
                        'gallery_img' => $rowIMG->slider_img
                    );
                }
            }

            if (count($schoolArrDataHistoryArr) >= 0) {


                foreach ($schoolArrDataHistoryArr as $key => $rowHis) {
                    $schoolArrDataHistory[] = array(

                        'school_year' => $rowHis->school_year,
                        'school_students' => $rowHis->school_students,
                        'school_notes' => $rowHis->school_notes
                    );
                }
            }

            if (count($schoolArrDataInstructorArr) >= 0) {


                foreach ($schoolArrDataInstructorArr as $key => $rowHis) {
                    $schoolArrDataInstro[] = array(

                        'name' => $rowHis->name,
                        'profile_url' => $rowHis->profile_url,
                    );
                }
            }

            if (count($schoolArrDataDocumentArr) >= 0) {


                foreach ($schoolArrDataDocumentArr as $key => $rowHis) {
                    $schoolArrDatadocu[] = array(

                        'doc_name' => $rowHis->doc_name,
                        'doc_info' => $rowHis->doc_info,
                    );
                }
            }


            //schools_slider_img


            $data[] = array(
                'school_data' => $schoolData,
                'school_dataIMG' => $schoolArrDataIMG,
                'school_dataHistory' => $schoolArrDataHistory,
                'school_dataInstructor' => $schoolArrDataInstro,
                'school_dataDocuments' => $schoolArrDatadocu,
            );
        }


        $message = strtoupper('SUCCESS-Sport');
        $message_action = "getSports-004";

        return $this->setSuccessResponse($data, $message, "getSports", '', $message_action);
    }
    //getSchoolList

    public function getSports(Request $request)
    {



        $data = DB::table('sports')
            ->where('is_deleted', 0)
            ->get();


        $message = strtoupper('SUCCESS-Sport');
        $message_action = "getSports-004";

        return $this->setSuccessResponse($data, $message, "getSports", '', $message_action);
    }
    public function getIntrest(Request $request)
    {



        $data = DB::table('interest')
            ->where('is_deleted', 0)
            ->get();


        $message = strtoupper('SUCCESS-Inerest');
        $message_action = "getIntrest-004";

        return $this->setSuccessResponse($data, $message, "getIntrest", '', $message_action);
    }

    public function register(Request $request)
    {

        // print_r($request->all());
        // die;
        $validatedData = $request->only('name', 'email_phone', 'password', 'confirm_password', 'birth_date', 'gender');
        $rules = [

            'name' => 'required',
            'password' => 'required',
            'birth_date' => 'required',
            'gender' => 'required',
            'confirm_password' => 'required'

        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input6');
            $message_action = "Auth:register-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        }
        if ($request->password != $request->confirm_password) {
            $message = strtoupper('Password does not matched');
            $message_action = "Auth:register-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        }
        $email_phone = $request->email_phone;
        $emailPhoneArr = explode('@', $email_phone);

        if (count($emailPhoneArr) == 2) {
            $email_phoneInput = $request->email_phone;
            $model = User::where('email', $email_phoneInput)
                ->first();
            $validatedData['email'] = $email_phoneInput;
            $strMSG = "Alreday Exists Email";
        } else {
            $email_phoneInput = $request->email_phone;
            $model = User::where('phone', $email_phoneInput)
                ->first();
            $validatedData['phone'] = $email_phoneInput;
            $strMSG = "Alreday Exists Phone";
        }


        if ($model != null) {
            $message = strtoupper($strMSG);
            $message_action = "Auth:register-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        } else {





            $validatedData['password'] = bcrypt($request->password);
            $interest = $request->interest;
            $sports = $request->sports;



            if (!isset($interest)) {
                $message = strtoupper('Interest  invalid input');
                $message_action = "Auth:register-001";
                return $this->setWarningResponse([], $message, $message_action, "", $message_action);
            }
            if (!isset($interest)) {
                $message = strtoupper('Interest  invalid input');
                $message_action = "Auth:register-001";
                return $this->setWarningResponse([], $message, $message_action, "", $message_action);
            }

            $user3 = User::create($validatedData);


            foreach ($interest as $key => $row) {
                DB::table('app_users_interest')->insert([
                    'interest_id' =>  $row['id'],
                    'user_id' =>  $user3->id
                ]);
            }




            foreach ($sports as $key => $row) {
                DB::table('app_users_sports')->insert([
                    'sport_id' =>  $row['id'],
                    'user_id' =>  $user3->id
                ]);
            }

            $accessToken = $user3->createToken('authToken')->accessToken;
            $userA = auth()->user();

            $dataUserData =  User::where('id', $user3->id)
                ->select('id as user_id', 'birth_date', 'name', 'gender', 'email', 'phone', 'created_at', 'avatar')
                ->first();
            //dataUserData = $userA->only(['id', 'name','gender', 'email', 'phone', 'user_position', 'address', 'created_at', 'avatar', 'base_path']);


            $app_users_interestData = DB::table('app_users_interest')
                ->join('interest', 'app_users_interest.interest_id', '=', 'interest.id')
                ->select('interest.id as interest_id', 'interest.name')
                ->where('app_users_interest.user_id', $user3->id)
                ->get();

            $app_users_sportsData = DB::table('app_users_sports')
                ->join('sports', 'app_users_sports.sport_id', '=', 'sports.id')
                ->select('sports.id as sport_id', 'sports.name')
                ->where('app_users_sports.user_id', $user3->id)
                ->get();




            $data = array(
                'user_data' => $dataUserData,
                'user_interest' => $app_users_interestData,
                'user_sports' => $app_users_sportsData,
            );


            $message = strtoupper('SUCCESS-LOGIN');
            $message_action = "Auth:Login-004";
            return $this->setSuccessResponse($data, $message, "Auth:Login", $accessToken, $message_action);
        }
        //  print_r($validatedData);
        //  die();


        //return response(['user' => $user, 'access_token' => $accessToken]);
    }

    public function login(Request $request)
    {

        $validatedData = $request->only('email_phone', 'password', 'type', 'provide_id');
        $rules = [

            'email_phone' => 'required',
            'password' => 'required'

        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "Auth:Login-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        }

        $email_phone = $request->email_phone;
        $emailPhoneArr = explode('@', $email_phone);


        $login_type = $request->type;

        if ($login_type == 1) {
            if (isset($emailPhoneArr[1])) {
                $email_phone = $request->email_phone;
                $loginType = 1;
            } else {
                $email_phone = $emailPhoneArr[0];
                $loginType = 2;
            }


            if ($loginType == 1) {

                $model = User::where('email', $email_phone)
                    ->first();
            } else {
                $model = User::where('phone', $email_phone)
                    ->first();
            }
        } else {
            $modelP = User::where('provider_id', $request->provide_id)->where('provider_type', $request->type)
                ->first();
            if ($modelP == null) {
                DB::table('users')->insert([
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'name' => $request->name,
                    'birth_date' => $request->birth_date,
                    'provider_type' => $request->type,
                    'provider_id' => $request->provider_id

                ]);
            }
            $model = User::where('provider_id', $request->provide_id)->where('provider_type', $request->type)
                ->first();
        }



        if ($model == null) {
            try {
                // attempt to verify the credentials and create a token for the user
                $message = strtoupper('Invalid Login credential');
                $message_action = "Auth:Login:002";
                return $this->setWarningResponse([], $message, "Auth:Login", "", $message_action);
            } catch (\Exception $ex) {
                return $this->setErrorResponse([], $ex->getMessage());
            }
        } else {

            $validator = Validator::make($validatedData, $rules);
            if ($validator->fails()) {
                $message = strtoupper('Invalid Credential');
                $message_action = "Auth:Login-003";
                return $this->setWarningResponse([], $message, "Auth:Login", "", $message_action);
            }

            if (Hash::check($request->password, $model->password, [])) {


                Auth::loginUsingId($model->id, true);

                $accessToken = auth()->user()->createToken('authToken')->accessToken;

                $userA = auth()->user();

                $dataUserData = DB::table('users')
                    ->select('id as user_id', 'birth_date', 'name', 'gender', 'email', 'phone', 'created_at', 'avatar')
                    ->where('id', $userA->id)
                    ->first();


                $app_users_interestData = DB::table('app_users_interest')
                    ->join('interest', 'app_users_interest.interest_id', '=', 'interest.id')
                    ->select('interest.id as interest_id', 'interest.name')
                    ->where('app_users_interest.user_id', $userA->id)
                    ->get();

                $app_users_sportsData = DB::table('app_users_sports')
                    ->join('sports', 'app_users_sports.sport_id', '=', 'sports.id')
                    ->select('sports.id as sport_id', 'sports.name')
                    ->where('app_users_sports.user_id', $userA->id)
                    ->get();


                //$app_users_sportsData=DB::table('app_users_sports')->where('user_id', $userA->id)->get();

                $data = array(
                    'user_data' => $dataUserData,
                    'user_interest' => $app_users_interestData,
                    'user_sports' => $app_users_sportsData,
                );
                $message = strtoupper('SUCCESS-LOGIN');
                $message_action = "Auth:Login-004";

                return $this->setSuccessResponse($data, $message, "Auth:Login", $accessToken, $message_action);
            } else {
                $message = strtoupper('Invalid Credentials');
                $message_action = "Auth:Login-005";
                return $this->setWarningResponse([], $message, "Auth:Login", "", $message_action);
            }
        }
    } //klogin

    //updateProfile
    //-----------------------------
    public function updateProfile(Request $request)
    {
        $validatedData = $request->only('user_id', 'name', 'avatar', 'email', 'phone', 'gender', 'age', 'location');
        $rules = [

            'user_id' => 'required',
            'name' => 'required',
            'avatar' => 'required',
            'email' => 'required',
            'gender' => 'required',
            'age' => 'required',
            'location' => 'required',
            'phone' => 'required'
        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "Auth:profile-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        } else {

            $users = User::where('id', $request->user_id)
                ->first();
            if ($users == null) {
                $message = strtoupper('Opps! not found');
                $message_action = "Auth:UpdateGetProfile-001";
                return $this->setWarningResponse([], $message, $message_action, "", $message_action);
            } else {

                $affected = DB::table('users')
                    ->where('id', $request->user_id)
                    ->update([
                        'name' => $request->name,
                        'email' => $request->email,
                        'gender' => $request->gender, 'phone' => $request->phone
                    ]);
                if ($request->hasFile('avatar')) {
                    $file = $request->file('avatar');
                    $filename = $request->emp_id . "_user_" . rand(10, 1000) . "_" . date('Ymshis') . '.' . $file->getClientOriginalExtension();
                    // save to local/public/uploads/photo/ as the new $filename
                    //var/www/larachat/local/public/storage/users-avatar

                    $filename = $file->storeAs('/doc', $filename, ['disk' => 'uploads']);

                    $affected = DB::table('users')
                        ->where('id', $request->user_id,)
                        ->update(['avatar' => $filename]);
                }


                $model = User::where('id', $request->user_id)->first();

                Auth::loginUsingId($model->id, true);

                $accessToken = auth()->user()->createToken('authToken')->accessToken;
                $userA = auth()->user();
                $data = $userA->only(['id', 'name', 'email', 'phone', 'created_at', 'avatar']);
                $message = strtoupper('SUCCESS-Update');
                $message_action = "Auth:Update-001";

                return $this->setSuccessResponse($data, $message, "Auth:UPDATE", $accessToken, $message_action);
            }
        }
    }
    // get Profile
    public function getProfile(Request $request)
    {
        $validatedData = $request->only('user_id');
        $rules = [

            'user_id' => 'required'
        ];
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "Auth:getprofile-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        } else {

            $users = User::where('id', $request->user_id)
                ->first();
            if ($users == null) {
                $message = strtoupper('Opps! not found');
                $message_action = "Auth:getProfile-001";
                return $this->setWarningResponse([], $message, $message_action, "", $message_action);
            } else {

                $model = User::where('id', $request->user_id)->first();
                Auth::loginUsingId($model->id, true);
                $accessToken = auth()->user()->createToken('authToken')->accessToken;
                $userA = auth()->user();

                $dataUserData = DB::table('users')
                    ->select('id as user_id', 'birth_date', 'name', 'gender', 'email', 'phone', 'created_at', 'avatar')
                    ->where('id', $userA->id)
                    ->first();

                $app_users_interestData = DB::table('app_users_interest')
                    ->join('interest', 'app_users_interest.interest_id', '=', 'interest.id')
                    ->select('interest.id as interest_id', 'interest.name')
                    ->where('app_users_interest.user_id', $userA->id)
                    ->get();

                $app_users_sportsData = DB::table('app_users_sports')
                    ->join('sports', 'app_users_sports.sport_id', '=', 'sports.id')
                    ->select('sports.id as sport_id', 'sports.name')
                    ->where('app_users_sports.user_id', $userA->id)
                    ->get();


                //$app_users_sportsData=DB::table('app_users_sports')->where('user_id', $userA->id)->get();

                $data = array(
                    'user_data' => $dataUserData,
                    'user_interest' => $app_users_interestData,
                    'user_sports' => $app_users_sportsData,
                );

                $message = strtoupper('SUCCESS-Update');
                $message_action = "Auth:Update-001";

                return $this->setSuccessResponse($data, $message, "Auth:UPDATE", $accessToken, $message_action);
            }
        }
    }


    /* Add Trip */
    public function addTrip(Request $request)
    {
        // print_r($request->all());
        // die();
        $validatedData = $request->only('user_id', 'trip_name', 'location', 'start_date', 'end_date', 'trip_infomation');
        $rules = [

            'user_id' => 'required',
            'trip_name' => 'required',
            'location' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'trip_infomation' => 'required'
        ];
        $validator = Validator::make($validatedData, $rules);

        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "Auth:trip-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        } else {

            $inserId = DB::table('user_trip')->insertGetId([
                'user_id' => $request->user_id,
                'trip_name' => $request->trip_name,
                'location' => $request->location,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'trip_infomation' => $request->trip_infomation,
                'created_at' => date('Y-m-d h:i:s'),
            ]);
            $userTripData = DB::table('user_trip')
                ->where('user_id', '=', $request->user_id)
                ->select('id as trip_id', 'user_Id', 'trip_name', 'location', 'start_date', 'end_date', 'trip_infomation', 'created_at')
                ->where('is_deleted', '=', 0)
                ->get();
            $data = $userTripData;
            $message = strtoupper('SUCCESS-Insert');
            $message_action = "Auth Trip-001";
            return $this->setSuccessResponse($data, $message, "Auth:trip", '', $message_action);
        }
    }


    /* Edit Trip */
    public function editTrip(Request $request)
    {
        // print_r($request->all());
        // die();
        $validatedData = $request->only('user_id', 'trip_name', 'location', 'start_date', 'end_date', 'trip_infomation', 'trip_id');
        $rules = [

            'user_id' => 'required',
            'trip_name' => 'required',
            'location' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'trip_infomation' => 'required',
            'trip_id' => 'required'
        ];
        $validator = Validator::make($validatedData, $rules);

        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "Auth:trip-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        } else {

            $trip = DB::table('user_trip')->where('id', $request->trip_id)
                ->first();
            if ($trip == null) {
                $message = strtoupper('Opps! not found');
                $message_action = "Auth:trip-002";
                return $this->setWarningResponse([], $message, $message_action, "", $message_action);
            } else {


                $affected = DB::table('user_trip')
                    ->where('id', $request->trip_id)
                    ->update([
                        'user_id' => $request->user_id,
                        'trip_name' => $request->trip_name,
                        'location' => $request->location,
                        'start_date' => $request->start_date,
                        'end_date' => $request->end_date,
                        'trip_infomation' => $request->trip_infomation
                    ]);




                $userTripData = DB::table('user_trip')
                    ->where('id', '=', $request->trip_id)
                    ->where('is_deleted', '=', 0)
                    ->select('id as trip_id', 'user_Id', 'trip_name', 'location', 'start_date', 'end_date', 'trip_infomation', 'created_at')
                    ->get();
                $data = $userTripData;

                $message = strtoupper('SUCCESS-Update');
                $message_action = "Auth Trip-004";
                return $this->setSuccessResponse($data, $message, "Auth:trip", '', $message_action);
            }
        }
    }

    /* Add Trip */
    public function getTripByUserID(Request $request)
    {
        $validatedData = $request->only('user_id');
        $rules = [
            'user_id' => 'required'
        ];
        $validator = Validator::make($validatedData, $rules);

        if ($validator->fails()) {
            $message = strtoupper('Invalid Input4');
            $message_action = "Auth:gettrip-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        } else {

            $data = DB::table('user_trip')
                ->where('user_id', '=', $request->user_id)
                ->where('is_deleted', '=', 0)
                ->select('id as trip_id', 'user_Id', 'trip_name', 'location', 'start_date', 'end_date', 'trip_infomation', 'created_at')
                ->get();

            $message = strtoupper('Record Found');
            $message_action = "Auth Trip-001";
            return $this->setSuccessResponse($data, $message, "Auth:gettrip", '', $message_action);
        }
    }

    public function getTripDelete(Request $request)
    {
        //print_r($request->all());
        //die();
        $validatedData = $request->only('trip_id', 'user_id');
        $rules = [
            'trip_id' => 'required',
            'user_id' => 'required'
        ];
        $validator = Validator::make($validatedData, $rules);

        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "Auth:deletetrip-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        } else {


            $userA = DB::table('user_trip')
                ->where('id', '=', $request->trip_id)
                ->where('user_Id', '=', $request->user_id)
                ->where('is_deleted', '=', 0)
                ->first();
            if ($userA != null) {
                $affected = DB::table('user_trip')
                    ->where('id', $request->trip_id)
                    ->update([
                        'is_deleted' => 1
                    ]);
                $message = strtoupper('Record Deleted');
                $message_action = "Auth TripDelete-003";

                $data = DB::table('user_trip')
                    ->where('user_id', '=', $request->user_id)
                    ->select('id as trip_id', 'user_Id', 'trip_name', 'location', 'start_date', 'end_date', 'trip_infomation', 'created_at')
                    ->where('is_deleted', '=', 0)
                    ->get();

                return $this->setSuccessResponse($data, $message, "Auth:deleteTrip", '', $message_action);
            } else {

                $message = strtoupper('No Record Deleted');
                $message_action = "Auth TripDelete-004";
                return $this->setSuccessResponse($userA, $message, "Auth:deleteTrip", '', $message_action);
            }
        }
    }


    public function setUserOTP(Request $request)
    {

        $validatedData = $request->only('user_id');
        $rules = [
            'user_id' => 'required'
        ];
        $validator = Validator::make($validatedData, $rules);

        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "Auth:setUserOTP-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        } else {
            $userA = DB::table('users')
                ->where('id', '=', $request->user_id)

                ->first();
            if ($userA) {
                $gen_otp = substr(str_shuffle("0123456789"), 0, 5);
                $gen_otp = 123456;
                $affected = DB::table('users')
                    ->where('id', $request->user_id)
                    ->update([
                        'user_otp' => $gen_otp
                    ]);
                $data = DB::table('users')
                    ->where('id', '=', $request->user_id)
                    ->select('id as user_id', 'user_otp')
                    ->first();

                $message = strtoupper('OTP send to your Email');
                $message_action = "Auth OTPSEND-002";
                return $this->setSuccessResponse($data, $message, "Auth:deleteTrip", '', $message_action);
            } else {
                $message = strtoupper('User ID Not found');
                $message_action = "Auth:setUserOTP-003";
                return $this->setWarningResponse([], $message, $message_action, "", $message_action);
            }
        }
    }



    public function setUserOTPResend(Request $request)
    {
        //print_r($request->all());
        //die();
        $validatedData = $request->only('user_id');
        $rules = [
            'user_id' => 'required'

        ];
        $validator = Validator::make($validatedData, $rules);

        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "Auth:Otpresend-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        } else {
            $userA = DB::table('users')
                ->where('id', '=', $request->user_id)
                ->first();
            if ($userA) {
                $gen_otp = substr(str_shuffle("0123456789"), 0, 5);
                $gen_otp = 123456;
                $affected = DB::table('users')
                    ->where('id', $request->user_id)
                    ->update([
                        'user_otp' => $gen_otp
                    ]);
                $data = DB::table('users')
                    ->where('id', '=', $request->user_id)
                    ->select('id as user_id', 'user_otp')
                    ->first();
                $message = strtoupper('OTP Re-send to your Email');
                $message_action = "Auth Otpresend-002";
                return $this->setSuccessResponse($data, $message, "Auth:Otpresend", '', $message_action);
            } else {
                $message = strtoupper('User ID Not found');
                $message_action = "Auth:setUserOTP-003";
                return $this->setWarningResponse([], $message, $message_action, "", $message_action);
            }
        }
    }


    public function getUserOTPVerify(Request $request)
    {
        //print_r($request->all());
        //die();
        $validatedData = $request->only('user_id', 'otp');
        $rules = [
            'user_id' => 'required',
            'otp' => 'required'
        ];
        $validator = Validator::make($validatedData, $rules);

        if ($validator->fails()) {
            $message = strtoupper('Invalid Input');
            $message_action = "Auth:OtpVerify-001";
            return $this->setWarningResponse([], $message, $message_action, "", $message_action);
        } else {
            $userA = DB::table('users')
                ->where('id', '=', $request->user_id)
                // ->where('email', '=', $request->email)
                ->where('user_otp', '=', $request->otp)
                ->first();
            if ($userA) {

                $message = strtoupper('OTP Verify Sucessfully');
                $message_action = "Auth Otpresend-002";
                return $this->setSuccessResponse($userA, $message, "Auth:Otpresend", '', $message_action);
            } else {
                $message = strtoupper('User ID Not found');
                $message_action = "Auth:OtpVerify-003";
                return $this->setWarningResponse([], $message, $message_action, "", $message_action);
            }
        }
    }
}
