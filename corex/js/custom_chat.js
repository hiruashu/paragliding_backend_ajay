

// Search action on keyup
var url=BASE_URL;
$('.messenger-search').on('keyup', function (e) {
   
    $.trim($(this).val()).length > 0
        ? $('.messenger-search').trigger('focus') + messengerSearch($(this).val())
        : $('.messenger-tab').hide() +
        $('.messenger-listView-tabs a[data-view="users"]').trigger('click');
});

function messengerSearch(input) {
    $.ajax({
        url: url + '/chat_search',
        method: 'GET',
        data: { 'input': input },
        dataType: 'JSON',
        beforeSend: () => {
            $('.search-records').html(listItemLoading(4));
        },
        success: (data) => {
            $('.search-records').find('svg').remove();
            data.addData == 'append'
                ? $('.search-records').append(data.records)
                : $('.search-records').html(data.records);
            // update data-action required with [responsive design]
            cssMediaQueries();
        },
        error: () => {
            console.error('Server error, check your response');
        }
    });
}

// loading placeholder for users list item

function listItemLoading(items) {
    let template = '';
    for (let i = 0; i < items; i++) {
        template += `
        <div class="d-flex align-items-center justify-content-between mb-5">
        <div class="d-flex align-items-center">
            <div class="symbol symbol-circle symbol-50 mr-3">
                <img alt="A" src="" />
            </div>
            <div class="d-flex flex-column">
                <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-lg">Matt Pears</a>
                <span class="text-muted font-weight-bold font-size-sm">test</span>
            </div>
        </div>
        <div class="d-flex flex-column align-items-end">
            <span class="text-muted font-weight-bold font-size-sm">35 mins</span>
        </div>
    </div>
        `;
    }
    return template;
}

//showUserChatDetail
function showUserChatDetail(user_id){
    $('#ajHeader').html(`	<div class="card-header align-items-center px-4 py-4 bg-primary text-white">
    <div class="text-left flex-grow-1">
        <!--begin::Aside Mobile Toggle-->
        <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md d-lg-none" id="kt_app_chat_toggle">
            <span class="svg-icon svg-icon-lg">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Adress-book2.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />
                        <path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
        </button>
        <!--end::Aside Mobile Toggle-->
        <!--begin::Dropdown Menu-->

        <!--end::Dropdown Menu-->
    </div>
    <div class="w-100 text-left flex-grow-1 d-flex align-items-center imgAj">
        <div class="symbol symbol-500 symbol-circle mr-4 ">
        <img alt="Zelos School" src="{{getBaseURL().'/svg_logo/logo.png'}}">

        </div>
        
    </div>
    <div class="text-right flex-grow-1">
        <!--begin::Dropdown Menu-->

        <!--end::Dropdown Menu-->
    </div>
</div>`)
    $('.card-footer').css('visibility','');
   

$.ajax({
    url: url + '/getUserChatDetails',
    method: 'GET',
    data: { 'user_id': user_id },
    dataType: 'JSON',
    beforeSend: () => {
        //$('.search-records').html(listItemLoading(4));
    },
    success: (data) => {

        $('.mainUserName').html(data.user_data.name);
        $('#txtUserReciever').val(data.user_data.name);
        $('#to_id').val(data.user_data.user_id);
        $('#to_id_model').val(data.user_data.user_id);
        
        // $('.imgAj').html(`<img alt="${data.user_data.name}" src="${data.user_data.photo}" />`);
        $('.imgAj').html(`<div class="symbol symbol-50 symbol-circle mr-4 ">
        <img alt="${data.user_data.name}" src="${data.user_data.photo}">

        </div>
        <div>
            <div class="font-weight-bold font-size-h5 mainUserIMG text-white"></div>
            <div>
                <span class="label label-sm label-dot label-success text-white bg-green"></span>
                <span class="font-weight-bold font-size-sm text-green-75">Active</span>
            </div>
        </div>`);
        $('.mainUserIMG').html(data.user_data.name);
        $('.messages_display').html(data.htmlChat);
        
        $('.scroll').scrollTop( $(document).height()+25545454 );

        // $('.scroll').animate({
        //     scrollTop: $('.scroll').get(0).scrollHeight+25555
            
        // }, 2000);
        // $('.search-records').find('svg').remove();
        // data.addData == 'append'
        //     ? $('.search-records').append(data.records)
        //     : $('.search-records').html(data.records);
        // update data-action required with [responsive design]
       // cssMediaQueries();
    },
    error: () => {
        console.error('Server error, check your response');
    }
});

}


function fetchMessages(id, type) {
    if (messenger != 0) {
        $.ajax({
            url: url + '/fetchMessages',
            method: 'POST',
            data: { '_token': access_token, 'id': id, 'type': type },
            dataType: 'JSON',
            success: (data) => {
                // Enable message form if messenger not = 0; means if data is valid
                if (messenger != 0) {
                    disableOnLoad(false);
                }
                messagesContainer.find('.messages').html(data.messages);
                // scroll to bottom
                scrollBottom(messagesContainer);
                // remove loading bar
                NProgress.done();
                NProgress.remove();

                // trigger seen event
                makeSeen(true);
            },
            error: () => {
                // remove loading bar
                NProgress.done();
                NProgress.remove();
                console.error('Failed to fetch messages! check your server response.');
            }
        });
    }
}

//btnSendchatMessage

//btnSendchatMessage
