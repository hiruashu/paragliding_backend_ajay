

//deleteGroupChat
function deleteGroupChat(rowID) {
  Swal.fire({
    title: "Are you sure?",
    text: "You wont be able to revert this!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonText: "Yes, delete it!",
  }).then(function (result) {
    if (result.value) {
      //ajax
      $.ajax({
        url: BASE_URL + "/deleteGroupChat",
        type: "POST",
        data: {
          _token: $('meta[name="csrf-token"]').attr("content"),
          rowid: rowID,
        },
        success: function (resp) {
          if (resp.status == 1) {
            Swal.fire("Deleted!", "Deleted Successfully.", "success");
            setTimeout(function () {
              // window.location.href = BASE_URL+'/orders'
              location.reload(1);
            }, 500);
          } else {
            Swal.fire("Deleted Alert!", "Cann't not Deleted", "error");
          }
        },
      });
      //ajax
    }
  });
}

//deleteGroupChat

//deleteSchoolGroupUser
function deleteSchoolGroupUser(rowID) {
  Swal.fire({
    title: "Are you sure?",
    text: "You wont be able to revert this!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonText: "Yes, remove it!",
  }).then(function (result) {
    if (result.value) {
      //ajax
      $.ajax({
        url: BASE_URL + "/removeGroupchatUser",
        type: "POST",
        data: {
          _token: $('meta[name="csrf-token"]').attr("content"),
          rowid: rowID,
        },
        success: function (resp) {
          if (resp.status == 1) {
            Swal.fire("Removed!", "Removed Successfully.", "success");
            setTimeout(function () {
              // window.location.href = BASE_URL+'/orders'
              location.reload(1);
            }, 500);
          } else {
            Swal.fire("Removed Alert!", "Cann't not Removed", "error");
          }
        },
      });
      //ajax
    }
  });
}
//deleteSchoolGroupUser

function deleteSchoolCouse(rowID) {
  Swal.fire({
    title: "Are you sure?",
    text: "You wont be able to revert this!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonText: "Yes, delete it!",
  }).then(function (result) {
    if (result.value) {
      //ajax
      $.ajax({
        url: BASE_URL + "/deleteSchoolCouse",
        type: "POST",
        data: {
          _token: $('meta[name="csrf-token"]').attr("content"),
          rowid: rowID,
        },
        success: function (resp) {
          if (resp.status == 1) {
            Swal.fire("Deleted!", "Your file has been deleted.", "success");
            setTimeout(function () {
              // window.location.href = BASE_URL+'/orders'
              location.reload(1);
            }, 500);
          } else {
            Swal.fire("Deleted Alert!", "Cann't not delete", "error");
          }
        },
      });
      //ajax
    }
  });
}

var KTFormControls = (function () {
    // Private functions
    
    var _initDemo_x = function () {
      FormValidation.formValidation(
        document.getElementById("ayra_kt_form_add_school"), {
          fields: {
            title: {
              validators: {
                notEmpty: {
                  message: "Title is required",
                },
              },
            },
            reg_no: {
              validators: {
                notEmpty: {
                  message: "Registration No. is required",
                },
              },
            },
            country: {
              validators: {
                notEmpty: {
                  message: "Country is required",
                },
              },
            },
            state: {
              validators: {
                notEmpty: {
                  message: "State is required",
                },
              },
            },
            city: {
              validators: {
                notEmpty: {
                  message: "City is required",
                },
              },
            },
            email: {
              validators: {
                notEmpty: {
                  message: "Email is required",
                },
                emailAddress: {
                  message: "The value is not a valid email address",
                },
              },
            },
  
            website: {
              validators: {
                notEmpty: {
                  message: "Website URL is required",
                },
                uri: {
                  message: "The website address is not valid",
                },
              },
            },
  
            digits: {
              validators: {
                notEmpty: {
                  message: "Digits is required",
                },
                digits: {
                  message: "The velue is not a valid digits",
                },
              },
            },
  
            creditcard: {
              validators: {
                notEmpty: {
                  message: "Credit card number is required",
                },
                creditCard: {
                  message: "The credit card number is not valid",
                },
              },
            },
  
            phone: {
              validators: {
                notEmpty: {
                  message: "Phone number is required",
                },
              },
            },
  
            option: {
              validators: {
                notEmpty: {
                  message: "Please select an option",
                },
              },
            },
  
            options: {
              validators: {
                choice: {
                  min: 2,
                  max: 5,
                  message: "Please select at least 2 and maximum 5 options",
                },
              },
            },
  
            memo: {
              validators: {
                notEmpty: {
                  message: "Please enter memo text",
                },
                stringLength: {
                  min: 50,
                  max: 100,
                  message: "Please enter a menu within text length range 50 and 100",
                },
              },
            },
  
            
  
         
  
            
          },
  
          plugins: {
            //Learn more: https://formvalidation.io/guide/plugins
            trigger: new FormValidation.plugins.Trigger(),
            // Bootstrap Framework Integration
            bootstrap: new FormValidation.plugins.Bootstrap(),
            // Validate fields when clicking the Submit button
            submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
            //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
          },
        }
      ).on("core.form.valid", function () {
        // Send the form data to back-end
        // You need to grab the form data and create an Ajax request to send them
        var _redirect = $("#ayra_kt_form_add_school").attr("data-redirect");
  
        // ajax
        var formData = {
          title: $("input[name=title]").val(),
          regno: $("input[name=regno]").val(),
          txtSID: $("input[name=txtSID]").val(),
          txtAction: $("input[name=txtAction]").val(),
          country: $("#kt_select2_1").val(),
          state: $("#kt_select2_2").val(),
          city: $("#kt_select2_3").val(),
          reg_no: $("input[name=reg_no]").val(),
          phone_code: $("#txtPhoneCode").val(),
          website: $("#website").val(),
          phone: $("input[name=phone]").val(),
          email: $("input[name=email]").val(),
          password: $("input[name=password]").val(),
          admin_comm: $("input[name=admin_comm]").val(),
          facebook: $("input[name=facebook]").val(),
          twitter: $("input[name=twitter]").val(),
          linkedin: $("input[name=linkedin]").val(),
          about: $("#kt-ckeditor-1").html(),
          _token: $('meta[name="csrf-token"]').attr("content"),
        };
        $.ajax({
          url: BASE_URL + "/saveSchool",
          type: "POST",
          data: formData,
          success: function (res) {
            if (res.status == 1) {
              swal
                .fire({
                  text: res.msg,
                  icon: "success",
                  buttonsStyling: false,
                  confirmButtonText: "Ok, got it!",
                  customClass: {
                    confirmButton: "btn font-weight-bold btn-light-primary",
                  },
                })
                .then(function () {
                  setTimeout(function () {
                    //KTUtil.scrollTop();
                    // location.reload();
                    location.assign(_redirect);
                  }, 500);
                });
            } else {
              swal
                .fire({
                  text: res.msg,
                  icon: "error",
                  buttonsStyling: false,
                  confirmButtonText: "Ok, got it!",
                  customClass: {
                    confirmButton: "btn font-weight-bold btn-light-primary",
                  },
                })
                .then(function () {
                  KTUtil.scrollTop();
                });
            }
          },
        });
        // ajax
      });
    };
  
    return {
      // public functions
      init: function () {
       
        // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html
  
        var myEditor;
        DecoupledEditor.create(document.querySelector("#kt-ckeditor-1"),{
          toolbar: [ 'bold', 'italic', 'link', 'undo', 'redo', 'numberedList', 'bulletedList' ]
      } )
          .then((editor) => {
            const toolbarContainer = document.querySelector(
              "#kt-ckeditor-1-toolbar"
            );
            myEditor = editor;
            toolbarContainer.appendChild(editor.ui.view.toolbar.element);
            
          })
          .catch((error) => {
            console.error(error);
          });
  
          _initDemo_x();
      },
    };
  })();




  $(".countrySchool").change(function () {
    var country_id = $(this).val();
    var formData = {
      country_id: country_id,
      _token: $('meta[name="csrf-token"]').attr("content"),
    };
    $.ajax({
      url: BASE_URL + "/getcityBycountry",
      type: "GET",
      data: formData,
      success: function (res) {
        console.log(res);
        $('.schoolCity').html(res);
      }
    });
      
   });


   var KTDropzoneDemo = (function () {
    // Private functions
  
    var demo2AyraUpload = function () {
      // set the dropzone container id
      var id = "#kt_dropzone_4";
  
  
      // set the preview element template
      var previewNode = $(id + " .dropzone-item");
      previewNode.id = "";
      var previewTemplate = previewNode.parent(".dropzone-items").html();
      previewNode.remove();
  
      var myDropzone4 = new Dropzone(id, {
        // Make the whole body a dropzone
        url: BASE_URL + "/uploadSchoolCourseDoc", // Set the url for your upload script location
        parallelUploads: 20,
  
        previewTemplate: previewTemplate,
        maxFilesize: 1, // Max filesize in MB
  
        autoQueue: false, // Make sure the files aren't queued until manually added
        previewsContainer: id + " .dropzone-items", // Define the container to display the previews
        clickable: id + " .dropzone-select", // Define the element that should be used as click trigger to select files.
      });
  
      myDropzone4.on("sending", function (file, xhr, formData) {
  
      if($("#txtDocInfo").val()==""){
        Swal.fire("Document Information Required", "Alert", "warning");
        return false;
       }else{
        formData.append("action_upload", '_upload_Avatar');
        formData.append("action_upload", '_upload_Avatar');
        formData.append("_token", $('meta[name="csrf-token"]').attr("content"));
        formData.append("doc_info", $("#txtDocInfo").val());
        formData.append("txtSID", $("#txtSID").val());
        formData.append("txtID", $("#txtID").val());
        formData.append("action_uploadv2", $("#action_uploadv2").val());
        
       }
  
        
      });
      myDropzone4.on("addedfile", function (file) {
        // Hookup the start button
        file.previewElement.querySelector(id + " .dropzone-start").onclick =
          function () {
            myDropzone4.enqueueFile(file);
          };
        $(document)
          .find(id + " .dropzone-item")
          .css("display", "");
        $(id + " .dropzone-upload, " + id + " .dropzone-remove-all").css(
          "display",
          "inline-block"
        );
      });
  
      // Update the total progress bar
      myDropzone4.on("totaluploadprogress", function (progress) {
        $(this)
          .find(id + " .progress-bar")
          .css("width", progress + "%");
      });
  
      myDropzone4.on("sending", function (file) {
        // Show the total progress bar when upload starts
        $(id + " .progress-bar").css("opacity", "1");
        // And disable the start button
        file.previewElement
          .querySelector(id + " .dropzone-start")
          .setAttribute("disabled", "disabled");
      });
  
      // Hide the total progress bar when nothing's uploading anymore
      myDropzone4.on("complete", function (progress) {
        var thisProgressBar = id + " .dz-complete";
        setTimeout(function () {
          $(
            thisProgressBar +
            " .progress-bar, " +
            thisProgressBar +
            " .progress, " +
            thisProgressBar +
            " .dropzone-start"
          ).css("opacity", "0");
          Swal.fire("Good job!", "Uploaded Successfully!", "success");
          location.reload(1);
        }, 2000);
      });
  
      // Setup the buttons for all transfers
      document.querySelector(id + " .dropzone-upload").onclick = function () {
        if($("#txtDocInfo").val()==""){
          Swal.fire("Document Information Required", "Alert", "warning");
          return false;
        }else{
          myDropzone4.enqueueFiles(myDropzone4.getFilesWithStatus(Dropzone.ADDED));
        }
        
      };
  
      // Setup the button for remove all files
      document.querySelector(id + " .dropzone-remove-all").onclick = function () {
        $(id + " .dropzone-upload, " + id + " .dropzone-remove-all").css(
          "display",
          "none"
        );
        myDropzone4.removeAllFiles(true);
      };
  
      // On all files completed upload
      myDropzone4.on("queuecomplete", function (progress) {
        $(id + " .dropzone-upload").css("display", "none");
      });
  
      // On all files removed
      myDropzone4.on("removedfile", function (file) {
        if (myDropzone4.files.length < 1) {
          $(id + " .dropzone-upload, " + id + " .dropzone-remove-all").css(
            "display",
            "none"
          );
        }
      });
    };
   
  
    
    
  
    
    return {
      // public functions
      init: function () {
       
        
        demo2AyraUpload();
       
        
        
        
      },
    };
  })();
  
  KTUtil.ready(function () {
    KTDropzoneDemo.init();
  });


  //btnCreateGroup
  $('#btnCreateGroup').click(function(){
    var txtGroupName=$('#txtGroupName').val();
    var txtGroupTitle=$('#txtGroupTitle').val();
    if(txtGroupName==""){
      swal
      .fire({
        text: 'Please Enter Group Name',
        icon: "error",
        buttonsStyling: false,
        confirmButtonText: "Ok, got it!",
        customClass: {
          confirmButton: "btn font-weight-bold btn-light-primary",
        },
      })
      .then(function () {
        KTUtil.scrollTop();
      });
      return false;

    }
    if(txtGroupTitle==""){
      swal
      .fire({
        text: 'Please Enter Group Title',
        icon: "error",
        buttonsStyling: false,
        confirmButtonText: "Ok, got it!",
        customClass: {
          confirmButton: "btn font-weight-bold btn-light-primary",
        },
      })
      .then(function () {
        KTUtil.scrollTop();
      });
      return false;

    }

    //ajax
    var formData = {
      txtGroupName: txtGroupName,
      txtGroupTitle: txtGroupTitle,      
      _token: $('meta[name="csrf-token"]').attr("content"),
    };
    $.ajax({
      url: BASE_URL + "/createGroupChat",
      type: "POST",
      data: formData,
      success: function (res) {
       
          if (res.status == 1) {
            swal
              .fire({
                text: res.msg,
                icon: "success",
                buttonsStyling: false,
                confirmButtonText: "Ok, got it!",
                customClass: {
                  confirmButton: "btn font-weight-bold btn-light-primary",
                },
              })
              .then(function () {
                setTimeout(function () {
                  //KTUtil.scrollTop();
                   location.reload();
                 
                }, 500);
              });
          }else {
            swal
              .fire({
                text: res.msg,
                icon: "error",
                buttonsStyling: false,
                confirmButtonText: "Ok, got it!",
                customClass: {
                  confirmButton: "btn font-weight-bold btn-light-primary",
                },
              })
              .then(function () {
                KTUtil.scrollTop();
              });
          }
        }
    });
    //ajax 

  });
  //btnCreateGroup


  //btnPasswordReset
$('#btnPasswordReset').click(function(){
  var curr_pass=$('#current').val();
  var new_pass=$('#password').val();
  var confirm_pass=$('#confirmed').val();
  if(curr_pass==""){
   
    Swal.fire("Password Reset", "Enter Current Password", "error");
    return false;
  }
  if(new_pass==""){
    
    Swal.fire("Password Reset", "Enter New Password", "error");
    
    return false;
  }
  if(confirm_pass==""){
    toasterOptions();
    toastr.error('Enter Confirm Password', 'Password Reset');
    return false;
  }
  if(confirm_pass!=new_pass){
    
    Swal.fire("Password Reset", "Password Mismatched", "error");
    return false;
  }
  var formData = {
    'current':curr_pass,
    'password':new_pass,
    'confirmed':confirm_pass,
    '_token':$('meta[name="csrf-token"]').attr('content'),
    'user_id':$('meta[name="UUID"]').attr('content'),

  };
  $.ajax({
    url: BASE_URL+'/UserResetPasswordV2',
    type: 'POST',
    data: formData,
    success: function(res) {
     if(res.status==1){
     
      Swal.fire("Password Reset", "Password successfully changed", "success");
      setTimeout(function () {
        //KTUtil.scrollTop();
        // location.reload();
        var redirect = BASE_URL;
        location.assign(redirect);
      }, 500);
     }
     if(res.status==2){
      
      
      Swal.fire("Password Reset", "Your current password does not matches with the password you provided", "error");
      return false;
     }
     if(res.status==3){     
      
      Swal.fire("Password Reset", "New Password cannot be same as your current password. Please choose a different password..", "error");

     }

    }

  });




});
//btnPasswordReset