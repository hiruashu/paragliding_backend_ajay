FormValidation.formValidation(
    document.getElementById("kt_form_add_static_contentXA"), {
      fields: {
        title: {
          validators: {
            notEmpty: {
              message: "Please Enter Title",
            },
          },
        },
        status: {
          validators: {
            notEmpty: {
              message: "Please Enter Title",
            },
          },
        },
      },
  
      plugins: {
        //Learn more: https://formvalidation.io/guide/plugins
        trigger: new FormValidation.plugins.Trigger(),
        // Bootstrap Framework Integration
        bootstrap: new FormValidation.plugins.Bootstrap(),
        // Validate fields when clicking the Submit button
        submitButton: new FormValidation.plugins.SubmitButton(),
        // Submit the form when all fields are valid
        //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
      },
    }
  ).on("core.form.valid", function () {
    var _redirect = $("#kt_form_add_static_content").attr("data-redirect");
  
    var formData = {
      title: $("input[name=title]").val(),
      txtStaticID: $("input[name=txtStaticID]").val(),
      isactive: $("#isactive").val(),
      content: $("#kt-ckeditor-1").html(),
      staticAction: $("input[name=staticAction]").val(),
      _token: $('meta[name="csrf-token"]').attr("content"),
    };
  
    $.ajax({
      url: BASE_URL + "/saveStaticContent",
      type: "POST",
      data: formData,
      success: function (res) {
        if (res.status == 1) {
          swal
            .fire({
              text: res.msg,
              icon: "success",
              buttonsStyling: false,
              confirmButtonText: "Ok, got it!",
              customClass: {
                confirmButton: "btn font-weight-bold btn-light-primary",
              },
            })
            .then(function () {
              setTimeout(function () {
                //KTUtil.scrollTop();
                // location.reload();
                var redirect = BASE_URL + "/" + _redirect;
                location.assign(redirect);
              }, 500);
            });
        } else {
          swal
            .fire({
              text: res.msg,
              icon: "error",
              buttonsStyling: false,
              confirmButtonText: "Ok, got it!",
              customClass: {
                confirmButton: "btn font-weight-bold btn-light-primary",
              },
            })
            .then(function () {
              KTUtil.scrollTop();
            });
        }
      },
    });
  });
   //save static data 