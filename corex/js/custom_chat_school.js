

// Search action on keyup
var url=BASE_URL;
$('.messenger-search').on('keyup', function (e) {
   
    $.trim($(this).val()).length > 0
        ? $('.messenger-search').trigger('focus') + messengerSearch($(this).val())
        : $('.messenger-tab').hide() +
        $('.messenger-listView-tabs a[data-view="users"]').trigger('click');
});

function messengerSearch(input) {
    $.ajax({
        url: url + '/chat_search',
        method: 'GET',
        data: { 'input': input },
        dataType: 'JSON',
        beforeSend: () => {
            $('.search-records').html(listItemLoading(4));
        },
        success: (data) => {
            $('.search-records').find('svg').remove();
            data.addData == 'append'
                ? $('.search-records').append(data.records)
                : $('.search-records').html(data.records);
            // update data-action required with [responsive design]
            cssMediaQueries();
        },
        error: () => {
            console.error('Server error, check your response');
        }
    });
}

// loading placeholder for users list item

function listItemLoading(items) {
    let template = '';
    for (let i = 0; i < items; i++) {
        template += `
        <div class="d-flex align-items-center justify-content-between mb-5">
        <div class="d-flex align-items-center">
            <div class="symbol symbol-circle symbol-50 mr-3">
                <img alt="A" src="" />
            </div>
            <div class="d-flex flex-column">
                <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-lg">Matt Pears</a>
                <span class="text-muted font-weight-bold font-size-sm">test</span>
            </div>
        </div>
        <div class="d-flex flex-column align-items-end">
            <span class="text-muted font-weight-bold font-size-sm">just now</span>
        </div>
    </div>
        `;
    }
    return template;
}

//showUserChatDetail
function showUserChatDetail(user_id){

$.ajax({
    url: url + '/getUserChatDetailsSchool',
    method: 'GET',
    data: { 'user_id': user_id },
    dataType: 'JSON',
    beforeSend: () => {
        //$('.search-records').html(listItemLoading(4));
    },
    success: (data) => {

        $('.mainUserName').html(data.user_data.name);
        $('#txtUserReciever').val(data.user_data.name);
        $('#to_id').val(data.user_data.user_id);
        
        // $('.mainUserIMG').html(`<img alt="${data.user_data.name}" src="${data.user_data.photo}" />`);
        $('.mainUserIMG').html(data.user_data.name);

        // $('.search-records').find('svg').remove();
        // data.addData == 'append'
        //     ? $('.search-records').append(data.records)
        //     : $('.search-records').html(data.records);
        // update data-action required with [responsive design]
       // cssMediaQueries();
    },
    error: () => {
        console.error('Server error, check your response');
    }
});

}


function fetchMessages(id, type) {
    if (messenger != 0) {
        $.ajax({
            url: url + '/fetchMessages',
            method: 'POST',
            data: { '_token': access_token, 'id': id, 'type': type },
            dataType: 'JSON',
            success: (data) => {
                // Enable message form if messenger not = 0; means if data is valid
                if (messenger != 0) {
                    disableOnLoad(false);
                }
                messagesContainer.find('.messages').html(data.messages);
                // scroll to bottom
                scrollBottom(messagesContainer);
                // remove loading bar
                NProgress.done();
                NProgress.remove();

                // trigger seen event
                makeSeen(true);
            },
            error: () => {
                // remove loading bar
                NProgress.done();
                NProgress.remove();
                console.error('Failed to fetch messages! check your server response.');
            }
        });
    }
}

//btnSendchatMessage

//btnSendchatMessage
