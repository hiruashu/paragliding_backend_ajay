"use strict";
var KTDatatablesSearchOptionsAdvancedSearch = (function () {
  $.fn.dataTable.Api.register("column().title()", function () {
    return $(this.header()).text().trim();
  });

  var initTable1 = function () {
    // begin first table
    var schoolListFrom = $("#schoolListFrom").val();

    var table = $("#kt_datatable_schoolList").DataTable({
      responsive: true,
      // Pagination settings
      dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
      // read more: https://datatables.net/examples/basic_init/dom.html

      lengthMenu: [5, 10, 25, 50],

      pageLength: 10,

      language: {
        lengthMenu: "Display _MENU_",
      },

      searchDelay: 500,
      processing: true,
      serverSide: true,
      ajax: {
        url: BASE_URL + "/getDatatableSchoolList",
        type: "GET",
        data: {
          _token: $('meta[name="csrf-token"]').attr("content"),
          schoolListFrom: schoolListFrom,

          // parameters for custom backend script demo
          columnsDef: [
            "RecordID",
            "school_title",
            "IndexID",
            "rating",
            "city",
            "email",
            "status",
            "profile_status",
            "Actions",
          ],
        },
      },
      columns: [
        { data: "RecordID" },
        { data: "IndexID" },
        { data: "school_title" },
        { data: "rating" },
        { data: "city" },
        { data: "email" },
        { data: "status" },
        { data: "profile_status" },
        { data: "Actions", responsivePriority: -1 },
      ],

      initComplete: function () {
        this.api()
          .columns()
          .every(function () {
            var column = this;

            switch (column.title()) {
              case "Country":
                column
                  .data()
                  .unique()
                  .sort()
                  .each(function (d, j) {
                    $('.datatable-input[data-col-index="2"]').append(
                      '<option value="' + d + '">' + d + "</option>"
                    );
                  });
                break;

              case "Status":
                var status = {
                  1: { title: "Pending", class: "label-light-primary" },
                  2: { title: "Delivered", class: " label-light-danger" },
                  3: { title: "Canceled", class: " label-light-primary" },
                  4: { title: "Success", class: " label-light-success" },
                  5: { title: "Info", class: " label-light-info" },
                  6: { title: "Danger", class: " label-light-danger" },
                  7: { title: "Warning", class: " label-light-warning" },
                };
                column
                  .data()
                  .unique()
                  .sort()
                  .each(function (d, j) {
                    $('.datatable-input[data-col-index="6"]').append(
                      '<option value="' +
                        d +
                        '">' +
                        status[d].title +
                        "</option>"
                    );
                  });
                break;

              case "Type":
                var status = {
                  1: { title: "Online", state: "danger" },
                  2: { title: "Retail", state: "primary" },
                  3: { title: "Direct", state: "success" },
                };
                column
                  .data()
                  .unique()
                  .sort()
                  .each(function (d, j) {
                    $('.datatable-input[data-col-index="7"]').append(
                      '<option value="' +
                        d +
                        '">' +
                        status[d].title +
                        "</option>"
                    );
                  });
                break;
            }
          });
      },

      columnDefs: [
        {
          targets: [0],
          visible: !1,
        },
        {
          targets: -1,

          title: "Actions",
          orderable: false,
          render: function (data, type, full, meta) {
            var EDIT_URL = BASE_URL + "/edit-school/" + full.RecordID;
            var VIEW_URL = BASE_URL + "/view-school/" + full.RecordID;
            var chatWindow = BASE_URL + "/zelosChat";

            return `<a href="${VIEW_URL}" class="btn btn-sm btn-clean btn-icon" title="View Details">\
        <i class="la la-eye"></i>
      </a>						  
    <a href="${EDIT_URL}" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
      <i class="la la-edit"></i>
    </a>
    <a href="javascript::void(0)" onclick="deleteSchool(${full.RecordID})"  class="btn btn-sm btn-clean btn-icon" title="Delete">\
      <i class="la la-trash"></i>
    </a>
    <a href="${chatWindow}"   class="btn btn-sm btn-clean btn-icon" title="Chat with school">\
    <i class="icon-1x text-dark-50 flaticon-chat"></i>
  </a>
  
      `;
          },
        },
        {
          targets: 3,
          width: 175,
          render: function (data, type, full, meta) {
            var strRating = "";
            switch (parseFloat(data)) {
              case 1:
                strRating = `
                                <i class="icon-xl la la-star text-warning" ></i>
                                <i class="icon-xl la la-star " ></i>                                
                                <i class="icon-xl la la-star " ></i>
                                <i class="icon-xl la la-star " ></i>
                                <i class="icon-xl la la-star" ></i>
                               
                               
                                `;
                break;
              case 1.5:
                strRating = `
                                    <i class="icon-xl la la-star text-warning" ></i>
                                    <i class="icon-xl la la-star-half-alt text-warning"></i>                             
                                    <i class="icon-xl la la-star " ></i>
                                    <i class="icon-xl la la-star " ></i>
                                    <i class="icon-xl la la-star" ></i>
                                   
                                   
                                    `;
                break;
              case 2:
                strRating = `
                                        <i class="icon-xl la la-star text-warning" ></i>
                                        <i class="icon-xl la la-star text-warning" ></i>                        
                                        <i class="icon-xl la la-star " ></i>
                                        <i class="icon-xl la la-star " ></i>
                                        <i class="icon-xl la la-star" ></i>
                                       
                                       
                                        `;
                break;
              case 2.5:
                strRating = `
                                            <i class="icon-xl la la-star text-warning" ></i>
                                            <i class="icon-xl la la-star text-warning" ></i>                        
                                            <i class="icon-xl la la-star-half-alt text-warning"></i>
                                            <i class="icon-xl la la-star " ></i>
                                            <i class="icon-xl la la-star" ></i>
                                           
                                           
                                            `;
                break;
              case 3:
                strRating = `
                                            <i class="icon-xl la la-star text-warning" ></i>
                                            <i class="icon-xl la la-star text-warning" ></i>                        
                                            <i class="icon-xl la la-star text-warning" ></i> 
                                            <i class="icon-xl la la-star " ></i>
                                            <i class="icon-xl la la-star" ></i>
                                           
                                           
                                            `;
                break;

              case 3.5:
                strRating = `
                                            <i class="icon-xl la la-star text-warning" ></i>
                                            <i class="icon-xl la la-star text-warning" ></i>                        
                                            <i class="icon-xl la la-star text-warning" ></i> 
                                            <i class="icon-xl la la-star-half-alt text-warning"></i>
                                            <i class="icon-xl la la-star" ></i>
                                           
                                           
                                            `;
                break;
              case 4:
                strRating = `
                                                <i class="icon-xl la la-star text-warning" ></i>
                                                <i class="icon-xl la la-star text-warning" ></i>                        
                                                <i class="icon-xl la la-star text-warning" ></i> 
                                                <i class="icon-xl la la-star text-warning" ></i> 
                                                <i class="icon-xl la la-star" ></i>
                                               
                                               
                                                `;
                break;
              case 4.5:
                strRating = `
                                    <i class="icon-xl la la-star text-warning" ></i>
                                    <i class="icon-xl la la-star text-warning" ></i>                                
                                    <i class="icon-xl la la-star text-warning" ></i>
                                    <i class="icon-xl la la-star text-warning" ></i>                                
                                    <i class="icon-xl la la-star-half-alt text-warning"></i>
                                   
                                    `;
                break;
              case 5:
                strRating = `
                                    <i class="icon-xl la la-star text-warning" ></i>
                                    <i class="icon-xl la la-star text-warning" ></i>                                
                                    <i class="icon-xl la la-star text-warning" ></i>
                                    <i class="icon-xl la la-star text-warning" ></i>                                
                                    <i class="icon-xl la la-star text-warning" ></i>        
                                   
                                    `;
                break;

              default:
                strRating = `NA`;
                break;
            }
            return "(" + data + "/5)<br>" + strRating;
          },
        },
        {
          targets: 6,
          width: 50,
          title: "Status",
          orderable: false,
          render: function (a, t, e, n) {
            var i = {
              1: {
                title: "Active",
                class: "primary",
              },
              2: {
                title: "Deactive",
                class: "danger",
              },
            };
            //return void 0 === i[a] ? a : '<span class="m-badge ' + i[a].class + ' m-badge--wide">' + i[a].title + "</span>"
            return (
              '<span class="font-weight-bold text-' +
              i[a].class +
              '">' +
              i[a].title +
              "</span>"
            );
          },
        },
        {
          targets: 7,
          width: 50,
          title: "Profile",
          orderable: false,
          render: function (a, t, e, n) {
            var i = {
              1: {
                title: "Completed",
                class: "primary",
              },
              2: {
                title: "Incomplete",
                class: "danger",
              },
            };
            //return void 0 === i[a] ? a : '<span class="m-badge ' + i[a].class + ' m-badge--wide">' + i[a].title + "</span>"
            return (
              '<span class="font-weight-bold text-' +
              i[a].class +
              '">' +
              i[a].title +
              "</span>"
            );
          },
        },
      ],
    });

    var filter = function () {
      var val = $.fn.dataTable.util.escapeRegex($(this).val());
      table
        .column($(this).data("col-index"))
        .search(val ? val : "", false, false)
        .draw();
    };

    var asdasd = function (value, index) {
      var val = $.fn.dataTable.util.escapeRegex(value);
      table.column(index).search(val ? val : "", false, true);
    };

    $("#kt_search").on("click", function (e) {
      e.preventDefault();
      var params = {};
      $(".datatable-input").each(function () {
        var i = $(this).data("col-index");
        if (params[i]) {
          params[i] += "|" + $(this).val();
        } else {
          params[i] = $(this).val();
        }
      });
      $.each(params, function (i, val) {
        // apply search params to datatable
        table.column(i).search(val ? val : "", false, false);
      });
      table.table().draw();
    });

    $("#kt_reset").on("click", function (e) {
      e.preventDefault();
      $(".datatable-input").each(function () {
        $(this).val("");
        table.column($(this).data("col-index")).search("", false, false);
      });
      table.table().draw();
    });

    $("#kt_datepicker").datepicker({
      todayHighlight: true,
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>',
      },
    });
  };
  var initTable1_payment = function () {
    // begin first table
    var schoolListFrom = $("#schoolListFrom").val();

    var table = $("#kt_datatable_schoolList_Payment").DataTable({
      responsive: true,
      // Pagination settings
      dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
      // read more: https://datatables.net/examples/basic_init/dom.html

      lengthMenu: [5, 10, 25, 50],

      pageLength: 10,

      language: {
        lengthMenu: "Display _MENU_",
      },

      searchDelay: 500,
      processing: true,
      serverSide: true,
      ajax: {
        url: BASE_URL + "/getDatatableSchoolPaymentList",
        type: "GET",
        data: {
          _token: $('meta[name="csrf-token"]').attr("content"),
          schoolListFrom: schoolListFrom,

          // parameters for custom backend script demo
          columnsDef: [
            "RecordID",
            "school_title",
            "IndexID",
            "school_regno",
            "school_total_rec",
            "admin_comm",
            "admin_total_rec",            
            "Actions",
          ],
        },
      },
      columns: [
        { data: "RecordID" },
        { data: "IndexID" },
        { data: "school_title" },
        { data: "school_regno" },
        { data: "school_total_rec" },
        { data: "admin_comm" },
        { data: "admin_total_rec" },        
        { data: "Actions", responsivePriority: -1 },
      ],

      initComplete: function () {
        this.api()
          .columns()
          .every(function () {
            var column = this;

            switch (column.title()) {
              case "Country":
                column
                  .data()
                  .unique()
                  .sort()
                  .each(function (d, j) {
                    $('.datatable-input[data-col-index="2"]').append(
                      '<option value="' + d + '">' + d + "</option>"
                    );
                  });
                break;

              case "Status":
                var status = {
                  1: { title: "Pending", class: "label-light-primary" },
                  2: { title: "Delivered", class: " label-light-danger" },
                  3: { title: "Canceled", class: " label-light-primary" },
                  4: { title: "Success", class: " label-light-success" },
                  5: { title: "Info", class: " label-light-info" },
                  6: { title: "Danger", class: " label-light-danger" },
                  7: { title: "Warning", class: " label-light-warning" },
                };
                column
                  .data()
                  .unique()
                  .sort()
                  .each(function (d, j) {
                    $('.datatable-input[data-col-index="6"]').append(
                      '<option value="' +
                        d +
                        '">' +
                        status[d].title +
                        "</option>"
                    );
                  });
                break;

              case "Type":
                var status = {
                  1: { title: "Online", state: "danger" },
                  2: { title: "Retail", state: "primary" },
                  3: { title: "Direct", state: "success" },
                };
                column
                  .data()
                  .unique()
                  .sort()
                  .each(function (d, j) {
                    $('.datatable-input[data-col-index="7"]').append(
                      '<option value="' +
                        d +
                        '">' +
                        status[d].title +
                        "</option>"
                    );
                  });
                break;
            }
          });
      },

      columnDefs: [
        {
          targets: [0],
          visible: !1,
        },
        {
          targets: -1,

          title: "Actions",
          orderable: false,
          render: function (data, type, full, meta) {
            var EDIT_URL = BASE_URL + "/edit-school/" + full.RecordID;
            var VIEW_URL = BASE_URL + "/view-school/" + full.RecordID;
            return '';
            return `<a href="${VIEW_URL}" class="btn btn-sm btn-clean btn-icon" title="View Details">\
        <i class="la la-eye"></i>
      </a>						  
    <a href="${EDIT_URL}" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
      <i class="la la-edit"></i>
    </a>
    <a href="javascript::void(0)" onclick="deleteSchool(${full.RecordID})"  class="btn btn-sm btn-clean btn-icon" title="Delete">\
      <i class="la la-trash"></i>
    </a>
    <a class="btn btn-sm btn-icon btn-bg-light btn-icon-warning btn-hover-warning" href="#" data-toggle="modal" data-target="#kt_chat_modal">
       <i class="flaticon2-chat-1"></i>
   </a>
      `;
          },
        },
        {
          targets: 2,
          title: 'School Name',
          orderable: false,
          render: function(data, type, full, meta) {
            var viewSchool_URL_ = BASE_URL + "/view-school-payment-details/" + full.RecordID;
            return `<a href="${viewSchool_URL_}">${full.school_title}</a>`
          },
        },
    
     
      ],
    });

    var filter = function () {
      var val = $.fn.dataTable.util.escapeRegex($(this).val());
      table
        .column($(this).data("col-index"))
        .search(val ? val : "", false, false)
        .draw();
    };

    var asdasd = function (value, index) {
      var val = $.fn.dataTable.util.escapeRegex(value);
      table.column(index).search(val ? val : "", false, true);
    };

    $("#kt_search").on("click", function (e) {
      e.preventDefault();
      var params = {};
      $(".datatable-input").each(function () {
        var i = $(this).data("col-index");
        if (params[i]) {
          params[i] += "|" + $(this).val();
        } else {
          params[i] = $(this).val();
        }
      });
      $.each(params, function (i, val) {
        // apply search params to datatable
        table.column(i).search(val ? val : "", false, false);
      });
      table.table().draw();
    });

    $("#kt_reset").on("click", function (e) {
      e.preventDefault();
      $(".datatable-input").each(function () {
        $(this).val("");
        table.column($(this).data("col-index")).search("", false, false);
      });
      table.table().draw();
    });

    $("#kt_datepicker").datepicker({
      todayHighlight: true,
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>',
      },
    });
  };
//  Aprl may june //
//kt_datatable_notifyList
var initTable22_notify = function () {
  // begin first table
 
  var table = $("#kt_datatable_notifyList").DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      lengthMenu: "Display _MENU_",
    },

    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + "/getDatatableNotifyList",
      type: "GET",
      data: {
        _token: $('meta[name="csrf-token"]').attr("content"),
      
        // parameters for custom backend script demo
        columnsDef: [
          "RecordID",
          "noti_name",
          "noti_type",
          "IndexID",
          "noti_message",
          "created_at",
          "is_send",         
          "Actions",
        ],
      },
    },
    columns: [
      { data: "RecordID" },
      { data: "IndexID" },
      { data: "noti_type" },
      { data: "noti_name" },    
      { data: "noti_message" }, 
      { data: "created_at" },  
      { data: "is_send" },    
      { data: "Actions", responsivePriority: -1 },
    ],

   

    columnDefs: [
      {
        targets: [0],
        visible: !1,
      },
      {
        targets: -1,

        title: "Actions",
        orderable: false,
        render: function (data, type, full, meta) {
          // var EDIT_URL = BASE_URL + "/edit-school/" + full.RecordID;
          // var VIEW_URL = BASE_URL + "/view-school-requested/" + full.RecordID;

          

          var HTML =`	  
      
        <a href="javascript::void(0)" onclick="deleteNoify(${full.RecordID})"  class="btn btn-sm btn-clean btn-icon" title="Delete">\
          <i class="la la-trash"></i>
        </a>
        <a href="javascript::void(0)" onclick="sendNoify(${full.RecordID})"  class="btn btn-sm-primary btn-clean btn-icon" title="Delete">\
          <i class="la la-send"></i>
        </a>`
      
       


       
          return HTML;
        },
      },
      {
        targets: 6,
        title: 'Status',
        orderable: false,
        render: function(data, type, full, meta) {
          if(full.is_send==1){
            return 'Pending';
          }else{
            return 'Completed';
          } 
        },
      },

    ],
  });

  var filter = function () {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    table
      .column($(this).data("col-index"))
      .search(val ? val : "", false, false)
      .draw();
  };

  var asdasd = function (value, index) {
    var val = $.fn.dataTable.util.escapeRegex(value);
    table.column(index).search(val ? val : "", false, true);
  };

  $("#kt_search").on("click", function (e) {
    e.preventDefault();
    var params = {};
    $(".datatable-input").each(function () {
      var i = $(this).data("col-index");
      if (params[i]) {
        params[i] += "|" + $(this).val();
      } else {
        params[i] = $(this).val();
      }
    });
    $.each(params, function (i, val) {
      // apply search params to datatable
      table.column(i).search(val ? val : "", false, false);
    });
    table.table().draw();
  });

  $("#kt_reset").on("click", function (e) {
    e.preventDefault();
    $(".datatable-input").each(function () {
      $(this).val("");
      table.column($(this).data("col-index")).search("", false, false);
    });
    table.table().draw();
  });

  $("#kt_datepicker").datepicker({
    todayHighlight: true,
    templates: {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>',
    },
  });
};
//kt_datatable_notifyList

  var initTable22 = function () {
    // begin first table
    var schoolListFrom = $("#schoolListFrom").val();

    var table = $("#kt_datatable_schoolListREQ").DataTable({
      responsive: true,
      // Pagination settings
      dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
      // read more: https://datatables.net/examples/basic_init/dom.html

      lengthMenu: [5, 10, 25, 50],

      pageLength: 10,

      language: {
        lengthMenu: "Display _MENU_",
      },

      searchDelay: 500,
      processing: true,
      serverSide: true,
      ajax: {
        url: BASE_URL + "/getDatatableSchoolListData",
        type: "GET",
        data: {
          _token: $('meta[name="csrf-token"]').attr("content"),
          schoolListFrom: schoolListFrom,

          // parameters for custom backend script demo
          columnsDef: [
            "RecordID",
            "is_approved",
            "school_title",
            "IndexID",
            "rating",
            "city",
            "email",
            "status",
            "profile_status",
            "Actions",
          ],
        },
      },
      columns: [
        { data: "RecordID" },
        { data: "IndexID" },
        { data: "school_title" },
        { data: "city" },
        { data: "email" },
        { data: "is_approved" },
        { data: "Actions", responsivePriority: -1 },
      ],

      initComplete: function () {
        this.api()
          .columns()
          .every(function () {
            var column = this;

            switch (column.title()) {
              case "Country":
                column
                  .data()
                  .unique()
                  .sort()
                  .each(function (d, j) {
                    $('.datatable-input[data-col-index="2"]').append(
                      '<option value="' + d + '">' + d + "</option>"
                    );
                  });
                break;

              case "Status":
                var status = {
                  1: { title: "Pending", class: "label-light-primary" },
                  2: { title: "Delivered", class: " label-light-danger" },
                  3: { title: "Canceled", class: " label-light-primary" },
                  4: { title: "Success", class: " label-light-success" },
                  5: { title: "Info", class: " label-light-info" },
                  6: { title: "Danger", class: " label-light-danger" },
                  7: { title: "Warning", class: " label-light-warning" },
                };
                column
                  .data()
                  .unique()
                  .sort()
                  .each(function (d, j) {
                    $('.datatable-input[data-col-index="6"]').append(
                      '<option value="' +
                        d +
                        '">' +
                        status[d].title +
                        "</option>"
                    );
                  });
                break;

              case "Type":
                var status = {
                  1: { title: "Online", state: "danger" },
                  2: { title: "Retail", state: "primary" },
                  3: { title: "Direct", state: "success" },
                };
                column
                  .data()
                  .unique()
                  .sort()
                  .each(function (d, j) {
                    $('.datatable-input[data-col-index="7"]').append(
                      '<option value="' +
                        d +
                        '">' +
                        status[d].title +
                        "</option>"
                    );
                  });
                break;
            }
          });
      },

      columnDefs: [
        {
          targets: [0],
          visible: !1,
        },
        {
          targets: -1,

          title: "Actions",
          orderable: false,
          render: function (data, type, full, meta) {
            var EDIT_URL = BASE_URL + "/edit-school/" + full.RecordID;
            var VIEW_URL = BASE_URL + "/view-school-requested/" + full.RecordID;

            

            var HTML =`<a href="${VIEW_URL}" class="btn btn-sm btn-clean btn-icon" title="View Details">\
							<i class="la la-eye"></i>
						</a>						  
				
					<a href="javascript::void(0)" onclick="deleteSchool(${full.RecordID})"  class="btn btn-sm btn-clean btn-icon" title="Delete">\
						<i class="la la-trash"></i>
					</a>`;
        
          if(full.is_approved!=2){
           
          }
          if(full.is_approved===0){
          
    
    
          HTML +=`<a style="margin:1px" class="btn btn-sm btn-icon btn-bg-light btn-icon" href="javascript:void(0)" onclick="schoolApprovalAction(1,${full.RecordID})" >
          
          <i class="icon-1x text-green-50 flaticon2-check-mark" style="color:green"></i>

      </a>`;

      HTML +=`<a style="margin:1px" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary" href="javascript:void(0)" onclick="schoolApprovalAction(2,${full.RecordID})" >
      <i class="icon-1x text-red-50 flaticon2-cancel-music" style="color:red"></i>
           </a>`;

          }else{

          }
         
         

 
         
						return HTML;
          },
        },

        {
          targets: 5,
          width: 50,
          title: "Approval Status",
          orderable: false,
          render: function (a, t, e, n) {
            var i = {
              0: {
                title: "Pending",
                class: "primary",
              },
              1: {
                title: "Approved",
                class: "primary",
              },
              2: {
                title: "Rejected",
                class: "danger",
              },
            };
            //return void 0 === i[a] ? a : '<span class="m-badge ' + i[a].class + ' m-badge--wide">' + i[a].title + "</span>"
            return (
              '<span class="font-weight-bold text-' +
              i[a].class +
              '">' +
              i[a].title +
              "</span>"
            );
          },
        },
      ],
    });

    var filter = function () {
      var val = $.fn.dataTable.util.escapeRegex($(this).val());
      table
        .column($(this).data("col-index"))
        .search(val ? val : "", false, false)
        .draw();
    };

    var asdasd = function (value, index) {
      var val = $.fn.dataTable.util.escapeRegex(value);
      table.column(index).search(val ? val : "", false, true);
    };

    $("#kt_search").on("click", function (e) {
      e.preventDefault();
      var params = {};
      $(".datatable-input").each(function () {
        var i = $(this).data("col-index");
        if (params[i]) {
          params[i] += "|" + $(this).val();
        } else {
          params[i] = $(this).val();
        }
      });
      $.each(params, function (i, val) {
        // apply search params to datatable
        table.column(i).search(val ? val : "", false, false);
      });
      table.table().draw();
    });

    $("#kt_reset").on("click", function (e) {
      e.preventDefault();
      $(".datatable-input").each(function () {
        $(this).val("");
        table.column($(this).data("col-index")).search("", false, false);
      });
      table.table().draw();
    });

    $("#kt_datepicker").datepicker({
      todayHighlight: true,
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>',
      },
    });
  };
//
//kt_datatable_schoolCertificate
var initTableA2 = function() {
  // begin first table
 
  var table = $('#kt_datatable_schoolPerformance').DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      'lengthMenu': 'Display _MENU_',
    },
    
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + '/getSchoolPerformance',
      type: 'GET',
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          'RecordID', 'IndexID', 'school_title', 'avg_rating', 'rating',
          'country_name','Actions',],
      },
    },
    columns: [
      {data: 'RecordID'},
      {data: 'IndexID'},
      {data: 'school_title'},
      {data: 'rating'},
      {data: 'avg_rating'},
      {data: 'country_name'},  
      {data: 'Actions', responsivePriority: -1},
    ],

   

    columnDefs: [
      {
        targets: [0],
        visible: !1,
      },
      {
        targets: -1,
        title: 'Actions',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-school-ratings/"+full.RecordID;
          return `
            
            <a href="${ViewSchooRating}" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
              <i class="la la-eye"></i>
            </a>
            
          `;
        },
      },
      {
        targets: 2,
        title: 'School',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-school-ratings/"+full.RecordID;
          return `
            
            <a href="${ViewSchooRating}" class="" title="">\
             ${full.school_title}
            </a>
            
          `;
        },
      },
      {
        targets: 4,
        width: 175,
        render: function (data, type, full, meta) {
          var strRating = "";
          switch (parseFloat(data)) {
            case 1:
              strRating = `
                              <i class="icon-xl la la-star text-warning" ></i>
                              <i class="icon-xl la la-star " ></i>                                
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star" ></i>
                             
                             
                              `;
              break;
            case 1.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>                             
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star" ></i>
                                 
                                 
                                  `;
              break;
            case 2:
              strRating = `
                                      <i class="icon-xl la la-star text-warning" ></i>
                                      <i class="icon-xl la la-star text-warning" ></i>                        
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star" ></i>
                                     
                                     
                                      `;
              break;
            case 2.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 3:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;

            case 3.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 4:
              strRating = `
                                              <i class="icon-xl la la-star text-warning" ></i>
                                              <i class="icon-xl la la-star text-warning" ></i>                        
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star" ></i>
                                             
                                             
                                              `;
              break;
            case 4.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>
                                 
                                  `;
              break;
            case 5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>        
                                 
                                  `;
              break;

            default:
              strRating = `NA`;
              break;
          }
          return "(" + data + "/5)<br>" + strRating;
        },
      },
     
    ],
  });

  var filter = function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    table.column($(this).data('col-index')).search(val ? val : '', false, false).draw();
  };

  var asdasd = function(value, index) {
    var val = $.fn.dataTable.util.escapeRegex(value);
    table.column(index).search(val ? val : '', false, true);
  };

  $('#kt_search').on('click', function(e) {
    e.preventDefault();
    var params = {};
    $('.datatable-input').each(function() {
      var i = $(this).data('col-index');
      if (params[i]) {
        params[i] += '|' + $(this).val();
      }
      else {
        params[i] = $(this).val();
      }
    });
    $.each(params, function(i, val) {
      // apply search params to datatable
      table.column(i).search(val ? val : '', false, false);
    });
    table.table().draw();
  });

  $('#kt_reset').on('click', function(e) {
    e.preventDefault();
    $('.datatable-input').each(function() {
      $(this).val('');
      table.column($(this).data('col-index')).search('', false, false);
    });
    table.table().draw();
  });

  $('#kt_datepicker').datepicker({
    todayHighlight: true,
    templates: {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>',
    },
  });

};
var initTableA2_My = function() {
  // begin first table
 
  var table = $('#kt_datatable_schoolPerformanceMy').DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      'lengthMenu': 'Display _MENU_',
    },
    
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + '/getSchoolPerformanceMy',
      type: 'GET',
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          'RecordID', 'IndexID', 'school_title', 'avg_rating', 'rating',
          'Actions',],
      },
    },
    columns: [
      {data: 'RecordID'},
      {data: 'IndexID'},
      {data: 'school_title'},
      {data: 'rating'},
      {data: 'avg_rating'},
       
      {data: 'Actions', responsivePriority: -1},
    ],

   

    columnDefs: [
      {
        targets: [0],
        visible: !1,
      },
      {
        targets: -1,
        title: 'Actions',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-my-school-ratings/"+full.RecordID;
          return `
            
            <a href="${ViewSchooRating}" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
              <i class="la la-eye"></i>
            </a>
            
          `;
        },
      },
      {
        targets: 2,
        title: 'School',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-my-school-ratings/"+full.RecordID;
          return `
            
            <a href="${ViewSchooRating}" class="" title="">\
             ${full.school_title}
            </a>
            
          `;
        },
      },
      {
        targets: 4,
        width: 175,
        render: function (data, type, full, meta) {
          var strRating = "";
          switch (parseFloat(data)) {
            case 1:
              strRating = `
                              <i class="icon-xl la la-star text-warning" ></i>
                              <i class="icon-xl la la-star " ></i>                                
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star" ></i>
                             
                             
                              `;
              break;
            case 1.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>                             
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star" ></i>
                                 
                                 
                                  `;
              break;
            case 2:
              strRating = `
                                      <i class="icon-xl la la-star text-warning" ></i>
                                      <i class="icon-xl la la-star text-warning" ></i>                        
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star" ></i>
                                     
                                     
                                      `;
              break;
            case 2.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 3:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;

            case 3.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 4:
              strRating = `
                                              <i class="icon-xl la la-star text-warning" ></i>
                                              <i class="icon-xl la la-star text-warning" ></i>                        
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star" ></i>
                                             
                                             
                                              `;
              break;
            case 4.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>
                                 
                                  `;
              break;
            case 5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>        
                                 
                                  `;
              break;

            default:
              strRating = `NA`;
              break;
          }
          return "(" + data + "/5)<br>" + strRating;
        },
      },
     
    ],
  });

  var filter = function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    table.column($(this).data('col-index')).search(val ? val : '', false, false).draw();
  };

  var asdasd = function(value, index) {
    var val = $.fn.dataTable.util.escapeRegex(value);
    table.column(index).search(val ? val : '', false, true);
  };

  $('#kt_search').on('click', function(e) {
    e.preventDefault();
    var params = {};
    $('.datatable-input').each(function() {
      var i = $(this).data('col-index');
      if (params[i]) {
        params[i] += '|' + $(this).val();
      }
      else {
        params[i] = $(this).val();
      }
    });
    $.each(params, function(i, val) {
      // apply search params to datatable
      table.column(i).search(val ? val : '', false, false);
    });
    table.table().draw();
  });

  $('#kt_reset').on('click', function(e) {
    e.preventDefault();
    $('.datatable-input').each(function() {
      $(this).val('');
      table.column($(this).data('col-index')).search('', false, false);
    });
    table.table().draw();
  });

  $('#kt_datepicker').datepicker({
    todayHighlight: true,
    templates: {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>',
    },
  });

};

var initTableA3 = function() {
  // begin first table
 var sid=$('#txtSID').val();
  var table = $('#kt_datatable_schoolRatingComment').DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      'lengthMenu': 'Display _MENU_',
    },
    
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + '/getSchoolRatingComments',
      type: 'GET',
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          'RecordID', 'IndexID', 'user_id','school_title', 'rating', 'user_name','created_at','comment',
          'user_pic','Actions',],
          sid:sid
      },
    },
    columns: [
      {data: 'RecordID'},
      {data: 'IndexID'},    
      {data: 'user_pic'},
      {data: 'rating'},
      {data: 'comment'},  
      {data: 'created_at'},  
      {data: 'Actions', responsivePriority: -1},
    ],

   

    columnDefs: [
      {
        targets: [0,-1],
        visible: !1,
      },
      {
        targets: -1,
        title: 'Actions',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-school-ratings/"+full.RecordID;
          return `
            
            
            
          `;
        },
      },
      {
        targets: 2,
        width: 150,
        title: "Photo",
        orderable: false,
        render: function (a, t, e, n) {
         var  userLINK=BASE_URL+"/view-user/"+e.user_id;
          return `<a href="${userLINK}"><div class="symbol symbol-circle symbol-lg-25">
          <img src="${e.user_pic}" alt="image">
        </div>  <span style="margin-top: 2px;
        position: absolute;
        margin-left: 8px;
        text-transform: capitalize;">${e.user_name} </span></a>`;
        },
      },
      {
        targets: 3,
        width: 175,
        render: function (data, type, full, meta) {
          var strRating = "";
          switch (parseFloat(data)) {
            case 1:
              strRating = `
                              <i class="icon-xl la la-star text-warning" ></i>
                              <i class="icon-xl la la-star " ></i>                                
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star" ></i>
                             
                             
                              `;
              break;
            case 1.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>                             
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star" ></i>
                                 
                                 
                                  `;
              break;
            case 2:
              strRating = `
                                      <i class="icon-xl la la-star text-warning" ></i>
                                      <i class="icon-xl la la-star text-warning" ></i>                        
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star" ></i>
                                     
                                     
                                      `;
              break;
            case 2.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 3:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;

            case 3.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 4:
              strRating = `
                                              <i class="icon-xl la la-star text-warning" ></i>
                                              <i class="icon-xl la la-star text-warning" ></i>                        
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star" ></i>
                                             
                                             
                                              `;
              break;
            case 4.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>
                                 
                                  `;
              break;
            case 5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>        
                                 
                                  `;
              break;

            default:
              strRating = `NA`;
              break;
          }
          return "(" + data + "/5)<br>" + strRating;
        },
      },
      {
        targets: 4,
        width: 400,
        title: "Comments",
        orderable: false,
        render: function (a, t, e, n) {
          return e.comment;
        }
      }
    ],
  });

  var filter = function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    table.column($(this).data('col-index')).search(val ? val : '', false, false).draw();
  };

  var asdasd = function(value, index) {
    var val = $.fn.dataTable.util.escapeRegex(value);
    table.column(index).search(val ? val : '', false, true);
  };

  $('#kt_search').on('click', function(e) {
    e.preventDefault();
    var params = {};
    $('.datatable-input').each(function() {
      var i = $(this).data('col-index');
      if (params[i]) {
        params[i] += '|' + $(this).val();
      }
      else {
        params[i] = $(this).val();
      }
    });
    $.each(params, function(i, val) {
      // apply search params to datatable
      table.column(i).search(val ? val : '', false, false);
    });
    table.table().draw();
  });

  $('#kt_reset').on('click', function(e) {
    e.preventDefault();
    $('.datatable-input').each(function() {
      $(this).val('');
      table.column($(this).data('col-index')).search('', false, false);
    });
    table.table().draw();
  });

  $('#kt_datepicker').datepicker({
    todayHighlight: true,
    templates: {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>',
    },
  });

};

var initTableA3_My = function() {
  // begin first table
 var sid=$('#txtSID').val();
  var table = $('#kt_datatable_schoolRatingCommentMy').DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      'lengthMenu': 'Display _MENU_',
    },
    
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + '/getSchoolRatingCommentsMy',
      type: 'GET',
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          'RecordID', 'IndexID', 'user_id','school_title', 'rating', 'user_name','created_at','comment',
          'user_pic','Actions',],
          sid:sid
      },
    },
    columns: [
      {data: 'RecordID'},
      {data: 'IndexID'},    
      {data: 'user_pic'},
      {data: 'rating'},
      {data: 'comment'},  
      {data: 'created_at'},  
      {data: 'Actions', responsivePriority: -1},
    ],

   

    columnDefs: [
      {
        targets: [0,-1],
        visible: !1,
      },
      {
        targets: -1,
        title: 'Actions',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-school-ratings/"+full.RecordID;
          return `
            
            
            
          `;
        },
      },
      {
        targets: 2,
        width: 150,
        title: "Photo",
        orderable: false,
        render: function (a, t, e, n) {
         var  userLINK=BASE_URL+"/view-user-details/"+e.user_id;
          return `<a href="${userLINK}"><div class="symbol symbol-circle symbol-lg-25">
          <img src="${e.user_pic}" alt="image">
        </div>  <span style="margin-top: 2px;
        position: absolute;
        margin-left: 8px;
        text-transform: capitalize;">${e.user_name} </span></a>`;
        },
      },
      {
        targets: 3,
        width: 175,
        render: function (data, type, full, meta) {
          var strRating = "";
          switch (parseFloat(data)) {
            case 1:
              strRating = `
                              <i class="icon-xl la la-star text-warning" ></i>
                              <i class="icon-xl la la-star " ></i>                                
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star" ></i>
                             
                             
                              `;
              break;
            case 1.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>                             
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star" ></i>
                                 
                                 
                                  `;
              break;
            case 2:
              strRating = `
                                      <i class="icon-xl la la-star text-warning" ></i>
                                      <i class="icon-xl la la-star text-warning" ></i>                        
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star" ></i>
                                     
                                     
                                      `;
              break;
            case 2.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 3:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;

            case 3.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 4:
              strRating = `
                                              <i class="icon-xl la la-star text-warning" ></i>
                                              <i class="icon-xl la la-star text-warning" ></i>                        
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star" ></i>
                                             
                                             
                                              `;
              break;
            case 4.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>
                                 
                                  `;
              break;
            case 5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>        
                                 
                                  `;
              break;

            default:
              strRating = `NA`;
              break;
          }
          return "(" + data + "/5)<br>" + strRating;
        },
      },
      {
        targets: 4,
        width: 400,
        title: "Comments",
        orderable: false,
        render: function (a, t, e, n) {
          return e.comment;
        }
      }
    ],
  });

  var filter = function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    table.column($(this).data('col-index')).search(val ? val : '', false, false).draw();
  };

  var asdasd = function(value, index) {
    var val = $.fn.dataTable.util.escapeRegex(value);
    table.column(index).search(val ? val : '', false, true);
  };

  $('#kt_search').on('click', function(e) {
    e.preventDefault();
    var params = {};
    $('.datatable-input').each(function() {
      var i = $(this).data('col-index');
      if (params[i]) {
        params[i] += '|' + $(this).val();
      }
      else {
        params[i] = $(this).val();
      }
    });
    $.each(params, function(i, val) {
      // apply search params to datatable
      table.column(i).search(val ? val : '', false, false);
    });
    table.table().draw();
  });

  $('#kt_reset').on('click', function(e) {
    e.preventDefault();
    $('.datatable-input').each(function() {
      $(this).val('');
      table.column($(this).data('col-index')).search('', false, false);
    });
    table.table().draw();
  });

  $('#kt_datepicker').datepicker({
    todayHighlight: true,
    templates: {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>',
    },
  });

};
var initTableA3_My_course = function() {
  // begin first table
 var sid=$('#txtSID').val();
  var table = $('#kt_datatable_schoolRatingCommentMyCourse').DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      'lengthMenu': 'Display _MENU_',
    },
    
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + '/getSchoolRatingCommentsMyCourse',
      type: 'GET',
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          'RecordID', 'IndexID', 'user_id','school_title', 'rating', 'user_name','created_at','comment',
          'user_pic','Actions',],
          sid:sid
      },
    },
    columns: [
      {data: 'RecordID'},
      {data: 'IndexID'},    
      {data: 'user_pic'},
      {data: 'rating'},
      {data: 'comment'},  
      {data: 'created_at'},  
      {data: 'Actions', responsivePriority: -1},
    ],

   

    columnDefs: [
      {
        targets: [0,-1],
        visible: !1,
      },
      {
        targets: -1,
        title: 'Actions',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-school-ratings/"+full.RecordID;
          return `
            
            
            
          `;
        },
      },
      {
        targets: 2,
        width: 150,
        title: "Photo",
        orderable: false,
        render: function (a, t, e, n) {
         var  userLINK=BASE_URL+"/view-user-details/"+e.user_id;
          return `<a href="${userLINK}"><div class="symbol symbol-circle symbol-lg-25">
          <img src="${e.user_pic}" alt="image">
        </div>  <span style="margin-top: 2px;
        position: absolute;
        margin-left: 8px;
        text-transform: capitalize;">${e.user_name} </span></a>`;
        },
      },
      {
        targets: 3,
        width: 175,
        render: function (data, type, full, meta) {
          var strRating = "";
          switch (parseFloat(data)) {
            case 1:
              strRating = `
                              <i class="icon-xl la la-star text-warning" ></i>
                              <i class="icon-xl la la-star " ></i>                                
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star" ></i>
                             
                             
                              `;
              break;
            case 1.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>                             
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star" ></i>
                                 
                                 
                                  `;
              break;
            case 2:
              strRating = `
                                      <i class="icon-xl la la-star text-warning" ></i>
                                      <i class="icon-xl la la-star text-warning" ></i>                        
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star" ></i>
                                     
                                     
                                      `;
              break;
            case 2.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 3:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;

            case 3.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 4:
              strRating = `
                                              <i class="icon-xl la la-star text-warning" ></i>
                                              <i class="icon-xl la la-star text-warning" ></i>                        
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star" ></i>
                                             
                                             
                                              `;
              break;
            case 4.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>
                                 
                                  `;
              break;
            case 5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>        
                                 
                                  `;
              break;

            default:
              strRating = `NA`;
              break;
          }
          return "(" + data + "/5)<br>" + strRating;
        },
      },
      {
        targets: 4,
        width: 400,
        title: "Comments",
        orderable: false,
        render: function (a, t, e, n) {
          return e.comment;
        }
      }
    ],
  });

  var filter = function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    table.column($(this).data('col-index')).search(val ? val : '', false, false).draw();
  };

  var asdasd = function(value, index) {
    var val = $.fn.dataTable.util.escapeRegex(value);
    table.column(index).search(val ? val : '', false, true);
  };

  $('#kt_search').on('click', function(e) {
    e.preventDefault();
    var params = {};
    $('.datatable-input').each(function() {
      var i = $(this).data('col-index');
      if (params[i]) {
        params[i] += '|' + $(this).val();
      }
      else {
        params[i] = $(this).val();
      }
    });
    $.each(params, function(i, val) {
      // apply search params to datatable
      table.column(i).search(val ? val : '', false, false);
    });
    table.table().draw();
  });

  $('#kt_reset').on('click', function(e) {
    e.preventDefault();
    $('.datatable-input').each(function() {
      $(this).val('');
      table.column($(this).data('col-index')).search('', false, false);
    });
    table.table().draw();
  });

  $('#kt_datepicker').datepicker({
    todayHighlight: true,
    templates: {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>',
    },
  });

};




//
var initTableA11 = function() {
  // begin first table
 var sid=$('#sid').val();
  var table = $('#kt_datatable_schoolEntrolledPayment').DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      'lengthMenu': 'Display _MENU_',
    },
    
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + '/getSchoolEntrollPayment',
      type: 'GET',
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          'RecordID', 'IndexID','user_id', 'user_pic', 'name', 'course_name',
          'course_date','payement_status','Actions'],
          sid:sid
      },
    },
    columns: [
      {data: 'RecordID'},
      {data: 'IndexID'},
      {data: 'user_pic'},     
      {data: 'course_name'},
      {data: 'course_date'},     
      {data: 'payement_status'}, 
     
      {data: 'Actions', responsivePriority: -1},
    ],

    initComplete: function() {
      this.api().columns().every(function() {
        var column = this;

        switch (column.title()) {
          case 'Country':
            column.data().unique().sort().each(function(d, j) {
              $('.datatable-input[data-col-index="2"]').append('<option value="' + d + '">' + d + '</option>');
            });
            break;

          case 'Status':
            var status = {
              1: {'title': 'Pending', 'class': 'label-light-primary'},
              2: {'title': 'Delivered', 'class': ' label-light-danger'},
              3: {'title': 'Canceled', 'class': ' label-light-primary'},
              4: {'title': 'Success', 'class': ' label-light-success'},
              5: {'title': 'Info', 'class': ' label-light-info'},
              6: {'title': 'Danger', 'class': ' label-light-danger'},
              7: {'title': 'Warning', 'class': ' label-light-warning'},
            };
            column.data().unique().sort().each(function(d, j) {
              $('.datatable-input[data-col-index="6"]').append('<option value="' + d + '">' + status[d].title + '</option>');
            });
            break;

          case 'Type':
            var status = {
              1: {'title': 'Online', 'state': 'danger'},
              2: {'title': 'Retail', 'state': 'primary'},
              3: {'title': 'Direct', 'state': 'success'},
            };
            column.data().unique().sort().each(function(d, j) {
              $('.datatable-input[data-col-index="7"]').append('<option value="' + d + '">' + status[d].title + '</option>');
            });
            break;
        }
      });
    },

    columnDefs: [
      {
        targets: [0,-1],
        visible: !1,
      },
      {
        targets: -1,
        title: 'Actions',
        orderable: false,
        render: function(data, type, full, meta) {
          var schoolCerDetail=BASE_URL+"/getEnrollSchool/"+full.RecordID;
          return `
            
            <a href="${schoolCerDetail}" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
              <i class="la la-eye"></i>
            </a>
            
          `;
        },
      },
      {
        targets: 2,
        width: 280,
        title: "User",
        orderable: false,
        render: function (a, t, e, n) {
          var  userLINK=BASE_URL+"/view-user/"+e.user_id;
           return `<a href="${userLINK}"><div class="symbol symbol-circle symbol-lg-25">
           <img src="${e.user_pic}" alt="image">
         </div>  <span style="margin-top: 2px;
         position: absolute;
         margin-left: 8px;
         text-transform: capitalize;">${e.name} </span></a>`;
         },
      },
     
    ],
  });

  var filter = function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    table.column($(this).data('col-index')).search(val ? val : '', false, false).draw();
  };

  var asdasd = function(value, index) {
    var val = $.fn.dataTable.util.escapeRegex(value);
    table.column(index).search(val ? val : '', false, true);
  };

  $('#kt_search').on('click', function(e) {
    e.preventDefault();
    var params = {};
    $('.datatable-input').each(function() {
      var i = $(this).data('col-index');
      if (params[i]) {
        params[i] += '|' + $(this).val();
      }
      else {
        params[i] = $(this).val();
      }
    });
    $.each(params, function(i, val) {
      // apply search params to datatable
      table.column(i).search(val ? val : '', false, false);
    });
    table.table().draw();
  });

  $('#kt_reset').on('click', function(e) {
    e.preventDefault();
    $('.datatable-input').each(function() {
      $(this).val('');
      table.column($(this).data('col-index')).search('', false, false);
    });
    table.table().draw();
  });

  $('#kt_datepicker').datepicker({
    todayHighlight: true,
    templates: {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>',
    },
  });

};

var initTableA1 = function() {
  // begin first table
 
  $('#txtTS').html("<b style='color:#16426B'> 0</b>");


  var table = $('#kt_datatable_schoolCertificate').DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      'lengthMenu': 'Display _MENU_',
    },
    
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + '/getSchoolCertificates',
      type: 'GET',
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          'RecordID', 'IndexID', 'stcount','school_title', 'certificate_title', 'total_student',
          'course_date','Actions',],
      },
    },
    columns: [
      {data: 'RecordID'},
      {data: 'IndexID'},
      {data: 'school_title'},
      {data: 'certificate_title'},
      {data: 'total_student'},
      {data: 'course_date'},     
     
      {data: 'Actions', responsivePriority: -1},
    ],

    initComplete: function() {
      this.api().columns().every(function() {
        var column = this;

        switch (column.title()) {
          case 'Country':
            column.data().unique().sort().each(function(d, j) {
              $('.datatable-input[data-col-index="2"]').append('<option value="' + d + '">' + d + '</option>');
            });
            break;

          case 'Status':
            var status = {
              1: {'title': 'Pending', 'class': 'label-light-primary'},
              2: {'title': 'Delivered', 'class': ' label-light-danger'},
              3: {'title': 'Canceled', 'class': ' label-light-primary'},
              4: {'title': 'Success', 'class': ' label-light-success'},
              5: {'title': 'Info', 'class': ' label-light-info'},
              6: {'title': 'Danger', 'class': ' label-light-danger'},
              7: {'title': 'Warning', 'class': ' label-light-warning'},
            };
            column.data().unique().sort().each(function(d, j) {
              $('.datatable-input[data-col-index="6"]').append('<option value="' + d + '">' + status[d].title + '</option>');
            });
            break;

          case 'Type':
            var status = {
              1: {'title': 'Online', 'state': 'danger'},
              2: {'title': 'Retail', 'state': 'primary'},
              3: {'title': 'Direct', 'state': 'success'},
            };
            column.data().unique().sort().each(function(d, j) {
              $('.datatable-input[data-col-index="7"]').append('<option value="' + d + '">' + status[d].title + '</option>');
            });
            break;
        }
      });
    },

    columnDefs: [
      {
        targets: [0],
        visible: !1,
      },
      {
        targets: -1,
        title: 'Actions',
        orderable: false,
        render: function(data, type, full, meta) {
          $('#txtTS').html("<b style='color:#16426B'> "+full.stcount+"</b>");

          var schoolCerDetail=BASE_URL+"/getEnrollSchool/"+full.RecordID;
          return `
            
            <a href="${schoolCerDetail}" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
              <i class="la la-eye"></i>
            </a>
            
          `;
        },
      },
      {
        targets: 3,
        title: 'Certificate4',
        orderable: false,
        render: function(data, type, full, meta) {
         return `<a href="javascript:void(0)" onclick="showCertiData(${full.RecordID})"  >${full.certificate_title}</a>`
        },
      },
     
    ],
  });

  var filter = function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    table.column($(this).data('col-index')).search(val ? val : '', false, false).draw();
  };

  var asdasd = function(value, index) {
    var val = $.fn.dataTable.util.escapeRegex(value);
    table.column(index).search(val ? val : '', false, true);
  };

  $('#kt_search').on('click', function(e) {
    e.preventDefault();
    var params = {};
    $('.datatable-input').each(function() {
      var i = $(this).data('col-index');
      if (params[i]) {
        params[i] += '|' + $(this).val();
      }
      else {
        params[i] = $(this).val();
      }
    });
    $.each(params, function(i, val) {
      // apply search params to datatable
      table.column(i).search(val ? val : '', false, false);
    });
    table.table().draw();
  });

  $('#kt_reset').on('click', function(e) {
    e.preventDefault();
    $('.datatable-input').each(function() {
      $(this).val('');
      table.column($(this).data('col-index')).search('', false, false);
    });
    table.table().draw();
  });

  $('#kt_datepicker').datepicker({
    todayHighlight: true,
    templates: {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>',
    },
  });

};

  return {
    //main function to initiate the module
    init: function () {
      initTable1();
      initTable1_payment();
      initTable22();
      initTableA1();
      initTableA2();
      initTableA2_My();
      initTableA3();
      initTableA3_My();
      initTableA3_My_course();
      initTableA11();
      initTable22_notify();
      

    },
  };
})();


function showCertiData(rowID){
 
  var formData = {
    rowID: rowID , 
    _token: $('meta[name="csrf-token"]').attr("content"),
  };
  $.ajax({
    url: BASE_URL + "/getCertifyByIDAdmin",
    type: "GET",
    data: formData,
    success: function (res) {
      $('.certifyHis').html(res);
      $('#getCertifyByIDAdminView').modal('show');
    }
  });
}

//btnSearchPaymentFilter
$('#btnSearchPaymentFilter').click(function(){
  var startDate=$('#startDate').val();
  var endDate=$('#endDate').val();
  var sid =$( "#sid" ).val();
  var paymentStatusRadio=$('input[name="paymentStatusRadio"]:checked').val();

  $( "#kt_datatable_schoolEntrolledPayment" ).dataTable().fnDestroy();
  

  var table = $('#kt_datatable_schoolEntrolledPayment').DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      'lengthMenu': 'Display _MENU_',
    },
    
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + '/getSchoolEntrollPaymentFilter',
      type: 'GET',
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          'RecordID', 'IndexID', 'user_id','user_pic', 'name', 'course_name',
          'course_date','payement_status','Actions'],
          sid:sid,
          startDate:startDate,
          endDate:endDate,
          paymentStatusRadio:paymentStatusRadio,
      },
    },
    columns: [
      {data: 'RecordID'},
      {data: 'IndexID'},
      {data: 'user_pic'},     
      {data: 'course_name'},
      {data: 'course_date'},     
      {data: 'payement_status'}, 
     
      {data: 'Actions', responsivePriority: -1},
    ],

    initComplete: function() {
      this.api().columns().every(function() {
        var column = this;

        switch (column.title()) {
          case 'Country':
            column.data().unique().sort().each(function(d, j) {
              $('.datatable-input[data-col-index="2"]').append('<option value="' + d + '">' + d + '</option>');
            });
            break;

          case 'Status':
            var status = {
              1: {'title': 'Pending', 'class': 'label-light-primary'},
              2: {'title': 'Delivered', 'class': ' label-light-danger'},
              3: {'title': 'Canceled', 'class': ' label-light-primary'},
              4: {'title': 'Success', 'class': ' label-light-success'},
              5: {'title': 'Info', 'class': ' label-light-info'},
              6: {'title': 'Danger', 'class': ' label-light-danger'},
              7: {'title': 'Warning', 'class': ' label-light-warning'},
            };
            column.data().unique().sort().each(function(d, j) {
              $('.datatable-input[data-col-index="6"]').append('<option value="' + d + '">' + status[d].title + '</option>');
            });
            break;

          case 'Type':
            var status = {
              1: {'title': 'Online', 'state': 'danger'},
              2: {'title': 'Retail', 'state': 'primary'},
              3: {'title': 'Direct', 'state': 'success'},
            };
            column.data().unique().sort().each(function(d, j) {
              $('.datatable-input[data-col-index="7"]').append('<option value="' + d + '">' + status[d].title + '</option>');
            });
            break;
        }
      });
    },

    columnDefs: [
      {
        targets: [0,-1],
        visible: !1,
      },
      {
        targets: -1,
        title: 'Actions',
        orderable: false,
        render: function(data, type, full, meta) {
          var schoolCerDetail=BASE_URL+"/getEnrollSchool/"+full.RecordID;
          return `
            
            <a href="${schoolCerDetail}" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
              <i class="la la-eye"></i>
            </a>
            
          `;
        },
      },
      {
        targets: 2,
        width: 280,
        title: "User",
        orderable: false,
        render: function (a, t, e, n) {
          var  userLINK=BASE_URL+"/view-user/"+e.user_id;
           return `<a href="${userLINK}"><div class="symbol symbol-circle symbol-lg-25">
           <img src="${e.user_pic}" alt="image">
         </div>  <span style="margin-top: 2px;
         position: absolute;
         margin-left: 8px;
         text-transform: capitalize;">${e.name} </span></a>`;
         },
      },
     
    ],
  });


});
//btnSearchPaymentFilter

//btnEnrolledFilterSuper
$('#btnEnrolledFilterSuper').click(function(){
  var txtSID=$('#txtSID').val();
  var couserID=$( ".schoolCourseAdmin option:selected" ).val();
  var getWeelData=$( ".getWeelData option:selected" ).val();
  var paymentType=$( ".paymentType option:selected" ).val();
  var startDate=$('#startDate').val();
  var endDate=$('#endDate').val();
  $( "#kt_datatable_userListAdminSuper" ).dataTable().fnDestroy();


  var table = $("#kt_datatable_userListAdminSuper").DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      lengthMenu: "Display _MENU_",
    },

    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + "/getDatatableAdminUserEnrolledCouseListFilterSuper",
      type: "GET",
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          "RecordID",
          "IndexID",
          "photo",
          "name",
          "course_name",
          "join_date",
          "payment_status",
          "payment_amount",
          "dueAMT",
          "Actions",
        ],
        couserID: couserID , 
        getWeelData: getWeelData , 
        startDate: startDate , 
        endDate: endDate , 
        txtSID: txtSID , 
        paymentType:paymentType,
        _token: $('meta[name="csrf-token"]').attr("content"),
      },
     
    },
    columns: [
      { data: "RecordID" },
      { data: "IndexID" },
      { data: "photo" },       
      { data: "course_name" },
      { data: "join_date" },
      { data: "payment_status" },
      { data: "payment_amount" },
      { data: "dueAMT" },
      { data: "Actions", responsivePriority: -1 },
    ],

    columnDefs: [
      {
        targets: [0],
        visible: !1,
      },
      {
        targets: -1,
        width: 150,
        title: "Actions",
        orderable: false,
        render: function (data, type, full, meta) {
          var EDIT_URL = BASE_URL + "/edit-user/" + full.RecordID;
          var AddCourseToUSer_URL = BASE_URL + "/add-course-to-user/" + full.RecordID;
          var SettleDueAmout = BASE_URL + "/settle-due-amount/" + full.RecordID;


          var VIEW_URL = BASE_URL + "/admin-view-user/" + full.RecordID;

           var HTML='';
          /*
            <a href="${EDIT_URL}" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
          <i class="la la-edit"></i>\
        </a>\
        <a href="javascript::void(0)" onclick="deleteUser(${full.RecordID})"  class="btn btn-sm btn-clean btn-icon" title="Delete">\
          <i class="la la-trash"></i>\
        </a>\

          /*/
          if(full.dueAMT<=0){
            HTML +=`<a href="javascript:void(0)" class="btn btn-sm btn-success" title="Settled">\
           Settled
          </a>
           
            `;
          }else{
            HTML +=`
            
            `;
          }
          HTML +=`
          <a href="javascript:void(0)" onclick="paymentHistoryAdmin(${full.RecordID})" class="btn btn-sm btn-clean btn-icon" title="View  Payment History">\
          <i class="la la-eye"></i>\
        </a>`;

          return HTML;
         

        },
      },
      {
        targets: 2,
        width: 250,
        title: "User",
        orderable: false,
        render: function (a, t, e, n) {
          var VIEW_URL = BASE_URL + "/admin-view-user/" + e.RecordID;

          return `<div class="symbol symbol-circle symbol-lg-50">
          <img src="${e.photo}" alt="image">
          
        </div><span style="margin-top: 10px;
  position: absolute;
  margin-left: 10px;"><b><a href="${VIEW_URL}">${e.name}</a></b></span>`;
        },
      },
      {
        targets: 5,
        render: function(data, type, full, meta) {
          var status = {
            
            2: {'title': 'Partial', 'class': ' label-light-warning'},           
            1: {'title': 'Full', 'class': ' label-light-success'},           
          };
          if (typeof status[data] === 'undefined') {
            return data;
          }
          return '<span class="label label-lg font-weight-bold' + status[data].class + ' label-inline">' + status[data].title + '</span>';
        },
      },
    ],
  });

  
 
  

});

//btnEnrolledFilterSuper

//btnEnrolledFilter
$('#btnEnrolledFilter').click(function(){
  var couserID=$( ".schoolCourseAdmin option:selected" ).val();
  var getWeelData=$( ".getWeelData option:selected" ).val();
  var paymentType=$( ".paymentType option:selected" ).val();
  var startDate=$('#startDate').val();
  var endDate=$('#endDate').val();
  $( "#kt_datatable_userListAdmin" ).dataTable().fnDestroy();


  var table = $("#kt_datatable_userListAdmin").DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      lengthMenu: "Display _MENU_",
    },

    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + "/getDatatableAdminUserEnrolledCouseListFilter",
      type: "GET",
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          "RecordID",
          "IndexID",
          "photo",
          "name",
          "course_name",
          "join_date",
          "payment_status",
          "payment_amount",
          "dueAMT",
          "Actions",
        ],
        couserID: couserID , 
        getWeelData: getWeelData , 
        startDate: startDate , 
        endDate: endDate , 
        paymentType:paymentType,
        _token: $('meta[name="csrf-token"]').attr("content"),
      },
     
    },
    columns: [
      { data: "RecordID" },
      { data: "IndexID" },
      { data: "photo" },       
      { data: "course_name" },
      { data: "join_date" },
      { data: "payment_status" },
      { data: "payment_amount" },
      { data: "dueAMT" },
      { data: "Actions", responsivePriority: -1 },
    ],

    columnDefs: [
      {
        targets: [0],
        visible: !1,
      },
      {
        targets: -1,
        width: 150,
        title: "Actions",
        orderable: false,
        render: function (data, type, full, meta) {
          var EDIT_URL = BASE_URL + "/edit-user/" + full.RecordID;
          var AddCourseToUSer_URL = BASE_URL + "/add-course-to-user/" + full.RecordID;
          var SettleDueAmout = BASE_URL + "/settle-due-amount/" + full.RecordID;


          var VIEW_URL = BASE_URL + "/admin-view-user/" + full.RecordID;

           var HTML='';
          /*
            <a href="${EDIT_URL}" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
          <i class="la la-edit"></i>\
        </a>\
        <a href="javascript::void(0)" onclick="deleteUser(${full.RecordID})"  class="btn btn-sm btn-clean btn-icon" title="Delete">\
          <i class="la la-trash"></i>\
        </a>\

          /*/
          if(full.dueAMT<=0){
            HTML +=`<a href="javascript:void(0)" class="btn btn-sm btn-success" title="Settled">\
           Settled
          </a>
           
            `;
          }else{
            HTML +=`
            <a href="${SettleDueAmout}" class="btn btn-sm btn-warning" title="Settle">\
            Settle
          </a>
            `;
          }
          HTML +=`
          <a href="javascript:void(0)" onclick="paymentHistory(${full.RecordID})" class="btn btn-sm btn-clean btn-icon" title="View  Payment History">\
          <i class="la la-eye"></i>\
        </a>`;

          return HTML;
         

        },
      },
      {
        targets: 2,
        width: 250,
        title: "User",
        orderable: false,
        render: function (a, t, e, n) {
          var VIEW_URL = BASE_URL + "/admin-view-user/" + e.RecordID;

          return `<div class="symbol symbol-circle symbol-lg-50">
          <img src="${e.photo}" alt="image">
          
        </div><span style="margin-top: 10px;
  position: absolute;
  margin-left: 10px;"><b><a href="${VIEW_URL}">${e.name}</a></b></span>`;
        },
      },
      {
        targets: 5,
        render: function(data, type, full, meta) {
          var status = {
            
            2: {'title': 'Partial', 'class': ' label-light-warning'},           
            1: {'title': 'Full', 'class': ' label-light-success'},           
          };
          if (typeof status[data] === 'undefined') {
            return data;
          }
          return '<span class="label label-lg font-weight-bold' + status[data].class + ' label-inline">' + status[data].title + '</span>';
        },
      },
    ],
  });

  
 
  

});
//btnEnrolledFilter

//function 
//btnDateRenage
$('#btnDateRenage').click(function(){
  
  
  $('#txtTS').html("");


  var startDate=$('#startDate').val();
  var endDate=$('#endDate').val();
  var sid =$( ".myschool option:selected" ).val();
  var course =$( ".myschoolCourse option:selected" ).val();
  var getWeelData =$( ".getWeelData option:selected" ).val();


  $( "#kt_datatable_schoolCertificate" ).dataTable().fnDestroy();

  var table = $('#kt_datatable_schoolCertificate').DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      'lengthMenu': 'Display _MENU_',
    },
    
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + '/getSchoolCertificatesBYFilter',
      type: 'GET',
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          'RecordID', 'IndexID','stcount', 'school_title', 'certificate_title', 'total_student',
          'course_date','Actions',],
          startDate:startDate,
          endDate:endDate,
          sid:sid,
          course:course 

      },
    },
    columns: [
      {data: 'RecordID'},
      {data: 'IndexID'},
      {data: 'school_title'},
      {data: 'certificate_title'},
      {data: 'total_student'},
      {data: 'course_date'},     
     
      {data: 'Actions', responsivePriority: -1},
    ],

    initComplete: function() {
      this.api().columns().every(function() {
        var column = this;

        switch (column.title()) {
          case 'Country':
            column.data().unique().sort().each(function(d, j) {
              $('.datatable-input[data-col-index="2"]').append('<option value="' + d + '">' + d + '</option>');
            });
            break;

          case 'Status':
            var status = {
              1: {'title': 'Pending', 'class': 'label-light-primary'},
              2: {'title': 'Delivered', 'class': ' label-light-danger'},
              3: {'title': 'Canceled', 'class': ' label-light-primary'},
              4: {'title': 'Success', 'class': ' label-light-success'},
              5: {'title': 'Info', 'class': ' label-light-info'},
              6: {'title': 'Danger', 'class': ' label-light-danger'},
              7: {'title': 'Warning', 'class': ' label-light-warning'},
            };
            column.data().unique().sort().each(function(d, j) {
              $('.datatable-input[data-col-index="6"]').append('<option value="' + d + '">' + status[d].title + '</option>');
            });
            break;

          case 'Type':
            var status = {
              1: {'title': 'Online', 'state': 'danger'},
              2: {'title': 'Retail', 'state': 'primary'},
              3: {'title': 'Direct', 'state': 'success'},
            };
            column.data().unique().sort().each(function(d, j) {
              $('.datatable-input[data-col-index="7"]').append('<option value="' + d + '">' + status[d].title + '</option>');
            });
            break;
        }
      });
    },

    columnDefs: [
      {
        targets: [0],
        visible: !1,
      },
      {
        targets: -1,
        title: 'Actions',
        orderable: false,
        render: function(data, type, full, meta) {
         
          $('#txtTS').html("<b style='color:#16426B'> "+full.stcount+"</b>");
         // console.log(full.stcount);
          var schoolCerDetail=BASE_URL+"/getEnrollSchool/"+full.RecordID;
          return `
            
            <a href="${schoolCerDetail}" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
              <i class="la la-eye"></i>
            </a>
            
          `;
        },
      },
      {
        targets: 3,
        title: 'Certificate4',
        orderable: false,
        render: function(data, type, full, meta) {
         return `<a href="javascript:void(0)" onclick="showCertiData(${full.RecordID})"  >${full.certificate_title}</a>`
        },
      },
     
    ],
  });


  


 


  
  
  //start 
 

  //start 
});
//btnDateRenage
//starRadioMy

$("input[name=starRadioMyS]").click(function(){
   //var countryID = $(this).children("option:selected").val(); 
  //var countryID =$('.ajcountry option:selected').val('id');
 var countryID =$( "#myselect option:selected" ).val();


  var starRadioVal=$('input[name="starRadioMyS"]:checked').val();
  var starRadioTOPVal=$('input[name="starRadioTOP"]:checked').val();
    
  $( "#kt_datatable_schoolPerformanceMy" ).dataTable().fnDestroy();
  var table = $('#kt_datatable_schoolPerformanceMy').DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      'lengthMenu': 'Display _MENU_',
    },
    
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + '/getSchoolPerformanceFilterMy',
      type: 'GET',
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          'RecordID', 'IndexID', 'school_title', 'avg_rating', 'rating',
         'Actions',],
          countryID:countryID,
          starRadioVal:starRadioVal,
          starRadioTOPVal:starRadioTOPVal

      },
    },
    columns: [
      {data: 'RecordID'},
      {data: 'IndexID'},
      {data: 'school_title'},
      {data: 'rating'},
      {data: 'avg_rating'},
      
      {data: 'Actions', responsivePriority: -1},
    ],

   

    columnDefs: [
      {
        targets: [0],
        visible: !1,
      },
      {
        targets: -1,
        title: 'Actions',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-my-school-ratings/"+full.RecordID;
          return `
            
            <a href="${ViewSchooRating}" class="" title="">\
              <i class="la la-eye"></i>
            </a>
            
          `;
        },
      },
      {
        targets: 2,
        title: 'School',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-my-school-ratings/"+full.RecordID;
          return `
            
            <a href="${ViewSchooRating}" class="" title="View Details">\
             ${full.school_title}
            </a>
            
          `;
        },
      },
      {
        targets: 4,
        width: 175,
        render: function (data, type, full, meta) {
          var strRating = "";
          switch (parseFloat(data)) {
            case 1:
              strRating = `
                              <i class="icon-xl la la-star text-warning" ></i>
                              <i class="icon-xl la la-star " ></i>                                
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star" ></i>
                             
                             
                              `;
              break;
            case 1.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>                             
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star" ></i>
                                 
                                 
                                  `;
              break;
            case 2:
              strRating = `
                                      <i class="icon-xl la la-star text-warning" ></i>
                                      <i class="icon-xl la la-star text-warning" ></i>                        
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star" ></i>
                                     
                                     
                                      `;
              break;
            case 2.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 3:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;

            case 3.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 4:
              strRating = `
                                              <i class="icon-xl la la-star text-warning" ></i>
                                              <i class="icon-xl la la-star text-warning" ></i>                        
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star" ></i>
                                             
                                             
                                              `;
              break;
            case 4.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>
                                 
                                  `;
              break;
            case 5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>        
                                 
                                  `;
              break;

            default:
              strRating = `NA`;
              break;
          }
          return "(" + data + "/5)<br>" + strRating;
        },
      },
     
    ],
  });


 

  
})

//starRadioMy

//starRadioTOP
$("input[name=starRadioTOP]").click(function(){
 

  //var countryID = $(this).children("option:selected").val(); 
  //var countryID =$('.ajcountry option:selected').val('id');
 var countryID =$( "#myselect option:selected" ).val();


  var starRadioVal=$('input[name="starRadio"]:checked').val();
  var starRadioTOPVal=$('input[name="starRadioTOP"]:checked').val();
    
  $( "#kt_datatable_schoolPerformance" ).dataTable().fnDestroy();
  var table = $('#kt_datatable_schoolPerformance').DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      'lengthMenu': 'Display _MENU_',
    },
    
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + '/getSchoolPerformanceFilterCountry',
      type: 'GET',
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          'RecordID', 'IndexID', 'school_title', 'avg_rating', 'rating',
          'country_name','Actions',],
          countryID:countryID,
          starRadioVal:starRadioVal,
          starRadioTOPVal:starRadioTOPVal

      },
    },
    columns: [
      {data: 'RecordID'},
      {data: 'IndexID'},
      {data: 'school_title'},
      {data: 'rating'},
      {data: 'avg_rating'},
      {data: 'country_name'},  
      {data: 'Actions', responsivePriority: -1},
    ],

   

    columnDefs: [
      {
        targets: [0],
        visible: !1,
      },
      {
        targets: -1,
        title: 'Actions',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-school-ratings/"+full.RecordID;
          return `
            
            <a href="${ViewSchooRating}" class="" title="">\
              <i class="la la-eye"></i>
            </a>
            
          `;
        },
      },
      {
        targets: 2,
        title: 'School',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-school-ratings/"+full.RecordID;
          return `
            
            <a href="${ViewSchooRating}" class="" title="Edit details">\
             ${full.school_title}
            </a>
            
          `;
        },
      },
      {
        targets: 4,
        width: 175,
        render: function (data, type, full, meta) {
          var strRating = "";
          switch (parseFloat(data)) {
            case 1:
              strRating = `
                              <i class="icon-xl la la-star text-warning" ></i>
                              <i class="icon-xl la la-star " ></i>                                
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star" ></i>
                             
                             
                              `;
              break;
            case 1.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>                             
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star" ></i>
                                 
                                 
                                  `;
              break;
            case 2:
              strRating = `
                                      <i class="icon-xl la la-star text-warning" ></i>
                                      <i class="icon-xl la la-star text-warning" ></i>                        
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star" ></i>
                                     
                                     
                                      `;
              break;
            case 2.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 3:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;

            case 3.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 4:
              strRating = `
                                              <i class="icon-xl la la-star text-warning" ></i>
                                              <i class="icon-xl la la-star text-warning" ></i>                        
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star" ></i>
                                             
                                             
                                              `;
              break;
            case 4.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>
                                 
                                  `;
              break;
            case 5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>        
                                 
                                  `;
              break;

            default:
              strRating = `NA`;
              break;
          }
          return "(" + data + "/5)<br>" + strRating;
        },
      },
     
    ],
  });


 

  
})

//starRadioTOP

//ajcountry

$("select.ajcountry").change(function(){
  var countryID = $(this).children("option:selected").val();  
  var starRadioVal=$('input[name="starRadio"]:checked').val();
  var starRadioTOPVal=$('input[name="starRadioTOP"]:checked').val();
    
  $( "#kt_datatable_schoolPerformance" ).dataTable().fnDestroy();
  var table = $('#kt_datatable_schoolPerformance').DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      'lengthMenu': 'Display _MENU_',
    },
    
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + '/getSchoolPerformanceFilterCountry',
      type: 'GET',
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          'RecordID', 'IndexID', 'school_title', 'avg_rating', 'rating',
          'country_name','Actions',],
          countryID:countryID,
          starRadioVal:starRadioVal,
          starRadioTOPVal:starRadioTOPVal

      },
    },
    columns: [
      {data: 'RecordID'},
      {data: 'IndexID'},
      {data: 'school_title'},
      {data: 'rating'},
      {data: 'avg_rating'},
      {data: 'country_name'},  
      {data: 'Actions', responsivePriority: -1},
    ],

   

    columnDefs: [
      {
        targets: [0],
        visible: !1,
      },
      {
        targets: -1,
        title: 'Actions',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-school-ratings/"+full.RecordID;
          return `
            
            <a href="${ViewSchooRating}" class="" title="">\
              <i class="la la-eye"></i>
            </a>
            
          `;
        },
      },
      {
        targets: 2,
        title: 'School',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-school-ratings/"+full.RecordID;
          return `
            
            <a href="${ViewSchooRating}" class="" title="Edit details">\
             ${full.school_title}
            </a>
            
          `;
        },
      },
      {
        targets: 4,
        width: 175,
        render: function (data, type, full, meta) {
          var strRating = "";
          switch (parseFloat(data)) {
            case 1:
              strRating = `
                              <i class="icon-xl la la-star text-warning" ></i>
                              <i class="icon-xl la la-star " ></i>                                
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star" ></i>
                             
                             
                              `;
              break;
            case 1.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>                             
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star" ></i>
                                 
                                 
                                  `;
              break;
            case 2:
              strRating = `
                                      <i class="icon-xl la la-star text-warning" ></i>
                                      <i class="icon-xl la la-star text-warning" ></i>                        
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star" ></i>
                                     
                                     
                                      `;
              break;
            case 2.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 3:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;

            case 3.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 4:
              strRating = `
                                              <i class="icon-xl la la-star text-warning" ></i>
                                              <i class="icon-xl la la-star text-warning" ></i>                        
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star" ></i>
                                             
                                             
                                              `;
              break;
            case 4.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>
                                 
                                  `;
              break;
            case 5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>        
                                 
                                  `;
              break;

            default:
              strRating = `NA`;
              break;
          }
          return "(" + data + "/5)<br>" + strRating;
        },
      },
     
    ],
  });


});
//ajcountry
//starRadioCommentMyCourse
$("input[name=starRadioCommentMyCourse]").click(function(){
  var starRadioVal=$(this).val();
  var sid=$('#txtSID').val();

  $( "#kt_datatable_schoolRatingCommentMyCourse" ).dataTable().fnDestroy()
 
var table = $('#kt_datatable_schoolRatingCommentMyCourse').DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      'lengthMenu': 'Display _MENU_',
    },
    
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + '/getSchoolRatingCommentsBySchoolMyCourse',
      type: 'GET',
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          'RecordID', 'IndexID','user_id', 'school_title', 'rating', 'user_name','created_at','comment',
          'user_pic','Actions',],
          sid:sid,
          starRadioVal:starRadioVal,
      },
    },
    columns: [
      {data: 'RecordID'},
      {data: 'IndexID'},    
      {data: 'user_pic'},
      {data: 'rating'},
      {data: 'comment'},  
      {data: 'created_at'},  
      {data: 'Actions', responsivePriority: -1},
    ],

   

    columnDefs: [
      {
        targets: [0,-1],
        visible: !1,
      },
      {
        targets: -1,
        title: 'Actions',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-school-ratings/"+full.RecordID;
          return `
            
            
            
          `;
        },
      },
      {
        targets: 2,
        width: 150,
        title: "Photo",
        orderable: false,
        render: function (a, t, e, n) {
          var  userLINK=BASE_URL+"/view-user/"+e.user_id;
           return `<a href="${userLINK}"><div class="symbol symbol-circle symbol-lg-25">
           <img src="${e.user_pic}" alt="image">
         </div>  <span style="margin-top: 2px;
         position: absolute;
         margin-left: 8px;
         text-transform: capitalize;">${e.user_name} </span></a>`;
         },
      },
      {
        targets: 3,
        width: 175,
        render: function (data, type, full, meta) {
          var strRating = "";
          switch (parseFloat(data)) {
            case 1:
              strRating = `
                              <i class="icon-xl la la-star text-warning" ></i>
                              <i class="icon-xl la la-star " ></i>                                
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star" ></i>
                             
                             
                              `;
              break;
            case 1.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>                             
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star" ></i>
                                 
                                 
                                  `;
              break;
            case 2:
              strRating = `
                                      <i class="icon-xl la la-star text-warning" ></i>
                                      <i class="icon-xl la la-star text-warning" ></i>                        
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star" ></i>
                                     
                                     
                                      `;
              break;
            case 2.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 3:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;

            case 3.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 4:
              strRating = `
                                              <i class="icon-xl la la-star text-warning" ></i>
                                              <i class="icon-xl la la-star text-warning" ></i>                        
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star" ></i>
                                             
                                             
                                              `;
              break;
            case 4.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>
                                 
                                  `;
              break;
            case 5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>        
                                 
                                  `;
              break;

            default:
              strRating = `NA`;
              break;
          }
          return "(" + data + "/5)<br>" + strRating;
        },
      },
      {
        targets: 4,
        width: 350,
        title: "Comments",
        orderable: false,
        render: function (a, t, e, n) {
          return e.comment;
        }
      }
    ],
  });

  
})

//starRadioCommentMyCourse

//starRadioCommentMy
$("input[name=starRadioCommentMy]").click(function(){
  var starRadioVal=$(this).val();
  var sid=$('#txtSID').val();

  $( "#kt_datatable_schoolRatingCommentMy" ).dataTable().fnDestroy()
 
var table = $('#kt_datatable_schoolRatingCommentMy').DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      'lengthMenu': 'Display _MENU_',
    },
    
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + '/getSchoolRatingCommentsBySchoolMy',
      type: 'GET',
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          'RecordID', 'IndexID','user_id', 'school_title', 'rating', 'user_name','created_at','comment',
          'user_pic','Actions',],
          sid:sid,
          starRadioVal:starRadioVal,
      },
    },
    columns: [
      {data: 'RecordID'},
      {data: 'IndexID'},    
      {data: 'user_pic'},
      {data: 'rating'},
      {data: 'comment'},  
      {data: 'created_at'},  
      {data: 'Actions', responsivePriority: -1},
    ],

   

    columnDefs: [
      {
        targets: [0,-1],
        visible: !1,
      },
      {
        targets: -1,
        title: 'Actions',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-school-ratings/"+full.RecordID;
          return `
            
            
            
          `;
        },
      },
      {
        targets: 2,
        width: 150,
        title: "Photo",
        orderable: false,
        render: function (a, t, e, n) {
          var  userLINK=BASE_URL+"/view-user/"+e.user_id;
           return `<a href="${userLINK}"><div class="symbol symbol-circle symbol-lg-25">
           <img src="${e.user_pic}" alt="image">
         </div>  <span style="margin-top: 2px;
         position: absolute;
         margin-left: 8px;
         text-transform: capitalize;">${e.user_name} </span></a>`;
         },
      },
      {
        targets: 3,
        width: 175,
        render: function (data, type, full, meta) {
          var strRating = "";
          switch (parseFloat(data)) {
            case 1:
              strRating = `
                              <i class="icon-xl la la-star text-warning" ></i>
                              <i class="icon-xl la la-star " ></i>                                
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star" ></i>
                             
                             
                              `;
              break;
            case 1.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>                             
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star" ></i>
                                 
                                 
                                  `;
              break;
            case 2:
              strRating = `
                                      <i class="icon-xl la la-star text-warning" ></i>
                                      <i class="icon-xl la la-star text-warning" ></i>                        
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star" ></i>
                                     
                                     
                                      `;
              break;
            case 2.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 3:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;

            case 3.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 4:
              strRating = `
                                              <i class="icon-xl la la-star text-warning" ></i>
                                              <i class="icon-xl la la-star text-warning" ></i>                        
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star" ></i>
                                             
                                             
                                              `;
              break;
            case 4.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>
                                 
                                  `;
              break;
            case 5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>        
                                 
                                  `;
              break;

            default:
              strRating = `NA`;
              break;
          }
          return "(" + data + "/5)<br>" + strRating;
        },
      },
      {
        targets: 4,
        width: 350,
        title: "Comments",
        orderable: false,
        render: function (a, t, e, n) {
          return e.comment;
        }
      }
    ],
  });

  
})

//starRadioCommentMy

//starRadioComment
$("input[name=starRadioComment]").click(function(){
  var starRadioVal=$(this).val();
  var sid=$('#txtSID').val();

  $( "#kt_datatable_schoolRatingComment" ).dataTable().fnDestroy()
 
var table = $('#kt_datatable_schoolRatingComment').DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      'lengthMenu': 'Display _MENU_',
    },
    
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + '/getSchoolRatingCommentsBySchool',
      type: 'GET',
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          'RecordID', 'IndexID','user_id', 'school_title', 'rating', 'user_name','created_at','comment',
          'user_pic','Actions',],
          sid:sid,
          starRadioVal:starRadioVal,
      },
    },
    columns: [
      {data: 'RecordID'},
      {data: 'IndexID'},    
      {data: 'user_pic'},
      {data: 'rating'},
      {data: 'comment'},  
      {data: 'created_at'},  
      {data: 'Actions', responsivePriority: -1},
    ],

   

    columnDefs: [
      {
        targets: [0,-1],
        visible: !1,
      },
      {
        targets: -1,
        title: 'Actions',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-school-ratings/"+full.RecordID;
          return `
            
            
            
          `;
        },
      },
      {
        targets: 2,
        width: 150,
        title: "Photo",
        orderable: false,
        render: function (a, t, e, n) {
          var  userLINK=BASE_URL+"/view-user/"+e.user_id;
           return `<a href="${userLINK}"><div class="symbol symbol-circle symbol-lg-25">
           <img src="${e.user_pic}" alt="image">
         </div>  <span style="margin-top: 2px;
         position: absolute;
         margin-left: 8px;
         text-transform: capitalize;">${e.user_name} </span></a>`;
         },
      },
      {
        targets: 3,
        width: 175,
        render: function (data, type, full, meta) {
          var strRating = "";
          switch (parseFloat(data)) {
            case 1:
              strRating = `
                              <i class="icon-xl la la-star text-warning" ></i>
                              <i class="icon-xl la la-star " ></i>                                
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star" ></i>
                             
                             
                              `;
              break;
            case 1.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>                             
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star" ></i>
                                 
                                 
                                  `;
              break;
            case 2:
              strRating = `
                                      <i class="icon-xl la la-star text-warning" ></i>
                                      <i class="icon-xl la la-star text-warning" ></i>                        
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star" ></i>
                                     
                                     
                                      `;
              break;
            case 2.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 3:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;

            case 3.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 4:
              strRating = `
                                              <i class="icon-xl la la-star text-warning" ></i>
                                              <i class="icon-xl la la-star text-warning" ></i>                        
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star" ></i>
                                             
                                             
                                              `;
              break;
            case 4.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>
                                 
                                  `;
              break;
            case 5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>        
                                 
                                  `;
              break;

            default:
              strRating = `NA`;
              break;
          }
          return "(" + data + "/5)<br>" + strRating;
        },
      },
      {
        targets: 4,
        width: 350,
        title: "Comments",
        orderable: false,
        render: function (a, t, e, n) {
          return e.comment;
        }
      }
    ],
  });

  
})

//starRadioComment

//starRadio
$("input[name=starRadio]").click(function(){
  var countryID =$( "#myselect option:selected" ).val();


  var starRadioVal=$('input[name="starRadio"]:checked').val();
  var starRadioTOPVal=$('input[name="starRadioTOP"]:checked').val();
    
  $( "#kt_datatable_schoolPerformance" ).dataTable().fnDestroy();
  var table = $('#kt_datatable_schoolPerformance').DataTable({
    responsive: true,
    // Pagination settings
    dom: `<'row'<'col-sm-12'tr>>
    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    // read more: https://datatables.net/examples/basic_init/dom.html

    lengthMenu: [5, 10, 25, 50],

    pageLength: 10,

    language: {
      'lengthMenu': 'Display _MENU_',
    },
    
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: {
      url: BASE_URL + '/getSchoolPerformanceFilterCountry',
      type: 'GET',
      data: {
        // parameters for custom backend script demo
        columnsDef: [
          'RecordID', 'IndexID', 'school_title', 'avg_rating', 'rating',
          'country_name','Actions',],
          countryID:countryID,
          starRadioVal:starRadioVal,
          starRadioTOPVal:starRadioTOPVal

      },
    },
    columns: [
      {data: 'RecordID'},
      {data: 'IndexID'},
      {data: 'school_title'},
      {data: 'rating'},
      {data: 'avg_rating'},
      {data: 'country_name'},  
      {data: 'Actions', responsivePriority: -1},
    ],

   

    columnDefs: [
      {
        targets: [0],
        visible: !1,
      },
      {
        targets: -1,
        title: 'Actions',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-school-ratings/"+full.RecordID;
          return `
            
            <a href="${ViewSchooRating}" class="" title="">\
              <i class="la la-eye"></i>
            </a>
            
          `;
        },
      },
      {
        targets: 2,
        title: 'School',
        orderable: false,
        render: function(data, type, full, meta) {
          var ViewSchooRating=BASE_URL+"/view-school-ratings/"+full.RecordID;
          return `
            
            <a href="${ViewSchooRating}" class="" title="Edit details">\
             ${full.school_title}
            </a>
            
          `;
        },
      },
      {
        targets: 4,
        width: 175,
        render: function (data, type, full, meta) {
          var strRating = "";
          switch (parseFloat(data)) {
            case 1:
              strRating = `
                              <i class="icon-xl la la-star text-warning" ></i>
                              <i class="icon-xl la la-star " ></i>                                
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star " ></i>
                              <i class="icon-xl la la-star" ></i>
                             
                             
                              `;
              break;
            case 1.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>                             
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star " ></i>
                                  <i class="icon-xl la la-star" ></i>
                                 
                                 
                                  `;
              break;
            case 2:
              strRating = `
                                      <i class="icon-xl la la-star text-warning" ></i>
                                      <i class="icon-xl la la-star text-warning" ></i>                        
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star " ></i>
                                      <i class="icon-xl la la-star" ></i>
                                     
                                     
                                      `;
              break;
            case 2.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 3:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star " ></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;

            case 3.5:
              strRating = `
                                          <i class="icon-xl la la-star text-warning" ></i>
                                          <i class="icon-xl la la-star text-warning" ></i>                        
                                          <i class="icon-xl la la-star text-warning" ></i> 
                                          <i class="icon-xl la la-star-half-alt text-warning"></i>
                                          <i class="icon-xl la la-star" ></i>
                                         
                                         
                                          `;
              break;
            case 4:
              strRating = `
                                              <i class="icon-xl la la-star text-warning" ></i>
                                              <i class="icon-xl la la-star text-warning" ></i>                        
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star text-warning" ></i> 
                                              <i class="icon-xl la la-star" ></i>
                                             
                                             
                                              `;
              break;
            case 4.5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star-half-alt text-warning"></i>
                                 
                                  `;
              break;
            case 5:
              strRating = `
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>
                                  <i class="icon-xl la la-star text-warning" ></i>                                
                                  <i class="icon-xl la la-star text-warning" ></i>        
                                 
                                  `;
              break;

            default:
              strRating = `NA`;
              break;
          }
          return "(" + data + "/5)<br>" + strRating;
        },
      },
     
    ],
  });


  
})
//starRadio




$("select.getWeelData").change(function(){
  var selectedVal = $(this).children("option:selected").val();
    //ajax
 
   if(selectedVal==""){
    $("#startDate").val('');
    $("#endDate").val('');
   }
    if(selectedVal==4){
      $("#startDate").prop('disabled', false);
      $("#endDate").prop('disabled', false);
    

    }else{
      $.ajax({
        url: BASE_URL + "/getSchoolCertificatesByWeek",
        type: "GET",
        data: {
          _token: $('meta[name="csrf-token"]').attr("content"),
          selectedVal: selectedVal             
  
        },
        success: function (resp) {
          console.log(resp);
         $('#startDate').val(resp.data.start_date);
         $('#endDate').val(resp.data.end_date);
         $('#startDate').attr('readonly', 'true');
         $('#endDate').attr('readonly', 'true');
        
         $("#startDate").prop('disabled', true);
         $("#endDate").prop('disabled', true);
  
  
        },
        dataType: "json"
      });
    }
   
 //ajax
  

  


});

//function 
var KTDatatablesSearchOptionsAdvancedSearch_courseListSchool = (function () {
  $.fn.dataTable.Api.register("column().title()", function () {
    return $(this.header()).text().trim();
  });

  var initTable1 = function () {
    // begin first table
    var table = $("#kt_datatable_courseListSchool").DataTable({
      responsive: true,
      // Pagination settings
      dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
      // read more: https://datatables.net/examples/basic_init/dom.html

      lengthMenu: [5, 10, 25, 50],

      pageLength: 10,

      language: {
        lengthMenu: "Display _MENU_",
      },
      searchDelay: 500,
      processing: true,
      serverSide: true,
      ajax: {
        url: BASE_URL + "/getDatatableSchoolCouserList",
        type: "GET",
        data: {
          // parameters for custom backend script demo
          columnsDef: [
            "RecordID",
            "IndexID",
            "certificate_title",
            "course_date",
            "regno",
            "status",          
            "Actions",
          ],
        },
      },
      columns: [
        { data: "RecordID" },
        { data: "IndexID" },       
        { data: "certificate_title" },       
        { data: "regno" },       
        { data: "course_date" },       
        { data: "status" },
        { data: "Actions", responsivePriority: -1 },
      ],

      columnDefs: [
        {
          targets: [0],
          visible: !1,
        },
        {
          targets: -1,
          width: 180,
          title: "Actions",
          orderable: false,
          render: function (data, type, full, meta) {
            var EDIT_URL = BASE_URL + "/edit-course/" + UID_ID+"/"+full.RecordID;
            var VIEW_URL = BASE_URL + "/view-course/"+ UID_ID+"/"+full.RecordID;
            var Document_URL = BASE_URL + "/document/"+ UID_ID+"/"+full.RecordID;

            return `					  
					<a href="${EDIT_URL}" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
						<i class="la la-edit"></i>\
					</a>\
          <a href="${VIEW_URL}" class="btn btn-sm btn-clean btn-icon" title="View details">\
          <i class="la la-eye"></i>\
        </a>\
        <a href="${Document_URL}" class="btn btn-sm btn-clean btn-icon" title="Documents">\
        <i class="icon-1x text-dark-50 flaticon-file-1"></i>
      </a>\
					<a href="javascript::void(0)" onclick="deleteSchoolCouse(${full.RecordID})"  class="btn btn-sm btn-clean btn-icon" title="Delete">\
						<i class="la la-trash"></i>\
					</a>\
						`;
          },
        },
        {
          targets: 5,
          width: 50,
          title: "Status",
          orderable: false,
          render: function (a, t, e, n) {
            if(e.status==1){
              return 'Active';
            }else{
              return 'Deactive';
            }
          },
        },
       
      ],
    });

    var filter = function () {
      var val = $.fn.dataTable.util.escapeRegex($(this).val());
      table
        .column($(this).data("col-index"))
        .search(val ? val : "", false, false)
        .draw();
    };

    var asdasd = function (value, index) {
      var val = $.fn.dataTable.util.escapeRegex(value);
      table.column(index).search(val ? val : "", false, true);
    };

    $("#kt_search").on("click", function (e) {
      e.preventDefault();
      var params = {};
      $(".datatable-input").each(function () {
        var i = $(this).data("col-index");
        if (params[i]) {
          params[i] += "|" + $(this).val();
        } else {
          params[i] = $(this).val();
        }
      });
      $.each(params, function (i, val) {
        // apply search params to datatable
        table.column(i).search(val ? val : "", false, false);
      });
      table.table().draw();
    });

    $("#kt_reset").on("click", function (e) {
      e.preventDefault();
      $(".datatable-input").each(function () {
        $(this).val("");
        table.column($(this).data("col-index")).search("", false, false);
      });
      table.table().draw();
    });

    $("#kt_datepicker").datepicker({
      todayHighlight: true,
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>',
      },
    });
  };

  return {
    //main function to initiate the module
    init: function () {
      initTable1();
    },
  };
})();

var KTDatatablesSearchOptionsAdvancedSearch_UserList = (function () {
  $.fn.dataTable.Api.register("column().title()", function () {
    return $(this.header()).text().trim();
  });

  var initTable1 = function () {
    // begin first table
    var table = $("#kt_datatable_userList").DataTable({
      responsive: true,
      // Pagination settings
      dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
      // read more: https://datatables.net/examples/basic_init/dom.html

      lengthMenu: [5, 10, 25, 50,500],

      pageLength: 10,

      language: {
        lengthMenu: "Display _MENU_",
      },

      searchDelay: 500,
      processing: true,
      serverSide: true,
      ajax: {
        url: BASE_URL + "/getDatatableUserList",
        type: "GET",
        data: {
          // parameters for custom backend script demo
          columnsDef: [
            "RecordID",
            "IndexID",
            "photo",
            "name",
            "email",
            "phone",
            "gender",
            "status",
            "country",
            "Actions",
          ],
        },
      },
      columns: [
        { data: "RecordID" },
        { data: "IndexID" },
        { data: "photo" },
        { data: "name" },
        { data: "email" },
        { data: "phone" },
        { data: "gender" },
        { data: "status" },
        { data: "country" },
        { data: "Actions", responsivePriority: -1 },
      ],

      columnDefs: [
        {
          targets: [0],
          visible: !1,
        },
        {
          targets: -1,
          width: 150,
          title: "Actions",
          orderable: false,
          render: function (data, type, full, meta) {
          
            var VIEW_URL = BASE_URL + "/view-user/" + full.RecordID;

            return `<a href="${VIEW_URL}" class="btn btn-sm btn-clean btn-icon" title="View Details">\
							<i class="la la-eye"></i>
						</a>		
           					  
				
					
						`;
          },
        },
        {
          targets: 2,
          width: 50,
          title: "Photo",
          orderable: false,
          render: function (a, t, e, n) {
            return `<div class="symbol symbol-circle symbol-lg-50">
            <img src="${e.photo}" alt="image">
          </div>`;
          },
        },
        {
          targets: 6,
          width: 50,
          title: "Gender",
          orderable: false,
          render: function (a, t, e, n) {
            var i = {
              1: {
                title: "Male",
                class: "primary",
              },
              2: {
                title: "Female",
                class: "primary",
              },
              3: {
                title: "Other",
                class: "primary",
              },
            };
            //return void 0 === i[a] ? a : '<span class="m-badge ' + i[a].class + ' m-badge--wide">' + i[a].title + "</span>"
            return (
              '<span class="font-weight-bold text-' +
              i[a].class +
              '">' +
              i[a].title +
              "</span>"
            );
          },
        },
        {
          targets: 7,
          width: 50,
          title: "Status",
          orderable: false,
          render: function (a, t, e, n) {
            var i = {
              1: {
                title: "Active",
                class: "primary",
              },
              2: {
                title: "Deactive",
                class: "danger",
              },
            };
            //return void 0 === i[a] ? a : '<span class="m-badge ' + i[a].class + ' m-badge--wide">' + i[a].title + "</span>"
            return (
              '<span class="font-weight-bold text-' +
              i[a].class +
              '">' +
              i[a].title +
              "</span>"
            );
          },
        },
      ],
    });

    var filter = function () {
      var val = $.fn.dataTable.util.escapeRegex($(this).val());
      table
        .column($(this).data("col-index"))
        .search(val ? val : "", false, false)
        .draw();
    };

    var asdasd = function (value, index) {
      var val = $.fn.dataTable.util.escapeRegex(value);
      table.column(index).search(val ? val : "", false, true);
    };

    $("#kt_search").on("click", function (e) {
      e.preventDefault();
      var params = {};
      $(".datatable-input").each(function () {
        var i = $(this).data("col-index");
        if (params[i]) {
          params[i] += "|" + $(this).val();
        } else {
          params[i] = $(this).val();
        }
      });
      $.each(params, function (i, val) {
        // apply search params to datatable
        table.column(i).search(val ? val : "", false, false);
      });
      table.table().draw();
    });

    $("#kt_reset").on("click", function (e) {
      e.preventDefault();
      $(".datatable-input").each(function () {
        $(this).val("");
        table.column($(this).data("col-index")).search("", false, false);
      });
      table.table().draw();
    });

    $("#kt_datepicker").datepicker({
      todayHighlight: true,
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>',
      },
    });
  };

  return {
    //main function to initiate the module
    init: function () {
      initTable1();
    },
  };
})();
var KTDatatablesSearchOptionsAdvancedSearch_AdminUserList = (function () {
  $.fn.dataTable.Api.register("column().title()", function () {
    return $(this.header()).text().trim();
  });

  var initTable1 = function () {
    // begin first table
    var table = $("#kt_datatable_userListAdmin").DataTable({
      responsive: true,
      // Pagination settings
      dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
      // read more: https://datatables.net/examples/basic_init/dom.html

      lengthMenu: [5, 10, 25, 50],

      pageLength: 10,

      language: {
        lengthMenu: "Display _MENU_",
      },

      searchDelay: 500,
      processing: true,
      serverSide: true,
      ajax: {
        url: BASE_URL + "/getDatatableAdminUserEnrolledCouseList",
        type: "GET",
        data: {
          // parameters for custom backend script demo
          columnsDef: [
            "RecordID",
            "IndexID",
            "photo",
            "name",
            "course_name",
            "join_date",
            "payment_status",
            "payment_amount",
            "dueAMT",
            "Actions",
          ],
        },
      },
      columns: [
        { data: "RecordID" },
        { data: "IndexID" },
        { data: "photo" },       
        { data: "course_name" },
        { data: "join_date" },
        { data: "payment_status" },
        { data: "payment_amount" },
        { data: "dueAMT" },
        { data: "Actions", responsivePriority: -1 },
      ],

      columnDefs: [
        {
          targets: [0],
          visible: !1,
        },
        {
          targets: -1,
          width: 150,
          title: "Actions",
          orderable: false,
          render: function (data, type, full, meta) {
            var EDIT_URL = BASE_URL + "/edit-user/" + full.RecordID;
            var AddCourseToUSer_URL = BASE_URL + "/add-course-to-user/" + full.RecordID;
            var SettleDueAmout = BASE_URL + "/settle-due-amount/" + full.RecordID;


            var VIEW_URL = BASE_URL + "/admin-view-user/" + full.RecordID;

             var HTML='';
            /*
            	<a href="${EDIT_URL}" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
						<i class="la la-edit"></i>\
					</a>\
					<a href="javascript::void(0)" onclick="deleteUser(${full.RecordID})"  class="btn btn-sm btn-clean btn-icon" title="Delete">\
						<i class="la la-trash"></i>\
					</a>\

            /*/
            if(full.dueAMT<=0){
              HTML +=`<a href="javascript:void(0)" class="btn btn-sm btn-success" title="Settled">\
              Settled
            </a>
             
              `;
            }else{
              HTML +=`
              <a href="${SettleDueAmout}" class="btn btn-sm btn-warning" title="Payment Settle">\
               Settle
            </a>
              `;
            }
            HTML +=`
            <a href="javascript:void(0)" onclick="paymentHistory(${full.RecordID})" class="btn btn-sm btn-clean btn-icon" title="View  Payment History">\
            <i class="la la-eye"></i>\
          </a>`;

            return HTML;
           

          },
        },
        {
          targets: 2,
          width: 250,
          title: "User",
          orderable: false,
          render: function (a, t, e, n) {
            var VIEW_URL = BASE_URL + "/admin-view-user/" + e.RecordID;

            return `<div class="symbol symbol-circle symbol-lg-50">
            <img src="${e.photo}" alt="image">
            
          </div><span style="margin-top: 10px;
    position: absolute;
    margin-left: 10px;"><b><a href="${VIEW_URL}">${e.name}</a></b></span>`;
          },
        },
    
        {
          targets: 5,
          render: function(data, type, full, meta) {
            var status = {
              
              2: {'title': 'Partial', 'class': ' label-light-warning'},           
              1: {'title': 'Full', 'class': ' label-light-success'},           
            };
            if (typeof status[data] === 'undefined') {
              return data;
            }
            return '<span class="label label-lg font-weight-bold' + status[data].class + ' label-inline">' + status[data].title + '</span>';
          },
        },
      ],
    });

    var filter = function () {
      var val = $.fn.dataTable.util.escapeRegex($(this).val());
      table
        .column($(this).data("col-index"))
        .search(val ? val : "", false, false)
        .draw();
    };

    var asdasd = function (value, index) {
      var val = $.fn.dataTable.util.escapeRegex(value);
      table.column(index).search(val ? val : "", false, true);
    };

    $("#kt_search").on("click", function (e) {
      e.preventDefault();
      var params = {};
      $(".datatable-input").each(function () {
        var i = $(this).data("col-index");
        if (params[i]) {
          params[i] += "|" + $(this).val();
        } else {
          params[i] = $(this).val();
        }
      });
      $.each(params, function (i, val) {
        // apply search params to datatable
        table.column(i).search(val ? val : "", false, false);
      });
      table.table().draw();
    });

    $("#kt_reset").on("click", function (e) {
      e.preventDefault();
      $(".datatable-input").each(function () {
        $(this).val("");
        table.column($(this).data("col-index")).search("", false, false);
      });
      table.table().draw();
    });

    $("#kt_datepicker").datepicker({
      todayHighlight: true,
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>',
      },
    });
  };
  var initTable1_suadmin = function () {
    var txtSID=$('#txtSID').val();
    // begin first table
    var table = $("#kt_datatable_userListAdminSuper").DataTable({
      responsive: true,
      // Pagination settings
      dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
      // read more: https://datatables.net/examples/basic_init/dom.html

      lengthMenu: [5, 10, 25, 50],

      pageLength: 10,

      language: {
        lengthMenu: "Display _MENU_",
      },

      searchDelay: 500,
      processing: true,
      serverSide: true,
      ajax: {
        url: BASE_URL + "/getDatatableAdminUserEnrolledCouseListSuper",
        type: "GET",
        data: {
          // parameters for custom backend script demo
          columnsDef: [
            "RecordID",
            "IndexID",
            "photo",
            "name",
            "course_name",
            "join_date",
            "payment_status",
            "payment_amount",
            "dueAMT",
            "Actions",
          ],
          'sid':txtSID
        },
      },
      columns: [
        { data: "RecordID" },
        { data: "IndexID" },
        { data: "photo" },       
        { data: "course_name" },
        { data: "join_date" },
        { data: "payment_status" },
        { data: "payment_amount" },
        { data: "dueAMT" },
        { data: "Actions", responsivePriority: -1 },
      ],

      columnDefs: [
        {
          targets: [0],
          visible: !1,
        },
        {
          targets: -1,
          width: 150,
          title: "Actions",
          orderable: false,
          render: function (data, type, full, meta) {
            var EDIT_URL = BASE_URL + "/edit-user/" + full.RecordID;
            var AddCourseToUSer_URL = BASE_URL + "/add-course-to-user/" + full.RecordID;
            var SettleDueAmout = BASE_URL + "/settle-due-amount/" + full.RecordID;


            var VIEW_URL = BASE_URL + "/admin-view-user/" + full.RecordID;

             var HTML='';
            /*
            	<a href="${EDIT_URL}" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
						<i class="la la-edit"></i>\
					</a>\
					<a href="javascript::void(0)" onclick="deleteUser(${full.RecordID})"  class="btn btn-sm btn-clean btn-icon" title="Delete">\
						<i class="la la-trash"></i>\
					</a>\

            /*/
            if(full.dueAMT<=0){

              HTML +=`<a href="javascript:void(0)" class="btn btn-sm btn-success" title="Settled">\
              Settled
            </a>
             
              `;
            }else{
              HTML +=`
              
              `;
            }
            HTML +=`
            <a href="javascript:void(0)" onclick="paymentHistoryAdmin(${full.RecordID})" class="btn btn-sm btn-clean btn-icon" title="View  Payment History">\
            <i class="la la-eye"></i>\
          </a>`;

            return HTML;
           

          },
        },
        {
          targets: 2,
          width: 250,
          title: "User",
          orderable: false,
          render: function (a, t, e, n) {
            var VIEW_URL = BASE_URL + "/admin-view-user/" + e.RecordID;

            return `<div class="symbol symbol-circle symbol-lg-50">
            <img src="${e.photo}" alt="image">
            
          </div><span style="margin-top: 10px;
    position: absolute;
    margin-left: 10px;"><b><a href="${VIEW_URL}">${e.name}</a></b></span>`;
          },
        },
    
        {
          targets: 5,
          render: function(data, type, full, meta) {
            var status = {
              
              2: {'title': 'Partial', 'class': ' label-light-warning'},           
              1: {'title': 'Full', 'class': ' label-light-success'},           
            };
            if (typeof status[data] === 'undefined') {
              return data;
            }
            return '<span class="label label-lg font-weight-bold' + status[data].class + ' label-inline">' + status[data].title + '</span>';
          },
        },
      ],
    });

    var filter = function () {
      var val = $.fn.dataTable.util.escapeRegex($(this).val());
      table
        .column($(this).data("col-index"))
        .search(val ? val : "", false, false)
        .draw();
    };

    var asdasd = function (value, index) {
      var val = $.fn.dataTable.util.escapeRegex(value);
      table.column(index).search(val ? val : "", false, true);
    };

    $("#kt_search").on("click", function (e) {
      e.preventDefault();
      var params = {};
      $(".datatable-input").each(function () {
        var i = $(this).data("col-index");
        if (params[i]) {
          params[i] += "|" + $(this).val();
        } else {
          params[i] = $(this).val();
        }
      });
      $.each(params, function (i, val) {
        // apply search params to datatable
        table.column(i).search(val ? val : "", false, false);
      });
      table.table().draw();
    });

    $("#kt_reset").on("click", function (e) {
      e.preventDefault();
      $(".datatable-input").each(function () {
        $(this).val("");
        table.column($(this).data("col-index")).search("", false, false);
      });
      table.table().draw();
    });

    $("#kt_datepicker").datepicker({
      todayHighlight: true,
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>',
      },
    });
  };
  // kt_datatable_userListAdminSuper

  return {
    //main function to initiate the module
    init: function () {
      initTable1();
      initTable1_suadmin();
    },
  };
})();

var KTDatatablesSearchOptionsAdvancedSearch_AdminGroupChatList = (function () {
  $.fn.dataTable.Api.register("column().title()", function () {
    return $(this.header()).text().trim();
  });

  var initTable1 = function () {
    // begin first table
    var table = $("#adminGroupchatList").DataTable({
      responsive: true,
      // Pagination settings
      dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
      // read more: https://datatables.net/examples/basic_init/dom.html

      lengthMenu: [5, 10, 25, 50],

      pageLength: 10,

      language: {
        lengthMenu: "Display _MENU_",
      },

      searchDelay: 500,
      processing: true,
      serverSide: true,
      ajax: {
        url: BASE_URL + "/getDatatableAdminGroupChatList",
        type: "GET",
        data: {
          // parameters for custom backend script demo
          columnsDef: [
            "RecordID",
            "IndexID",
            "group_name",
            "group_title",
            "created_at",
            "created_by",
            "photo",
            "status",
            "membercount",           
            "Actions",
          ],
        },
      },
      columns: [
        { data: "RecordID" },       
        { data: "group_name" },       
        { data: "group_title" },
        { data: "membercount" },
        { data: "created_at" },
        
        { data: "status" },
        { data: "Actions", responsivePriority: -1 },
      ],

      columnDefs: [
        {
          targets: [0],
          visible: !1,
        },
        {
          targets: -1,
          width: 150,
          title: "Actions",
          orderable: false,
          render: function (data, type, full, meta) {
            var Add_StudetInGrp =BASE_URL+'/add-in-group-chat/'+full.RecordID;
            var view_GroupChat =BASE_URL+'/view-group-chat/'+full.RecordID;
            var edit_GroupChat =BASE_URL+'/edit-group-chat/'+full.RecordID;

            return `<a href="${Add_StudetInGrp}"  title="Add Student in Group" class="btn btn-sm btn-primary btn-icon">\
            <i class="la la-plus"></i>
          </a>
          <a href="${view_GroupChat}"  title="Group Chat" class="btn btn-sm btn-s btn-icon">\
            <i class="la la-eye"></i>
          </a>
         
         <a href="${edit_GroupChat}" class="btn btn-sm btn-clean btn-icon" title="Delete">\
           <i class="la la-edit"></i>\
         </a>
         <a href="javascript::void(0)" onclick="deleteGroupChat(${full.RecordID})"  class="btn btn-sm btn-clean btn-icon" title="Delete">\
         <i class="la la-trash"></i>\
       </a>
          `
            return `<a href="${Add_StudetInGrp}" class="btn btn-primary">Add Student</a>
            <a  style="margin:5px" href="${view_GroupChat}" class="btn btn-success">Group Chat</a>`;
           

          },
        },
        {
          width: "75px",
          targets: 1,
          render: function (data, type, full, meta) {
            return `<span class="symbol-label">
            <img src="${full.photo}" width="45px" class="h-75 align-self-end" alt="">
          </span><a href="javascript:void(0)" onclick=showGroupUserModel(${full.RecordID})>${full.group_name}</a>`
          }
        },
    
        {
          targets: 5,
          render: function(data, type, full, meta) {
            var status = {
              
              0: {'title': 'Deactive', 'class': ' label-light-warning'},           
              1: {'title': 'Active', 'class': ' label-light-success'},           
            };
            if (typeof status[data] === 'undefined') {
              return data;
            }
            return '<span class="label label-lg font-weight-bold' + status[data].class + ' label-inline">' + status[data].title + '</span>';
          },
        },
      ],
    });

    var filter = function () {
      var val = $.fn.dataTable.util.escapeRegex($(this).val());
      table
        .column($(this).data("col-index"))
        .search(val ? val : "", false, false)
        .draw();
    };

    var asdasd = function (value, index) {
      var val = $.fn.dataTable.util.escapeRegex(value);
      table.column(index).search(val ? val : "", false, true);
    };

    $("#kt_search").on("click", function (e) {
      e.preventDefault();
      var params = {};
      $(".datatable-input").each(function () {
        var i = $(this).data("col-index");
        if (params[i]) {
          params[i] += "|" + $(this).val();
        } else {
          params[i] = $(this).val();
        }
      });
      $.each(params, function (i, val) {
        // apply search params to datatable
        table.column(i).search(val ? val : "", false, false);
      });
      table.table().draw();
    });

    $("#kt_reset").on("click", function (e) {
      e.preventDefault();
      $(".datatable-input").each(function () {
        $(this).val("");
        table.column($(this).data("col-index")).search("", false, false);
      });
      table.table().draw();
    });

    $("#kt_datepicker").datepicker({
      todayHighlight: true,
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>',
      },
    });
  };

  return {
    //main function to initiate the module
    init: function () {
      initTable1();
    },
  };
})();



jQuery(document).ready(function () {
  KTDatatablesSearchOptionsAdvancedSearch.init();
  KTDatatablesSearchOptionsAdvancedSearch_UserList.init();
  KTDatatablesSearchOptionsAdvancedSearch_AdminUserList.init();
  KTDatatablesSearchOptionsAdvancedSearch_courseListSchool.init();
  KTDatatablesSearchOptionsAdvancedSearch_AdminGroupChatList.init();
});

//paymentHistoryAdmin
function paymentHistoryAdmin(rowID){

  var formData = {
    rowID: rowID , 
    _token: $('meta[name="csrf-token"]').attr("content"),
  };
  $.ajax({
    url: BASE_URL + "/getPaymentHistoryAdmin",
    type: "GET",
    data: formData,
    success: function (res) {
      $('.payHis').html(res);
      $('#PaymentHisModalSizeLg').modal('show');
    }
  });
  
}
//paymentHistoryAdmin

function paymentHistory(rowID){

  var formData = {
    rowID: rowID , 
    _token: $('meta[name="csrf-token"]').attr("content"),
  };
  $.ajax({
    url: BASE_URL + "/getPaymentHistory",
    type: "GET",
    data: formData,
    success: function (res) {
      $('.payHis').html(res);
      $('#PaymentHisModalSizeLg').modal('show');
    }
  });
  
}
//function

// HTML TABLE
var KTDatatablesDataSourceHtml = (function () {
  var initTable1 = function () {
    var table = $("#kt_datatable_static_content");

    // begin first table
    table.DataTable({
      responsive: true,
      dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
      // read more: https://datatables.net/examples/basic_init/dom.html

      columnDefs: [
        {
          targets: [0],
          visible: !1,
        },
        {
          targets: -1,
          title: "Actions",
          orderable: false,
          render: function (data, type, full, meta) {
            console.log(full);
            var EDIT_URL = BASE_URL + "/edit-static-content/" + full[0];

            return `
							
							<a href="${EDIT_URL}" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
								<i class="la la-edit"></i>\
							</a>\
							<a href="javascript:void(0)" onclick="deleteMeStatic(1,${full[0]})" class="btn btn-sm btn-clean btn-icon" title="Delete">\
								<i class="la la-trash"></i>\
							</a>\
						`;
          },
        },
        {
          width: "75px",
          targets: 4,
          render: function (data, type, full, meta) {
            var status = {
              0: { title: "Deactive", state: "danger" },
              1: { title: "Active", state: "primary" },
            };
            if (typeof status[data] === "undefined") {
              return data;
            }
            return (
              "</span>" +
              '<span class="font-weight-bold text-' +
              status[data].state +
              '">' +
              status[data].title +
              "</span>"
            );
          },
        },
      ],
    });
  };
  var initTable2 = function () {
    var table = $("#kt_datatable_school_certificate");

    // begin first table
    table.DataTable({
      responsive: true,
      footerCallback: function(row, data, start, end, display) {

				var column = 4;
				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function(i) {
					return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
				};

				// Total over all pages
				var total = api.column(column).data().reduce(function(a, b) {
					return intVal(a) + intVal(b);
				}, 0);

				// Total over this page
				var pageTotal = api.column(column, {page: 'current'}).data().reduce(function(a, b) {
					return intVal(a) + intVal(b);
				}, 0);

				// Update footer
				$(api.column(column).footer()).html(
					'Total :' + KTUtil.numberString(pageTotal.toFixed(0)) + ' Studnts<br/> <a href="">More ..</a>)',
				);
			},
      dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
      // read more: https://datatables.net/examples/basic_init/dom.html

      columnDefs: [
        {
          targets: [1],
          visible: !1,
        },
        {
          targets: -1,
          title: "Actions",
          orderable: false,
          render: function (data, type, full, meta) {
           
            var VIEW_URL = BASE_URL + "/view-school-details/" + full[0];

            return `
							
							
							
						`;
          },
        },
      ],
    });
    $("#kt_searchA").on("click", function (e) {
      alert(55);
      e.preventDefault();
      var params = {};
      $(".datatable-input").each(function () {
        var i = $(this).data("col-index");
        if (params[i]) {
          params[i] += "|" + $(this).val();
        } else {
          params[i] = $(this).val();
        }
      });
      $.each(params, function (i, val) {
        // apply search params to datatable
        table.column(i).search(val ? val : "", false, false);
      });
      table.table().draw();
    });

  };
  var initTable3 = function () {
    var table = $("#kt_datatable_sport1");

    // begin first table
    table.DataTable({
      responsive: true,
      dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
      // read more: https://datatables.net/examples/basic_init/dom.html

      columnDefs: [
        {
          targets: [0],
          visible: !1,
        },
        {
          targets: -1,
          title: "Actions",
          orderable: false,
          render: function (data, type, full, meta) {
            console.log(full);
            var EDIT_URL = BASE_URL + "/edit-static-content/" + full[0];

            return `
							
						
							<a href="javascript:void(0)" onclick="deleteMe(1,${full[0]})" class="btn btn-sm btn-clean btn-icon" title="Delete">\
								<i class="la la-trash"></i>\
							</a>\
						`;
          },
        },
      ],
    });
  };

  var initTable4 = function () {
    var table = $("#kt_datatable_interest");

    // begin first table
    table.DataTable({
      responsive: true,
      dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
      // read more: https://datatables.net/examples/basic_init/dom.html

      columnDefs: [
        {
          targets: [0],
          visible: !1,
        },
        {
          targets: -1,
          title: "Actions",
          orderable: false,
          render: function (data, type, full, meta) {
            console.log(full);
            var EDIT_URL = BASE_URL + "/edit-static-content/" + full[0];

            return `
							
						
            <a href="javascript:void(0)" onclick="deleteMe(2,${full[0]})" class="btn btn-sm btn-clean btn-icon" title="Delete">\
            <i class="la la-trash"></i>\
          </a>\
						`;
          },
        },
      ],
    });
  };

  return {
    //main function to initiate the module
    init: function () {
      initTable1();
      initTable2();
      initTable3();
      initTable4();
    },
  };
})();

jQuery(document).ready(function () {
  KTDatatablesDataSourceHtml.init();
});

//kt_datatable_school_certificate


function showGroupUserModel(rowID){
  //alert(rowID);
  
  // $('#txtGID').val(rowID);
  // initTableA3_My_GroupchatUser();
  $( "#kt_datatable_charGroupUser" ).dataTable().fnDestroy();
   var table = $('#kt_datatable_charGroupUser').DataTable({
     responsive: true,
     // Pagination settings
     // dom: `<'row'<'col-sm-12'tr>>
     // <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
     // read more: https://datatables.net/examples/basic_init/dom.html
 
     lengthMenu: [5, 10, 25, 50],
 
     pageLength: 10,
 
     language: {
       'lengthMenu': 'Display _MENU_',
     },
     
     searchDelay: 500,
     processing: true,
     serverSide: true,
     ajax: {
       url: BASE_URL + '/getSchoolGroupchatUsers',
       type: 'GET',
       data: {
         // parameters for custom backend script demo
         columnsDef: [
           'RecordID', 'IndexID', 'user_id', 'user_name','created_at',,
           'user_pic','Actions',],
          
           rowID:rowID
       },
     },
     columns: [
       {data: 'RecordID'},          
       {data: 'user_pic'},
       {data: 'created_at'},     
       {data: 'Actions', responsivePriority: -1},
     ],
 
    
 
     columnDefs: [
       {
         targets: [0],
         visible: !1,
       },
       {
         targets: -1,
         title: 'Actions',
         orderable: false,
         render: function(data, type, full, meta) {
           var ViewSchooRating=BASE_URL+"/view-school-ratings/"+full.RecordID;
           return `
           <a href="javascript::void(0)" onclick="deleteSchoolGroupUser(${full.RecordID})"  class="btn btn-sm btn-clean btn-icon" title="Delete">\
           <i class="la la-trash"></i>\
         </a>
             
             
           `;
         },
       },
       {
         targets: 1,
         width: 150,
         title: "Photo",
         orderable: false,
         render: function (a, t, e, n) {
          var  userLINK=BASE_URL+"/view-user-details/"+e.user_id;
           return `<a href="${userLINK}"><div class="symbol symbol- symbol-lg-50">
           <img src="${e.user_pic}" alt="image">
         </div>  <span style="margin-top: 2px;
         position: absolute;
         margin-left: 8px;
         text-transform: capitalize;">${e.user_name} </span></a>`;
         },
       },
      
      
     ],
   });
 
   var filter = function() {
     var val = $.fn.dataTable.util.escapeRegex($(this).val());
     table.column($(this).data('col-index')).search(val ? val : '', false, false).draw();
   };
 
   var asdasd = function(value, index) {
     var val = $.fn.dataTable.util.escapeRegex(value);
     table.column(index).search(val ? val : '', false, true);
   };
 
   $('#kt_search').on('click', function(e) {
     e.preventDefault();
     var params = {};
     $('.datatable-input').each(function() {
       var i = $(this).data('col-index');
       if (params[i]) {
         params[i] += '|' + $(this).val();
       }
       else {
         params[i] = $(this).val();
       }
     });
     $.each(params, function(i, val) {
       // apply search params to datatable
       table.column(i).search(val ? val : '', false, false);
     });
     table.table().draw();
   });
 
   $('#kt_reset').on('click', function(e) {
     e.preventDefault();
     $('.datatable-input').each(function() {
       $(this).val('');
       table.column($(this).data('col-index')).search('', false, false);
     });
     table.table().draw();
   });
 
   $('#kt_datepicker').datepicker({
     todayHighlight: true,
     templates: {
       leftArrow: '<i class="la la-angle-left"></i>',
       rightArrow: '<i class="la la-angle-right"></i>',
     },
   });

  $('#charUserGroupModel').modal('show');
}