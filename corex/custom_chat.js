$('.messenger-search').on('keyup', function (e) {
    
    $.trim($(this).val()).length > 0
        ? $('.messenger-search').trigger('focus') + messengerSearch($(this).val())
        : $('.messenger-tab').hide() +
        $('.messenger-listView-tabs a[data-view="users"]').trigger('click');
});
